package com.ignight.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterGroupMembers extends ArrayAdapter<User> {
	private ActivityGroup activity;
	private AppBase app;
	private BufferedReader bufferReader;
	private Context context;
	private Gson gson;
	private HttpClient httpClient;
	private HttpEntity httpEntity;
	private HttpPost httpPost;
	private HttpResponse httpResponse;
	private String httpResult;
	private InputStream inputStream;
	private JSONObject jsonObject;
	private StringBuilder stringBuilder;
	private User user;
	private ArrayList<User> users;
	private LayoutInflater layoutInflater;
	private Transformation transformation;
	
	public AdapterGroupMembers(Context context, int textViewResourceId, ArrayList<User> items, AppBase appBase, ActivityGroup activityGroup){
		super(context, textViewResourceId, items);
		this.activity = activityGroup;
		this.app = appBase;
		this.context = context;
		this.users = items;
		
		transformation = new RoundedTransformationBuilder()
        .borderColor(Color.WHITE)
        .borderWidthDp(1)
        .cornerRadiusDp(35)
        .oval(false)
        .build();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View view = convertView;
		GridHolder holder;
		
		if(view == null){
			layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layoutInflater.inflate(R.layout.list_member, parent, false);
			
			holder = new GridHolder();
			holder.txtGroupAdmin = (TextView)view.findViewById(R.id.txtGroupAdmin);
			holder.txtMemberName = (TextView)view.findViewById(R.id.txtGroupMemberName);
			holder.imgAddGroup = (ImageView)view.findViewById(R.id.imgAddGroupMember);
			holder.imgGroupUser = (ImageView)view.findViewById(R.id.imgGroupUser);
			view.setTag(holder);
		}else{
			holder = (GridHolder) view.getTag();
		}
		
		user = users.get(position);
		
		if(user != null){
			if(holder.txtMemberName != null){holder.txtMemberName.setText(user.getUserName());}
			if(holder.imgAddGroup != null){
				if(user.getGroupStatus() == 0){
					holder.imgAddGroup.setVisibility(View.VISIBLE);
					holder.imgAddGroup.setImageResource(R.drawable.list_add_active_green);
					holder.imgAddGroup.setTag(user.getId());
					holder.imgAddGroup.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							view.setVisibility(View.INVISIBLE);
							view.setOnClickListener(null);
							new AddGroupMemberTask().execute(String.valueOf(view.getTag()));
						}
					});
				}else if(user.getGroupStatus() == 1){
					holder.imgAddGroup.setVisibility(View.VISIBLE);
					holder.imgAddGroup.setImageResource(R.drawable.list_add);
					holder.imgAddGroup.setTag(user.getUserName());
					holder.imgAddGroup.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							app.showMessage((String)view.getTag() + " already has a pending invite.");
						}
					});
				}else{
					holder.imgAddGroup.setVisibility(View.INVISIBLE);
					holder.imgAddGroup.setOnClickListener(null);
				}
			}
			if(holder.txtGroupAdmin != null){
				if(user.getGroupAdmin()){
					holder.txtGroupAdmin.setVisibility(View.VISIBLE);
				}else{
					holder.txtGroupAdmin.setVisibility(View.GONE);
				}
			}
			if(holder.imgGroupUser != null){
				if(!user.getUserPhoto().isEmpty() && !user.getUserPhoto().equalsIgnoreCase("default"))
					Picasso.with(context).load(user.getUserPhoto()).fit().transform(transformation).into(holder.imgGroupUser);
				else
					Picasso.with(context).load(R.drawable.avatar_default).fit().transform(transformation).into(holder.imgGroupUser);
			}
		}
		
		return view;
	}
	
	static class GridHolder {
		TextView txtGroupAdmin;
		TextView txtMemberName;
		ImageView imgAddGroup;
		ImageView imgGroupUser;
    }
	
	private class AddGroupMemberTask extends AsyncTask<String, Void, String>{

		@Override
		protected String doInBackground(String... args) {
			User activeUser = app.getActiveUser();
			String json = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				gson = new GsonBuilder().create();
				MessageGroupInvite msg = new MessageGroupInvite(activeUser.getCity().getId(), app.ActiveGroup.getGroupId(), Integer.parseInt(args[0]), activeUser.getId(), "");
				json = gson.toJson(msg);
				
				StringEntity stringEntity = new StringEntity(json);
				
				httpClient = new CustomHttpClient(context);
				httpPost = new HttpPost(context.getString(R.string.api_base) + context.getString(R.string.api_group_invite) + app.ActiveGroup.getGroupId());
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(context.getString(R.string.api_token_key),context.getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID",""+activeUser.getId());
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					
					if(httpResult != null){
						retVal = httpResult;
						jsonObject = new JSONObject(httpResult);
						
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							retVal = "Error, could not add member to group.";
						}
					}else{
						retVal = "Error, no result.";
					}
				}
			}catch(ClientProtocolException e){
				retVal = "Client Protocal Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "IO Error, " + e.getMessage();
			
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			activity.hideProgress();
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			activity.showProgress("Inviting member...");
		}
		
		@Override
		protected void onPostExecute(String result){
			if(result.equals("success"))
				app.showMessage("Member successfully invited.");
			else
				app.showMessage(result);
		}
	}
}
