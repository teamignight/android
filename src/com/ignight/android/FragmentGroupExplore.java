package com.ignight.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class FragmentGroupExplore extends android.support.v4.app.Fragment{
	private AdapterPopularGroups adapterPopularGroups;
	private AppBase app;
	private ActivityGroups activityGroups;
	private BufferedReader bufferReader;
	private boolean didSearch;
	private String enteredSearch;
	private View footerView;
	private Activity fragmentActivity;
	private Group group;
	private ArrayList<Group> groups;
	private View headerView;
	private HttpClient httpClient;
	private HttpGet httpGet;
	private HttpEntity httpEntity;
	private HttpResponse httpResponse;
	private String httpResult;
	private InputStream inputStream;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private ListView listGroups;
	private boolean loadingSearchResults;
	private boolean searching;
	private String searchTerm;
	private StringBuilder stringBuilder;
	private SwipeRefreshLayout swipeContainer;
	private TextView txtAddGroupHeader;
	private TextView txtClearSearch;
	private EditText txtSearch;
	private TextView txtSearchText;
	private User user;
	private View view;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_group_explore, container, false);
        
        activityGroups = ((ActivityGroups)getActivity());
        user = activityGroups.CurrentUser;
        
        footerView = inflater.inflate(R.layout.list_loading_footer, null, false);
        headerView = inflater.inflate(R.layout.list_groups_header, null, false);
        
        txtAddGroupHeader = (TextView)headerView.findViewById(R.id.txtAddGroupHeader);
        txtClearSearch = (TextView)headerView.findViewById(R.id.txtClearGroupsSearch);
        txtSearch = (EditText)headerView.findViewById(R.id.txtGroupsSearch);
        txtSearchText = (TextView)footerView.findViewById(R.id.txtLoadingFooter);
        listGroups = (ListView)view.findViewById(R.id.exploreGroups);
        
        if (fragmentActivity != null) {
        	app = ((AppBase)fragmentActivity.getApplication());
        }
        
        if(txtAddGroupHeader != null){
	        txtAddGroupHeader.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(activityGroups, ActivityAddGroup.class);
					startActivity(intent);
				}
			});
        }
        
        searching = false;
        swipeContainer = (SwipeRefreshLayout)view.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new OnRefreshListener() {
			@Override
			public void onRefresh() {
				txtSearch.setText("");
				updateList();
			}
		});
        swipeContainer.setColorScheme(android.R.color.holo_blue_dark, android.R.color.holo_green_dark, android.R.color.holo_orange_dark, android.R.color.holo_red_dark);
        txtClearSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				txtSearch.setText("");
				updateList();
			}
		});
        txtSearch.setHint("Search Groups");
        txtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                	searchGroups(txtSearch.getText().toString());
                    return true;
                }
                return false;
            }
        });
        
        listGroups.addFooterView(footerView);
        listGroups.addHeaderView(headerView);
        listGroups.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if(position > 0)
					position = position - 1;
				app.ActiveGroup = adapterPopularGroups.getItem(position);
				Intent intent;
				if(app.ActiveGroup.getIsMember())
					intent = new Intent(fragmentActivity, ActivityGroup.class);
				else
					intent = new Intent(fragmentActivity, ActivityGroupDetails.class);
				
				startActivity(intent);
			}
		});
        
        activityGroups.highlightExplore();
        
        new GetPopularGroups().execute();
        
        return view;
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		fragmentActivity = activity;
	}
	
	@Override
	public void onStop(){
		if(activityGroups != null)
			activityGroups.hideProgress();
		super.onStop();
	}
	
	public void refresh(){
		updateList();
	}
	
	private void searchGroups(String query){
		searchTerm = query;
		if(loadingSearchResults){
			return;
		}
		
		if(query.length() == 0 && didSearch){
			searching = false;
			didSearch = false;
			
			if(adapterPopularGroups != null)
				adapterPopularGroups.clear();
			adapterPopularGroups = null;
			
			new GetPopularGroups().execute();
		}else if(query.length() > 1){
			searching = true;
			didSearch = true;
			txtClearSearch.setVisibility(View.VISIBLE);
			if(adapterPopularGroups != null)
				adapterPopularGroups.clear();
			adapterPopularGroups = null;
			
			new SearchGroups().execute();
		}
	}
	
	private void updateList(){
		if(adapterPopularGroups != null){
			adapterPopularGroups.clear();
			adapterPopularGroups = null;
		}
		new GetPopularGroups().execute();
		txtClearSearch.setVisibility(View.INVISIBLE);
	}
	
	private void startGetPopularGroups(){
		activityGroups.showProgress("Loading groups...");
	}
	
	private void getPopularGroupsResult(String result){
		groups = new ArrayList<Group>();
		
		adapterPopularGroups = new AdapterPopularGroups(fragmentActivity, R.layout.list_explore_groups, groups, app);
		listGroups.setAdapter(adapterPopularGroups);
		
		if(result.equalsIgnoreCase("success")){
			try{
				jsonArray = jsonObject.getJSONArray("body");
				if(jsonArray != null){
					JSONObject jObject;
					for(int i = 0; i < jsonArray.length(); i++){
						jObject = jsonArray.getJSONObject(i);
						group = new Group();
						group.setGroupId(jObject.getInt("groupId"));
						group.setGroupName(jObject.getString("groupName"));
						group.setMemberCount(jObject.getInt("noOfMembers"));
						group.setIsMember(jObject.getBoolean("isMember"));
						adapterPopularGroups.add(group);
					}
				}
				
				adapterPopularGroups.notifyDataSetChanged();
				
				if(searching){
					txtSearch.requestFocus();
				}
			}catch(JSONException e){
				activityGroups.showMessage("Error parsing groups: " + e.getMessage());
			}catch(Exception ex){
				activityGroups.showMessage("Error loading groups: " + ex.getMessage());
			}
		}else{
			activityGroups.showMessage("Error, could not get groups.");
		}
		if(searching){
			if(jsonArray.length() == 0){
				txtSearchText.setText("No groups by that name exist. Click the + on the top right to create it yourself!");
			}else{
				txtSearchText.setText("No more results.");
			}
		}else{
			txtSearchText.setText("No more results.");
		}
		
		swipeContainer.setRefreshing(false);
	}
	
	private class GetPopularGroups extends AsyncTask<Void, Void, String>{

		@Override
		protected String doInBackground(Void... params) {
			String retVal = "";
			String singleLine = "";

			try{
				httpClient = new CustomHttpClient(fragmentActivity);
				httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_groups_popular) + app.getActiveUser().getId() + "/?count=50");
				httpGet.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpGet.setHeader("PLATFORM","Android");
				httpGet.setHeader("USER_ID",""+app.getActiveUser().getId());
				httpResponse = httpClient.execute(httpGet);
				
				httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();
				bufferReader = new BufferedReader(new InputStreamReader(inputStream));
				stringBuilder = new StringBuilder();
				
				while((singleLine = bufferReader.readLine()) != null){
					stringBuilder.append(singleLine);
				}
				
				httpResult = stringBuilder.toString();
				inputStream.close();
				jsonObject = new JSONObject(httpResult);
				
				if(jsonObject.getBoolean("res")){
					retVal = "success";
				}else{
					retVal = "failed";
				}
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			activityGroups.hideProgress();
			
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startGetPopularGroups();
		}
		
		@Override
		protected void onPostExecute(String result){
			getPopularGroupsResult(result);
		}
	}

	private void searchGroupsResult(String result){
		groups = new ArrayList<Group>();
		
		adapterPopularGroups = new AdapterPopularGroups(fragmentActivity, R.layout.list_explore_groups, groups, app);
		listGroups.setAdapter(adapterPopularGroups);
		
		if(result.equalsIgnoreCase("success")){
			try{
				jsonArray = jsonObject.getJSONArray("body");
				if(jsonArray != null){
					JSONObject jObject;
					for(int i = 0; i < jsonArray.length(); i++){
						jObject = jsonArray.getJSONObject(i);
						group = new Group();
						group.setGroupId(jObject.getInt("id"));
						group.setGroupName(jObject.getString("name"));
						group.setMemberCount(jObject.getInt("members"));
						group.setIsMember(jObject.getBoolean("isMember"));
						adapterPopularGroups.add(group);
					}
				}
				
				adapterPopularGroups.notifyDataSetChanged();
				
				loadingSearchResults = false;
				enteredSearch = txtSearch.getText().toString();
				if(searchTerm != null && searchTerm.length() > 0 && !searchTerm.equalsIgnoreCase(enteredSearch)){
					searchGroups(enteredSearch);
				}
				
				if(searching){
					txtSearch.requestFocus();
					if(jsonArray.length() == 0){
						txtSearchText.setText("No groups by that name exist. Click the + on the top right to create it yourself!");
					}else{
						txtSearchText.setText("No more results.");
					}
				}else{
					txtSearchText.setText("No more results.");
				}
			}catch(JSONException e){
				activityGroups.showMessage("Error parsing groups: " + e.getMessage());
			}catch(Exception ex){
				activityGroups.showMessage("Error loading groups: " + ex.getMessage());
			}
		}else{
			activityGroups.showMessage("Error, could not get groups.");
		}
		
		swipeContainer.setRefreshing(false);
	}
	
	private class SearchGroups extends AsyncTask<Void, Void, String>{
		@Override
		protected String doInBackground(Void... params) {
			String retVal = "";
			String singleLine = "";

			try{
				httpClient = new CustomHttpClient(fragmentActivity);
				httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_groups_search) + app.getActiveUser().getId() + "/?search=" + URLEncoder.encode(searchTerm, "UTF-8"));
				httpGet.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpGet.setHeader("PLATFORM","Android");
				httpGet.setHeader("USER_ID",""+app.getActiveUser().getId());
				httpResponse = httpClient.execute(httpGet);
				
				httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();
				bufferReader = new BufferedReader(new InputStreamReader(inputStream));
				stringBuilder = new StringBuilder();
				
				while((singleLine = bufferReader.readLine()) != null){
					stringBuilder.append(singleLine);
				}
				
				httpResult = stringBuilder.toString();
				//Log.d("ExplorerSearch", httpResult);
				inputStream.close();
				jsonObject = new JSONObject(httpResult);
				
				if(jsonObject.getBoolean("res")){
					retVal = "success";
				}else{
					retVal = "failed";
				}
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			//activityGroups.hideProgress();
			
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			//startGetPopularGroups();
			txtSearchText.setText("Searching.");
		}
		
		@Override
		protected void onPostExecute(String result){
			searchGroupsResult(result);
		}
	}
}
