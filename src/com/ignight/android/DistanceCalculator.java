package com.ignight.android;

public class DistanceCalculator {
	
	public static double getDistanceInMeters(Coordinate one, Coordinate two) {
		/*double theta = one.longitude - two.longitude;
		double dist = Math.sin(deg2rad(one.latitude)) * Math.sin(deg2rad(two.latitude)) +
						Math.cos(deg2rad(one.latitude)) * Math.cos(deg2rad(two.latitude)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		dist = dist * 60 * 1.1515;
		dist = dist * 1.609344 * 1000;
		return (dist);*/
		
		double R = 6371.0;
		double dLat = (deg2rad(two.latitude - one.latitude));
		double dLon = (deg2rad(two.longitude - two.longitude));

		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
			+ Math.cos(deg2rad(one.latitude))
			* Math.cos(deg2rad(two.latitude)) * Math.sin(dLon / 2)
			* Math.sin(dLon / 2);

		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double d = R * c;
		return d * 1000;
	}
	
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts decimal degrees to radians             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double deg2rad(double deg) {
      return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts radians to decimal degrees             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double rad2deg(double rad) {
      return (rad * 180.0 / Math.PI);
    }
}
