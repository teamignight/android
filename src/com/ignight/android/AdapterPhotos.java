package com.ignight.android;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

public class AdapterPhotos extends ArrayAdapter<Buzz> {
	private Context context;
	private Buzz buzz;
	private ArrayList<Buzz> buzzList;
	
	public AdapterPhotos(Context context, int textViewResourceId, ArrayList<Buzz> items){
		super(context, textViewResourceId, items);
		
		this.context = context;
		this.buzzList = items;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View view = convertView;
		LayoutInflater layoutInflater;
		ImageWithProgress imgAndLayout;
		ImageView imgBuzzText;
		
		if(view == null){
			layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layoutInflater.inflate(R.layout.grid_photos, null);
		}
		
		imgBuzzText = (ImageView)view.findViewById(R.id.imgBuzzPhoto);
		
		buzz = buzzList.get(position);
		//imgBuzzText.setImageBitmap(null);

		imgBuzzText.setTag(buzz.getBuzzText());
		Picasso.with(context).load(buzz.getBuzzText()).placeholder(R.drawable.image_placeholder).fit().centerCrop().into(imgBuzzText);
		return view;
	}
}
