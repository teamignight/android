package com.ignight.android;

public class MessageReportVenue {
	private class NewReportVenue{
		private String address;
		private int cityId;
		private String comment;
		private String number;
		private String type;
		private String url;
		private int userId;
		private int venueId;
		private String venueName;
		
		NewReportVenue(String address, int cityId, String comment, String phoneNumber, String url, int userId, int venueId, String venueName){
			this.address = address;
			this.cityId = cityId;
			this.comment = comment;
			this.number = phoneNumber;
			this.type = "reportVenueDetailsError";
			this.url = url;
			this.userId = userId;
			this.venueId = venueId;
			this.venueName = venueName;
		}
	}
	private NewReportVenue msg;
	MessageReportVenue(String address, int cityId, String comment, String phoneNumber, String url, int userId, int venueId, String venueName){
		this.msg = new NewReportVenue(address, cityId, comment, phoneNumber, url, userId, venueId, venueName);
	}
}
