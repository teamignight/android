package com.ignight.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ignight.android.R.drawable;
import com.squareup.picasso.Picasso;

public class FragmentVenueInfo extends android.support.v4.app.Fragment implements OnMarkerClickListener{
	private AppBase app;
	private Activity fragmentActivity;
	private GoogleMap googleMap;
	private ImageView imgCrowdAge;
	private ImageView imgCrowdAtmosphere;
	private ImageView imgCrowdMusic;
	private ImageView imgCrowdSpending;
	private ImageView imgDown;
	private ImageView imgTrend;
	private ImageView imgUp;
	private AdapterVenueTrendingGroups trendingGroupsAdapter;
	private User user;
	private Venue venue;
	private ActivityVenue venueActivity;
	private TextView venueAddress;
	private TextView venueCrowdAge;
	private TextView venueCrowdAtmosphere;
	private TextView venueCrowdMusic;
	private TextView venueCrowdSpending;
	private TextView venueDistance;
	private TextView venueName;
	private TextView venuePhone;
	private TextView venueReport;
	private TextView venueWebsite;
	private TextView txtNoVenueTrendingStats;
	private ListView listGroups;
	private boolean hasStats = false;
	
	private BufferedReader bufferReader;
	private HttpClient httpClient;
	private HttpGet httpGet;
	private HttpEntity httpEntity;
	private HttpResponse httpResponse;
	private String httpResult;
	private InputStream inputStream;
	private JSONObject jsonObject;
	
	private LinearLayout layoutVenueAddress;
	private LinearLayout layoutVenuePhone;
	private LinearLayout layoutVenueWebsite;
	private View dividerAddress;
	private View dividerPhone;
	private View dividerWebsite;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View view = inflater.inflate(R.layout.fragment_venue_info, container, false);
        venueActivity = ((ActivityVenue)getActivity());
        if (fragmentActivity != null) {
	        app = venueActivity.app;
	        user = venueActivity.CurrentUser;
	        venue = app.ActiveVenue;
	        
	        imgCrowdAge = (ImageView)view.findViewById(R.id.imgCrowdAge);
	        imgCrowdAtmosphere = (ImageView)view.findViewById(R.id.imgCrowdAtmosphere);
	        imgCrowdMusic = (ImageView)view.findViewById(R.id.imgCrowdMusic);
	        imgCrowdSpending = (ImageView)view.findViewById(R.id.imgCrowdSpending);
	        venueAddress = (TextView)view.findViewById(R.id.txtVenueAddress);
	        venueCrowdAge = (TextView)view.findViewById(R.id.txtVenueCrowdAge);
	        venueCrowdAtmosphere = (TextView)view.findViewById(R.id.txtVenueCrowdAtmosphere);
	        venueCrowdMusic = (TextView)view.findViewById(R.id.txtVenueCrowdMusic);
	        venueCrowdSpending = (TextView)view.findViewById(R.id.txtVenueCrowdSpending);
	        venueDistance = (TextView)view.findViewById(R.id.VenueDistance);
	        venueName = (TextView)view.findViewById(R.id.VenueName);
	        venuePhone = (TextView)view.findViewById(R.id.txtVenuePhone);
	        venueReport = (TextView)view.findViewById(R.id.txtVenueReport);
	        venueWebsite = (TextView)view.findViewById(R.id.txtVenueWebsite);
	        txtNoVenueTrendingStats = (TextView)view.findViewById(R.id.txtNoVenueTrendingStats);
	        imgUp = (ImageView)view.findViewById(R.id.imgUp);
	        imgDown = (ImageView)view.findViewById(R.id.imgDown);
	        imgTrend = (ImageView)view.findViewById(R.id.imgTrend);
	        listGroups = (ListView)view.findViewById(R.id.listVenueTrendingGroups);
	        layoutVenueAddress = (LinearLayout)view.findViewById(R.id.layoutVenueAddress);
	    	layoutVenuePhone = (LinearLayout)view.findViewById(R.id.layoutVenuePhone);
	    	layoutVenueWebsite = (LinearLayout)view.findViewById(R.id.layoutVenueWebsite);
	    	dividerAddress = (View)view.findViewById(R.id.dividerAddress);
	    	dividerPhone = (View)view.findViewById(R.id.dividerPhone);
	    	dividerWebsite = (View)view.findViewById(R.id.dividerWebsite);

	    	venueActivity.imgVenueBuzz.setImageResource(drawable.venue_buzz);
        	venueActivity.imgVenueInfo.setImageResource(drawable.venue_info_active);
        	venueActivity.imgVenuePhotos.setImageResource(drawable.venue_photos);
	    	
	        new GetVenueTask().execute();
		}
        return view;
	}
	
	@Override
	public boolean onMarkerClick(Marker arg0) {
		return false;
	}
	
	public void updateVote(int vote){
		if(vote == 1){
        	imgUp.setImageResource(R.drawable.upvote_on);
        	imgDown.setImageResource(R.drawable.downvote_off);
        }else if(vote == -1){
        	imgUp.setImageResource(R.drawable.upvote_off);
        	imgDown.setImageResource(R.drawable.downvote_on);
        }else{
        	imgUp.setImageResource(R.drawable.upvote_off);
        	imgDown.setImageResource(R.drawable.downvote_off);
        }
	}
	
	@Override
	public void onDetach(){
		killMap();
		super.onDetach();
	}
	
	public void onDestroyView(){
		killMap();
		super.onDestroyView();
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		fragmentActivity = activity;
	}
	
	@Override
	public void onResume(){
		venueActivity.updateHeaderText("Info");
		super.onResume();
	}
	
	public void killMap(){
		android.support.v4.app.FragmentManager fm = getFragmentManager();
		
		SupportMapFragment mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.venueMap);
		
		try{
			if(mapFragment != null){
				fm.beginTransaction().remove(mapFragment).commit();
			}
		}catch(Exception ex){
			
		}
	}
	
	private void setVenueDetails(){
		if(venue.getAddress().isEmpty()){
			layoutVenueAddress.setVisibility(View.GONE);
			dividerAddress.setVisibility(View.GONE);
		}else{
			venueAddress.setText(venue.getAddress());
        	venueAddress.setPaintFlags(venueAddress.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
		}
        
        double distance;
        Coordinate userCoordinate;
        Coordinate venueCoordinate;
        userCoordinate = new Coordinate();
		userCoordinate.latitude = user.getLatitude();
		userCoordinate.longitude = user.getLongitude();
        venueCoordinate = new Coordinate();
		venueCoordinate.latitude = venue.getLatitude();
		venueCoordinate.longitude = venue.getLongitude();
		distance = DistanceCalculator.getDistanceInMeters(userCoordinate, venueCoordinate);
		double miles = distance * 0.00062137119;
		DecimalFormat twoDForm = new DecimalFormat("#.##");
		venueDistance.setText(twoDForm.format(miles) + " miles");
		
        venueName.setText(venue.getName());
        if(venue.getPhoneNumber().isEmpty()){
        	layoutVenuePhone.setVisibility(View.GONE);
        	dividerPhone.setVisibility(View.GONE);
        }else{
        	venuePhone.setText(venue.getPhoneNumber());
        	venuePhone.setPaintFlags(venuePhone.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        }
        venueReport.setPaintFlags(venueReport.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        if(venue.getUrl().isEmpty()){
        	layoutVenueWebsite.setVisibility(View.GONE);
        	dividerWebsite.setVisibility(View.GONE);
        }else{
        	venueWebsite.setText(venue.getUrl());
        	venueWebsite.setPaintFlags(venueWebsite.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        }
        
        int temp;
        temp = venue.getMusicType();
        if(temp != -1){
        	hasStats = true;
        	venueCrowdMusic.setText(DNAOptions.MusicOptions().get(temp).getName());
        	Picasso.with(fragmentActivity).load(DNAOptions.MusicOptions().get(temp).getResourceId(DNAOptions.MusicOptions().get(temp).getId())).into(imgCrowdMusic);
        	//imgCrowdMusic.setImageResource(DNAOptions.MusicOptions().get(venue.getMusicType() == -1 ? 0 : venue.getMusicType()).getResourceId(DNAOptions.MusicOptions().get(venue.getMusicType() == -1 ? 0 : venue.getMusicType()).getId()));
        }
        temp = venue.getAtmosphere();
        if(temp != -1){
        	hasStats = true;
        	venueCrowdAtmosphere.setText(DNAOptions.AtmosphereOptions().get(temp).getName());
        	Picasso.with(fragmentActivity).load(DNAOptions.AtmosphereOptions().get(temp).getResourceId(DNAOptions.AtmosphereOptions().get(temp).getId())).into(imgCrowdAtmosphere);
        	//imgCrowdAtmosphere.setImageResource(DNAOptions.AtmosphereOptions().get(venue.getAtmosphere() == -1 ? 0 : venue.getAtmosphere()).getResourceId(DNAOptions.AtmosphereOptions().get(venue.getAtmosphere() == -1 ? 0 : venue.getAtmosphere()).getId()));
        }
        temp = venue.getAge();
        if(temp != -1){
        	hasStats = true;
        	venueCrowdAge.setText(DNAOptions.AgeOptions().get(temp).getName());
        	Picasso.with(fragmentActivity).load(DNAOptions.AgeOptions().get(temp).getResourceId(DNAOptions.AgeOptions().get(temp).getId())).into(imgCrowdAge);
        	//imgCrowdAge.setImageResource(DNAOptions.AgeOptions().get(venue.getAge() == -1 ? 0 : venue.getAge()).getResourceId(DNAOptions.AgeOptions().get(venue.getAge() == -1 ? 0 : venue.getAge()).getId()));
        }
        temp = venue.getAverageSpendingLimit();
        if(temp != -1){
        	hasStats = true;
        	venueCrowdSpending.setText(DNAOptions.SpendingPreferenceOptions().get(temp).getName());
        	Picasso.with(fragmentActivity).load(DNAOptions.SpendingPreferenceOptions().get(temp).getResourceId(DNAOptions.SpendingPreferenceOptions().get(temp).getId())).into(imgCrowdSpending);
        	//imgCrowdSpending.setImageResource(DNAOptions.SpendingPreferenceOptions().get(venue.getAverageSpendingLimit() == -1 ? 0 : venue.getAverageSpendingLimit()).getResourceId(DNAOptions.SpendingPreferenceOptions().get(venue.getAverageSpendingLimit() == -1 ? 0 : venue.getAverageSpendingLimit()).getId()));
        }
        
        if(!hasStats)
        	txtNoVenueTrendingStats.setVisibility(View.VISIBLE);
        
        updateVote(venue.getUserInput());
        
        int resId;
        
        if(venue.getUserBarColor() == 0){
			resId = R.drawable.trend_4;
		}else if(venue.getUserBarColor() == 1){
			resId = R.drawable.trend_3;
		}else if(venue.getUserBarColor() == 2){
			resId = R.drawable.trend_2;
		}else if(venue.getUserBarColor() == 3){
			resId = R.drawable.trend_1;
		}else if(venue.getUserBarColor() == 4){
			resId = R.drawable.trend_0;
		}else{
			resId = R.drawable.trend_0;
		}
		imgTrend.setImageResource(resId);
        
        //set map
        googleMap = ((SupportMapFragment) venueActivity.getSupportFragmentManager().findFragmentById(R.id.venueMap)).getMap();
        if(googleMap != null){
        	googleMap.getUiSettings().setZoomControlsEnabled(false);
        	resId = R.drawable.map_pin_0;
        	
        	if(venue.getUserBarColor() == 0){
				resId = R.drawable.map_pin_4;
			}else if(venue.getUserBarColor() == 1){
				resId = R.drawable.map_pin_3;
			}else if(venue.getUserBarColor() == 2){
				resId = R.drawable.map_pin_2;
			}else if(venue.getUserBarColor() == 3){
				resId = R.drawable.map_pin_1;
			}else{
				resId = R.drawable.map_pin_0;
			}
    		
    		BitmapDrawable bd = (BitmapDrawable) getResources().getDrawable(resId);
    		Bitmap original = bd.getBitmap();
    		Bitmap resized = Bitmap.createScaledBitmap(original, original.getWidth() / 2, original.getHeight() / 2, false);
    		
    		googleMap.addMarker(new MarkerOptions()
    		.position(new LatLng(venue.getLatitude(), venue.getLongitude()))
    		.icon(BitmapDescriptorFactory.fromBitmap(resized)));
        	
        	LatLng initView = new LatLng(app.ActiveVenue.getLatitude(), venue.getLongitude());
    		CameraPosition cameraPosition = new CameraPosition.Builder()
    			.target(initView)
    			.zoom(17)
    			.build();
    		googleMap.getUiSettings().setAllGesturesEnabled(false);
    		googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
        
        //set trending groups
        ArrayList<Group> trendingGroups = new ArrayList<Group>();
        for(int counter = 0; counter < venue.getTrendingGroups().size(); counter++){
        	if(counter == 3)
        		break;
        	trendingGroups.add(venue.getTrendingGroups().get(counter));
        }
        trendingGroupsAdapter = new AdapterVenueTrendingGroups(fragmentActivity, R.layout.list_trending_group, trendingGroups);
        listGroups.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Group g = venue.getTrendingGroups().get(position);
				app.ActiveGroup = g;
				if(g.getIsMember()){
					loadGroup();
				}else {
					loadGroupDetails();
				}
			}
		});
        listGroups.setAdapter(trendingGroupsAdapter);
        app.setListViewHeightBasedOnChildren(listGroups);
	}
	
	private void loadGroupDetails(){
		Intent intent = new Intent(fragmentActivity, ActivityGroupDetails.class);
		startActivity(intent);
	}
	
	private void loadGroup(){
		Intent intent;
    	intent = new Intent(fragmentActivity, ActivityGroup.class);
    	startActivity(intent);
	}
	
	private void getVenueResult(String result){
		JSONObject jsonVenue;
		JSONArray jsonVenueGroups;
		JSONObject jsonVenueStats;
		Group trendingGroup;
		Venue venue = app.ActiveVenue;
		
		if(result.equals("success")){
			try {
				jsonVenue = jsonObject.getJSONObject("body").getJSONObject("venue");
				venue.setAddress(jsonVenue.getString("address"));
				//venue.setState(jsonVenue.getString("state"));
				//venue.setZip(jsonVenue.getString("zip"));
				venue.setPhoneNumber(jsonVenue.getString("phoneNumber"));
				venue.setUrl(jsonVenue.getString("url"));
				venue.setUserInput(jsonObject.getJSONObject("body").getInt("userInput"));
				venue.setLatitude(jsonVenue.getDouble("latitude"));
				venue.setLongitude(jsonVenue.getDouble("longitude"));
				venue.setUserBarColor(jsonObject.getJSONObject("body").getInt("userBarColor"));
				City city = new City(user.getCity().getId(), user.getCity().getName());
				venue.setVenueCity(city);
				
				jsonVenueStats = jsonObject.getJSONObject("body").getJSONObject("venueStats");
				if(jsonVenueStats != null){
					venue.setAge(jsonVenueStats.getInt("ageBucketMode"));
					venue.setAverageSpendingLimit(jsonVenueStats.getInt("avgSpendingLimit"));
					venue.setAtmosphere(jsonVenueStats.getInt("atmosphereTypeMode"));
					venue.setMusicType(jsonVenueStats.getInt("musicTypeMode"));
				}
				if(jsonObject.getJSONObject("body").optBoolean("buzzActive")){
					venueActivity.showBuzzActive();
				}
				jsonVenueGroups = jsonObject.getJSONObject("body").getJSONArray("trendingGroups");
				venue.setTrendingGroups(new ArrayList<Group>());
				if(jsonVenueGroups != null){
					for(int i = 0; i < jsonVenueGroups.length(); i++){
						trendingGroup = new Group();
						trendingGroup.setGroupId(jsonVenueGroups.getJSONObject(i).getInt("groupId"));
						//Log.d("VenueGroup",jsonVenueGroups.getJSONObject(i).getInt("groupId") + "");
						trendingGroup.setGroupName(jsonVenueGroups.getJSONObject(i).getString("groupName"));
						trendingGroup.setIsPrivate(jsonVenueGroups.getJSONObject(i).getBoolean("privateGroup"));
						trendingGroup.setIsMember(jsonVenueGroups.getJSONObject(i).getBoolean("isMemberOfGroup"));
						venue.getTrendingGroups().add(trendingGroup);
					}
				}
				
				setVenueDetails();
			} catch (JSONException e) {
				venueActivity.showMessage("Error, no venue returned." + e.getMessage());
			}
		}else{
			
		}
	}
	
	private void startGetVenue(){
		venueActivity.showProgressDialog("Loading venue...");
	}
	
	private class GetVenueTask extends AsyncTask<Void, Void, String>{

		@Override
		protected String doInBackground(Void... args) {
			String retVal = "";
			String singleLine = "";
			StringBuilder stringBuilder;
			
			try{
				httpClient = new CustomHttpClient(fragmentActivity);
				httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_venue_info) + app.ActiveVenue.getId() + "/?userId=" + app.getActiveUser().getId());
				httpGet.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpGet.setHeader("PLATFORM","Android");
				httpGet.setHeader("USER_ID",""+app.getActiveUser().getId());
				httpResponse = httpClient.execute(httpGet);
				httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();
				bufferReader = new BufferedReader(new InputStreamReader(inputStream));
				stringBuilder = new StringBuilder();
				
				while((singleLine = bufferReader.readLine()) != null){
					stringBuilder.append(singleLine);
				}
				
				httpResult = stringBuilder.toString();
				inputStream.close();
				
				if(httpResult.isEmpty()){
					retVal = "Error, empty result";
				}else{
					Log.d("Venue", httpResult);
					jsonObject = new JSONObject(httpResult);

					if(jsonObject.getBoolean("res")){
						retVal = "success";
					}else{
						retVal = "failed";
					}
				}
				
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			venueActivity.hideProgressDialog();
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startGetVenue();
		}
		
		@Override
		protected void onPostExecute(String result){
			getVenueResult(result);
		}
	}
	
}
