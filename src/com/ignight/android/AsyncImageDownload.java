package com.ignight.android;

import java.io.InputStream;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class AsyncImageDownload extends AsyncTask<ImageWithProgress, Void, Bitmap>{
	ImageView imageView = null;
	RelativeLayout loader = null;
	
	@Override
	protected Bitmap doInBackground(ImageWithProgress... image) {
		this.imageView = (ImageView)image[0].getImg();
		this.loader = (RelativeLayout)image[0].getLayout();
		return getBitmapDownloaded((String) imageView.getTag());
	}
	
	protected void onPostExecute(Bitmap result) {
		if(loader != null){
			imageView.setVisibility(View.VISIBLE);
			loader.setVisibility(View.GONE);
		}
        imageView.setImageBitmap(result); //set the bitmap to the imageview.
    }

	/** This function downloads the image and returns the Bitmap **/
    private Bitmap getBitmapDownloaded(String url) {
        System.out.println("String URL " + url);
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream((InputStream) new URL(url)
                    .getContent());
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }
    
    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);
        // RECREATE THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }
}
