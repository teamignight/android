package com.ignight.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class ActivityForgotPassword extends ActivityBase {
	private ActivityForgotPassword activity;
	private BufferedReader bufferReader;
	private Gson gson;
	private String httpResult;
	private HttpClient httpClient;
	private HttpGet httpGet;
	private HttpEntity httpEntity;
	private HttpPost httpPost;
	private HttpResponse httpResponse;
	private InputStream inputStream;
	private JSONObject jsonObject;
	private ProgressDialog progressDialog;
	private StringBuilder stringBuilder;
	private EditText userName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_forgot_password);
		activity = this;
	}
	
	public void btnRemindMe_ClickEvent(View view){
		userName = (EditText)findViewById(R.id.txtUserName);
		if(userName.getText().toString().isEmpty()){
			showMessage("Username is required.");
		}else{
			new GetPasswordTask().execute();
		}
	}
	
	private void startGetPassword(){
		progressDialog = ProgressDialog.show(this, "", "Retrieving temporary password....", true);
	}
	
	private void getPasswordResult(String result){
		if(result.equals("success")){
			showMessage("Your password has been sent to your email address.");
			Intent intent = new Intent(ActivityForgotPassword.this, ActivityResetPassword.class);
			intent.putExtra("userName", userName.getText().toString());
			startActivity(intent);
		}else{
			showMessage(result);
		}
	}
	
	private class GetPasswordTask extends AsyncTask<Void,Void,String>{
		@Override
		protected String doInBackground(Void... args) {
			String retVal = "";
			String singleLine = "";
			
			try{
				httpClient = new CustomHttpClient(activity);
				httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_forgot_password) + URLEncoder.encode(userName.getText().toString(), "UTF-8") + "/?userName="+URLEncoder.encode(userName.getText().toString(), "UTF-8"));
				httpGet.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpGet.setHeader("PLATFORM","Android");
				httpGet.setHeader("USER_ID","");
				httpResponse = httpClient.execute(httpGet);
				
				httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();
				bufferReader = new BufferedReader(new InputStreamReader(inputStream));
				stringBuilder = new StringBuilder();
				
				while((singleLine = bufferReader.readLine()) != null){
					stringBuilder.append(singleLine);
				}
				
				httpResult = stringBuilder.toString();
				inputStream.close();
				jsonObject = new JSONObject(httpResult);
				Log.d("Password", httpResult);
				if(jsonObject.getBoolean("res")){
					retVal = "success";
				}else{
					//retVal = jsonObject.getString("reason");
					retVal = "Username not found.";
				}
				
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			if(progressDialog != null && progressDialog.isShowing())
				progressDialog.dismiss();
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startGetPassword();
		}
		
		@Override
		protected void onPostExecute(String result){
			getPasswordResult(result);
		}
	}
}
