package com.ignight.android;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

public class AdapterTrending extends ArrayAdapter<Venue> {
	private AppBase app;
	private BufferedReader bufferReader;
	private Context context;
	private Gson gson;
	private String httpResult;
	private HttpClient httpClient;
	private HttpEntity httpEntity;
	private HttpPost httpPost;
	private HttpResponse httpResponse;
	private InputStream inputStream;
	private JSONObject jsonObject;
	private User user;
	private Coordinate userCoordinate;
	private Venue venue;
	private ArrayList<Venue> venues;
	private ViewHolder holder;	
	private VoteObject voteObject;
	private boolean showTrendingText;
    private Coordinate venueCoordinate;
    private double distance;
    private double miles;
    private final DecimalFormat twoDForm = new DecimalFormat("#.##");
	
	public AdapterTrending(Context context, int textViewResourceId, ArrayList<Venue> items, User activeUser, AppBase appBase){
		super(context, textViewResourceId, items);
		
		this.context = context;
		this.venues = items;
		this.app = appBase;
		
		user = activeUser;
		userCoordinate = new Coordinate();
		userCoordinate.latitude = user.getLatitude();
		userCoordinate.longitude = user.getLongitude();
		showTrendingText = this.app.TrendingType.equalsIgnoreCase("group");
	}
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {	
		String color = "";
		View view = convertView;
		LayoutInflater layoutInflater;
		int resId = R.drawable.trend_0;

		layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if(view == null){			
			view = layoutInflater.inflate(R.layout.list_trending, null);
			holder = new ViewHolder();
			holder.venueName = (TextView)view.findViewById(R.id.txtTrendingVenueName);
			holder.trendingDistance = (TextView)view.findViewById(R.id.txtTrendingDistance);
			holder.imgTrend = (ImageView)view.findViewById(R.id.imgTrend);
			holder.imgUp = (ImageView)view.findViewById(R.id.imgUp);
			holder.imgDown = (ImageView)view.findViewById(R.id.imgDown);
			holder.txtTrendText = (TextView)view.findViewById(R.id.txtTrendText);
			view.setTag(holder);
		}else{
			holder = (ViewHolder)view.getTag();
		}

		venue = venues.get(position);

		if(venue != null){
			//Log.d("VenueInput","Venue: " + venue.getName() + " : " + venue.getUserInput());
			
			voteObject = new VoteObject(venue.getId(), venue.getUserInput());
			
			if(app.TrendingType.equalsIgnoreCase("group")){
				holder.imgTrend.setVisibility(View.GONE);
			}else{
				if(holder.imgTrend != null){
					if(venue.getUserBarColor() == 0){
						resId = R.drawable.trend_4;
					}else if(venue.getUserBarColor() == 1){
						resId = R.drawable.trend_3;
					}else if(venue.getUserBarColor() == 2){
						resId = R.drawable.trend_2;
					}else if(venue.getUserBarColor() == 3){
						resId = R.drawable.trend_1;
					}else if(venue.getUserBarColor() == 4){
						resId = R.drawable.trend_0;
					}
					holder.imgTrend.setImageResource(resId);
					Picasso.with(context).load(resId).noFade().fit().into(holder.imgTrend);
				}
			}
			if(showTrendingText){
				if(holder.txtTrendText != null){
					color = venue.getUserVenueValue() == 0 ? "#000000" : venue.getUserVenueValue() > 0 ? "#62C012" : "#E00708";
					holder.txtTrendText.setVisibility(View.VISIBLE);
					if(venue.getUserVenueValue() == -9999){
						holder.txtTrendText.setText("-");
						color = "#000000";
					}
					else
						holder.txtTrendText.setText((venue.getUserVenueValue() > 0 ? "+" : "") + String.valueOf((int)venue.getUserVenueValue()));
					holder.txtTrendText.setTextColor(Color.parseColor(color));
				}
			}
			
			if(holder.venueName != null){ 
				holder.venueName.setText(venue.getName());
			}
			if(holder.trendingDistance != null){
				venueCoordinate = new Coordinate();
				venueCoordinate.latitude = venue.getLatitude();
				venueCoordinate.longitude = venue.getLongitude();
				distance = DistanceCalculator.getDistanceInMeters(userCoordinate, venueCoordinate);
				miles = distance * 0.00062137119;
				
				holder.trendingDistance.setText(twoDForm.format(miles) + " miles"); 
			}
			if(holder.imgDown != null){ 
				if(venue.getUserInput() == -1)
					resId = R.drawable.downvote_on;
				else
					resId = R.drawable.downvote_off;
				
				Picasso.with(context).load(resId).noFade().into(holder.imgDown);
				holder.imgDown.setTag(voteObject);
				holder.imgDown.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View view) {
						Venue selectedVenue = null;
						
						for(Venue v: app.Venues){
							if(v.getId() == ((VoteObject)view.getTag()).venueId){
								selectedVenue = v;
								break;
							}
						}
						if(selectedVenue != null){
							//Log.d("TrendingAdapter","Venue: " + selectedVenue.getName());
							View parent = (View)view.getParent();
							if(selectedVenue.getUserInput() == -1){
								selectedVenue.setUserInput(0);
								Picasso.with(context).load(R.drawable.downvote_off).noFade().into(((ImageView) view));
							}else{
								selectedVenue.setUserInput(-1);
								Picasso.with(context).load(R.drawable.downvote_on).noFade().into(((ImageView) view));
								
								if(parent != null)
									Picasso.with(context).load(R.drawable.upvote_off).noFade().into(((ImageView) parent.findViewById(R.id.imgUp)));
							}
							voteObject = new VoteObject(selectedVenue.getId(), selectedVenue.getUserInput());
							view.setTag(voteObject);
							new UpdateVoteTask().execute(String.valueOf(selectedVenue.getUserInput()), String.valueOf(selectedVenue.getId()));
						}
					}
				});
			}
			if(holder.imgUp != null){ 
				if(venue.getUserInput() == 1)
					resId = R.drawable.upvote_on;
				else
					resId = R.drawable.upvote_off;

				Picasso.with(context).load(resId).noFade().into(holder.imgUp);
				holder.imgUp.setTag(voteObject);
				holder.imgUp.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View view) {
						Venue selectedVenue = null;
						
						for(Venue v: app.Venues){
							if(v.getId() == ((VoteObject)view.getTag()).venueId){
								selectedVenue = v;
								break;
							}
						}
						if(selectedVenue != null){
							View parent = (View)view.getParent();
							if(selectedVenue.getUserInput() == 1){
								selectedVenue.setUserInput(0);
								Picasso.with(context).load(R.drawable.upvote_off).noFade().into(((ImageView) view));
							}else{
								selectedVenue.setUserInput(1);
								Picasso.with(context).load(R.drawable.upvote_on).noFade().into(((ImageView) view));
								
								if(parent != null)
									Picasso.with(context).load(R.drawable.downvote_off).noFade().into(((ImageView) parent.findViewById(R.id.imgDown)));
							}
							voteObject = new VoteObject(selectedVenue.getId(), selectedVenue.getUserInput());
							view.setTag(voteObject);
							new UpdateVoteTask().execute(String.valueOf(selectedVenue.getUserInput()), String.valueOf(selectedVenue.getId()));
						}
					}
				});
			}
		}
		
		return view;
	}
	
	public static class ViewHolder{
		ImageView imgTrend;
		TextView venueName;
		TextView trendingDistance;
		ImageView imgUp;
		ImageView imgDown;
		TextView txtTrendText;
	}
	
	private class VoteObject{
		int venueId;
		int vote;
		
		VoteObject(int vId, int v){
			this.venueId = vId;
			this.vote = v;
		}
	}
	
	private class UpdatedVenue{
		String result;
		int venueId;
		int vote;
	}
	
	private class UpdateVoteTask extends AsyncTask<String, Void, UpdatedVenue>{

		@Override
		protected UpdatedVenue doInBackground(String... vote) {
			String json = "";
			UpdatedVenue retVal = new UpdatedVenue();
			String singleLine = "";
			StringBuilder stringBuilder;
			
			try{
				gson = new GsonBuilder().create();
				retVal.venueId = Integer.parseInt(vote[1]);
				retVal.vote = Integer.parseInt(vote[0]);
				
				MessageVoteVenue voteVenue = new MessageVoteVenue(Integer.parseInt(vote[0]), user.getId(), Integer.parseInt(vote[1]));
				json = gson.toJson(voteVenue);
				StringEntity stringEntity = new StringEntity(json);
				httpClient = new CustomHttpClient(context);
				httpPost = new HttpPost(context.getResources().getString(R.string.api_base) + context.getResources().getString(R.string.api_venue_activity) + vote[1]);
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(context.getResources().getString(R.string.api_token_key),context.getResources().getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID",""+user.getId());
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					
					if(httpResult != null){
						//Log.d("Adapter", "result: " + httpResult);
						jsonObject = new JSONObject(httpResult);
						
						if(jsonObject.getBoolean("res")){
							retVal.result = "success";
						}else{
							retVal.result = "failed";
						}
					}else{
						retVal.result = "failed";
					}
				}
				retVal.result = "success";
			}catch(Exception ex){
				retVal.result = "failed";
			}
			
			return retVal;
		}
		
		@Override
		protected void onPostExecute(UpdatedVenue retVal){

		}
	}
}
