package com.ignight.android;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

public class ActivityTutorial extends ActivityBase {
	private AdapterTutorial adapterTutorial;
	private ArrayList<Integer> items;
	private static final int NUM_PAGES = 4;
	private CirclePageIndicator mIndicator;
	private ViewPager mPager;
	private PagerAdapter mPagerAdapter;
	private String label;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_tutorial);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,R.layout.module_title_bar);
		
		wireupHeader("Tutorial", "");
		if(app.NewTutorial){
			app.NewTutorial = false;
			hideHeaderButton();
			label = "Skip";
			toggleTutorialMenu(true, label);
		}
		
		if(txtTutorialMenu != null){
			txtTutorialMenu.setClickable(true);
			txtTutorialMenu.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					closeTutorial();
				}
			});
		}
		
		items = new ArrayList<Integer>();
		items.add(R.drawable.tutorial_city_trending);
		items.add(R.drawable.tutorial_venue_profile);
		items.add(R.drawable.tutorial_groups_portal);
		items.add(R.drawable.tutorial_group_trending);
		
		adapterTutorial = new AdapterTutorial(this, items);
		
		mPager = (ViewPager) findViewById(R.id.viewPager);
        //mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(adapterTutorial);
        mPager.setPageTransformer(true, new ZoomOutPageTransformer());
        
        mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        mIndicator.setViewPager(mPager);
        mIndicator.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				if(txtTutorialMenu != null)
					txtTutorialMenu.setText(position == 3 ? "Done" : "Skip");
			}
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
	}
	
	@Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }
	
	private void closeTutorial(){
		Intent intent = new Intent(this, ActivityTrending.class);
		startActivity(intent);
	}
	
	private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
        	Fragment fragment = new FragmentScreenSlidePage();
        	((FragmentScreenSlidePage)fragment).setImageSrc(R.drawable.avatar_default);
            return fragment;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
}
