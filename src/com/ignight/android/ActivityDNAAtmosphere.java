package com.ignight.android;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class ActivityDNAAtmosphere extends ActivityBase {
	private AdapterAtmosphere adapter;
	private GridView gridView;
	private final int[] overlays = { R.drawable.selected_one, R.drawable.selected_two, R.drawable.selected_three, R.drawable.selected_four, R.drawable.selected_five };
	protected TextView title;
	private User user;
	private Button btnSignup;
	private boolean ready;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dna_atmosphere);
        
        app = ((AppBase)getApplication());
        user = app.getActiveUser();
        
        btnSignup = (Button)findViewById(R.id.btnSignup);
        adapter = new AdapterAtmosphere(this, DNAOptions.AtmosphereOptions(), user);
        if(app.hash == null)
        	app.hash = new SparseArray<DNAAtmosphere>();
        
        gridView = (GridView)findViewById(R.id.grdAtmosphere);
		gridView.setAdapter(adapter); 
		gridView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id){
				ImageView numberView;
				int pos = 0;
				DNAAtmosphere currentItem;
				int currItemPos = 0;
				DNAAtmosphere item = DNAOptions.AtmosphereOptions().get(position);
				
				numberView = (ImageView)v.findViewById(R.id.gridAtmosphereNumber);
				if(app.hash.get(position) != null){
					for(int i = 0; i < user.getAtmosphereOptions().size(); i++){
						currentItem = user.getAtmosphereOptions().get(i);
						if(currentItem.getId() == app.hash.get(position).getId()){
							currItemPos = i;
						}
					}
					if((currItemPos + 1) < user.getAtmosphereOptions().size()){
						checkIfReady();
						return;
					}
					user.getAtmosphereOptions().remove(app.hash.get(position));
					app.hash.remove(position);
					numberView.setImageDrawable(null);
					checkIfReady();
					return;
				}
				if(user.getAtmosphereOptions().size() == 3 && app.hash.get(position) == null){
					checkIfReady();
					return;
				}
				user.getAtmosphereOptions().add(item);
				app.hash.put(position, item);
				for(int i = 0; i < user.getAtmosphereOptions().size(); i++){
					if(app.hash.get(position).getId() == user.getAtmosphereOptions().get(i).getId()){
						pos = i;
						break;
					}
				}
				//Picasso.with(ActivityDNAAtmosphere.this).load(overlays[pos]).into(numberView);
				numberView.setImageResource(overlays[pos]);
				checkIfReady();
			}
		});
	}
	
	@Override
	public void onResume(){
		super.onResume();
	}
	
	@Override
	public void onStop(){
		super.onStop();
	}

	public void btnNext_ClickEvent(View button){
		Intent intent;
		if(user.getAtmosphereOptions().size() < 1){
			showMessage("Please select at least 1 atmosphere preference.");
		}else{
			intent = new Intent(ActivityDNAAtmosphere.this, ActivityDNASpendingPreference.class);
			startActivity(intent);
		}
	}
	
	private void checkIfReady(){
		ready = user.getAtmosphereOptions().size() > 0;
		if(ready){
			btnSignup.setTextColor(Color.parseColor("#d849b10a"));
		}else{
			btnSignup.setTextColor(Color.parseColor("#626262"));
		}
	}
}
