package com.ignight.android;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public class TaskFragment  extends Fragment {

	  /**
	   * Callback interface through which the fragment will report the
	   * task's progress and results back to the Activity.
	   */
	  static interface TaskCallbacks {
	    void onPreExecute();
	    void onProgressUpdate(int percent);
	    void onCancelled();
	    void onPostExecute();
	  }

	  private TaskCallbacks mCallbacks;
	  private DummyTask mTask;
	  
	  private BufferedReader bufferReader;
	  private Gson gson;
	  private HttpClient httpClient;
	  private HttpEntity httpEntity;
	  private HttpPost httpPost;
	  private HttpResponse httpResponse;
	  private String httpResult;
	  private InputStream inputStream;
	  private JSONObject jsonObject;
	  private StringBuilder stringBuilder;
	  
	  public String taskResult;
	  public String buzzPost;
	  public String filePath;
	  public int userId;
	  public int venueId;

	  /**
	   * Hold a reference to the parent Activity so we can report the
	   * task's current progress and results. The Android framework 
	   * will pass us a reference to the newly created Activity after 
	   * each configuration change.
	   */
	  @Override
	  public void onAttach(Activity activity) {
	    super.onAttach(activity);
	    mCallbacks = (TaskCallbacks) activity;
	  }

	  /**
	   * This method will only be called once when the retained
	   * Fragment is first created.
	   */
	  @Override
	  public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);

	    // Retain this fragment across configuration changes.
	    setRetainInstance(true);

	    // Create and execute the background task.
	    mTask = new DummyTask();
	    mTask.execute();
	  }

	  /**
	   * Set the callback to null so we don't accidentally leak the 
	   * Activity instance.
	   */
	  @Override
	  public void onDetach() {
	    super.onDetach();
	    mCallbacks = null;
	  }

	  /**
	   * A dummy task that performs some (dumb) background work and
	   * proxies progress updates and results back to the Activity.
	   *
	   * Note that we need to check if the callbacks are null in each
	   * method in case they are invoked after the Activity's and
	   * Fragment's onDestroy() method have been called.
	   */
	  private class DummyTask extends AsyncTask<Void, Integer, Void> {

		    @Override
		    protected void onPreExecute() {
		      if (mCallbacks != null) {
		        mCallbacks.onPreExecute();
		      }
		    }
	
		    /**
		     * Note that we do NOT call the callback object's methods
		     * directly from the background thread, as this could result 
		     * in a race condition.
		     */
		    @Override
		    protected Void doInBackground(Void... ignore) {
		    	String json = "";
				String retVal = "";
				String singleLine = "";
				
				try{
					gson = new GsonBuilder().create();
					MessageBuzz buzz = new MessageBuzz(buzzPost, userId, venueId);
					json = gson.toJson(buzz);
					
					httpClient = new DefaultHttpClient();
					httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_venue));
					
					MultipartEntityBuilder meBuilder = MultipartEntityBuilder.create();
					meBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
					
					File file = new File(filePath);
					FileBody fb = new FileBody(file);
					
					meBuilder.addPart("userImage", fb);
					meBuilder.addTextBody("json_body",json);
					httpPost.setEntity(meBuilder.build());
					httpPost.setHeader("Accept","application/json");
					
					httpResponse = httpClient.execute(httpPost);
					httpEntity = httpResponse.getEntity();
					if(httpEntity != null){
						inputStream = httpEntity.getContent();
						bufferReader = new BufferedReader(new InputStreamReader(inputStream));
						stringBuilder = new StringBuilder();
						
						while((singleLine = bufferReader.readLine()) != null){
							stringBuilder.append(singleLine);
						}
						
						httpResult = stringBuilder.toString();
						inputStream.close();
						
						if(httpResult != null){
							jsonObject = new JSONObject(httpResult);
							
							if(jsonObject.getBoolean("res")){
								retVal = "success";
							}else{
								retVal = "Error, posting photo.";
							}
						}else{
							retVal = "Error, no result.";
						}
					}
					
				}catch(Exception e){
					retVal = "General Error, " + e.getMessage();
				}
				
				taskResult = retVal;
				return null;
		    }
	
		    @Override
		    protected void onProgressUpdate(Integer... percent) {
		      if (mCallbacks != null) {
		        mCallbacks.onProgressUpdate(percent[0]);
		      }
		    }
	
		    @Override
		    protected void onCancelled() {
		      if (mCallbacks != null) {
		        mCallbacks.onCancelled();
		      }
		    }
	
		    @Override
		    protected void onPostExecute(Void retVal) {
		      if (mCallbacks != null) {
		        mCallbacks.onPostExecute();
		      }
		    }
	  }
}
