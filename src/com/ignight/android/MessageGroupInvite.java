package com.ignight.android;

public class MessageGroupInvite {
	private class NewGroupInvite{
		private int cityId;
		private int groupId;
		private int targetUserId;
		private int userId;
		private String type; //inviteUser
		
		NewGroupInvite(int cityId, int groupId, int targetUserId, int userId, String type){
			this.cityId = cityId;
			this.groupId = groupId;
			this.targetUserId = targetUserId;
			this.userId = userId;
			this.type = type;
		}
	}
	private NewGroupInvite msg;
	MessageGroupInvite(int cityId, int groupId, int targetUserId, int userId, String type){
		this.msg = new NewGroupInvite(cityId, groupId, targetUserId, userId, "inviteUser");
	}
}
