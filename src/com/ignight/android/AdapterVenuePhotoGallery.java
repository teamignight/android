package com.ignight.android;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class AdapterVenuePhotoGallery extends PagerAdapter {
	private Context context;
	private ArrayList<VenueImage> venueImages;
	
	AdapterVenuePhotoGallery(Context context, ArrayList<VenueImage> venueImages){
		this.context = context;
		this.venueImages = venueImages;
	}
	
	@Override
	public int getCount(){
		return venueImages.size();
	}
	
	@Override
	public boolean isViewFromObject(View view, Object object){
		return view == ((ImageView) object);
	}
	
	@Override
	public Object instantiateItem(ViewGroup container, int position){
		Bitmap image;
		ImageView imageView = new ImageView(context);
		
		image = venueImages.get(position).getImage();
		imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
		if(image != null){
			Log.d("Adapter", "Image not null");
			imageView.setImageBitmap(venueImages.get(position).getImage());
		}else{
			imageView.setImageResource(R.drawable.dna_rap);
			Log.d("Adapter", "Image is null");
		}
		((ViewPager)container).addView(imageView, 0);
		
		return imageView;
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object){
		((ViewPager) container).removeView((ImageView) object);
	}
}
