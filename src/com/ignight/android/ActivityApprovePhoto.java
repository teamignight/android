package com.ignight.android;

import java.io.File;

import com.squareup.picasso.Picasso;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

public class ActivityApprovePhoto extends ActivityBase{
	private ImageView imgFullPhoto;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_approve_photo);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,R.layout.module_title_bar);
		
		wireupHeader("Approve Photo", "");
		
		hideHeaderButton();
		
		imgFullPhoto = (ImageView)findViewById(R.id.imgFullPhoto);
		
		File imgFile = new  File(app.LocalImagePath);
		if(imgFile.exists()){
			Picasso.with(this).load(imgFile).resize(480, 360).centerInside().noFade().into(imgFullPhoto);
		}else{
			showMessage("Image does not exist");
			finish();
		}
	}
	
	public void approvePhoto(View view){
		app.AddBuzzPhoto = true;
		finish();
	}
	
	public void denyPhoto(View view){
		app.LocalImagePath = "";
		app.AddBuzzPhoto = false;
		finish();
	}
}
