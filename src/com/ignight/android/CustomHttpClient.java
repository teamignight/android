package com.ignight.android;

import java.io.InputStream;
import java.security.KeyStore;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

public class CustomHttpClient extends DefaultHttpClient{
	final Context context;

    public CustomHttpClient(Context context) {
        this.context = context;
    }
    
    @Override
    protected ClientConnectionManager createClientConnectionManager() {
        SchemeRegistry registry = new SchemeRegistry();

        registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));

        // Register for port 443 our SSLSocketFactory with our keystore to the ConnectionManager
        registry.register(new Scheme("https", newSslSocketFactory(), 443));
        return new SingleClientConnManager(getParams(), registry);
    }
    
    private SSLSocketFactory newSslSocketFactory() {
    	AssetManager assetManager;
    	InputStream inputStream;
    	SSLContext sslContext;
    	SSLSocketFactory sslSocketFactory;
    	KeyStore trustStore;
    	TrustManagerFactory tmf;
    	
        try {
        	assetManager = context.getAssets();

        	trustStore = KeyStore.getInstance("BKS");
        	
        	//inputStream = assetManager.open("ignight.store");
        	inputStream = assetManager.open("androidProd.bks");
            //Log.d("http", "server cert created");
            
            try {
            	trustStore.load(inputStream, "ignight1234".toCharArray());
            	
            	//tmf = TrustManagerFactory.getInstance("X509");
            	//tmf.init(trustStore);
            	
            	//Log.d("http", "server cert loaded");
            } finally {
            	if(inputStream != null)
            		inputStream.close();
            }
            
            sslSocketFactory = new SSLSocketFactory(trustStore);
            //Log.d("http", "ssl socket created");
            sslSocketFactory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            
            return sslSocketFactory;
        } catch (Exception e) {
            throw new AssertionError(e);
        }
    }
}
