package com.ignight.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ActivityRegister extends ActivityBase {
	private ActivityRegister activity;
	private BufferedReader bufferReader;
	private Gson gson;
	private String httpResult;
	private HttpClient httpClient;
	private HttpEntity httpEntity;
	private HttpPost httpPost;
	private HttpResponse httpResponse;
	private InputStream inputStream;
	private JSONObject jsonObject;
	private ProgressDialog progressDialog;
	private StringBuilder stringBuilder;
	private User user;
	
	private EditText txtEmail;
	private EditText txtUserName;
	private TextView txtTerms;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_register);
		
		app = ((AppBase)getApplication());
		app.clearApp();
		user = new User();
		
		txtEmail = (EditText)findViewById(R.id.txtEmailAddress);
		txtUserName = (EditText)findViewById(R.id.txtUsername);
		txtTerms = (TextView)findViewById(R.id.txtTerms);
		
		txtTerms.setText(Html.fromHtml("By creating an account with IgNight, you acknowledge that you have read and agree to our <a href=\"http://www.ignight.com/terms/\">Terms of Use</a> and <a href=\"http://www.ignight.com/privacy/\">Privacy Policy</a>"));
		txtTerms.setMovementMethod(LinkMovementMethod.getInstance());
		activity = this;
		//Button btnFacebook = (Button)findViewById(R.id.btnFacebook);
		//Drawable iconFacebook = getResources().getDrawable(R.drawable.icon_button_facebook);
		//iconFacebook.setBounds(0,0, (int)(iconFacebook.getIntrinsicWidth() * 0.5), (int)(iconFacebook.getIntrinsicHeight() * 0.5));
		//btnFacebook.setCompoundDrawables(iconFacebook, null, null, null);
		
		//Button btnTwitter = (Button)findViewById(R.id.btnTwitter);
		//Drawable iconTwitter = getResources().getDrawable(R.drawable.icon_button_twitter);
		//iconTwitter.setBounds(0,0, (int)(iconTwitter.getIntrinsicWidth() * 0.5), (int)(iconTwitter.getIntrinsicHeight() * 0.5));
		//btnTwitter.setCompoundDrawables(iconTwitter, null, null, null);
	}
	
	public void btnSignup_ClickEvent(View button){
		EditText txtPassword = (EditText)findViewById(R.id.txtPassword);
		
		if(txtUserName.getText().toString().equals("") || txtPassword.getText().toString().equals("") || txtEmail.getText().toString().equals("")){
			showMessage("Please complete all 3 fields.");
		} if(txtPassword.getText().length() < 6){
			showMessage("Password must be at least 6 characters.");
		} else if(!Pattern.matches("^(.+)@([^@]+[^.]).([^.])$", txtEmail.getText().toString())){
			showMessage("Please enter a valid email address.");
		}else{
			user.setUserName(txtUserName.getText().toString());
			user.setUserEmail(txtEmail.getText().toString());
			user.setUserPassword(txtPassword.getText().toString());
			new AddUserTask().execute();
		}
	}
	
	private void startAddingUser(){
		progressDialog = ProgressDialog.show(this, "", "Please wait....", true);
	}
	
	private void addUserResult(String result){
		Intent intent;
		JSONObject jsonUser;
		
		if(result.equals("success")){
			try {
				jsonUser = jsonObject.getJSONObject("body");
				user.setId(jsonUser.getInt("id"));
				app.setActiveUser(user);
				app.setPushNotificationKey();
				intent = new Intent(ActivityRegister.this, ActivityDNAUserInfo.class);
				startActivity(intent);
			} catch (Exception e) {
				showMessage("Error getting user id: " + e.getMessage());
				//progressDialog = ProgressDialog.show(this, "", jsonObject.toString(), true);
			}
		}else if(!result.equals("failed")){
			showMessage(result);
		}else{
			showMessage("This email address is already in use.");
		}
	}
	
	private class AddUserTask extends AsyncTask<Void, Void, String>{

		@Override
		protected String doInBackground(Void... args) {
			String json = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				gson = new GsonBuilder().create();
				MessageNewUser newUser = new MessageNewUser(user.getUserEmail(), user.getUserName(), user.getUserPassword());
				json = gson.toJson(newUser);
				
				StringEntity stringEntity = new StringEntity(json);
				httpClient = new CustomHttpClient(activity);
				httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_user));
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID","");
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					if(httpResult != null){
						//Log.d("register", httpResult);
						jsonObject = new JSONObject(httpResult);
						
						if(jsonObject.getBoolean("res")){
							retVal = "success";
							
							DataAdapter dataAdapter = new DataAdapter(ActivityRegister.this);
							dataAdapter.cleanDB();
						}else{
							retVal = jsonObject.getString("reason");
						}
					}else{
						retVal = "Error, no result.";
					}
				}else{
					retVal = "Error, null entity.";
				}
				
			}catch(ClientProtocolException e){
				retVal = "Client Protocal Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "IO Error, " + e.getMessage();
			
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			if(progressDialog != null && progressDialog.isShowing())
				progressDialog.dismiss();
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startAddingUser();
		}
		
		@Override
		protected void onPostExecute(String result){
			addUserResult(result);
		}
	}
}
