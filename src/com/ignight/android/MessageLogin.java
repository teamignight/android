package com.ignight.android;

public class MessageLogin {
	private class SimpleUser{
		private String userName;
		private String password;
		private String type;
		
		SimpleUser(String userName, String password){
			this.userName = userName;
			this.password = password;
			this.type = "login";
		}
	}
	private SimpleUser msg;
	
	MessageLogin(String userName, String password){
		this.msg = new SimpleUser(userName, password);
	}
}
