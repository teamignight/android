package com.ignight.android;

import com.squareup.picasso.Picasso;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import static android.widget.ImageView.ScaleType.CENTER_CROP;

public class AdapterPhotoBrowser extends SimpleCursorAdapter{
	private Cursor cursor;
	private Context context;
	private LayoutInflater inflater;
	private int layout;
	
	final Uri thumbUri = MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI;
	final String thumb_DATA = MediaStore.Images.Thumbnails.DATA;
	final String thumb_IMAGE_ID = MediaStore.Images.Thumbnails.IMAGE_ID;
	
	public AdapterPhotoBrowser(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
		super(context, layout, c, from, to, flags);
		cursor = c;
		this.context = context;
		this.layout = layout;
		inflater = LayoutInflater.from(context);
	}
	
	@Override
	public View newView(Context ctx, Cursor cursor, ViewGroup parent) {
	    View vView = inflater.inflate(layout, parent, false);
	    
	    ViewHolder holder = new ViewHolder();
	    holder.imageView = (ImageView)vView.findViewById(R.id.imgBuzzPhoto);
	    
	    vView.setTag(holder);
	    return vView;
	}

	@Override
    public void bindView(View v, Context ctx, Cursor c) {
		int iCol_ID = c.getColumnIndex(MediaStore.Images.Media._ID);
        int iCol_Image = c.getColumnIndex(MediaStore.Images.Media.DATA);
        int id = c.getInt(iCol_ID);
        String thumbPath = c.getString (iCol_Image);
        
        ViewHolder holder = (ViewHolder) v.getTag();
        
        //Log.d("Photo", "Thumb: " + thumbUri.getPath() + thumbPath);
        
        //Bitmap thumb = MediaStore.Images.Thumbnails.getThumbnail(ctx.getContentResolver(), id, MediaStore.Images.Thumbnails.MINI_KIND, null);
        holder.imageView.setScaleType(CENTER_CROP);
    	Picasso.with(context).load("file://" + thumbPath).placeholder(R.drawable.image_placeholder).resize(150, 150).centerCrop().noFade().into(holder.imageView);
        //holder.imageView.setImageBitmap ( thumb );
    }
	
	private class ViewHolder {
        ImageView imageView;
    }
}
