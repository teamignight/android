package com.ignight.android;

import com.squareup.picasso.Picasso;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

public class ActivityGroups extends ActivityBase {
	private FragmentGroupExplore fragmentGroupExplore;
	private FragmentGroupInbox fragmentGroupInbox;
	private FragmentMyGroups fragmentMyGroups;
	private FragmentTransaction fragmentTransaction;
	private ImageView imgExploreGroups;
	private ImageView imgGroupInbox;
	private ImageView imgMyGroup;
	private ProgressDialog progressDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_groups);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,R.layout.module_title_bar);
		
		wireupHeader("Groups","My Groups");
		
		showAddGroupButton();
		
		imgExploreGroups = (ImageView)findViewById(R.id.imgExploreGroups);
		imgGroupInbox = (ImageView)findViewById(R.id.imgGroupInbox);
		imgMyGroup = (ImageView)findViewById(R.id.imgMyGroup);
		
		fragmentGroupExplore = new FragmentGroupExplore();
		fragmentGroupInbox = new FragmentGroupInbox();
		fragmentMyGroups = new FragmentMyGroups();
		
		if(app.ShowInbox){
			app.ShowInbox = false;
			fragmentTransaction = getSupportFragmentManager().beginTransaction();
			fragmentTransaction.add(R.id.fragmentHolder, fragmentGroupInbox);
			fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			fragmentTransaction.commit();
		}else{
			fragmentTransaction = getSupportFragmentManager().beginTransaction();
			fragmentTransaction.add(R.id.fragmentHolder, fragmentMyGroups);
			fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			fragmentTransaction.commit();
		}
	}
	
	@Override
	public void onResume(){
		super.onResume();
		if(app.LeftGroup){
			app.LeftGroup = false;
			if(fragmentMyGroups != null && fragmentMyGroups.isVisible())
				fragmentMyGroups.refresh();
		}
	}
	
	public void loadMyGroups(View view){
		fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.fragmentHolder, fragmentMyGroups, "FragmentMyGroups");
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
		
		updateHeaderText("My Groups");
	}
	
	public void loadInbox(View view){
		fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.fragmentHolder, fragmentGroupInbox, "FragmentInbox");
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
		
		updateHeaderText("Inbox");
	}
	
	public void loadExploreGroups(View view){
		fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.fragmentHolder, fragmentGroupExplore, "FragmentExplore");
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
		
		updateHeaderText("Explore Groups");
	}
	
	public void confirmGroupInvite(View view){
		fragmentGroupInbox.confirmGroupInvite(view);
	}
	
	public void denyGroupInvite(View view){
		fragmentGroupInbox.denyGroupInvite(view);
	}

	public void showProgress(String msg){
		progressDialog = ProgressDialog.show(this, "", msg, true, true);
	}
	
	public void hideProgress(){
		if(progressDialog != null && progressDialog.isShowing())
			progressDialog.dismiss();
	}
	
	public void highlightExplore(){
		Picasso.with(this).load(R.drawable.explore_active).noFade().into(imgExploreGroups);
		Picasso.with(this).load(R.drawable.groups_inbox).noFade().into(imgGroupInbox);
		Picasso.with(this).load(R.drawable.groups_list).noFade().into(imgMyGroup);
	}
	
	public void highlightInbox(){
		Picasso.with(this).load(R.drawable.explore).noFade().into(imgExploreGroups);
		Picasso.with(this).load(R.drawable.groups_inbox_active).noFade().into(imgGroupInbox);
		Picasso.with(this).load(R.drawable.groups_list).noFade().into(imgMyGroup);
	}
	
	public void highlightMyGroups(){
		Picasso.with(this).load(R.drawable.explore).noFade().into(imgExploreGroups);
		Picasso.with(this).load(R.drawable.groups_inbox).noFade().into(imgGroupInbox);
		Picasso.with(this).load(R.drawable.groups_list_active).noFade().into(imgMyGroup);
	}
}
