package com.ignight.android;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterVenueTrendingGroups extends ArrayAdapter<Group> {
	private Context context;
	private Group group;
	private ArrayList<Group> groups;
	
	public AdapterVenueTrendingGroups(Context context, int textViewResourceId, ArrayList<Group> items){
		super(context, textViewResourceId, items);
		
		this.context = context;
		this.groups = items;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View view = convertView;
		ViewHolder holder = null;
		LayoutInflater layoutInflater;
		
		if(view == null){
			layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layoutInflater.inflate(R.layout.list_trending_group, null);
			
			holder = new ViewHolder();
			holder.txtGroupName = (TextView)view.findViewById(R.id.txtVenueTrendingGroup);
			holder.trendingGroupPublic = (ImageView)view.findViewById(R.id.trendingGroupPublic);
			holder.trendingGroupPrivate = (ImageView)view.findViewById(R.id.trendingGroupPrivate);
			view.setTag(holder);
		}else{
			holder = (ViewHolder) view.getTag();
		}
		
		group = groups.get(position);
		
		if(group != null){
			holder.txtGroupName = (TextView)view.findViewById(R.id.txtVenueTrendingGroup);
			if(holder.txtGroupName != null){holder.txtGroupName.setText(group.getGroupName());}
			if(group.getIsPrivate()){
				holder.trendingGroupPrivate.setVisibility(View.VISIBLE);
				holder.trendingGroupPublic.setVisibility(View.GONE);
			}else{
				holder.trendingGroupPrivate.setVisibility(View.GONE);
				holder.trendingGroupPublic.setVisibility(View.VISIBLE);
			}
		}
		
		return view;
	}
	
	static class ViewHolder {
		ImageView trendingGroupPublic;
		ImageView trendingGroupPrivate;
		TextView txtGroupName;
    }
}
