package com.ignight.android;

public class MessageToggleGroupBuzzNotification {
	private class NewToggle{
		private int userId;
		
		NewToggle(int userId){
			this.userId = userId;
		}
	}
	private NewToggle msg;
	MessageToggleGroupBuzzNotification(int userId){
		this.msg = new NewToggle(userId);
	}
}
