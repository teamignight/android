package com.ignight.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

public class ActivityRouteBuzz extends ActivityBase{
	private ActivityRouteBuzz activity;
	private BufferedReader bufferReader;
	private String httpResult;
	private HttpClient httpClient;
	private HttpEntity httpEntity;
	private HttpGet httpGet;
	private HttpResponse httpResponse;
	private int itemId;
	private String itemType;
	private InputStream inputStream;
	private JSONObject jsonObject;
	private ProgressDialog progressDialog;
	private StringBuilder stringBuilder;
	private User user;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		activity = this;
		Intent i = getIntent();
		
		user = CurrentUser;
		
		itemType = i.getStringExtra("itemType");
		itemId = i.getIntExtra("itemId", 0);
		
		if(itemType != null){
			if(itemType.equalsIgnoreCase("group")){
				new GetGroupTask().execute();
			}
		}else{
			Intent intent = new Intent(this, ActivityTrending.class);
			startActivity(intent);
		}
	}
	
	private void showProgress(String msg){
		progressDialog = ProgressDialog.show(this, "", msg, true, true);
	}
	
	private void hideProgress(){
		if(progressDialog != null && progressDialog.isShowing())
			progressDialog.dismiss();
	}
	
	private class GetGroupTask extends AsyncTask<Void, Void, String>{
		@Override
		protected String doInBackground(Void... args) {
			String retVal = "";
			String singleLine = "";
			
			try{httpClient = new CustomHttpClient(activity);
				httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_group_info) + itemId + "/?userId=" + user.getId());
				httpGet.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpGet.setHeader("PLATFORM","Android");
				httpGet.setHeader("USER_ID",""+user.getId());
				httpResponse = httpClient.execute(httpGet);
				
				httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();
				bufferReader = new BufferedReader(new InputStreamReader(inputStream));
				stringBuilder = new StringBuilder();
				
				while((singleLine = bufferReader.readLine()) != null){
					stringBuilder.append(singleLine);
				}
				
				httpResult = stringBuilder.toString();
				inputStream.close();
				jsonObject = new JSONObject(httpResult);
	
				if(jsonObject.getBoolean("res")){
					retVal = "success";
				}else{
					retVal = jsonObject.getString("reason");
				}
				
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			showProgress("Loading group buzz...");
		}
		
		@Override
		protected void onPostExecute(String result){
			Intent intent;
			JSONObject jsonGroup;
			
			hideProgress();
			
			if(result.equalsIgnoreCase("success")){
				app.ActiveGroup = new Group();
				app.ActiveGroup.setGroupId(itemId);
				try{
					jsonGroup = jsonObject.getJSONObject("body");
					app.ActiveGroup.setIsPrivate(!jsonGroup.getBoolean("public"));
					app.ActiveGroup.setIsMember(jsonGroup.getBoolean("isMember"));
					app.ActiveGroup.setGroupName(jsonGroup.getString("name"));
					app.ActiveGroup.setIsSubscribed(jsonGroup.getBoolean("subscribed"));
					
					app.ShowBuzz = true;
					intent = new Intent(activity, ActivityGroup.class);
				}catch(JSONException e) {
					intent = new Intent(activity, ActivityGroups.class);
				}
			}else{
				intent = new Intent(activity, ActivityGroups.class);
			}
			startActivity(intent);
		}
	}
}
