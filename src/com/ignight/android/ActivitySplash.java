package com.ignight.android;

import android.content.Intent;
import android.os.Bundle;

public class ActivitySplash extends ActivityBase {
	private Intent intent;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		process();
	}
	
	@Override
	public void onResume(){
		process();
		super.onResume();
	}
	
	private void process(){
		try{
			DataAdapter dataAdapter = new DataAdapter(this);
			//dataAdapter.deleteGroup();
			//dataAdapter.deleteUser();
			
			getUserLocation();
			dataAdapter.initDB();
			
			if(CurrentUser == null){
				dataAdapter.cleanDB();
				intent = new Intent(this, ActivityLogin.class);
			}else{
				intent = new Intent(this, ActivityTrending.class);
			}
			startActivity(intent);
		}catch(Exception ex){
			finish();
		}
	}
}
