package com.ignight.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class FragmentGroupMembers extends android.support.v4.app.Fragment {
	private int adminId;
	private AppBase app;
	private View currentView;
	private BufferedReader bufferReader;
	private View footerViewSearch;
	private Activity fragmentActivity;
	private Gson gson;
	private ActivityGroup groupActivity;
	private ArrayList<User> groupMembers;
	private HttpClient httpClient;
	private HttpGet httpGet;
	private HttpEntity httpEntity;
	private HttpPost httpPost;
	private HttpResponse httpResponse;
	private String httpResult;
	private InputStream inputStream;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private ListView listUsers;
	private boolean loadingSearchResults;
	private AdapterGroupMembers groupMemberAdapter;
	private String searchTerm;
	private StringBuilder stringBuilder;
	private TextView txtLoadingFooter;
	private EditText txtSearchMember;
	private TextView txtClearSearch;
	private User user;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View view = inflater.inflate(R.layout.fragment_group_members, container, false);
        
        listUsers = (ListView)view.findViewById(R.id.listGroupMembers);
        footerViewSearch = inflater.inflate(R.layout.list_loading_footer, null, false);
        listUsers.addFooterView(footerViewSearch);
        txtLoadingFooter = (TextView)footerViewSearch.findViewById(R.id.txtLoadingFooter);
        txtSearchMember = (EditText)view.findViewById(R.id.txtSearchMember);
        txtClearSearch = (TextView)view.findViewById(R.id.txtClearSearch);
        
        if (fragmentActivity != null) {
        	groupActivity = ((ActivityGroup)getActivity());
        	app = groupActivity.app;
	        user = groupActivity.CurrentUser;
		}
        
        if(app.ActiveGroup != null && app.DirtyGroup == app.ActiveGroup.getGroupId()){
			app.TrendingType = "trending";
			Intent intent = new Intent(fragmentActivity, ActivityTrending.class);
			startActivity(intent);
		}
        
        //Moved this logic to the adapter.
        //Keep click event for when we add user profiles
        /*listUsers.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            	selectedUser = groupMemberAdapter.getItem(position);
            	if(selectedUser.getGroupStatus() == 0){
            		new AddGroupMemberTask().execute();
            		currentView = view;
            	}else if(selectedUser.getGroupStatus() == 1){
            		groupActivity.showMessage(selectedUser.getUserName() + " already has a pending invite.");
            	}
            }
        });*/
        
        txtClearSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				txtSearchMember.setText("");
				filterMembers("");
			}
		});
        txtSearchMember.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                	filterMembers(txtSearchMember.getText().toString());
                    return true;
                }
                return false;
            }
        });
        
        new GetGroupMembersTask().execute(String.valueOf(0), String.valueOf(99));
        
        return view;
	}
	
	@Override
	public void onResume(){
		super.onResume();
	}
	
	@Override
	public void onDetach(){
		super.onDetach();
		if(txtSearchMember != null)
			txtSearchMember.setText("");
		if(txtClearSearch != null)
			txtClearSearch.setVisibility(View.INVISIBLE);
	}
	
	public void onDestroyView(){
		super.onDestroyView();
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		fragmentActivity = activity;
	}
	
	private void filterMembers(String filter){
		searchTerm = filter;
		
		if(loadingSearchResults)
			return;
		
		if(filter == null || filter.equals("") || filter.length() == 0){
			txtClearSearch.setVisibility(View.INVISIBLE);
			new GetGroupMembersTask().execute(String.valueOf(0), String.valueOf(99));
		}else{
			new SearchUsersTask().execute();
			txtClearSearch.setVisibility(View.VISIBLE);
		}
	}
	
	private void startSearchUsers(){
		loadingSearchResults = true;
		if(listUsers.getFooterViewsCount() == 0)
			listUsers.addFooterView(footerViewSearch);
		txtLoadingFooter.setText("Searching...");
	}
	
	private void searchUsersResult(String result){
		JSONObject jObject;
		User member;
		ArrayList<User> searchedMembers = new ArrayList<User>();

		if(result.equals("success")){
			try{
				groupMemberAdapter = new AdapterGroupMembers(fragmentActivity, R.layout.list_member, searchedMembers, app, groupActivity);
				listUsers.setAdapter(groupMemberAdapter);
				
				jsonArray = jsonObject.optJSONArray("body");
				if(jsonArray != null){
					for(int i = 0; i < jsonArray.length(); i++){
						jObject = jsonArray.getJSONObject(i);
						member = new User();
						member.setUserName(jObject.getString("userName"));
						member.setId(jObject.getInt("userId"));
						member.setUserPhoto(jObject.optString("chatPictureURL", app.DefaultChatUrl));
						member.setGroupStatus(jObject.getInt("groupStatus"));
						member.setGroupAdmin(adminId == member.getId());
						groupMemberAdapter.add(member);
					}
				}
				groupMemberAdapter.notifyDataSetChanged();
			} catch(JSONException e) {
				groupActivity.showMessage("Error parsing users: " + e.getMessage());
			}
		}else{
			groupActivity.showMessage("Error loading users: " + result);
		}
		if(groupMemberAdapter != null){
			if(groupMemberAdapter.getCount() == 0)
				txtLoadingFooter.setText("No results.");
			else if(groupMemberAdapter.getCount() > 0)
				txtLoadingFooter.setText("No more results.");
		}else{
			txtLoadingFooter.setText("No members found.");
		}
		
		loadingSearchResults = false;
		if(searchTerm != null && !searchTerm.equalsIgnoreCase(txtSearchMember.getText().toString()))
			filterMembers(txtSearchMember.getText().toString());
	}
	
	private class SearchUsersTask extends AsyncTask<Void, Void, String>{
		
		@Override
		protected String doInBackground(Void... args) {
			String retVal = "";
			String singleLine = "";
			
			try{
				httpClient = new CustomHttpClient(fragmentActivity);
				httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_group_search_users) + user.getId() + "/?groupId="+app.ActiveGroup.getGroupId()+"&search=" + URLEncoder.encode(searchTerm, "UTF-8"));
				httpGet.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpGet.setHeader("PLATFORM","Android");
				httpGet.setHeader("USER_ID",""+user.getId());
				httpResponse = httpClient.execute(httpGet);
				httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();
				bufferReader = new BufferedReader(new InputStreamReader(inputStream));
				stringBuilder = new StringBuilder();
				//Log.d("UserSearch", getString(R.string.api_base) + getString(R.string.api_user) + "?type=searchForUsers&userId="+user.getId()+"&cityId="+user.getCity().getId()+"&groupId="+app.ActiveGroup.getGroupId()+"&search=" + URLEncoder.encode(searchTerm, "UTF-8"));
				while((singleLine = bufferReader.readLine()) != null){
					stringBuilder.append(singleLine);
				}
				
				httpResult = stringBuilder.toString();
				inputStream.close();
				jsonObject = new JSONObject(httpResult);
				//Log.d("SearchUser", httpResult);
				if(jsonObject.getBoolean("res")){
					retVal = "success";
				}else{
					retVal = "failed";
				}
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startSearchUsers();
		}
		
		@Override
		protected void onPostExecute(String result){
			searchUsersResult(result);
		}
	}
	
	private void getGroupMembersResult(String result){
		JSONObject jObject;
		User member;

		groupActivity.hideProgress();
		groupMembers = new ArrayList<User>();
		if(result.equals("success")){
			try{
				groupMemberAdapter = new AdapterGroupMembers(fragmentActivity, R.layout.list_member, new ArrayList<User>(), app, groupActivity);
				listUsers.setAdapter(groupMemberAdapter);
				
				jsonArray = jsonObject.getJSONObject("body").getJSONArray("groupMembers");
				adminId = jsonObject.getJSONObject("body").getInt("adminId");
				if(jsonArray != null){
					for(int i = 0; i < jsonArray.length(); i++){
						jObject = jsonArray.getJSONObject(i);
						member = new User();
						member.setId(jObject.getInt("userId"));
						member.setUserName(jObject.getString("userName"));
						member.setUserPhoto(jObject.optString("chatPictureUrl",app.DefaultChatUrl));
						member.setBelongToGroup(true);
						member.setGroupStatus(2);
						member.setGroupAdmin(adminId == member.getId());
						groupMembers.add(member);
						groupMemberAdapter.add(member);
					}
				}
				groupMemberAdapter.notifyDataSetChanged();
			}catch(JSONException e){
				groupActivity.showMessage("Error parsing users: " + e.getMessage());
			}
		}else{
			groupActivity.showMessage("Error loading users");
		}
		txtLoadingFooter.setText("No more results.");
	}
	
	private void startGetGroupMembers(){
		groupActivity.showProgress("Getting members...");
	}
	
	private class GetGroupMembersTask extends AsyncTask<String, Void, String>{

		@Override
		protected String doInBackground(String... args) {
			String retVal = "";
			String singleLine = "";
			
			try{
				httpClient = new CustomHttpClient(fragmentActivity);
				httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_group_members) + app.ActiveGroup.getGroupId());
				httpGet.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpGet.setHeader("PLATFORM","Android");
				httpGet.setHeader("USER_ID",""+app.getActiveUser().getId());
				httpResponse = httpClient.execute(httpGet);
				
				httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();
				bufferReader = new BufferedReader(new InputStreamReader(inputStream));
				stringBuilder = new StringBuilder();
				
				while((singleLine = bufferReader.readLine()) != null){
					stringBuilder.append(singleLine);
				}
				
				httpResult = stringBuilder.toString();
				inputStream.close();
				jsonObject = new JSONObject(httpResult);
				//Log.d("members", httpResult);
				if(jsonObject.getBoolean("res")){
					retVal = "success";
				}else{
					retVal = "failed";
				}
				
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startGetGroupMembers();
		}
		
		@Override
		protected void onPostExecute(String result){
			getGroupMembersResult(result);
		}
    }
}
