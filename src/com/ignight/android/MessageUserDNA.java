package com.ignight.android;

import java.util.ArrayList;

public class MessageUserDNA {
	private class UserDNA{
		private int age;
		private ArrayList<Integer> atmosphere;
		private long cityId;
		private long genderId;
		private ArrayList<Integer> music;
		private int spendingLimit;
		private long userId;
		
		/*
		 * "{msg:{
		 * \"music\":[0,0,0],\"picture\":\"www.indiebychoice.com\",\"lastName\":\"Brown\",
		 * \"cityId\":0,\"age\":21,\"userId\":1,\"atmosphere\":[0,0,0],\"genderId\":0,\"venue\":[1,2,3],
		 * \"firstName\":\"Raimone\",\"type\":\"setCompleteUserInfo\",\"spendingLimit\":2
		 * }
		 * }";
		*/
		UserDNA(int age, ArrayList<Integer> atmosphere, long cityId, long genderId, 
				ArrayList<Integer> music, int spendingLimit, long userId){
			this.age = age;
			this.atmosphere = atmosphere;
			this.cityId = cityId;
			this.genderId = genderId;
			this.music = music;
			this.spendingLimit = spendingLimit;
			this.userId = userId;
		}
	}
	private UserDNA msg;
	
	MessageUserDNA(int age, ArrayList<Integer> atmosphere, long cityId, long genderId, 
			ArrayList<Integer> music, int spendingLimit, long userId){
		this.msg = new UserDNA(age, atmosphere, cityId,genderId,music, spendingLimit, userId);
	}
}
