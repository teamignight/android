package com.ignight.android;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class AdapterTutorial extends PagerAdapter{
	private Context context;
	private ArrayList<Integer> items;
	
	AdapterTutorial(Context context, ArrayList<Integer> items){
		this.context = context;
		this.items = items;
	}
	
	@Override
	public int getCount(){
		return items.size();
	}
	
	@Override
	public boolean isViewFromObject(View view, Object object){
		return view == ((ImageView) object);
	}
	
	@Override
	public Object instantiateItem(ViewGroup container, int position){
		ImageView imageView = new ImageView(context);
		Picasso.with(context).load(items.get(position)).into(imageView);
		((ViewPager) container).addView(imageView, 0);
		return imageView;
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object){
		((ViewPager) container).removeView((ImageView) object);
	}
}
