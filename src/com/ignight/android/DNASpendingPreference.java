package com.ignight.android;

public class DNASpendingPreference {
	private int Id;
	private String Name;
	private int Position;
	private int[] ResourceIds = {R.drawable.dna_spending_1, R.drawable.dna_spending_2, R.drawable.dna_spending_3, R.drawable.dna_spending_4};
	
	public DNASpendingPreference(){ }
	
	public DNASpendingPreference(int id, String name){
		this.Id = id;
		this.Name = name;
	}
	
	public int getId(){
		return this.Id;
	}
	
	public void setId(int id){
		this.Id = id;
	}
	
	public String getName(){
		return this.Name;
	}
	
	public void setName(String name){
		this.Name = name;
	}
	
	public int getPosition(){
		return this.Position;
	}
	
	public void setPosition(int position){
		this.Position = position;
	}
	
	public int getResourceId(int id){
		return ResourceIds[id];
	}
}
