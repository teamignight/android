package com.ignight.android;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class FragmentScreenSlidePage extends android.support.v4.app.Fragment {
	private ImageView imgTutorial;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_screen_slide_page, container, false);
        imgTutorial = (ImageView)rootView.findViewById(R.id.imgTutorial);
        return rootView;
    }
	
	public void setImageSrc(int srcId){
		imgTutorial.setImageResource(srcId);
	}
}
