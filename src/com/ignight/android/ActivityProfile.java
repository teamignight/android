package com.ignight.android;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

public class ActivityProfile extends ActivityBase  {
	private ActivityProfile activity;
	private AdapterAtmosphere adapterAtmosphere;
	private AdapterMusic adapterMusic;
	private AdapterSpendingPreference adapterSpending;
	private BufferedReader bufferReader;
	private DataAdapter dataAdapter;
	private LinearLayout editAge;
	private LinearLayout editCity;
	private String editType;
	private GridView grdEditAtmosphere;
	private GridView grdEditDNAMusicItems;
	private GridView grdEditDNASpendingPreference;
	private Gson gson;
	private HttpClient httpClient;
	private HttpEntity httpEntity;
	private HttpPost httpPost;
	private HttpResponse httpResponse;
	private String httpResult;
	private InputStream inputStream;
	private ImageView imgPhoto;
	private JSONObject jsonObject;
	private LinearLayout layoutAtmosphere2;
	private LinearLayout layoutAtmosphere3;
	private LinearLayout layoutMusicChoice2;
	private LinearLayout layoutMusicChoice3;
	private ToggleButton prefInvite;
	private ToggleButton prefName;
	private ProgressDialog progressDialog;
	private StringBuilder stringBuilder;
	private TextView txtAge;
	private TextView txtAtmosphere1;
	private TextView txtAtmosphere2;
	private TextView txtAtmosphere3;
	private TextView txtCity;
	private EditText txtEditEmailAddress;
	private TextView txtFullName;
	private TextView txtMusicGenre1;
	private TextView txtMusicGenre2;
	private TextView txtMusicGenre3;
	private TextView txtProfileEmail;
	private TextView txtSpendingPreference;
	private ImageView imgSpendingPreference;
	private User user;
	private final int[] overlays = { R.drawable.selected_one, R.drawable.selected_two, R.drawable.selected_three };
	private DNAMusic lastMusicOption;
	private DNAAtmosphere lastAtmosphereOption;
	
	private BroadcastReceiver mReceiver;
	private boolean receiverRegistered;
	private ImageView editIconMusic, editIconAtmosphere, editIconSpending, editIconAge, editIconEmail;
	private boolean editing;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_profile);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,R.layout.module_title_bar);
		
		user = CurrentUser;
		
		if (savedInstanceState != null && app.UploadingPhoto)
        {
			app.UploadPhotoInit = true;
			progressDialog = ProgressDialog.show(this, "", "Updating photo....", true, true);
        }
		
		IntentFilter intentFilter = new IntentFilter("com.ignight.android.updateprofilephoto");
		mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
            	handleReceiver();
            }
		};
		this.registerReceiver(mReceiver, intentFilter);
		receiverRegistered = true;

		wireupHeader("Settings", "");
		activity = this;
		app.TrendingFilterShowing = false;
		txtAge = (TextView)findViewById(R.id.txtAge);
		txtAtmosphere1 = (TextView)findViewById(R.id.txtAtmosphere1);
		txtAtmosphere2 = (TextView)findViewById(R.id.txtAtmosphere2);
		txtAtmosphere3 = (TextView)findViewById(R.id.txtAtmosphere3);
		txtCity = (TextView)findViewById(R.id.txtCity);
		txtFullName = (TextView)findViewById(R.id.txtFullName);
		txtMusicGenre1 = (TextView)findViewById(R.id.txtMusicGenre1);
		txtMusicGenre2 = (TextView)findViewById(R.id.txtMusicGenre2);
		txtMusicGenre3 = (TextView)findViewById(R.id.txtMusicGenre3);
		layoutAtmosphere2 = (LinearLayout)findViewById(R.id.layoutAtmosphere2);
		layoutAtmosphere3 = (LinearLayout)findViewById(R.id.layoutAtmosphere3);
		layoutMusicChoice2 = (LinearLayout)findViewById(R.id.layoutMusicChoice2);
		layoutMusicChoice3 = (LinearLayout)findViewById(R.id.layoutMusicChoice3);
		txtProfileEmail = (TextView)findViewById(R.id.txtProfileEmail);
		txtSpendingPreference = (TextView)findViewById(R.id.txtSpendingPreference);
		prefInvite = (ToggleButton)findViewById(R.id.toggleInvite);
		prefName = (ToggleButton)findViewById(R.id.toggleName);
		imgPhoto = (ImageView)findViewById(R.id.imgPhoto);
		
		txtEditEmailAddress = (EditText)findViewById(R.id.txtEditEmailAddress);
		editAge = (LinearLayout)findViewById(R.id.editAge);
		editCity = (LinearLayout)findViewById(R.id.editCity);
		grdEditDNAMusicItems = (GridView)findViewById(R.id.grdEditDNAMusicItems);
		grdEditAtmosphere = (GridView)findViewById(R.id.grdEditAtmosphere);
		grdEditDNASpendingPreference = (GridView)findViewById(R.id.grdEditDNASpendingPreference);
		editIconMusic = (ImageView)findViewById(R.id.editIconMusic);
		editIconAtmosphere = (ImageView)findViewById(R.id.editIconAtomosphere);
		editIconSpending = (ImageView)findViewById(R.id.editIconSpending);
		editIconAge = (ImageView)findViewById(R.id.editIconAge);
		editIconEmail = (ImageView)findViewById(R.id.editIconEmail);

		if(!user.getUserPhoto().isEmpty() && !user.getUserPhoto().equalsIgnoreCase("default")){
			Picasso.with(this).load(user.getUserPhoto()).transform(new CircleTransform()).into(imgPhoto);
		}else{
			Picasso.with(this).load(R.drawable.avatar_default).transform(new CircleTransform()).into(imgPhoto);
		}
		
		int temp = user.getAge();
		txtAge.setText(DNAOptions.AgeOptions().get(temp).getName());
		
		populateAtmosphereOptions();
		
		txtFullName.setText(user.getUserName());
		txtCity.setText(user.getCity().getName());
		
		populateMusicOptions();
		
		txtProfileEmail.setText(user.getUserEmail());
		//txtEditEmailAddress.setText(user.getUserEmail());
		txtEditEmailAddress.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus && !txtEditEmailAddress.getText().toString().isEmpty() && !txtEditEmailAddress.getText().toString().equalsIgnoreCase(user.getUserEmail()))
                	updateEmail();
            }
        });
		txtSpendingPreference.setText(DNAOptions.SpendingPreferenceOptions().get(user.getSpendingPreference()).getName());
		imgSpendingPreference = (ImageView)findViewById(R.id.imgSpendingPreference);
		Picasso.with(this).load(DNAOptions.SpendingPreferenceOptions().get(user.getSpendingPreference()).getResourceId(DNAOptions.SpendingPreferenceOptions().get(user.getSpendingPreference()).getId())).fit().into(imgSpendingPreference);
		//imgSpendingPreference.setImageResource(DNAOptions.SpendingPreferenceOptions().get(user.getSpendingPreference()).getResourceId(DNAOptions.SpendingPreferenceOptions().get(user.getSpendingPreference()).getId()));

		int buttonCounter = 0;
		final int ageChildCount = editAge.getChildCount();
		for(int i = 0; i < ageChildCount; i++){
			final View child = editAge.getChildAt(i);
			final int viewChildCount = ((ViewGroup) child).getChildCount();
			for(int j = 0; j < viewChildCount; j++){
				final View viewGroupChild = ((ViewGroup) child).getChildAt(j);
				if(viewGroupChild instanceof Button){
					if(buttonCounter == user.getAge()){
						viewGroupChild.setSelected(true);
						buttonCounter++;
						break;
					}
					buttonCounter++;
				}
			}
		}
		
		buttonCounter = 0;
		final int cityChildCount = editCity.getChildCount();
		for(int i = 0; i < cityChildCount; i++){
			final View cityChild = editCity.getChildAt(i);
			final int cityChildChildCount = ((ViewGroup) cityChild).getChildCount();
			for(int j = 0; j < cityChildChildCount; j++){
				final View cityChildView = ((ViewGroup) cityChild).getChildAt(j);
				if(cityChildView instanceof Button){
					if(buttonCounter == user.getCity().getId()){
						cityChildView.setSelected(true);
						buttonCounter++;
						break;
					}buttonCounter++;
				}
			}
		}

		prefInvite.setChecked(user.getUserPreference().getInvites() == 1);
		prefName.setChecked(user.getUserPreference().getDisplayName() == 1);
	}
	
	@Override
    public void onBackPressed() {
        super.onBackPressed();
    }
	
	@Override
	public void onResume(){
		super.onResume();
		if(app.AddBuzzPhoto){
			app.AddBuzzPhoto = false;
			Bitmap bitmap;
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.RGB_565;
			File picFile = new File(app.LocalImagePath);
			
			if(picFile.exists()){
				bitmap = BitmapFactory.decodeFile(picFile.getAbsolutePath(), options);
				filePath = app.LocalImagePath;
				takenPictureData = bitmap;
				new UpdatePhotoTask().execute();
			}
		}
	}
	
	/*
	 * profile photo edit
	 */
	public void getProfilePhoto(View view){
		AlertDialog.Builder builder = new AlertDialog.Builder(ActivityProfile.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                	getPictureFromCamera();
                } else if (items[item].equals("Choose from Library")) {
                	getPictureFromGallery();
                } else{
                	dialog.dismiss();
                }
            }
        });
        builder.show();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    
	    takenPictureData = null;

	    switch(requestCode){
	    case PICTURE_TAKEN_FROM_GALLERY:                
            if(resultCode ==  Activity.RESULT_OK) {
            	takenPictureData = handleResultFromChooser(data);
            }
            break;
	    case PICTURE_TAKEN_FROM_CAMERA:
	    	if(resultCode==Activity.RESULT_OK) {
	    		progressDialog = ProgressDialog.show(this, "", "Updating photo....", true, true);
	    		app.UploadingPhoto = true;
                takenPictureData = handleResultFromCamera(data);
            }       
	    	break;
	    }
	    
	    if(takenPictureData!=null){
	    	new UpdatePhotoTask().execute();
	    }
	}
	
	@Override
	protected void onStop(){
		super.onStop();
		if(progressDialog != null && progressDialog.isShowing())
			progressDialog.dismiss();
		
		if(receiverRegistered){
			try{
				this.unregisterReceiver(this.mReceiver);
			}catch(Exception ex){
				
			}
		}
	}
	
	protected void updatePhotoResult(){
		new UpdatePhotoTask().execute();
	}
	
	private void handleReceiver(){
		app.UploadingPhoto = app.UploadPhotoInit = false;
    	Picasso.with(this).load(user.getUserPhoto()).fit().transform(new CircleTransform()).into(((ImageView)findViewById(R.id.imgPhoto)));
		if(progressDialog != null && progressDialog.isShowing())
			progressDialog.dismiss();
	}
	
	private void startUpdatePhoto(){
		if(!app.UploadingPhoto)
			progressDialog = ProgressDialog.show(this, "", "Updating photo....", true, true);
	}
	
	private void updatePhotoResult(String result){
		if(result.equals("success")){
			//dataAdapter.updateUserProfilePic("");
			dataAdapter = new DataAdapter(this);
			try {
				String photo = jsonObject.getString("body");
				if(dataAdapter.updateUserProfilePic(photo) == 1){
					user.setUserPhoto(photo);
					//imgPhoto.setImageBitmap(takenPictureData);
					if(!app.UploadingPhoto || (app.UploadingPhoto && !app.UploadPhotoInit))
					{
						Picasso.with(this).load(photo).fit().transform(new CircleTransform()).into(imgPhoto);
						app.UploadingPhoto = false;
					}else{
						Intent i = new Intent("com.ignight.android.updateprofilephoto");
				        sendBroadcast(i);
					}
				}else{
					showMessage("Error, could not save image.");
				}
			} catch (JSONException e) {
				showMessage("Error, could not save pic: " + e.getMessage());
			}
		}else{
			showMessage("Error, could not update photo: " + result);
		}
	}
	
	private class UpdatePhotoTask  extends AsyncTask<Void, Void, String>{
		@Override
		protected String doInBackground(Void... args) {
			String json = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				gson = new GsonBuilder().create();
				MessageProfilePhoto profilePhoto = new MessageProfilePhoto(user.getId());
				json = gson.toJson(profilePhoto);
				
				httpClient = new CustomHttpClient(activity);
				httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_user_profile_image) + app.getActiveUser().getId());
				httpPost.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID",""+app.getActiveUser().getId());
				
				MultipartEntityBuilder meBuilder = MultipartEntityBuilder.create();
				meBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
				
				String compressImagePath = filePath;
				
				//get compress bitmap
				Bitmap b = getCompressBitmap(filePath);
				
				//fix rotation
	            ExifInterface exif = new ExifInterface(filePath);
		        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

		        int angle = 0;

		        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
		            angle = 90;
		        } 
		        else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
		            angle = 180;
		        } 
		        else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
		            angle = 270;
		        }

		        Matrix mat = new Matrix();
		        mat.postRotate(angle);
		        
		        BitmapFactory.Options options = new BitmapFactory.Options();
		        options.inSampleSize = 2;
		        
		        Bitmap bmp = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), mat, true);
				
				//save file to temp
	            File f = new File(ActivityProfile.this.getCacheDir().getAbsolutePath(), new java.util.Date().getTime() + ".jpg");
	            
	            compressImagePath = f.getAbsolutePath();
	            
	            //Log.d("File", "location: " + compressImagePath);
	            FileOutputStream fos = null;
	            try {
	                fos = new FileOutputStream(f);
	                bmp.compress(Bitmap.CompressFormat.JPEG, 80, fos);
	                fos.flush();
	                fos.close();
	            } catch (FileNotFoundException e) {
	                e.printStackTrace();
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	            b.recycle();
				
				File file = new File(compressImagePath);
				FileBody fb = new FileBody(file);
				
				meBuilder.addPart("userImage", fb);
				meBuilder.addTextBody("json_body",json);
				httpPost.setEntity(meBuilder.build());
				httpPost.setHeader("Accept","application/json");
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					
					if(httpResult != null){
						jsonObject = new JSONObject(httpResult);
						
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							retVal = "Error, posting photo.";
						}
						//Log.d("UpdateProfilePic", httpResult);
					}else{
						retVal = "Error, no result.";
					}
				}
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			if(progressDialog != null && progressDialog.isShowing())
				progressDialog.dismiss();
			return retVal;
		}
		@Override
		protected void onPreExecute(){
			startUpdatePhoto();
		}
		
		@Override
		protected void onPostExecute(String result){
			updatePhotoResult(result);
		}
	}
	/*
	 * end profile photo edit
	 */
	
	public void toggleEditEmail(View view){
		if(txtEditEmailAddress.getVisibility() == View.VISIBLE){
			collapse(txtEditEmailAddress);
			Picasso.with(activity).load(R.drawable.edit_icon).noFade().into(editIconEmail);
		}else{
			expand(txtEditEmailAddress);
			txtEditEmailAddress.requestFocus();
			Picasso.with(activity).load(R.drawable.edit_icon_active).noFade().into(editIconEmail);
		}
	}
	
	public void toggleEditAge(View view){
		if(editAge.getVisibility() == View.VISIBLE){
			collapse(editAge);
			Picasso.with(activity).load(R.drawable.edit_icon).noFade().into(editIconAge);
		}else{
			expand(editAge);
			Picasso.with(activity).load(R.drawable.edit_icon_active).noFade().into(editIconAge);
		}
	}
	
	public void toggleEditCity(View view){
		if(editCity.getVisibility() == View.VISIBLE){
			collapse(editCity);
		}else{
			expand(editCity);
		}
	}
	
	public void toggleMusic(View view){
		if(grdEditDNAMusicItems.getVisibility() == View.VISIBLE){
			grdEditDNAMusicItems.setVisibility(View.GONE);
			grdEditDNAMusicItems.setAdapter(null);
			adapterMusic = null;
			if(user.getMusicOptions().size() == 0){
				user.getMusicOptions().add(lastMusicOption);
				populateMusicOptions();
			}
			Picasso.with(activity).load(R.drawable.edit_icon).noFade().into(editIconMusic);
		}else{
			lastMusicOption = null;
			adapterMusic = new AdapterMusic(this, DNAOptions.MusicOptions(), user);
			grdEditDNAMusicItems.setAdapter(adapterMusic);
			grdEditDNAMusicItems.setOnItemClickListener(new OnItemClickListener(){
				@Override
				public void onItemClick(AdapterView<?> parent, View v, int position, long id){
					DNAMusic item = DNAOptions.MusicOptions().get(position);
					ImageView numberView = (ImageView)v.findViewById(R.id.gridNumber);
					int pos = Integer.parseInt(numberView.getTag().toString());
					int size = user.getMusicOptions().size();
					
					//Log.d("Music", "size: " + size + ", pos: " + pos);
					
					if(pos != -1 && ((pos + 1) == size)){
						if(size == 1)
							lastMusicOption = item;
						
						numberView.setTag(-1);
						numberView.setImageDrawable(null);
						
						if(size > 1){
							user.getMusicOptions().remove(pos);
							editType = "updateMusic";
							new UpdateUserTask().execute();
						}
						return;
					}else if(pos != -1 && ((pos + 1) < size)){
						return;
					}
					
					if(size == 3){
						return;
					}
					
					if(size == 1 && user.getMusicOptions().get(0).getId() == item.getId()){
						numberView.setTag(0);
						numberView.setImageResource(overlays[0]);
						lastMusicOption = null;
					}else{
						if(size == 1 && lastMusicOption != null)
							user.getMusicOptions().remove(0);
						lastMusicOption = null;
						
						user.getMusicOptions().add(item);
						editType = "updateMusic";
						pos = user.getMusicOptions().size() - 1;
						numberView.setTag(pos);
						numberView.setImageResource(overlays[pos]);
						new UpdateUserTask().execute();
					}
				}
			});
			app.setGridViewHeightBasedOnChildren(grdEditDNAMusicItems);
			grdEditDNAMusicItems.setVisibility(View.VISIBLE);
			Picasso.with(activity).load(R.drawable.edit_icon_active).noFade().into(editIconMusic);
		}
	}
	
	public void toggleAtmosphere(View view){
		if(grdEditAtmosphere.getVisibility() == View.VISIBLE){
			grdEditAtmosphere.setVisibility(View.GONE);
			grdEditAtmosphere.setAdapter(null);
			adapterAtmosphere = null;
			if(user.getAtmosphereOptions().size() == 0){
				user.getAtmosphereOptions().add(lastAtmosphereOption);
				populateAtmosphereOptions();
			}
			Picasso.with(activity).load(R.drawable.edit_icon).noFade().into(editIconAtmosphere);
		}else{
			lastAtmosphereOption = null;
			adapterAtmosphere = new AdapterAtmosphere(this, DNAOptions.AtmosphereOptions(), user);
			grdEditAtmosphere.setAdapter(adapterAtmosphere);
			grdEditAtmosphere.setOnItemClickListener(new OnItemClickListener(){
				@Override
				public void onItemClick(AdapterView<?> parent, View v, int position, long id){
					DNAAtmosphere item = DNAOptions.AtmosphereOptions().get(position);
					ImageView numberView = (ImageView)v.findViewById(R.id.gridAtmosphereNumber);
					int pos = Integer.parseInt(numberView.getTag().toString());
					int size = user.getAtmosphereOptions().size();

					if(pos != -1 && ((pos + 1) == size)){
						if(user.getAtmosphereOptions().size() == 1)
							lastAtmosphereOption = item;
						
						numberView.setTag(-1);
						numberView.setImageDrawable(null);
						
						if(size > 1){
							user.getAtmosphereOptions().remove(pos);
							editType = "updateAtmospheres";
							new UpdateUserTask().execute();
						}
						return;
					}else if(pos != -1 && ((pos + 1) < size)){
						return;
					}
					
					if(size == 3){
						return;
					}
					
					if(size == 1 && user.getAtmosphereOptions().get(0).getId() == item.getId()){
						numberView.setTag(0);
						numberView.setImageResource(overlays[0]);
						lastAtmosphereOption = null;
					}else{
						if(size == 1 && lastAtmosphereOption != null)
							user.getAtmosphereOptions().remove(0);
						lastAtmosphereOption = null;
						user.getAtmosphereOptions().add(item);
						editType = "updateAtmospheres";
						pos = user.getAtmosphereOptions().size() - 1;
						numberView.setTag(pos);
						numberView.setImageResource(overlays[pos]);
						new UpdateUserTask().execute();
					}
				}
			});
			app.setGridViewHeightBasedOnChildren(grdEditAtmosphere);
			grdEditAtmosphere.setVisibility(View.VISIBLE);
			Picasso.with(activity).load(R.drawable.edit_icon_active).noFade().into(editIconAtmosphere);
		}
	}
	
	public void toggleSpending(View view){
		if(grdEditDNASpendingPreference.getVisibility() == View.VISIBLE){
			grdEditDNASpendingPreference.setVisibility(View.GONE);
			grdEditDNASpendingPreference.setAdapter(null);
			adapterSpending = null;
			Picasso.with(activity).load(R.drawable.edit_icon).noFade().into(editIconSpending);
		}else{
			adapterSpending = new AdapterSpendingPreference(this, DNAOptions.SpendingPreferenceOptions(), user);
			grdEditDNASpendingPreference.setAdapter(adapterSpending);
			grdEditDNASpendingPreference.setOnItemClickListener(new OnItemClickListener(){
				@Override
				public void onItemClick(AdapterView<?> parent, View v, int position, long id){
					View childView;
					ImageView imgView;
					ImageView numberView = (ImageView)v.findViewById(R.id.gridSpendingNumber);
					int pos = Integer.parseInt(numberView.getTag().toString());
					
					if(numberView.getTag() != null && pos > 0){
						return;
					}
					for(int i = 0; i < grdEditDNASpendingPreference.getChildCount(); i++){
						childView = grdEditDNASpendingPreference.getChildAt(i);
						imgView = (ImageView)childView.findViewById(R.id.gridSpendingNumber);
						
						if(imgView.getTag() != null && Integer.parseInt(imgView.getTag().toString()) > -1){
							imgView.setTag(-1);
							imgView.setImageDrawable(null);
							break;
						}
					}
					user.setSpendingPreference(position);
					numberView.setTag(position);
					numberView.setImageResource(R.drawable.dna_selected);
					editType = "updateSpendingLimit";
					new UpdateUserTask().execute();
				}
			});
			app.setGridViewHeightBasedOnChildren(grdEditDNASpendingPreference);
			grdEditDNASpendingPreference.setVisibility(View.VISIBLE);
			Picasso.with(activity).load(R.drawable.edit_icon_active).noFade().into(editIconSpending);
		}
	}
	
	public void togglePrefInvites(View view){
		boolean on = ((ToggleButton) view).isChecked();
		editType = "updateUserGroupPreferences";
		
		if(on){
			user.getUserPreference().setInvites(1);
		}else{
			user.getUserPreference().setInvites(0);
		}
		new UpdateUserTask().execute();
	}
	
	public void togglePrefName(View view){
		boolean on = ((ToggleButton) view).isChecked();
		editType = "updateUserGroupPreferences";
		
		if(on){
			user.getUserPreference().setDisplayName(1);
		}else{
			user.getUserPreference().setDisplayName(0);
		}
		new UpdateUserTask().execute();
	}
	
	public void btnButton_ClickEvent(View button){
		boolean activeTag;
		int pos = 0;
		String tag = button.getTag().toString();
		String text = ((Button)button).getText().toString();
		int val = -1;
		
		if(button.isSelected()){
			return;
		}
		button.setSelected(true);
		
		if(button.isSelected()){
			ViewGroup parent = (ViewGroup)button.getParent().getParent();
			final int childCount = parent.getChildCount();
			for(int i = 0; i < childCount; i++){
				final View child = parent.getChildAt(i);
				if(child instanceof Button){
					activeTag = child.getTag().toString().equals(tag);
					if(activeTag){
						if(child.getId() != button.getId()){
							child.setSelected(false);
							pos++;
						}else{
							val = pos;
						}
					}
				}else if(child instanceof ViewGroup){
					final ViewGroup childViewGroup = (ViewGroup)child;
					final int viewGroupChildCount = childViewGroup.getChildCount();
					for(int j = 0; j < viewGroupChildCount; j++){
						final View viewGroupChild = childViewGroup.getChildAt(j);
						if(viewGroupChild instanceof Button){
							activeTag = viewGroupChild.getTag().toString().equals(tag);
							if(activeTag){
								if(viewGroupChild.getId() != button.getId()){
									viewGroupChild.setSelected(false);
									pos++;
								}else{
									val = pos;
								}
							}
						}
					}
				}
			}
		}
		
		if(tag.equals("city")){
			City city = new City(val, text);
			user.setCity(city);
			editType = "updateCity";
		}else if(tag.equals("age")){
			editType = "updateAge";
			user.setAge(val);
		}else if(tag.equals("gender")){
			user.setGender(text);
		}
		
		new UpdateUserTask().execute();
	}
	
	private void populateMusicOptions(){
		if(user.getMusicOptions().size() == 0)
			return;
		txtMusicGenre1.setText(user.getMusicOptions().get(0).getName());
		ImageView imgMusicGenre1 = (ImageView)findViewById(R.id.imgMusicGenre1);
		Picasso.with(this).load(user.getMusicOptions().get(0).getResourceId(user.getMusicOptions().get(0).getId())).fit().into(imgMusicGenre1);
		//imgMusicGenre1.setImageResource(user.getMusicOptions().get(0).getResourceId(user.getMusicOptions().get(0).getId()));
		if(user.getMusicOptions().size() > 1){
			layoutMusicChoice2.setVisibility(View.VISIBLE);
			txtMusicGenre2.setText(user.getMusicOptions().get(1).getName());
			ImageView imgMusicGenre2 = (ImageView)findViewById(R.id.imgMusicGenre2);
			Picasso.with(this).load(user.getMusicOptions().get(1).getResourceId(user.getMusicOptions().get(1).getId())).fit().into(imgMusicGenre2);
			//imgMusicGenre2.setImageResource(user.getMusicOptions().get(1).getResourceId(user.getMusicOptions().get(1).getId()));
		}else{
			layoutMusicChoice2.setVisibility(View.INVISIBLE);
			txtMusicGenre2.setText("");
		}
		if(user.getMusicOptions().size() > 2){
			layoutMusicChoice3.setVisibility(View.VISIBLE);
			txtMusicGenre3.setText(user.getMusicOptions().get(2).getName());
			ImageView imgMusicGenre3 = (ImageView)findViewById(R.id.imgMusicGenre3);
			Picasso.with(this).load(user.getMusicOptions().get(2).getResourceId(user.getMusicOptions().get(2).getId())).fit().into(imgMusicGenre3);
			//imgMusicGenre3.setImageResource(user.getMusicOptions().get(2).getResourceId(user.getMusicOptions().get(2).getId()));
		}else{
			layoutMusicChoice3.setVisibility(View.INVISIBLE);
			txtMusicGenre3.setText("");
		}
	}
	
	private void populateAtmosphereOptions(){
		if(user.getAtmosphereOptions().size() == 0)
			return;
		txtAtmosphere1.setText(user.getAtmosphereOptions().get(0).getName());
		ImageView imgAtmosphere1 = (ImageView)findViewById(R.id.imgAtmosphere1);
		Picasso.with(this).load(user.getAtmosphereOptions().get(0).getResourceId(user.getAtmosphereOptions().get(0).getId())).fit().into(imgAtmosphere1);
		//imgAtmosphere1.setImageResource(user.getAtmosphereOptions().get(0).getResourceId(user.getAtmosphereOptions().get(0).getId()));
		if(user.getAtmosphereOptions().size() > 1){
			layoutAtmosphere2.setVisibility(View.VISIBLE);
			txtAtmosphere2.setText(user.getAtmosphereOptions().get(1).getName());
			ImageView imgAtmosphere2 = (ImageView)findViewById(R.id.imgAtmosphere2);
			Picasso.with(this).load(user.getAtmosphereOptions().get(1).getResourceId(user.getAtmosphereOptions().get(1).getId())).fit().into(imgAtmosphere2);
			//imgAtmosphere2.setImageResource(user.getAtmosphereOptions().get(1).getResourceId(user.getAtmosphereOptions().get(1).getId()));
		}else{
			layoutAtmosphere2.setVisibility(View.INVISIBLE);
			txtAtmosphere2.setText("");
		}
		if(user.getAtmosphereOptions().size() > 2){
			layoutAtmosphere3.setVisibility(View.VISIBLE);
			txtAtmosphere3.setText(user.getAtmosphereOptions().get(2).getName());
			ImageView imgAtmosphere3 = (ImageView)findViewById(R.id.imgAtmosphere3);
			Picasso.with(this).load(user.getAtmosphereOptions().get(2).getResourceId(user.getAtmosphereOptions().get(2).getId())).fit().into(imgAtmosphere3);
			//imgAtmosphere3.setImageResource(user.getAtmosphereOptions().get(2).getResourceId(user.getAtmosphereOptions().get(2).getId()));
		}else{
			layoutAtmosphere3.setVisibility(View.INVISIBLE);
			txtAtmosphere3.setText("");
		}
	}
	
	private void updateEmail(){
		String newEmail = txtEditEmailAddress.getText().toString();
		
		if(!newEmail.isEmpty() && !newEmail.equals(user.getUserEmail())){
			if(!Pattern.matches("^(.+)@([^@]+[^.]).([^.])$", newEmail)){
				showMessage("Please enter a valid email address.");
			}else{
				editType = "updateEmail";
				user.setUserEmail(txtEditEmailAddress.getText().toString());
				new UpdateUserTask().execute();
			}
		}
	}
	
	private void startUpdateUser(){
		progressDialog = ProgressDialog.show(this, "", "Updating profile....", true);
	}
	
	private void updateUserResult(String result){
		editing = false;
		if(result.equals("success")){
			if(editType.equals("updateEmail")){
				txtProfileEmail.setText(user.getUserEmail());
				
				if(txtEditEmailAddress.getVisibility() == View.VISIBLE){
					collapse(txtEditEmailAddress);
				}
			}else if(editType.equals("updateAge")){
				int temp = user.getAge();
				txtAge.setText(DNAOptions.AgeOptions().get(temp).getName());
				if(editAge.getVisibility() == View.VISIBLE){
					collapse(editAge);
				}
			}else if(editType.equals("updateMusic")){
				populateMusicOptions();
			}else if(editType.equals("updateAtmospheres")){
				populateAtmosphereOptions();
			}else if(editType.equals("updateSpendingLimit")){
				txtSpendingPreference.setText(DNAOptions.SpendingPreferenceOptions().get(user.getSpendingPreference()).getName());
				//imgSpendingPreference.setImageResource(DNAOptions.SpendingPreferenceOptions().get(user.getSpendingPreference()).getResourceId(DNAOptions.SpendingPreferenceOptions().get(user.getSpendingPreference()).getId()));
				Picasso.with(this).load(DNAOptions.SpendingPreferenceOptions().get(user.getSpendingPreference()).getResourceId(DNAOptions.SpendingPreferenceOptions().get(user.getSpendingPreference()).getId())).fit().into(imgSpendingPreference);
			}else if(editType.equals("updateCity")){
				app.getVenues(user.getCity().getId());
				TextView sideBarCity = (TextView)findViewById(R.id.sideBarCity);
				if(sideBarCity != null){
					sideBarCity.setText(user.getCity().getName());
				}
			}
			dataAdapter = new DataAdapter(this);
			dataAdapter.UpdateUser(user);
		}else{
			showMessage("Error, could not update member: " + result);
			if(editType.equals("updateEmail")){
				user.setUserEmail(txtProfileEmail.getText().toString());
			}else if(editType.equals("updateAge:")){
				for(int i = 0; i < DNAOptions.AgeOptions().size(); i++){
					if(DNAOptions.AgeOptions().get(i).getName().equalsIgnoreCase(txtAge.getText().toString())){
						user.setAge(DNAOptions.AgeOptions().get(i).getId());
					}
				}
			}else if(editType.equals("updateMusic")){
				
			}else if(editType.equals("updateAtmospheres")){
				
			}else if(editType.equals("updateSpendingLimit")){
				
			}
		}
	}
	
	private class UpdateUserTask extends AsyncTask<Void, Void, String>{
		@Override
		protected String doInBackground(Void... args) {
			ArrayList<Integer> atmosphere;
			ArrayList<Integer> music;
			String json = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				atmosphere = new ArrayList<Integer>();
				for(int i = 0; i < user.getAtmosphereOptions().size(); i++){
					atmosphere.add(user.getAtmosphereOptions().get(i).getId());
				}
				music = new ArrayList<Integer>();
				for(int i = 0; i < user.getMusicOptions().size(); i++){
					music.add(user.getMusicOptions().get(i).getId());
				}
				gson = new GsonBuilder().create();
				MessageEditUser editUser = new MessageEditUser(user.getAge(), atmosphere, user.getCity().getId(), 
						user.getUserPreference().getDisplayName() == 1, user.getUserEmail(), user.getGender().equalsIgnoreCase("male") ? 1 : 0, 
								music, user.getUserPreference().getInvites() == 1, user.getSpendingPreference(), user.getId(), editType);
				json = gson.toJson(editUser);
				StringEntity stringEntity = new StringEntity(json);
				httpClient = new CustomHttpClient(activity);
				httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_user) + "/" + app.getActiveUser().getId());
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID",""+app.getActiveUser().getId());
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					
					if(httpResult != null){
						jsonObject = new JSONObject(httpResult);
						
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							retVal = "Error, could not update member.";
						}
					}else{
						retVal = "Error, no result.";
					}
				}
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			progressDialog.dismiss();
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startUpdateUser();
		}
		
		@Override
		protected void onPostExecute(String result){
			updateUserResult(result);
		}
	}
}
