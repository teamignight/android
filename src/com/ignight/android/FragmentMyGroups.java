package com.ignight.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class FragmentMyGroups extends android.support.v4.app.Fragment{
	private AdapterMyGroups adapterMyGroups;
	private AppBase app;
	private BufferedReader bufferReader;
	private ArrayList<Group> filteredGroups;
	private View footerView;
	private Activity fragmentActivity;
	private Group group;
	private ArrayList<Group> groups;
	private ActivityGroups groupsActivity;
	private View headerView;
	private HttpClient httpClient;
	private HttpGet httpGet;
	private HttpEntity httpEntity;
	private HttpResponse httpResponse;
	private String httpResult;
	private InputStream inputStream;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private ListView listMyGroups;
	private BroadcastReceiver newBuzzReceiver;
	private boolean searching;
	private StringBuilder stringBuilder;
	private SwipeRefreshLayout swipeContainer;
	private TextView txtAddGroupHeader;
	private TextView txtClearSearch;
	private EditText txtSearch;
	private TextView txtSearchText;
	private View view;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_groups, container, false);
        
        footerView = inflater.inflate(R.layout.list_loading_footer, null, false);
        headerView = inflater.inflate(R.layout.list_groups_header, null, false);
        
        txtAddGroupHeader = (TextView)headerView.findViewById(R.id.txtAddGroupHeader);
        txtClearSearch = (TextView)headerView.findViewById(R.id.txtClearGroupsSearch);
        txtSearch = (EditText)headerView.findViewById(R.id.txtGroupsSearch);
        txtSearchText = (TextView)footerView.findViewById(R.id.txtLoadingFooter);
        listMyGroups = (ListView)view.findViewById(R.id.myGroups);
        
        if (fragmentActivity != null) {
        	app = ((AppBase)fragmentActivity.getApplication());
        	app.RefreshGroupsBuzz = false;
        }
        
        groupsActivity = ((ActivityGroups)getActivity());
        searching = false;
        
        if(txtAddGroupHeader != null){
	        txtAddGroupHeader.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(groupsActivity, ActivityAddGroup.class);
					startActivity(intent);
				}
			});
        }
        
        swipeContainer = (SwipeRefreshLayout)view.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new OnRefreshListener() {
			@Override
			public void onRefresh() {
				txtSearch.setText("");
			}
		});
        swipeContainer.setColorScheme(android.R.color.holo_blue_dark, android.R.color.holo_green_dark, android.R.color.holo_orange_dark, android.R.color.holo_red_dark);
        txtClearSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				txtSearch.setText("");
			}
		});
        txtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                	filterGroups(txtSearch.getText().toString());
                    return true;
                }
                return false;
            }
        });
        
        listMyGroups.addFooterView(footerView);
        listMyGroups.addHeaderView(headerView);
        listMyGroups.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if(position > 0)
					position = position - 1;
				
				app.ActiveGroup = adapterMyGroups.getItem(position);
				Intent intent = new Intent(fragmentActivity, ActivityGroup.class);
				startActivity(intent);
			}
		});
        
        IntentFilter intentFilter = new IntentFilter("com.ignight.android.updateBuzz");
    	newBuzzReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
            	Bundle extras = intent.getExtras();
            	if(extras != null && !extras.isEmpty()){
            		new GetMyGroupsTask().execute();
            	}
            }
		};
		fragmentActivity.registerReceiver(newBuzzReceiver, intentFilter);
        
        groupsActivity.highlightMyGroups();
        
        new GetMyGroupsTask().execute();
        
        return view;
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		fragmentActivity = activity;
	}
	
	@Override
	public void onStop(){
		try{
			fragmentActivity.unregisterReceiver(this.newBuzzReceiver);
		}catch(Exception ex){
			
		}
		app.RefreshGroupsBuzz = true;
		super.onStop();
	}
	
	@Override
	public void onResume(){
		super.onResume();
		if(app.RefreshGroupsBuzz){
			app.RefreshGroupsBuzz = false;
			new GetMyGroupsTask().execute();
		}
	}
	
	public void refresh(){
		updateList();
	}
	
	private void showMessage(String msg){
		groupsActivity.showMessage(msg);
	}
	
	private void filterGroups(String filter){
		if(!filter.isEmpty()){
			txtClearSearch.setVisibility(View.VISIBLE);
			filteredGroups = new ArrayList<Group>();
			if(adapterMyGroups != null){
				adapterMyGroups.clear();
				adapterMyGroups = null;
			}
			adapterMyGroups = new AdapterMyGroups(fragmentActivity, R.layout.list_my_group, filteredGroups, app);
			listMyGroups.setAdapter(adapterMyGroups);
			
			for(int i = 0; i < groups.size(); i++){
				if(groups.get(i).getGroupName().toLowerCase(Locale.ENGLISH).indexOf(filter) != -1){
					adapterMyGroups.add(groups.get(i));
				}
			}
			adapterMyGroups.notifyDataSetChanged();
			
			if(adapterMyGroups.getCount() == 0){
				txtSearchText.setText("You aren't in a group with that name. Check out the Explore tab to find one like it, or click the + on the top right to create it yourself!");
			}else{
				txtSearchText.setText("No more results.");
			}
			
			txtSearch.requestFocus();
		}else{
			updateList();
		}
	}
	
	private void updateList(){
		if(adapterMyGroups != null){
			adapterMyGroups.clear();
			adapterMyGroups = null;
		}
		new GetMyGroupsTask().execute();
		txtClearSearch.setVisibility(View.INVISIBLE);
		filteredGroups = null;
	}
	
	private void startGetMyGroups(){
		groupsActivity.showProgress("Loading your groups...");
	}
	
	private void getMyGroupsResult(String result){
		groups = new ArrayList<Group>();
		filteredGroups = new ArrayList<Group>();
		
		adapterMyGroups = new AdapterMyGroups(fragmentActivity, R.layout.list_my_group, filteredGroups, app);
		listMyGroups.setAdapter(adapterMyGroups);
		
		if(result.equalsIgnoreCase("success")){
			try{
				jsonArray = jsonObject.getJSONArray("body");
				if(jsonArray != null){
					JSONObject jObject;
					for(int i = 0; i < jsonArray.length(); i++){
						jObject = jsonArray.getJSONObject(i);
						group = new Group();
						group.setGroupId(jObject.getInt("groupId"));
						group.setGroupName(jObject.getString("name"));
						group.setIsPrivate(jObject.getBoolean("private"));
						group.setNewGroupBuzzAvailable(jObject.getBoolean("newGroupBuzzAvailable"));
						group.setMemberCount(jObject.getInt("members"));
						group.setDateStamp(jObject.getLong("lastBuzzTs"));
						adapterMyGroups.add(group);
						groups.add(group);
					}
				}
				
				adapterMyGroups.notifyDataSetChanged();
				
				if(searching){
					txtSearch.requestFocus();
				}
			}catch(JSONException e){
				showMessage("Error parsing groups: " + e.getMessage());
			}catch(Exception ex){
				showMessage("Error loading groups: " + ex.getMessage());
			}
		}else{
			showMessage("Error, could not get groups: " + result);
		}
		if(adapterMyGroups.getCount() > 0)
			txtSearchText.setText("No more results.");
		else
			txtSearchText.setText("You don't have any groups yet.");
		swipeContainer.setRefreshing(false);
	}
	
	private class GetMyGroupsTask extends AsyncTask<Void,Void,String>{

		@Override
		protected String doInBackground(Void... params) {
			String retVal = "";
			String singleLine = "";
			
			try{
				httpClient = new CustomHttpClient(fragmentActivity);
				httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_groups_user) + app.getActiveUser().getId());
				httpGet.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpGet.setHeader("PLATFORM","Android");
				httpGet.setHeader("USER_ID",""+app.getActiveUser().getId());
				httpResponse = httpClient.execute(httpGet);
				httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();
				bufferReader = new BufferedReader(new InputStreamReader(inputStream));
				stringBuilder = new StringBuilder();
				
				while((singleLine = bufferReader.readLine()) != null){
					stringBuilder.append(singleLine);
				}
				
				httpResult = stringBuilder.toString();
				inputStream.close();
				jsonObject = new JSONObject(httpResult);
				//Log.d("Trending",httpResult);
				if(jsonObject.getBoolean("res")){
					retVal = "success";
				}else{
					retVal = "failed";
				}
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			groupsActivity.hideProgress();
			
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startGetMyGroups();
		}
		
		@Override
		protected void onPostExecute(String result){
			getMyGroupsResult(result);
		}
	}
}
