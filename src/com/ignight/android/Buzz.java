package com.ignight.android;

import java.util.Calendar;
import java.util.Locale;

import android.graphics.Bitmap;
import android.text.format.DateFormat;
import android.util.Log;

public class Buzz {
	private boolean BuzzReported;
	private boolean BuzzReporting;
	private User BuzzUser;
	private String BuzzText;
	private String ChatPictureURL;
	private long DateStamp;
	private int GroupId;
	private int Id;
	private boolean IsImage;
	private boolean IsNew;
	private boolean NewUpload;
	private Bitmap NewUploadImage;
	private String Status;
	private int Type;
	private int[] UpVoteList;
	private int UserId;
	private int VenueId;
	private String VenueName;
	
	public Buzz(){
		this.BuzzUser = new User();
	}
	
	public Buzz(String buzzText, int buzzId, int venueId, String venueName){
		this.BuzzText = buzzText;
		this.Id = buzzId;
		this.VenueId = venueId;
		this.VenueName = venueName;
		this.BuzzUser = new User();
	}
	
	public boolean getBuzzReported(){
		return this.BuzzReported;
	}
	
	public void setBuzzReported(boolean buzzReported){
		this.BuzzReported = buzzReported;
	}
	
	public boolean getBuzzReporting(){
		return this.BuzzReporting;
	}
	
	public void setBuzzReporting(boolean buzzReporting){
		this.BuzzReporting = buzzReporting;
	}
	
	public User getBuzzUser(){
		return this.BuzzUser;
	}
	
	public void setBuzzUser(User user){
		this.BuzzUser = user;
	}
	
	public String getBuzzText(){
		return this.BuzzText;
	}
	
	public void setBuzzText(String text){
		this.BuzzText = text;
	}
	
	public String getChatPictureURL(){
		return this.ChatPictureURL;
	}
	
	public void setChatPictureURL(String url){
		this.ChatPictureURL = url;
	}
	
	public long getDateStamp(){
		return this.DateStamp;
	}
	
	public void setDateStamp(long dateStamp){
		this.DateStamp = dateStamp;
	}
	
	public int getGroupId(){
		return this.GroupId;
	}
	
	public void setGroupId(int groupId){
		this.GroupId = groupId;
	}
	
	public int getId(){
		return this.Id;
	}
	
	public void setId(int buzzId){
		this.Id = buzzId;
	}
	
	public boolean getIsImage(){
		return this.IsImage;
	}
	
	public void setIsImage(boolean value){
		this.IsImage = value;
	}
	
	public boolean getIsNew(){
		return this.IsNew;
	}
	
	public void setIsNew(boolean val){
		this.IsNew = val;
	}
	
	public boolean getNewUpload(){
		return this.NewUpload;
	}
	
	public void setNewUpload(boolean value){
		this.NewUpload = value;
	}
	
	public Bitmap getNewUploadImage(){
		return this.NewUploadImage;
	}
	
	public void setNewUploadImage(Bitmap value){
		this.NewUploadImage = value;
	}
	
	public String getTime(){
		double difference;
		int holder;
		double minutes;
		double seconds;
		long today;
		java.util.Date todayDate = new java.util.Date();
		
		today = todayDate.getTime();
		difference = (today - DateStamp) / 1000 / 60 / 60;

		/*Log.d("TimeStamp", "today: " + today);
		Log.d("TimeStamp", "datestamp: " + DateStamp);
		Log.d("TimeStamp", "difference: " + difference);
		Log.d("TimeStamp", "minutes: " + Math.round((today - DateStamp) / 1000 / 60));
		Log.d("TimeStamp", "seconds: " + Math.round((today - DateStamp) / 1000));*/
		
		if(difference < 1){
			minutes = (today - DateStamp) / 1000 / 60;
			if(minutes <= 0){
				seconds = (today - DateStamp) / 1000;
				if(seconds <= 0)
					return "Now";
				else{
					holder = (int)Math.round(seconds);
					return holder + (holder == 1 ? " second ago" : " seconds ago");
				}
			} else {
				holder = (int)Math.round(minutes);
				return holder + (holder == 1 ? " minute ago" : " minutes ago");
			}
		} else if(difference < 24) {
			holder = (int)Math.round(difference);
			return holder + (holder == 1 ? " hour ago" : " hours ago");
		} else {
			Calendar cal = Calendar.getInstance(Locale.ENGLISH);
		    cal.setTimeInMillis(DateStamp);
		    String date = DateFormat.format("MMM dd, yyyy", cal).toString();
		    return date;
		}
	}
	
	public String getStatus(){
		return this.Status;
	}
	
	public void setStatus(String status){
		this.Status = status;
	}
	
	public int getType(){
		return this.Type;
	}
	
	public void setType(int type){
		this.Type = type;
	}
	
	public int[] getUpVoteList(){
		return this.UpVoteList;
	}
	
	public void setUpVoteList(int[] upVoteList){
		this.UpVoteList = upVoteList;
	}
	
	public int getUserId(){
		return this.UserId;
	}
	
	public void setUserId(int userId){
		this.UserId = userId;
	}
	
	public int getVenueId(){
		return this.VenueId;
	}
	
	public void setVenueId(int venueId){
		this.VenueId = venueId;
	}
	
	public String getVenueName(){
		return this.VenueName;
	}
	
	public void setVenueName(String name){
		this.VenueName = name;
	}
}
