package com.ignight.android;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterGroups extends ArrayAdapter<Group> {
	private AppBase app;
	private Context context;
	private Group group;
	private ArrayList<Group> groups;
	
	public AdapterGroups(Context context, int textViewResourceId, ArrayList<Group> items, AppBase appBase){
		super(context, textViewResourceId, items);
		
		this.app = appBase;
		this.context = context;
		this.groups = items;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View view = convertView;
		LayoutInflater layoutInflater;
		ViewHolder holder;
		boolean buzzedIn = false;
		
		layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if(view == null){
			view = layoutInflater.inflate(R.layout.list_group, null);
			holder = new ViewHolder();
			
			holder.txtGroupName = (TextView)view.findViewById(R.id.txtGroupName);
			holder.imgGroupIcon = (ImageView)view.findViewById(R.id.imgGroupIcon);
			holder.imgGroupMessage = (ImageView)view.findViewById(R.id.imgGroupMessage);
			holder.deleteGroupSide = (ImageView)view.findViewById(R.id.deleteGroupSide);
			
			view.setTag(holder);
		}else{
			holder = (ViewHolder)view.getTag();
		}
		
		group = groups.get(position);
		
		if(group != null){
			if(holder.txtGroupName != null){holder.txtGroupName.setText(group.getGroupName());}
			
			if(app.IsGroupSearch){
				holder.imgGroupIcon.setVisibility(View.GONE);
				holder.imgGroupMessage.setVisibility(View.GONE);
				holder.txtGroupName.setPadding(15, 15, 0, 15);
			}else{
				if(holder.imgGroupIcon != null){
					if(group.getIsPrivate()){
						holder.imgGroupIcon.setImageResource(R.drawable.private_group_icon);
					} else
						holder.imgGroupIcon.setImageResource(R.drawable.public_group_icon);
				}
				
				if(holder.imgGroupMessage != null){
					holder.imgGroupMessage.setTag(group);
					holder.imgGroupMessage.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							app.ActiveGroup = (Group) v.getTag();
							app.ShowBuzz = true;
							Intent intent;
					    	intent = new Intent(context, ActivityGroup.class);
					    	context.startActivity(intent);
						}
					});
					buzzedIn = group.getNewGroupBuzzAvailable();
					if(buzzedIn)
						holder.imgGroupMessage.setImageResource(R.drawable.sidebar_message);
					else
						holder.imgGroupMessage.setImageResource(R.drawable.sidebar_no_message);
						
				}
			}
		}
		
		return view;
	}
	
	public static class ViewHolder{
		TextView txtGroupName;
		ImageView imgGroupIcon;
		ImageView imgGroupMessage;
		ImageView deleteGroupSide;
	}
}
