package com.ignight.android;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ignight.android.R.drawable;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class FragmentBuzz extends android.support.v4.app.Fragment {
	private boolean addingNewBuzz;
	private AppBase app;
	private BufferedReader bufferReader;
	private AdapterBuzz buzzAdapter;
	private ArrayList<Buzz> buzzes;
	private View buzzHeader;
	private View footerView;
	private Activity fragmentActivity;
	private Group group;
	private ActivityGroup groupActivity;
	private HttpClient httpClient;
	private HttpGet httpGet;
	private HttpEntity httpEntity;
	private HttpResponse httpResponse;
	private String httpResult;
	private InputStream inputStream;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private ListView listBuzz;
	private User user;
	private Venue venue;
	private ActivityVenue venueActivity;
	private TextView txtNoBuzz;
	private TextView txtPostBuzz;
	private EditText txtVenueBuzz;
	private String updateItemId;
	private BroadcastReceiver newBuzzReceiver;
	private boolean firstLoad;
	private int firstBuzzId;
	private int lastBuzzId;
	private Button btnShowMoreBuzz;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View view = inflater.inflate(R.layout.fragment_buzz, container, false);
        txtNoBuzz = (TextView)view.findViewById(R.id.txtNoBuzz);
        txtVenueBuzz = (EditText)view.findViewById(R.id.txtVenueBuzz);
        txtPostBuzz = (TextView)view.findViewById(R.id.txtPostBuzz);
        app = ((AppBase)fragmentActivity.getApplication());

        firstLoad = true;
        if(app.BuzzType.equalsIgnoreCase("venue")){
        	txtNoBuzz.setText(Html.fromHtml("Be the first to Buzz in <b>\""+app.ActiveVenue.getName()+"\"</b> today."));
        }else{
        	txtNoBuzz.setText(Html.fromHtml("Be the first to Buzz in <b>\""+app.ActiveGroup.getGroupName()+"\"</b>."));
        }
        
        footerView = inflater.inflate(R.layout.list_loading_footer, null, false);
        buzzHeader = inflater.inflate(R.layout.header_buzz, null, false);
        btnShowMoreBuzz = (Button)buzzHeader.findViewById(R.id.btnShowMoreBuzz);
        btnShowMoreBuzz.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				btnShowMoreBuzz.setText("Loading buzz...");
				new PageBuzzTask().execute();
			}
		});
        
        listBuzz = (ListView)view.findViewById(R.id.venueBuzzList);
        listBuzz.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            	//Log.d("Buzz", "position: " + position + ", size: " + buzzes.size());
            	if(listBuzz.getHeaderViewsCount() == 1)
            		position = position == 0 ? position : position - 1;
				Buzz buzz = buzzes.get(position);
				
				if(buzz.getIsImage()){
					Buzz imgBuzz = buzzes.get(position);
					for(int i = 0; i < app.BuzzImages.size(); i++){
						if(app.BuzzImages.get(i).getId() == imgBuzz.getId()){
							app.BuzzImagePosition = i;
							break;
						}
					}
					app.LargeImage = buzzes.get(position).getBuzzText();
	            	app.BuzzImageId = buzzes.get(position).getId();
	            	
					Intent i = new Intent(fragmentActivity, ActivityVenuePhoto.class);
					startActivity(i);
				}
			}
        });
        
        if (fragmentActivity != null) {
        	if(app.BuzzType.equalsIgnoreCase("venue")){
            	venueActivity = ((ActivityVenue)getActivity());
        		user = venueActivity.CurrentUser;
            	venueActivity.setBuzzTextBox(txtVenueBuzz, txtPostBuzz);
            	
            	venue = app.ActiveVenue;
            	
            	venueActivity.imgVenueBuzz.setImageResource(drawable.venue_buzz_active);
            	venueActivity.imgVenueInfo.setImageResource(drawable.venue_info);
            	venueActivity.imgVenuePhotos.setImageResource(drawable.venue_photos);
            }else if(app.BuzzType.equalsIgnoreCase("group")){
            	if(app.ActiveGroup != null && app.DirtyGroup == app.ActiveGroup.getGroupId()){
        			app.TrendingType = "trending";
        			showMessage("You have removed this group.");
        			Intent intent = new Intent(fragmentActivity, ActivityTrending.class);
        			startActivity(intent);
        		}
            	groupActivity = ((ActivityGroup)getActivity());
            	
            	user = groupActivity.CurrentUser;
            	groupActivity.setBuzzTextBox(txtVenueBuzz, txtPostBuzz);
            	
            	group = app.ActiveGroup;
            }

        	if(app.UploadPhotoInit){
        		if(app.BuzzType.equalsIgnoreCase("group")){
        			groupActivity.showProgress("Uploading photo....");
        		}else{
        			venueActivity.showProgressDialog("Uploading photo....");
        		}
            } else {
            	new GetBuzzTask().execute();
            }
        }
        app.ShowBuzz = false;
        
        IntentFilter intentFilter = new IntentFilter("com.ignight.android.updateBuzz");
    	newBuzzReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
            	Bundle extras = intent.getExtras();
            	if(extras != null && !extras.isEmpty()){
            		updateItemId = extras.getString("itemToUpdate");
            		//Log.d("GroupBuzz","buzz id: " + updateItemId + ", groupId: " + app.ActiveGroup.getGroupId());
            		if(updateItemId != null && !updateItemId.isEmpty()){
            			//Log.d("GroupBuzz", "type: " + app.BuzzType + " active group: " + app.ActiveGroup.getGroupId() + " true: " + updateItemId.equals(String.valueOf(app.ActiveGroup.getGroupId())));
            			if(app.BuzzType.equalsIgnoreCase("group") && updateItemId.equals(String.valueOf(app.ActiveGroup.getGroupId()))){
            				//Log.d("GroupBuzz","Refresh group buzzz");
            				new GetNewBuzzTask().execute();
            			}else if(app.BuzzType.equalsIgnoreCase("venue") && updateItemId.equals(String.valueOf(app.ActiveVenue.getId())))
            					new GetNewBuzzTask().execute();
            		}
            	}
            }
		};
		fragmentActivity.registerReceiver(newBuzzReceiver, intentFilter);
        
        if(!user.getPushSet())
        	app.checkPushNotification();
        
        return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		fragmentActivity = activity;
		buzzAdapter = null;
	}
	
	@Override
	public void onResume(){
		super.onResume();
		if(app.AddBuzzPhoto){
			app.AddBuzzPhoto = false;
			postBuzzImage();
		}
	}
	
	@Override
	public void onPause(){
		super.onPause();
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
	}
	
	@Override
	public void onStop(){
		try{
			fragmentActivity.unregisterReceiver(this.newBuzzReceiver);
		}catch(Exception ex){
			
		}
		super.onStop();
	}
	
	public void updateAdapter(Buzz buzz){
		if(buzzAdapter == null)
			new GetBuzzTask().execute();
		else{
			buzzAdapter.add(buzz);
			buzzAdapter.notifyDataSetChanged();
			
			if(buzz.getIsImage())
				app.BuzzImages.add(buzz);
			
			if(buzzAdapter.getCount() > 0)
				txtNoBuzz.setVisibility(View.GONE);
		}
	}
	
	public void refreshBuzz(){
		new GetBuzzTask().execute();
    	app.ShowBuzz = false;
	}
	
	private void postBuzzImage(){
		Bitmap bitmap;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		File picFile = new File(app.LocalImagePath);
		if(picFile.exists()){
			bitmap = BitmapFactory.decodeFile(picFile.getAbsolutePath(), options);
			if(app.BuzzType.equalsIgnoreCase("group")){
				groupActivity.takenPictureData = bitmap;
				groupActivity.filePath = app.LocalImagePath;
				groupActivity.postPhotoFromGallery();
			}else{
				venueActivity.takenPictureData = bitmap;
				venueActivity.filePath = app.LocalImagePath;
				venueActivity.postPhotoFromGallery();
			}
		}else
			showMessage("Invalid image");
	}
	
	private void showMessage(String msg){
		if(app.BuzzType.equalsIgnoreCase("venue")){
			venueActivity.showMessage(msg);
		}else if(app.BuzzType.equalsIgnoreCase("group")){
			groupActivity.showMessage(msg);
		}
	}
	
	private class PageBuzzTask extends AsyncTask<Void, Void, String>{
		@Override
		protected String doInBackground(Void... args) {
			String retVal = "";
			String singleLine = "";
			StringBuilder stringBuilder;
			
			try{
				httpClient = new CustomHttpClient(fragmentActivity);
				if(app.BuzzType.equalsIgnoreCase("group")){
					httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_group_buzz) + group.getGroupId() + "/?&userId="+user.getId()+"&lastBuzzId="+firstBuzzId+"&count=20&latest=0");
				}else{
					httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_venue_buzz) + venue.getId() + "/?userId="+user.getId()+"&lastBuzzId="+firstBuzzId+"&count=20&latest=0");
				}
				httpGet.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpGet.setHeader("PLATFORM","Android");
				httpGet.setHeader("USER_ID",""+user.getId());

				httpResponse = httpClient.execute(httpGet);
				httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();
				bufferReader = new BufferedReader(new InputStreamReader(inputStream));
				stringBuilder = new StringBuilder();
				
				while((singleLine = bufferReader.readLine()) != null){
					stringBuilder.append(singleLine);
				}
				
				httpResult = stringBuilder.toString();
				inputStream.close();
				//Log.d("Got Buzz", httpResult);
				if(httpResult.isEmpty()){
					retVal = "Error, empty result";
				}else{
					jsonObject = new JSONObject(httpResult);
					
					if(jsonObject.getBoolean("res")){
						retVal = "success";
					}else{
						retVal = "failed";
					}
				}
				
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}

			return retVal;
		}
		
		@Override
		protected void onPostExecute(String result){
			Buzz buzz;
			String picUrl;
			
			if(result.equalsIgnoreCase("success")){
				try{
					jsonArray = jsonObject.getJSONObject("body").getJSONArray("buzz");
					if(buzzAdapter != null){
						for(int i = 0; i < jsonArray.length(); i++){
							buzz = new Buzz();
							buzz.setBuzzText(jsonArray.getJSONObject(i).getString("buzzText"));
							buzz.setId(jsonArray.getJSONObject(i).getInt("buzzId"));
							buzz.setUserId(jsonArray.getJSONObject(i).getInt("userId"));
							buzz.setVenueId(0);
							buzz.setDateStamp(jsonArray.getJSONObject(i).getLong("buzzDateStamp"));
							User buzzUser = new User();
							buzzUser.setId(jsonArray.getJSONObject(i).getInt("userId"));
							buzzUser.setUserName(jsonArray.getJSONObject(i).getString("userName"));
							buzz.setBuzzUser(buzzUser);
							picUrl = jsonArray.getJSONObject(i).optString("chatPictureURL", null);
							buzz.setBuzzText(buzz.getBuzzText());
							buzz.setIsImage(!jsonArray.getJSONObject(i).optString("buzzType", "TEXT").equalsIgnoreCase("text"));
							if(app.BuzzType.equalsIgnoreCase("group")){
								buzz.setGroupId(app.ActiveGroup.getGroupId());
								buzz.setVenueId(-1);
							}else{
								buzz.setGroupId(-1);
								buzz.setVenueId(app.ActiveVenue.getId());
							}
							
							if(picUrl != null && !picUrl.isEmpty()){
								buzz.setChatPictureURL(picUrl);
							}else{
								buzz.setChatPictureURL("");
							}
							buzzes.add(0,buzz);
							
							if(buzz.getIsImage())
								app.BuzzImages.add(buzz);
						}
						
						buzzAdapter.notifyDataSetChanged();
						//listBuzz.setSelection(buzzAdapter.getCount() - 1);
						//listBuzz.setSelectionFromTop(0, 0);
						lastBuzzId = buzzAdapter.getCount() > 0 ? buzzAdapter.getItem(buzzAdapter.getCount() - 1).getId() : -1;
						firstBuzzId = buzzAdapter.getCount() > 0 ? buzzAdapter.getItem(0).getId() : -1;
					}
					if(jsonArray.length() < 19){
						if(listBuzz.getHeaderViewsCount() > 0)
							listBuzz.removeHeaderView(buzzHeader);
					}else{
						if(listBuzz.getHeaderViewsCount() > 0)
							btnShowMoreBuzz.setText("Load more");
					}
				}catch(Exception e){
					showMessage("Error parsing buzz: " + e.getMessage());
				}
			}
		}
	}
	
	private void getBuzzResult(String result){
		Buzz buzz;
		String picUrl;
		if(result.equals("success")){
			try{
				jsonArray = jsonObject.getJSONObject("body").getJSONArray("buzz");
				if(addingNewBuzz){
					//Log.d("GroupBuzz", "adding new buzz");
					if(buzzAdapter == null){
						buzzes = new ArrayList<Buzz>();	
						app.BuzzImages = new ArrayList<Buzz>();
						buzzAdapter = new AdapterBuzz(fragmentActivity, R.layout.list_buzz, buzzes, user);
						listBuzz.setAdapter(buzzAdapter);
					}
					addingNewBuzz = false;
				}else{
					buzzes = new ArrayList<Buzz>();	
					app.BuzzImages = new ArrayList<Buzz>();
					buzzAdapter = new AdapterBuzz(fragmentActivity, R.layout.list_buzz, buzzes, user);
					listBuzz.setAdapter(buzzAdapter);
				}
				
				for(int i = 0; i < jsonArray.length(); i++){
					buzz = new Buzz();
					buzz.setBuzzText(jsonArray.getJSONObject(i).getString("buzzText"));
					buzz.setId(jsonArray.getJSONObject(i).getInt("buzzId"));
					buzz.setUserId(jsonArray.getJSONObject(i).getInt("userId"));
					buzz.setVenueId(0);
					buzz.setDateStamp(jsonArray.getJSONObject(i).getLong("buzzDateStamp"));
					//buzz.setType(jsonArray.getJSONObject(i).getInt("dnaMatchBarColor"));
					User buzzUser = new User();
					buzzUser.setId(jsonArray.getJSONObject(i).getInt("userId"));
					buzzUser.setUserName(jsonArray.getJSONObject(i).getString("userName"));
					buzz.setBuzzUser(buzzUser);
					picUrl = jsonArray.getJSONObject(i).optString("chatPictureURL", null);
					buzz.setIsImage(!jsonArray.getJSONObject(i).optString("buzzType", "TEXT").equalsIgnoreCase("text"));
					if(app.BuzzType.equalsIgnoreCase("group")){
						buzz.setGroupId(app.ActiveGroup.getGroupId());
						buzz.setVenueId(-1);
					}else{
						buzz.setVenueId(app.ActiveVenue.getId());
						buzz.setGroupId(-1);
					}
						
					if(picUrl != null && !picUrl.isEmpty()){
						buzz.setChatPictureURL(picUrl);
					}else{
						buzz.setChatPictureURL("");
					}
					buzzAdapter.add(buzz);
					
					if(buzz.getIsImage())
						app.BuzzImages.add(buzz);
				}
				
				buzzAdapter.notifyDataSetChanged();
				lastBuzzId = buzzAdapter.getCount() > 0 ? buzzAdapter.getItem(buzzAdapter.getCount() - 1).getId() : -1;
				firstBuzzId = buzzAdapter.getCount() > 0 ? buzzAdapter.getItem(0).getId() : -1;
				
				if(buzzAdapter.getCount() >= 19)
					listBuzz.addHeaderView(buzzHeader);
				
				if(buzzAdapter.getCount() > 0)
					txtNoBuzz.setVisibility(View.GONE);
				
				if(app.BuzzType.equalsIgnoreCase("group")){
					groupActivity.hideProgress();
				}else{
					venueActivity.hideProgressDialog();
				}
			}catch(Exception e){
				showMessage("Error parsing buzz: " + e.getMessage());
			}
		}else{
			showMessage("Error, could not get buzz: " + result);
		}
		
		if(listBuzz.getFooterViewsCount() > 0){
			listBuzz.removeFooterView(footerView);
		}
	}
	
	private class GetNewBuzzTask extends AsyncTask<Void, Void, String>{
		@Override
		protected String doInBackground(Void... args) {
			String retVal = "";
			String singleLine = "";
			StringBuilder stringBuilder;
			
			try{
				httpClient = new CustomHttpClient(fragmentActivity);
				if(app.BuzzType.equalsIgnoreCase("group")){
					httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_group_buzz) + group.getGroupId() + "/?&userId="+user.getId()+"&lastBuzzId="+lastBuzzId+"&count=2&latest=1");
				}else{
					httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_venue_buzz) + venue.getId() + "/?userId="+user.getId()+"&lastBuzzId="+lastBuzzId+"&count=2&latest=1");
				}
				httpGet.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpGet.setHeader("PLATFORM","Android");
				httpGet.setHeader("USER_ID",""+user.getId());

				httpResponse = httpClient.execute(httpGet);
				httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();
				bufferReader = new BufferedReader(new InputStreamReader(inputStream));
				stringBuilder = new StringBuilder();
				
				while((singleLine = bufferReader.readLine()) != null){
					stringBuilder.append(singleLine);
				}
				
				httpResult = stringBuilder.toString();
				inputStream.close();
				//Log.d("Got Buzz", httpResult);
				if(httpResult.isEmpty()){
					retVal = "Error, empty result";
				}else{
					jsonObject = new JSONObject(httpResult);
					
					if(jsonObject.getBoolean("res")){
						retVal = "success";
					}else{
						retVal = "failed";
					}
				}
				
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}

			return retVal;
		}
		
		@Override
		protected void onPostExecute(String result){
			if(result.equalsIgnoreCase("success")){
				addingNewBuzz = true;
				getBuzzResult(result);
			}
		}
	}
	
	private class GetBuzzTask extends AsyncTask<Void, Void, String>{

		@Override
		protected String doInBackground(Void... args) {
			String retVal = "";
			String singleLine = "";
			StringBuilder stringBuilder;
			
			try{
				httpClient = new CustomHttpClient(fragmentActivity);
				if(app.BuzzType.equalsIgnoreCase("group")){
					httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_group_buzz) + group.getGroupId() + "/?&userId="+user.getId()+"&lastBuzzId=-1&count=20&latest=1");
				}else{
					httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_venue_buzz) + venue.getId() + "/?userId="+user.getId()+"&lastBuzzId=-1&count=20&latest=1");
				}
				httpGet.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpGet.setHeader("PLATFORM","Android");
				httpGet.setHeader("USER_ID",""+user.getId());
				
				httpResponse = httpClient.execute(httpGet);
				//Log.d("Got Buzz", "Sent request");
				httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();
				bufferReader = new BufferedReader(new InputStreamReader(inputStream));
				stringBuilder = new StringBuilder();
				
				while((singleLine = bufferReader.readLine()) != null){
					stringBuilder.append(singleLine);
				}
				
				httpResult = stringBuilder.toString();
				inputStream.close();
				//Log.d("Got Buzz", httpResult);
				if(httpResult.isEmpty()){
					retVal = "Error, empty result";
				}else{
					jsonObject = new JSONObject(httpResult);
					
					if(jsonObject.getBoolean("res")){
						retVal = "success";
					}else{
						retVal = "failed";
					}
				}
				
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}

			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			listBuzz.addFooterView(footerView);
			if(firstLoad){
				firstLoad = false;
				if(app.BuzzType.equalsIgnoreCase("venue"))
					venueActivity.showProgressDialog("Loading...");
				else
					groupActivity.showProgress("Loading...");
			}
		}
		
		@Override
		protected void onPostExecute(String result){
			getBuzzResult(result);
			if(app.BuzzType.equalsIgnoreCase("venue"))
				venueActivity.hideProgressDialog();
			else
				groupActivity.hideProgress();
		}
	}
}
