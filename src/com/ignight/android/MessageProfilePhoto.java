package com.ignight.android;

public class MessageProfilePhoto {
	private class NewProfilePhoto{
		private int userId;
		private String type;
		
		NewProfilePhoto(int userId){
			this.userId = userId;
			this.type = "updateProfilePicture";
		}
	}
	private NewProfilePhoto msg;
	MessageProfilePhoto(int userId){
		msg = new NewProfilePhoto(userId);
	}
}
