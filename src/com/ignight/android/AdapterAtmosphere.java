package com.ignight.android;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import com.squareup.picasso.Picasso;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterAtmosphere extends BaseAdapter {
	private LayoutInflater inflater;
	private Context mContext;
	private ArrayList<DNAAtmosphere> options;
	private final int[] overlays = { R.drawable.selected_one, R.drawable.selected_two, R.drawable.selected_three, R.drawable.selected_four, R.drawable.selected_five };
	private User user;
	private Bitmap mPlaceHolderBitmap;
	
	public AdapterAtmosphere(Context context, ArrayList<DNAAtmosphere> dnaAtmosphere, User user){
		this.mContext = context;
		this.options = dnaAtmosphere;
		this.user = user;
		
		mPlaceHolderBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.dna_selected);
	}
	
	@Override
	public int getCount() {
		return options.size();
	}

	@Override
	public Object getItem(int position) {
		return options.get(position);
	}

	@Override
	public long getItemId(int position) {
		return options.get(position).getId();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		DNAAtmosphere option;
		boolean found = false;
		GridHolder holder = null;
		View view = convertView;
		int pos = 0;
		
		if(view == null){
			inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.list_dna_atmosphere, null);
			
			holder = new GridHolder();
			holder.imageView = (ImageView)view.findViewById(R.id.gridAtmosphereDNAImage);
			holder.imageNumber = (ImageView)view.findViewById(R.id.gridAtmosphereNumber);
			holder.txtTitle = (TextView)view.findViewById(R.id.gridAtmosphereTitle);
			view.setTag(holder);
		}else{
			holder = (GridHolder) view.getTag();
		}
		
		option = options.get(position);
		holder.txtTitle.setText(option.getName());
		Picasso.with(mContext).load(option.getResourceId(option.getId())).noFade().into(holder.imageView);
		//loadBitmap(option.getResourceId(option.getId()), holder.imageView);
		//holder.imageView.setImageBitmap(decodeSampledBitmapFromResource(mContext.getResources(),option.getResourceId(option.getId()),90,90));
		//holder.imageView.setImageResource(option.getResourceId(option.getId()));
		
		for(DNAAtmosphere item: user.getAtmosphereOptions()){
			if(item.getId() == option.getId()){
				found = true;
				//option.setPosition(user.getAtmosphereOptions().indexOf(item));
				pos = user.getAtmosphereOptions().indexOf(item);
				holder.imageNumber.setImageResource(overlays[pos]);
				holder.imageNumber.setTag(pos);
				break;
			}
		}
		
		if(found){
			//Picasso.with(mContext).load(overlays[option.getPosition()]).into(holder.imageNumber);
			//loadBitmap(overlays[option.getPosition()], holder.imageNumber);
			//holder.imageNumber.setImageResource(overlays[option.getPosition()]);
			//holder.imageNumber.setTag(option.getPosition());
		}else{
			holder.imageNumber.setImageDrawable(null);
			holder.imageNumber.setTag(-1);
		}
		
		return view;
	}
	
	static class GridHolder {
		ImageView imageNumber;
		ImageView imageView;
		TextView txtTitle;
    }
	
	private void loadBitmap(int resId, ImageView imageView) {
		if (cancelPotentialWork(resId, imageView)) {
	        final BitmapWorkerTask task = new BitmapWorkerTask(imageView);
	        final AsyncDrawable asyncDrawable = new AsyncDrawable(mContext.getResources(), mPlaceHolderBitmap, task);
	        imageView.setImageDrawable(asyncDrawable);
	        task.execute(resId);
	    }
	}
	
	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
	    // Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;
	
	    if (height > reqHeight || width > reqWidth) {
	
	        final int halfHeight = height / 2;
	        final int halfWidth = width / 2;
	
	        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
	        // height and width larger than the requested height and width.
	        while ((halfHeight / inSampleSize) > reqHeight
	                && (halfWidth / inSampleSize) > reqWidth) {
	            inSampleSize *= 2;
	        }
	    }
	
	    return inSampleSize;
	}
	
	public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
	        int reqWidth, int reqHeight) {

	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeResource(res, resId, options);

	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    return BitmapFactory.decodeResource(res, resId, options);
	}
	
	class BitmapWorkerTask extends AsyncTask<Integer, Void, Bitmap> {
	    private final WeakReference<ImageView> imageViewReference;
	    private int data = 0;

	    public BitmapWorkerTask(ImageView imageView) {
	        // Use a WeakReference to ensure the ImageView can be garbage collected
	        imageViewReference = new WeakReference<ImageView>(imageView);
	    }

	    // Decode image in background.
	    @Override
	    protected Bitmap doInBackground(Integer... params) {
	        data = params[0];
	        return decodeSampledBitmapFromResource(mContext.getResources(), data, 100, 100);
	    }

	    // Once complete, see if ImageView is still around and set bitmap.
	    @Override
	    protected void onPostExecute(Bitmap bitmap) {
	    	if (isCancelled()) {
	            bitmap = null;
	        }

	        if (imageViewReference != null && bitmap != null) {
	            final ImageView imageView = imageViewReference.get();
	            final BitmapWorkerTask bitmapWorkerTask =
	                    getBitmapWorkerTask(imageView);
	            if (this == bitmapWorkerTask && imageView != null) {
	                imageView.setImageBitmap(bitmap);
	            }
	        }
	    }
	}
	
	static class AsyncDrawable extends BitmapDrawable {
	    private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

	    public AsyncDrawable(Resources res, Bitmap bitmap, BitmapWorkerTask bitmapWorkerTask) {
	        super(res, bitmap);
	        bitmapWorkerTaskReference = new WeakReference<BitmapWorkerTask>(bitmapWorkerTask);
	    }

	    public BitmapWorkerTask getBitmapWorkerTask() {
	        return bitmapWorkerTaskReference.get();
	    }
	}
	
	public static boolean cancelPotentialWork(int data, ImageView imageView) {
	    final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

	    if (bitmapWorkerTask != null) {
	        final int bitmapData = bitmapWorkerTask.data;
	        // If bitmapData is not yet set or it differs from the new data
	        if (bitmapData == 0 || bitmapData != data) {
	            // Cancel previous task
	            bitmapWorkerTask.cancel(true);
	        } else {
	            // The same work is already in progress
	            return false;
	        }
	    }
	    // No task associated with the ImageView, or an existing task was cancelled
	    return true;
	}
	
	private static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
	   if (imageView != null) {
	       final Drawable drawable = imageView.getDrawable();
	       if (drawable instanceof AsyncDrawable) {
	           final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
	           return asyncDrawable.getBitmapWorkerTask();
	       }
	    }
	    return null;
	}
}
