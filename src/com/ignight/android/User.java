package com.ignight.android;

import java.util.ArrayList;

public class User {
	//attributes
	private int Age;
	private ArrayList<DNAAtmosphere> AtmosphereOptions;
	private boolean BelongToGroup;
	private String FirstName;
	private String Gender;
	private boolean GroupAdmin;
	private ArrayList<Group> Groups;
	private ArrayList<Group> GroupsImIn;
 	private ArrayList<Group> GroupsIOwn;
 	private int GroupStatus;
 	private int HasGroups;
 	private int HasLastBuzzed;
	private int Id;
	private String LastName;
	private double Latitude;
	private String Location;
	private double Longitude;
	private ArrayList<DNAMusic> MusicOptions;
	private boolean PushSet;
	private int SpendingPreference;
	private ArrayList<DNAVenue> VenueOptions;
	private boolean UpdatingPhoto;
	private City UserCity;
	private String UserEmail;
	private String UserName;
	private String UserPassword;
	private String UserPhoto;
	private Preference UserPreference;
	private ArrayList<Venue> VenuesIBuzzedIn;
	
	//constructors
	public User() {
		this.Age = 0;
		this.SpendingPreference = -1;
		this.AtmosphereOptions = new ArrayList<DNAAtmosphere>();
		this.MusicOptions = new ArrayList<DNAMusic>();
		this.VenueOptions = new ArrayList<DNAVenue>();
		this.Groups = new ArrayList<Group>();
		this.GroupsImIn = new ArrayList<Group>();
		this.GroupsIOwn = new ArrayList<Group>();
		this.UserPreference = new Preference();
		this.VenuesIBuzzedIn = new ArrayList<Venue>();
		
		this.FirstName = "";
		this.LastName = "";
	}
	public User(String userEmail, String userPassword){
		this.UserEmail = userEmail;
		this.UserPassword = userPassword;
		this.SpendingPreference = -1;
		
		this.AtmosphereOptions = new ArrayList<DNAAtmosphere>();
		this.MusicOptions = new ArrayList<DNAMusic>();
		this.VenueOptions = new ArrayList<DNAVenue>();
		this.Groups = new ArrayList<Group>();
		this.GroupsImIn = new ArrayList<Group>();
		this.GroupsIOwn = new ArrayList<Group>();
		this.UserPreference = new Preference();
		this.VenuesIBuzzedIn = new ArrayList<Venue>();
		
		this.FirstName = "";
		this.LastName = "";
	}
	//properties
	public int getAge(){
		return this.Age;
	}
	public void setAge(int age){
		this.Age = age;
	}
	public boolean getBelongToGroup(){
		return this.BelongToGroup;
	}
	public void setBelongToGroup(boolean belongToGroup){
		this.BelongToGroup = belongToGroup;
	}
	public City getCity(){
		return this.UserCity;
	}
	public void setCity(City city){
		this.UserCity = city;
	}
	public ArrayList<DNAAtmosphere> getAtmosphereOptions(){
		return this.AtmosphereOptions;
	}
	public void setAtmosphereOptions(ArrayList<DNAAtmosphere> options){
		this.AtmosphereOptions = options;
	}
	public String getFirstName(){
		return this.FirstName;
	}
	public void setFirstName(String firstName){
		this.FirstName = firstName;
	}
	public String getGender(){
		return this.Gender;
	}
	public void setGender(String gender){
		this.Gender = gender;
	}
	public boolean getGroupAdmin(){
		return this.GroupAdmin;
	}
	public void setGroupAdmin(boolean groupAdmin){
		this.GroupAdmin = groupAdmin;
	}
	public ArrayList<Group> getGroups(){
		return this.Groups;
	}
	public void setGroups(ArrayList<Group> groups){
		this.Groups = groups;
	}
	public ArrayList<Group> getGroupsImIn(){
		return this.GroupsImIn;
	}
	public void setGroupsImIn(ArrayList<Group> groups){
		this.GroupsImIn = groups;
	}
	public ArrayList<Group> getGroupsIOwn(){
		return this.GroupsIOwn;
	}
	public void setGroupsIOwn(ArrayList<Group> groups){
		this.GroupsIOwn = groups;
	}
	public int getGroupStatus(){
		return this.GroupStatus;
	}
	public void setGroupStatus(int groupStatus){
		this.GroupStatus = groupStatus;
	}
	public int getHasGroups(){
		return this.HasGroups;
	}
	public void setHasGroups(int hasGroups){
		this.HasGroups = hasGroups;
	}
	public int getHasLastBuzzed(){
		return this.HasLastBuzzed;
	}
	public void setHasLastBuzzed(int hasLastBuzzed){
		this.HasLastBuzzed = hasLastBuzzed;
	}
	public int getId(){
		return this.Id;
	}
	public void setId(int id){
		this.Id = id;
	}
	public String getLastName(){
		return this.LastName;
	}
	public void setLastName(String lastName){
		this.LastName = lastName;
	}
	public String getFullName(){
		return this.FirstName + " " + this.LastName;
	}
	public double getLatitude(){
		return this.Latitude;
	}
	public void setLatitude(double latitude){
		this.Latitude = latitude;
	}
	public String getLocation(){
		return this.Location;
	}
	public void setLocation(String location){
		this.Location = location;
	}
	public double getLongitude(){
		return this.Longitude;
	}
	public void setLongitude(double longitude){
		this.Longitude = longitude;
	}
	public ArrayList<DNAMusic> getMusicOptions(){
		return this.MusicOptions;
	}
	public void setMusicOptions(ArrayList<DNAMusic> options){
		this.MusicOptions = options;
	}
	public boolean getPushSet(){
		return this.PushSet;
	}
	public void setPushSet(boolean pushSet){
		this.PushSet = pushSet;
	}
	public String getUserEmail(){
		return this.UserEmail;
	}
	public void setUserEmail(String userEmail){
		this.UserEmail = userEmail;
	}
	public String getUserName(){
		return this.UserName;
	}
	public void setUserName(String userName){
		this.UserName = userName;
	}
	public String getUserPassword(){
		return this.UserPassword;
	}
	public void setUserPassword(String userPassword){
		this.UserPassword = userPassword;
	}
	public String getUserPhoto(){
		if(this.UserPhoto == null)
			return "";
		
		return this.UserPhoto;
	}
	public void setUserPhoto(String value){
		this.UserPhoto = value;
	}
	public Preference getUserPreference(){
		return this.UserPreference;
	}
	public void setUserPreference(Preference userPreference){
		this.UserPreference = userPreference;
	}
	public int getSpendingPreference(){
		return this.SpendingPreference;
	}
	public void setSpendingPreference(int spendingPreference){
		this.SpendingPreference = spendingPreference;
	}
	public boolean getUpdatingPhoto(){
		return this.UpdatingPhoto;
	}
	public void setUpdatingPhoto(boolean val){
		this.UpdatingPhoto = val;
	}
	public ArrayList<Venue> getVenuesIBuzzedIn(){
		return this.VenuesIBuzzedIn;
	}
	public void setVenuesIBuzzedIn(ArrayList<Venue> venues){
		this.VenuesIBuzzedIn = venues;
	}
	public ArrayList<DNAVenue> getVenueOptions(){
		return this.VenueOptions;
	}
	public void setVenueOptions(ArrayList<DNAVenue> options){
		this.VenueOptions = options;
	}
}
