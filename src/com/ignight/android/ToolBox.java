package com.ignight.android;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

public class ToolBox {
	public static boolean device_isHardwareFeatureAvailable(Context context, String hardwareFeature){
		return context.getPackageManager().hasSystemFeature(hardwareFeature);
	}
	
	/**
	 * This function allows to know if there is an application that responds to
	 * the specified action.
	 * 
	 * @param context
	 * @param action	Action that requires an application.
	 * @return
	 */
	public static boolean system_isIntentAvailable(Context context, String action) {
	    final PackageManager packageManager = context.getPackageManager();
	    final Intent intent = new Intent(action);
	    List<ResolveInfo> list =
	            packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
	    return list.size() > 0;
	}
	
	/**
	  * This function returns a File object pointing to a public folder of the the specified 
	  * type in the external drive.
	  * 
	  * User media is saved here. Be carefull.
	  * 	  
	  * Returns null if the media is not mounted.
	  * 
	  * @param folderType	The type of files directory to return. Use one of the avaliable
	  * 					Environment.DIRECTORY_ values.
	  * @param folder		An specific folder in the public folder. May be null to get 
	  * 					the root of the public folder type.
	  * @param createFolder	Set to TRUE to create the folder if it does not exists.  
	  * 					
	  * @return
	  */
	 public static File storage_getExternalPublicFolder(String folderType, String folder, boolean createFolder){
		 File res=null;
		 if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)){
			  res=new File(android.os.Environment.getExternalStoragePublicDirectory(folderType),folder);
			  
			  if(!res.exists()){
				  //We create the folder if is desired.
				  if(folder!=null && folder.length()>0 && createFolder){
					 if(!res.mkdir()){
						 res = null;
					 }
				  }else{
					  res = null;
				  }
			  }
		 }
		 return res;
	 }
	 
	 /**
		 * Return a temporal empty file.
		 * 
		 * @param filePrefix
		 * @param fileSuffix
		 * @param outputDirectory
		 * @return
		 * @throws IOException
		 */
		public static File storage_createUniqueFileName(String filePrefix, String fileSuffix, File outputDirectory) throws IOException {
		    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
		    String fileName = filePrefix + timeStamp;
		    File filePath = File.createTempFile(fileName, fileSuffix, outputDirectory);
		    if(filePath.exists()){
		    	return filePath;
		    }else{
		    	return null;
		    }	    
		}
}
