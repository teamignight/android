package com.ignight.android;

import android.widget.ImageView;
import android.widget.RelativeLayout;

public class ImageWithProgress {
	private ImageView img;
	private RelativeLayout layout;
	
	public ImageView getImg() {
        return img;
    }
 
    public void setImg(ImageView img) {
        this.img = img;
    }
 
    public RelativeLayout getLayout() {
        return layout;
    }
 
    public void setLayout(RelativeLayout l) {
        this.layout = l;
    }
}
