package com.ignight.android;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ignight.android.R.drawable;

public class ActivityVenue extends ActivityBase{
	private ActivityVenue activity;
	private BufferedReader bufferReader;
	private FragmentTransaction fragmentTransaction;
	private FragmentBuzz fragmentBuzz;
	private FragmentVenueEdit fragmentEdit;
	private FragmentVenueInfo fragmentInfo;
	private FragmentPhotos fragmentPhotos;
	private Gson gson;
	private HttpClient httpClient;
	private HttpGet httpGet;
	private HttpEntity httpEntity;
	private HttpPost httpPost;
	private HttpResponse httpResponse;
	private String httpResult;
	public ImageView imgVenueBuzz;
	public ImageView imgVenueInfo;
	public ImageView imgVenuePhotos;
	private InputStream inputStream;
	private JSONObject jsonObject;
	private int originalVote;
	private PopupWindow popupWindowActivity;
	private ProgressDialog progressDialog;
	private boolean showingBuzz;
	private boolean showingInfo;
	private boolean showingPhotos;
	private StringBuilder stringBuilder;
	private EditText txtBuzzText;
	private boolean uploadedPhoto;
	private User user;
	private BroadcastReceiver mReceiver;
	private TextView txtPostBuzz;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_venue);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,R.layout.module_title_bar);
		
		if(app.ActiveVenue == null){
			Intent notiIntent = new Intent(this, ActivityTrending.class);
			startActivity(notiIntent);
			return;
		}
		
		activity = this;
		if (savedInstanceState != null && app.UploadingPhoto)
        {
			app.UploadPhotoInit = true;
			app.ShowBuzz = true;
        }
		
		IntentFilter intentFilter = new IntentFilter("com.ignight.android.updatebuzzphoto");
		mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
            	handleReceiver();
            }
		};
		this.registerReceiver(mReceiver, intentFilter);
        user = CurrentUser;
        
        app.BuzzType = "Venue";
        app.PhotoGalleryType = "Venue";
        
        fragmentBuzz = new FragmentBuzz();
        fragmentEdit = new FragmentVenueEdit();
        fragmentInfo = new FragmentVenueInfo();
        fragmentPhotos = new FragmentPhotos();
        
        imgVenueBuzz = (ImageView)findViewById(R.id.imgVenueBuzz);
        imgVenueInfo = (ImageView)findViewById(R.id.imgVenueInfo);
        imgVenuePhotos = (ImageView)findViewById(R.id.imgVenuePhotos);
        
        showingBuzz = false;
        showingInfo = true;
        showingPhotos = false;

        uploadedPhoto = false;
        
        if (savedInstanceState != null && app.UploadingPhoto)
        {
			app.UploadPhotoInit = true;
			app.ShowBuzz = true;
			
			wireupHeader(app.ActiveVenue.getName(),"Buzz");
        	imgVenueBuzz.setImageResource(drawable.venue_buzz_active);
        	imgVenueInfo.setImageResource(drawable.venue_info);
        	fragmentTransaction = getSupportFragmentManager().beginTransaction();
    		fragmentTransaction.add(R.id.fragmentHolder, fragmentBuzz, "FragmentBuzz");
    		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
    		fragmentTransaction.commit();
        }else if(app.ShowReportVenue){
        	fragmentTransaction = getSupportFragmentManager().beginTransaction();
    		fragmentTransaction.add(R.id.fragmentHolder, fragmentEdit, "FragmentEdit");
    		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
    		//fragmentTransaction.addToBackStack(null);
    		fragmentTransaction.commit();
        }else if(app.ShowBuzz){
        	app.ShowBuzz = false;
        	wireupHeader(app.ActiveVenue.getName(),"Buzz");
        	imgVenueBuzz.setImageResource(drawable.venue_buzz_active);
        	imgVenueInfo.setImageResource(drawable.venue_info);
        	fragmentTransaction = getSupportFragmentManager().beginTransaction();
    		fragmentTransaction.add(R.id.fragmentHolder, fragmentBuzz, "FragmentBuzz");
    		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
    		fragmentTransaction.commit();
        }else{
        	wireupHeader(app.ActiveVenue.getName(), "Info");
        	
        	fragmentTransaction = getSupportFragmentManager().beginTransaction();
    		fragmentTransaction.add(R.id.fragmentHolder, fragmentInfo, "FragmentInfo");
    		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
    		//fragmentTransaction.addToBackStack(null);
    		fragmentTransaction.commit();
    		
    		imgVenuePhotos.setImageResource(drawable.venue_photos);
    		imgVenueBuzz.setImageResource(drawable.venue_buzz);
    		imgVenueInfo.setImageResource(drawable.venue_info_active);
    		showingInfo = true;
    		showingPhotos = false;
    		showingBuzz = false;
        }
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event){
		if(keyCode == KeyEvent.KEYCODE_BACK){
			if(fragmentInfo != null && fragmentInfo.isVisible() && app.CalledFromTrending){
				app.RefreshTrending = true; 
				app.CalledFromVenueInfo = true;
				app.CalledFromTrending = false;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	protected void onStop(){
		super.onStop();
		if(progressDialog != null && progressDialog.isShowing())
			progressDialog.dismiss();
		try{
			this.unregisterReceiver(this.mReceiver);
		}catch(Exception ex){
			
		}
	}
	
	public void postPhotoFromGallery(){
		if(takenPictureData!=null){
	    	new PostPhotoTask().execute();
	    }
	}
	
	public void toggleBar(boolean hideBar){
		LinearLayout topVenueNavBar = (LinearLayout)findViewById(R.id.topVenueNavBar);
		
		if(hideBar){
			topVenueNavBar.setVisibility(View.GONE);
		}else{
			topVenueNavBar.setVisibility(View.VISIBLE);
		}
	}
	
	public void loadEdit(View view){
		fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.fragmentHolder, fragmentEdit, "FragmentEdit");
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
		
		app.CalledFromTrending = false;
		
		imgVenueInfo.setImageResource(drawable.venue_info);
		updateHeaderText("Report an Error");
	}
	
	public void loadVenueBuzz(View view){
		fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.fragmentHolder, fragmentBuzz, "FragmentBuzz");
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
		updateHeaderText("Buzz");
		
		if(showingPhotos){
			imgVenuePhotos.setImageResource(drawable.venue_photos);
		}
		if(showingInfo){
			imgVenueInfo.setImageResource(drawable.venue_info);
		}
		imgVenueBuzz.setImageResource(drawable.venue_buzz_active);
		showingInfo = false;
		showingPhotos = false;
		showingBuzz = true;
	}
	
	public void btnSubmitGroupEdit_ClickEvent(View view){
		fragmentEdit.reportVenue();
	}
	
	public void btnCancelSubmitEdit_ClickEvent(View view){
		if(app.CalledFromTrending){
			app.CalledFromTrending = false;
			finish();
		}else{
			loadVenueInfo(null);
		}
	}
	
	public void finishActivity(){
		finish();
	}
	
	public void loadVenueInfo(View view){
		fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.fragmentHolder, fragmentInfo, "FragmentInfo");
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
		updateHeaderText("Info");
		
		imgVenuePhotos.setImageResource(drawable.venue_photos);
		imgVenueBuzz.setImageResource(drawable.venue_buzz);
		imgVenueInfo.setImageResource(drawable.venue_info_active);
		showingInfo = true;
		showingPhotos = false;
		showingBuzz = false;
	}
	
	public void loadVenuePhotos(View view){
		fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.fragmentHolder, fragmentPhotos, "FragmentPhotos");
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
		updateHeaderText("Photos");
		
		imgVenueBuzz.setImageResource(drawable.venue_buzz);
		imgVenueInfo.setImageResource(drawable.venue_info);
		imgVenuePhotos.setImageResource(drawable.venue_photos_active);
		showingInfo = false;
		showingPhotos = true;
		showingBuzz = false;
	}

	public void showVenueActionsWindow(View view){
		LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View venueActionsLayout = inflater.inflate(R.layout.popup_venue_actions, (ViewGroup)findViewById(R.id.popup_venue_actions));
		
		popupWindowActivity = new PopupWindow(venueActionsLayout, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, true);
		popupWindowActivity.setOutsideTouchable(true);
		popupWindowActivity.showAtLocation(venueActionsLayout, Gravity.CENTER_HORIZONTAL, 0, 0);
	}
	
	public void loadWebsite(View view){
		if(!app.ActiveVenue.getUrl().isEmpty()){
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(app.ActiveVenue.getUrl()));
			startActivity(browserIntent);
		}else{
			showMessage("No website was found for this venue");
		}
	}
	
	public void callVenue(View view){
		if(!app.ActiveVenue.getPhoneNumber().isEmpty()){
			Intent phoneIntent = new Intent(Intent.ACTION_DIAL);
			phoneIntent.setData(Uri.parse("tel:" + app.ActiveVenue.getPhoneNumber()));
			startActivity(phoneIntent);
		}else{
			showMessage("No valid phone number found.");
		}
	}
	
	public void getDirections(View view){
		Intent navIntent = null;
		if(app.ActiveVenue.getLatitude() != 0 && app.ActiveVenue.getLongitude() != 0 && user.getLatitude() != 0 && user.getLongitude() != 0){
			navIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr="+user.getLatitude() + "," + user.getLongitude() + "&daddr="+app.ActiveVenue.getLatitude() + "," + app.ActiveVenue.getLongitude()));
		}else if(!app.ActiveVenue.getAddress().isEmpty()){
			navIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr=" + app.ActiveVenue.getAddress() + " " + app.ActiveVenue.getVenueCity().getName() + ", " + app.ActiveVenue.getZip()));
		}
		
		if(navIntent != null){
			startActivity(navIntent);
		}else{
			showMessage("No valid address for venue");
		}
	}
	
	public void getPhoto(View view){
		AlertDialog.Builder builder = new AlertDialog.Builder(ActivityVenue.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                	Log.d("Photo", "getting from camera");
                	app.ShowBuzz = true;
                	app.UploadingPhoto = true;
                	getPictureFromCamera();
                } else if (items[item].equals("Choose from Library")) {
                	getPictureFromGallery();
                } else{
                	dialog.dismiss();
                }
            }
        });
        builder.show();
	}
	
	public void voteDown(View view){
		int input = app.ActiveVenue.getUserInput();
		originalVote = input;
		switch(input){
			case -1:
				app.ActiveVenue.setUserInput(0);
				break;
			case 0:
				app.ActiveVenue.setUserInput(-1);
				break;
			case 1:
				app.ActiveVenue.setUserInput(-1);
				break;
		}
		
		new VoteVenueTask().execute();
	}
	
	public void voteUp(View view){
		int input = app.ActiveVenue.getUserInput();
		originalVote = input;
		switch(input){
			case 1:
				app.ActiveVenue.setUserInput(0);
				break;
			case 0:
				app.ActiveVenue.setUserInput(1);
				break;
			case -1:
				app.ActiveVenue.setUserInput(1);
				break;
		}
		
		new VoteVenueTask().execute();
	}
	
	public void showBuzzActive(){
		imgVenueBuzz.setImageResource(drawable.list_icon_buzz_on);
	}
	
	public void startUploadingPhoto(){
		app.UploadingPhoto = app.UploadPhotoInit = false;
		new PostPhotoTask().execute();
	}
	
	private void handleReceiver(){
		app.UploadingPhoto = app.UploadPhotoInit = false;
		if(fragmentBuzz.isVisible())
			fragmentBuzz.refreshBuzz();
	}
	
	private void voteResult(String result){
		if(result.equals("success")){
			if(fragmentInfo != null){
				fragmentInfo.updateVote(app.ActiveVenue.getUserInput());
			}
			app.UpdatedTrendingVenue = true;
		}else{
			app.ActiveVenue.setUserInput(originalVote);
		}
	}
	
	private void startVote(){
		
	}
	
	private class VoteVenueTask extends AsyncTask<Void, Void, String>{
		@Override
		protected String doInBackground(Void... args) {
			String json = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				gson = new GsonBuilder().create();
				MessageVoteVenue voteVenue = new MessageVoteVenue(app.ActiveVenue.getUserInput(), user.getId(), app.ActiveVenue.getId());
				json = gson.toJson(voteVenue);
				StringEntity stringEntity = new StringEntity(json);
				httpClient = new CustomHttpClient(activity);
				httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_venue_activity) + app.ActiveVenue.getId());
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID",""+app.getActiveUser().getId());
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					
					if(httpResult != null){
						jsonObject = new JSONObject(httpResult);
						
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							retVal = "failed";
						}
					}else{
						retVal = "failed";
					}
				}
				retVal = "success";
			}catch(Exception ex){
				retVal = "failed";
			}
			
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startVote();
		}
		
		@Override
		protected void onPostExecute(String result){
			voteResult(result);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);

	    takenPictureData = null;

	    switch(requestCode){
	    case PICTURE_TAKEN_FROM_GALLERY:                
            if(resultCode ==  Activity.RESULT_OK) {
            	takenPictureData = handleResultFromChooser(data);
            }
            break;
	    case PICTURE_TAKEN_FROM_CAMERA:
	    	if(resultCode==Activity.RESULT_OK) {
                takenPictureData = handleResultFromCamera(data);
            }       
	    	break;
	    }
	    
	    if(takenPictureData!=null){
	    	new PostPhotoTask().execute();
	    }
	}
	
	public void postBuzz(View view){
		if(txtBuzzText == null){
			showMessage("Error, no valid textbox");
		}else if(txtBuzzText.getText().toString().isEmpty()){
			showMessage("Buzz text required.");
		}else{
			txtPostBuzz.setText("posting...");
			txtPostBuzz.setClickable(false);
			new PostBuzzTask().execute();
		}
	}
	
	public void setBuzzTextBox(EditText textBox, TextView postBuzz){
		txtBuzzText = textBox;
		txtPostBuzz = postBuzz;
	}
	
	protected void showProgressDialog(String msg){
		if(progressDialog == null || !progressDialog.isShowing())
			progressDialog = ProgressDialog.show(this, "", msg, true, true);
	}
	
	protected void hideProgressDialog(){
		if(progressDialog != null && progressDialog.isShowing())
			progressDialog.dismiss();
	}
	
	private void startPostBuzz(){
		
	}
	
	private void postBuzzResult(String result){
		Buzz buzz;
		JSONObject jsonBody;
		String text = "";
		if(result.equals("success")){
			try{
				buzz = new Buzz();
				jsonBody = jsonObject.getJSONObject("body");
				
				if(uploadedPhoto){
					uploadedPhoto = false;
					buzz.setIsImage(true);
					buzz.setNewUpload(true);
					//buzz.setNewUploadImage(takenPictureData);
					takenPictureData = null;
					text = jsonBody.getString("buzzUrl");
				}else{
					buzz.setIsImage(false);
					text = txtBuzzText.getText().toString();
					txtBuzzText.setText("");
				}
				buzz.setBuzzText(text);
				buzz.setId(jsonBody.getInt("buzzId"));
				buzz.setUserId(user.getId());
				buzz.setVenueId(app.ActiveVenue.getId());
				buzz.setBuzzUser(user);
				buzz.setType(0);
				java.util.Date date= new java.util.Date();
				buzz.setDateStamp(date.getTime());
				buzz.setChatPictureURL(user.getUserPhoto().isEmpty() ? "" : user.getUserPhoto());
				//app.ActiveVenue.getBuzz().add(buzz);
				if(buzz.getIsImage()){
					if(app.BuzzImages == null)
						app.BuzzImages = new ArrayList<Buzz>();
					app.BuzzImages.add(buzz);
				}
				if(!app.UploadingPhoto || (app.UploadingPhoto && !app.UploadPhotoInit))
					fragmentBuzz.updateAdapter(buzz);
				else{
					Intent i = new Intent("com.ignight.android.updatebuzzphoto");
			        sendBroadcast(i);
				}
			}catch(Exception ex){
				showMessage("Error, failed to add buzz: " + ex.getMessage());
			}
		}else{
			showMessage("Error, failed to add buzz to IgNight");
		}
		txtPostBuzz.setText("Send");
		txtPostBuzz.setClickable(true);
		
		hideProgressDialog();
	}
	
	private class PostBuzzTask extends AsyncTask<Void, Void, String>{
		@Override
		protected String doInBackground(Void... args) {
			String json = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				gson = new GsonBuilder().create();
				MessageBuzz buzz = new MessageBuzz(txtBuzzText.getText().toString(), user.getId(), app.ActiveVenue.getId());
				json = gson.toJson(buzz);
				
				StringEntity stringEntity = new StringEntity(json);
				httpClient = new CustomHttpClient(activity);
				httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_venue_buzz_add_text) + app.ActiveVenue.getId());
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID",""+app.getActiveUser().getId());
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					
					if(httpResult != null){
						jsonObject = new JSONObject(httpResult);
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							retVal = "Invalid login.";
						}
					}else{
						retVal = "Error, no result.";
					}
				}
				
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}

			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startPostBuzz();
		}
		
		@Override
		protected void onPostExecute(String result){
			postBuzzResult(result);
		}
	}

	private void startPostPhoto(){
		uploadedPhoto = true;
		showProgressDialog("Uploading photo...");
	}
	
	private class PostPhotoTask extends AsyncTask<Void, Void, String>{

		@Override
		protected String doInBackground(Void... params) {
			String json = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				gson = new GsonBuilder().create();
				MessageBuzz buzz = new MessageBuzz(txtBuzzText.getText().toString(), user.getId(), app.ActiveVenue.getId());
				json = gson.toJson(buzz);
				
				httpClient = new CustomHttpClient(activity);
				httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_venue_buzz_add_image) + app.ActiveVenue.getId());
				httpPost.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID",""+app.getActiveUser().getId());
				
				MultipartEntityBuilder meBuilder = MultipartEntityBuilder.create();
				meBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
				
				String compressImagePath = filePath;
				
				//get compress bitmap
				Bitmap b = getCompressBitmap(filePath);
				
				//fix rotation
	            ExifInterface exif = new ExifInterface(filePath);
		        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

		        int angle = 0;

		        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
		            angle = 90;
		        } 
		        else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
		            angle = 180;
		        } 
		        else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
		            angle = 270;
		        }

		        Matrix mat = new Matrix();
		        mat.postRotate(angle);
		        
		        BitmapFactory.Options options = new BitmapFactory.Options();
		        options.inSampleSize = 2;
		        
		        Bitmap bmp = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), mat, true);
				
				//save file to temp
	            File f = new File(ActivityVenue.this.getCacheDir().getAbsolutePath(), new java.util.Date().getTime() + ".jpg");
	            //File outputDir = ToolBox.storage_getExternalPublicFolder(Environment.DIRECTORY_PICTURES, "IgNight", true);
	            //app.OutFile = ToolBox.storage_createUniqueFileName("cameraPic", ".jpg", outputDir);
	            
	            compressImagePath = f.getAbsolutePath();
	            
	            FileOutputStream fos = null;
	            try {
	                fos = new FileOutputStream(f);
	                bmp.compress(Bitmap.CompressFormat.JPEG, 80, fos);
	                fos.flush();
	                fos.close();
	            } catch (FileNotFoundException e) {
	                e.printStackTrace();
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	            b.recycle();

				File file = new File(compressImagePath);
				FileBody fb = new FileBody(file);
				
				meBuilder.addPart("userImage", fb);
				meBuilder.addTextBody("json_body",json);
				httpPost.setEntity(meBuilder.build());
				httpPost.setHeader("Accept","application/json");
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					
					if(httpResult != null){
						jsonObject = new JSONObject(httpResult);
						
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							retVal = "Error, posting photo.";
						}
					}else{
						retVal = "Error, no result.";
					}
				}
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startPostPhoto();
		}
		
		@Override
		protected void onPostExecute(String result){
			postBuzzResult(result);
		}
	}
}
