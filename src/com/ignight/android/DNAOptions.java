package com.ignight.android;

import java.util.ArrayList;

public class DNAOptions {
	public static ArrayList<DNAAtmosphere> AtmosphereOptions(){
		String[] _AtmosphereOptions = { "Cocktail Lounge", "Dive Bar", "Gay Bar", "Hipster Lounge","Live Music", 
				"Sports Bar", "Techno Club", "VIP Club" };
		int cursor = 0;
		ArrayList<DNAAtmosphere> options = new ArrayList<DNAAtmosphere>();
		
		for(String option:_AtmosphereOptions){
			options.add(new DNAAtmosphere(cursor, option));
			cursor++;
		}
		
		return options;
	}
	
	public static ArrayList<DNAMusic> MusicOptions(){
		ArrayList<DNAMusic> options = new ArrayList<DNAMusic>();
		int cursor = 0;
		String[] _MusicOptions = {"Classic Rock", "Classical", "Country", "Electronic", "Folk", "Indie Rock", "Jazz", "Latin",  "Oldies", 
				"Pop", "R&B", "Rap", "Reggae", "Rock", "Top 40" };
		
		for(String option:_MusicOptions){
			options.add(new DNAMusic(cursor, option));
			cursor++;
		}
		
		return options;
	}
	
	public static ArrayList<DNASpendingPreference> SpendingPreferenceOptions(){
		ArrayList<DNASpendingPreference> options = new ArrayList<DNASpendingPreference>();
		int cursor = 0;
		String[] _SpendingPreferenceOptions = { "Broke", "Getting By", "Doing Well", "Living The Dream" };
		
		for(String option:_SpendingPreferenceOptions){
			options.add(new DNASpendingPreference(cursor, option));
			cursor++;
		}
		
		return options;
	}
	
	public static ArrayList<DNAAge> AgeOptions(){
		ArrayList<DNAAge> options = new ArrayList<DNAAge>();
		int cursor = 0;
		String[] _AgeOptions = {"18-20","21-25","26-30","31-35","36-40","41+"};
		
		for(String option:_AgeOptions){
			options.add(new DNAAge(cursor, option));
			cursor++;
		}
		
		return options;
	}
}
