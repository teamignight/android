package com.ignight.android;

public class MessageGroupBuzz {
	private class NewBuzz{
		private String buzzText;
		private int cityId;
		private int userId;
		private int groupId;
		private String type;
		
		NewBuzz(String buzzText, int cityId, int userId, int groupId, String type){
			this.buzzText = buzzText;
			this.cityId = cityId;
			this.userId = userId;
			this.groupId = groupId;
			this.type = type;
		}
	}
	
	private NewBuzz msg;
	MessageGroupBuzz(String buzzText, int cityId, int userId, int groupId){
		this.msg = new NewBuzz(buzzText, cityId, userId, groupId, "addGroupBuzz");
	}
}
