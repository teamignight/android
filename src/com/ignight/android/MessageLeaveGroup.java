package com.ignight.android;

public class MessageLeaveGroup {
	private class NewLeaveGroup{
		private int cityId;
		private int userId;
		private int groupId;
		private String type;
		
		NewLeaveGroup(int userId, int groupId, int cityId){
			this.cityId = cityId;
			this.userId = userId;
			this.groupId = groupId;
			this.type = "leaveGroup";
		}
	}
	private NewLeaveGroup msg;
	MessageLeaveGroup(int userId, int groupId, int cityId){
		msg = new NewLeaveGroup(userId, groupId, cityId);
	}
}
