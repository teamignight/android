package com.ignight.android;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ActivityTrending extends ActivityBase {
	private LinearLayout btnTrendingMusic;
	private Button btnSwitchMap;
	private FragmentMusicFilter fragmentMusicFilter;
	private FragmentTransaction fragmentTransaction;
	private FragmentTrendingList fragmentTrendingList;
	private FragmentTrendingMap fragmentTrendingMap;
	private ImageView imgTabMusic;
	private ImageView imgTabUpvote;
	private ProgressDialog progressDialog;
	public TextView txtTabMusic;
	private TextView txtTabUpvote;
	private User user;

	public int filterValue;
	public int musicPreference;
	public double searchRadious;
	public boolean isSearching;
	public boolean upVoteOn;
	public boolean musicOn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_trending);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,R.layout.module_title_bar);
		
		if(app.RefreshTrending){
			app.RefreshTrending = false;
		}
		app.SaveListPosition = true;
		btnTrendingMusic = (LinearLayout)findViewById(R.id.btnTrendingMusic);

        user = CurrentUser;
        
        wireupHeader(user.getCity().getName(), "Trending");
        
        GPSTracker gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation())
        {
        	user.setLatitude(gpsTracker.latitude);
        	user.setLongitude(gpsTracker.longitude);
        }else{
        	user.setLatitude(UserLatitude);
        	user.setLongitude(UserLongitude);
        }
        
		app.Venues = null;
		app.UpdatedTrendingMap = false;
		
		if(app.LocalVenues == null){
			DataAdapter dataAdapter = new DataAdapter(this);
			app.LocalVenues = dataAdapter.GetVenues();
		}

		if(app.LocalVenues == null || app.LocalVenues.size() == 0){
			app.getVenues(user.getCity().getId());
		}
		
		fragmentTrendingList = new FragmentTrendingList();
        fragmentTrendingMap = new FragmentTrendingMap();
        fragmentMusicFilter = new FragmentMusicFilter();

        filterValue = -1;
        app.TrendingFilterShowing = false;
        app.TrendingMusicFilter = null;
        app.TrendingState = "Normal";
        app.TrendingType = "Trending";
        app.TrendingView = "ListView";
        
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.add(R.id.fragmentHolder, fragmentTrendingList);
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.commit();
        
        btnSwitchMap = (Button)findViewById(R.id.btnSwitchMap);
        btnSwitchMap.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(app.TrendingFilterShowing){
					app.TrendingFilterShowing = false;
					app.TrendingMusicFilter = null;
					app.TrendingState = "Normal";
					
					if(app.TrendingView.equals("MapView")){
						app.TrendingView = "ListView";
					}else{
						app.TrendingView = "MapView";
					}
					imgTabMusic.setImageResource(R.drawable.tab_icon_music_off);
					txtTabMusic.setTextColor(Color.parseColor("#6a6a6a"));
					imgTabUpvote.setImageResource(R.drawable.tab_icon_upvote_off);
					txtTabUpvote.setTextColor(Color.parseColor("#6a6a6a"));
					txtTabMusic.setText("Music");
					musicOn = false;
					app.TrendingMusicPref = -1;
				}
				trendingToggle(btnSwitchMap);
			}
		});
        
        imgTabMusic = (ImageView)findViewById(R.id.imgTabMusic);
        imgTabUpvote = (ImageView)findViewById(R.id.imgTabUpvote);
        txtTabMusic = (TextView)findViewById(R.id.txtTabMusic);
        txtTabUpvote = (TextView)findViewById(R.id.txtTabUpvote);
	}
	
	public void clearFilters(){
		filterValue = -1;
        app.TrendingFilterShowing = false;
        app.TrendingMusicFilter = null;
        app.TrendingState = "Normal";
        app.TrendingType = "Trending";
        app.TrendingView = "ListView";
        musicOn = false;
        upVoteOn = false;
        imgTabMusic.setImageResource(R.drawable.tab_icon_music_off);
		txtTabMusic.setTextColor(Color.parseColor("#6a6a6a"));
		imgTabUpvote.setImageResource(R.drawable.tab_icon_upvote_off);
		txtTabUpvote.setTextColor(Color.parseColor("#6a6a6a"));
		txtTabMusic.setText("Music");
	}
	
	@Override
	protected void onResume(){
		app.TrendingType = "trending";
		if(app.RefreshTrending && app.CalledFromVenueInfo && app.UpdatedTrendingVenue){
			app.UpdatedTrendingVenue = false;
			app.RefreshTrending = false;
			app.CalledFromVenueInfo = false;
			if(app.TrendingView.equals("MapView")){
				if(fragmentTrendingMap != null && fragmentTrendingMap.isVisible())
					fragmentTrendingMap.reloadMap();
			}else{
				if(fragmentTrendingList != null && fragmentTrendingList.isVisible())
					fragmentTrendingList.updateList();
			}
		}
		super.onResume();
	}
	
	@Override
	protected void onStop(){
		super.onStop();
		if(progressDialog != null && progressDialog.isShowing())
			progressDialog.dismiss();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event){
		if(keyCode == KeyEvent.KEYCODE_BACK){
			app.TrendingFilterShowing = true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	public void clearSearch(){
		fragmentTrendingList.clearSearch();
	}
	
	public void toggleMapButton(boolean value){
		isSearching = value;
		if(value){
			btnSwitchMap.setVisibility(View.GONE);
			imgTabMusic.setImageResource(R.drawable.tab_icon_music);
			imgTabUpvote.setImageResource(R.drawable.tab_icon_upvote);
			txtTabMusic.setTextColor(Color.parseColor("#a7a7a7"));
			txtTabUpvote.setTextColor(Color.parseColor("#a7a7a7"));
		} else{
			btnSwitchMap.setVisibility(View.VISIBLE);
			if(musicOn){
				imgTabMusic.setImageResource(R.drawable.tab_icon_music_active_green);
				txtTabMusic.setTextColor(Color.parseColor("#78c542"));
			} else {
				imgTabMusic.setImageResource(R.drawable.tab_icon_music_off);
				txtTabMusic.setTextColor(Color.parseColor("#6a6a6a"));
			}
			if(upVoteOn){
				imgTabUpvote.setImageResource(R.drawable.tab_icon_upvote_active_green);
				txtTabUpvote.setTextColor(Color.parseColor("#78c542"));
			}else{
				imgTabUpvote.setImageResource(R.drawable.tab_icon_upvote_off);
				txtTabUpvote.setTextColor(Color.parseColor("#6a6a6a"));
			}
			
		}
	}
	
	public void gotoAddVenue(View view){
		app.ShowReportVenue = true;
		app.ActiveVenue = new Venue();
		Intent intent;
    	intent = new Intent(this, ActivityVenue.class);
    	startActivity(intent);
	}
	
	public void filterMusic(int pos){
		filterValue = -1;
		musicPreference = pos;
		app.TrendingFilterShowing = false;
		app.TrendingUpvoteShowing = false;
		app.TrendingMusicFilter = DNAOptions.MusicOptions().get(pos);
		app.TrendingState = "MusicFiltered";
		
		if(app.TrendingView.equals("MapView")){
			app.TrendingView = "ListView";
		}else{
			app.TrendingView = "MapView";
		}
		
		imgTabMusic.setImageResource(R.drawable.tab_icon_music_active_green);
		txtTabMusic.setTextColor(Color.parseColor("#78c542"));
		musicOn = true;
		txtTabMusic.setText(DNAOptions.MusicOptions().get(pos).getName());
		imgTabUpvote.setImageResource(R.drawable.tab_icon_upvote);
		txtTabUpvote.setTextColor(Color.parseColor("#a7a7a7"));
		upVoteOn = false;
		
		trendingToggle(btnSwitchMap);
	}
	
	public void trendingToggle(View view){
		if(!app.TrendingView.equals("MapView")){
			app.TrendingView = "MapView";
			((Button)view).setText("Switch to List View");
			loadTrendingMap();
		}else{
			app.TrendingView = "ListView";
			((Button)view).setText("Switch to Map View");
			loadTrendingList();
		}
	}
	
	public void btnTrendingMusic_ClickEvent(View view){
		if(!app.TrendingView.equals("MapView") && isSearching){
			return;
		}
		
		if(app.TrendingFilterShowing){
			app.TrendingFilterShowing = false;;
			app.TrendingMusicFilter = null;
			
			if(app.TrendingView.equals("MapView")){
				app.TrendingView = "ListView";
			}else{
				app.TrendingView = "MapView";
			}
			trendingToggle(btnSwitchMap);

			if(!musicOn)
				txtTabMusic.setText("Music");
		}else{
			app.TrendingFilterShowing = true;
			
			fragmentTransaction = getSupportFragmentManager().beginTransaction();
			fragmentTransaction.replace(R.id.fragmentHolder, fragmentMusicFilter);
			fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			fragmentTransaction.addToBackStack(null);
			fragmentTransaction.commit();
			
			btnSwitchMap.setText("Clear Filter");
		}
	}
	
	public void btnTrendingActivity_ClickEvent(View view){
		if(!app.TrendingView.equals("MapView") && isSearching){
			return;
		}
		
		//0 = upvotes, 1 = downvotes, 2 = ?, -1 = all
		if(app.TrendingUpvoteShowing){
			app.TrendingUpvoteShowing = false;
			filterValue = -1;
			upVoteOn = false;
			app.TrendingState = "Normal";
		}else{
			app.TrendingUpvoteShowing = true;
			app.TrendingMusicPref = -1;
			filterValue = 0;
			musicOn = false;
			upVoteOn = true;
			app.TrendingMusicFilter = null;
			app.TrendingState = "UpvoteFiltered";
		}
		
		if(!app.TrendingView.equals("MapView")){
			fragmentTrendingList.updateList();
		}else{
			fragmentTrendingMap.reloadMap();
		}
		
		if(!app.TrendingUpvoteShowing){
			imgTabUpvote.setImageResource(R.drawable.tab_icon_upvote_off);
			txtTabUpvote.setTextColor(Color.parseColor("#6a6a6a"));
			imgTabMusic.setImageResource(R.drawable.tab_icon_music_off);
			txtTabMusic.setTextColor(Color.parseColor("#6a6a6a"));
			
		}else{
			imgTabUpvote.setImageResource(R.drawable.tab_icon_upvote_active_green);
			txtTabUpvote.setTextColor(Color.parseColor("#78C542"));
			imgTabMusic.setImageResource(R.drawable.tab_icon_music);
			txtTabMusic.setTextColor(Color.parseColor("#a7a7a7"));
			txtTabMusic.setText("Music");
		}
	}
	
	public void dismissProgress(){
		progressDialog.dismiss();
	}
	
	public void showProgress(String message){
		progressDialog = ProgressDialog.show(this, "", message, true, true);
	}
	
	public void clearSearch(View view){
		fragmentTrendingList.clearSearch();
	}
	
	private void loadTrendingMap(){
		fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.fragmentHolder, fragmentTrendingMap);
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
		app.SaveListPosition = false;
	}
	
	private void loadTrendingList(){
		fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.fragmentHolder, fragmentTrendingList);
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
	}
}
