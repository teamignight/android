package com.ignight.android;

public class MessageBuzz {
	private class NewBuzz{
		private String buzzText;
		private int userId;
		private int venueId;
		private String type;
		
		NewBuzz(String buzzText, int userId, int venueId, String type){
			this.buzzText = buzzText;
			this.userId = userId;
			this.venueId = venueId;
			this.type = type;
		}
	}
	
	private NewBuzz msg;
	MessageBuzz(String buzzText, int userId, int venueId){
		this.msg = new NewBuzz(buzzText, userId, venueId, "addBuzz");
	}
}
