package com.ignight.android;

public class MessageGroupResponse {
	private class GroupResponse{
		private boolean accept;
		private int cityId;
		private int groupId;
		private int userId;
		private String type; //respondToInvite
		
		GroupResponse(boolean accept, int cityId, int groupId, int userId, String type){
			this.accept = accept;
			this.cityId = cityId;
			this.groupId = groupId;
			this.userId = userId;
			this.type = type;
		}
	}
	private GroupResponse msg;
	MessageGroupResponse(boolean accept, int cityId, int groupId, int userId){
		this.msg = new GroupResponse(accept, cityId, groupId, userId, "respondToInvite");
	}
}
