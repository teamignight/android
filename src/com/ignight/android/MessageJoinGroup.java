package com.ignight.android;

public class MessageJoinGroup {
	private class NewJoinGroup{
		private int cityId;
		private int groupId;
		private int userId;
		private String type; //joinGroup
		
		NewJoinGroup(int cityId, int groupId, int userId, String type){
			this.cityId = cityId;
			this.groupId = groupId;
			this.userId = userId;
			this.type = type;
		}
	}
	private NewJoinGroup msg;
	MessageJoinGroup(int cityId, int groupId, int userId, String type){
		this.msg = new NewJoinGroup(cityId, groupId, userId, "joinGroup");
	}
}
