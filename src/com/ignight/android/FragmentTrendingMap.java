package com.ignight.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.squareup.picasso.Picasso;
import com.google.maps.android.ui.*;

public class FragmentTrendingMap extends android.support.v4.app.Fragment implements OnMarkerClickListener {
	private ActivityTrending trendingActivity;
	private Activity fragmentActivity;
	private View fragmentView;
	private GoogleMap googleMap;
	private ActivityGroup groupActivity;
	private AppBase app;
	private LinearLayout btnNearMe;
	private LinearLayout btnRefresh;
	private TextView txtMapCity;
	private HashMap<Marker, Venue> markerHash;
	private int resId;
	private Venue venue;
	private boolean nearMeShowing;
	private BufferedReader bufferReader;
	private HttpClient httpClient;
	private HttpGet httpGet;
	private HttpEntity httpEntity;
	private HttpResponse httpResponse;
	private String httpResult;
	private InputStream inputStream;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private StringBuilder stringBuilder;
	private User user;
	private ImageView imgMapCity;
	private ImageView imgMapNear;
	private ImageView imgMapRefresh;
	private LinearLayout btnMapCity;
	private float radius;
	private boolean cityShowing;
	
	private boolean canRefresh;
	private boolean initLoad;
	public boolean moveCamera;
	private double searchLong;
	private double searchLat;
	private boolean zoomedIn;
	private boolean overrideZoom;
	private boolean locationClicked = false;
	private boolean nearMeClicked = false;
	private boolean mapLoaded = false;
	private boolean loadMarkers = false;
	
	private static final float defaultRadius = 40233.6F; //25 miles
	
 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        fragmentView = inflater.inflate(R.layout.fragment_trending_map, container, false);
        
        if (fragmentActivity != null) {
	        app = ((AppBase)fragmentActivity.getApplication());
	        if(app.TrendingType.equalsIgnoreCase("group")){
	        	groupActivity = ((ActivityGroup)getActivity());
	        	user = groupActivity.CurrentUser;
	        }else{
	        	trendingActivity = ((ActivityTrending)getActivity());
	        	user = trendingActivity.CurrentUser;
	        }
        }
        
        if(app.UpdatedTrendingMap){
        	radius = app.MapRadius;
        	searchLat = app.MapSearchLat;
        	searchLong = app.MapSearchLong;
        	nearMeShowing = app.NearMeShowing;
        	overrideZoom = true;
        }else{
        	radius = defaultRadius;
        	app.MapZoom = 10;
        	getDefaultLatLong();
            nearMeShowing = false;
            overrideZoom = false;
        }
        moveCamera = true;
        canRefresh = false;
        initLoad = true;
        zoomedIn = false;
        
        googleMap = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.trending_map)).getMap();
        
        if(app != null && app.Venues != null){
        	if(googleMap != null){
        		googleMap.setMyLocationEnabled(true);
        		googleMap.setOnCameraChangeListener(new OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(CameraPosition pos) {
                    	//Log.d("Map", "camera changed called");
                    	if(app.MapIsTouched){
                    		initLoad = false;
                    	}
                    	if(!app.MapIsTouched && !initLoad){
                            cityShowing = false;
        					Picasso.with(fragmentActivity).load(R.drawable.refresh_active).noFade().into(imgMapRefresh);
                    	}
                    	
                    	if(locationClicked){
                    		locationClicked = false;
                    		updateNearMe(pos.target.longitude, pos.target.latitude);
                    	}
                    }
        		});
        		googleMap.setOnMyLocationChangeListener(new OnMyLocationChangeListener() {
					@Override
					public void onMyLocationChange(Location loc) {
						
						if(loadMarkers){
							loadMarkers = false;
							//Log.d("Map", "called loading markers");
							new GetMapVenuesTask().execute();
						}
						if(nearMeClicked){
							//Log.d("Map", "location changed for near me");
							nearMeClicked = false;
							//loadMarkers = true;
							updateNearMe(loc.getLongitude(), loc.getLatitude());
							new GetMapVenuesTask().execute();
						}
					}
				});
        		googleMap.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener() {
					@Override
					public boolean onMyLocationButtonClick() {
						locationClicked = true;
						loadMarkers = true;
						//new GetMapVenuesTask().execute();
						return false;
					}
				});
        		googleMap.setOnMapLoadedCallback(new OnMapLoadedCallback() {
					@Override
					public void onMapLoaded() {
						//Log.d("Map", "Map loaded");
						LatLng initView  = new LatLng(41.88414F, -87.63237900000001F);
						searchLat = 41.88414F;
			        	searchLong = -87.63237900000001F;
			        	
						mapLoaded = true;
						//loadMarkers = true;
						
						CameraPosition cameraPosition = new CameraPosition.Builder()
						.target(initView)
						.zoom(app.MapZoom)
						.build();
						googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
						new GetMapVenuesTask().execute();
					}
				});
        		
        		googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
					@Override
					public void onInfoWindowClick(Marker arg0) {
						app.CalledFromTrending = true;
						Venue v = markerHash.get(arg0);
						app.ActiveVenue = v;
						Intent intent = new Intent(fragmentActivity, ActivityVenue.class);
						startActivity(intent);
					}
				});
        		googleMap.getUiSettings().setZoomControlsEnabled(false);
        		googleMap.setOnMarkerClickListener(this);
        	}else{
        		trendingActivity.showMessage("Error, null google map");
        	}
        }
        
        txtMapCity = (TextView)fragmentView.findViewById(R.id.txtMapCity);
        txtMapCity.setText(app.getActiveUser().getCity().getName());
        imgMapCity = (ImageView)fragmentView.findViewById(R.id.imgMapCity);
        imgMapNear = (ImageView)fragmentView.findViewById(R.id.imgMapNear);
        imgMapRefresh = (ImageView)fragmentView.findViewById(R.id.imgMapRefresh);
        
        btnNearMe = (LinearLayout)fragmentView.findViewById(R.id.btnNearMe);
        btnNearMe.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(googleMap != null){
					//Log.d("Map", "clicked near me");
					if(user.getLatitude() == 0){
						nearMeClicked = true;
						googleMap.getMyLocation();
					}else{
						//Log.d("Map", "used users latlong");
						//loadMarkers = true;
						updateNearMe(user.getLongitude(), user.getLatitude());
						new GetMapVenuesTask().execute();
					}
				}
			}
		});
        
        btnMapCity = (LinearLayout)fragmentView.findViewById(R.id.btnMapCity);
        btnMapCity.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!cityShowing){
					cityShowing = true;
					nearMeShowing = app.NearMeShowing = false;
					app.UpdatedTrendingMap = false;
					zoomedIn = false;
					initLoad = true;
					radius = app.MapRadius = defaultRadius;
					app.MapZoomedIn = false;
					app.MapZoom = 10;
					
					searchLat = 41.88414F;
		        	searchLong = -87.63237900000001F;
		        	
		        	//loadMarkers = true;
					moveMap(new LatLng(searchLat, searchLong));
					new GetMapVenuesTask().execute();
					//getDefaultLatLong();
					//new GetMapVenuesTask().execute();
					
					Picasso.with(fragmentActivity).load(R.drawable.city_on).noFade().into(imgMapCity);
					Picasso.with(fragmentActivity).load(R.drawable.near_me_off).noFade().into(imgMapNear);
				}
			}
		});
        
        btnRefresh = (LinearLayout)fragmentView.findViewById(R.id.btnRefresh);
        btnRefresh.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				moveCamera = false;
				nearMeShowing = app.NearMeShowing = false;
				app.MapZoom = googleMap.getCameraPosition().zoom;
				app.MapZoomedIn = true;
				updateMapRadius();
				
				Picasso.with(fragmentActivity).load(R.drawable.city_off).noFade().into(imgMapCity);
				Picasso.with(fragmentActivity).load(R.drawable.near_me_off).noFade().into(imgMapNear);
				Picasso.with(fragmentActivity).load(R.drawable.refresh_off).noFade().into(imgMapRefresh);
				
				app.UpdatedTrendingMap = true;
			}
		});
        return fragmentView;
	}
 	
 	private void moveMap(LatLng initView){
 		CameraPosition cameraPosition = new CameraPosition.Builder()
		.target(initView)
		.zoom(app.MapZoom)
		.build();
		googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
 	}
 	
 	private void updateNearMe(double longitude, double latitude){
 		//Log.d("Map", "update near me called");
 		nearMeShowing = app.NearMeShowing = true;
		cityShowing = false;
		zoomedIn = false;
		initLoad = true;
		app.MapZoom = 13;
		radius = app.MapRadius = 2414.02f;
		searchLat = latitude;
		app.MapSearchLat = searchLat;
		searchLong = longitude;
		app.MapSearchLong = searchLong;
		app.UpdatedTrendingMap = true;
		moveMap(new LatLng(latitude, longitude));
		Picasso.with(fragmentActivity).load(R.drawable.city_off).noFade().into(imgMapCity);
		Picasso.with(fragmentActivity).load(R.drawable.near_me_on).noFade().into(imgMapNear);
 	}

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
	}
	
	@Override
 	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		fragmentActivity = activity;
	}
	
	@Override
	public void onDetach(){
		killMap();
		markerHash = null;
		super.onDetach();
	}
	
	public void onDestroyView(){
		killMap();
		super.onDestroyView();
	}
	
	@Override
	public boolean onMarkerClick(Marker arg0) {
		return false;
	}
	
	private void killMap(){
		android.support.v4.app.FragmentManager fm = getFragmentManager();
		
		SupportMapFragment mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.trending_map);
		if(mapFragment != null){
			fm.beginTransaction().remove(mapFragment).commit();
		}
	}	
	
	public void reloadMap(){
		new GetMapVenuesTask().execute();
	}
	
	public void updateMapRadius(){
		LatLng center;
		double distance;
		Location nearLocation;
		Location farLocation;
		VisibleRegion region;

		region = googleMap.getProjection().getVisibleRegion();
		nearLocation = new Location("");
		nearLocation.setLatitude(region.nearLeft.latitude);
		nearLocation.setLongitude(region.nearLeft.longitude);
		farLocation = new Location("");
		farLocation.setLatitude(region.nearRight.latitude);
		farLocation.setLongitude(region.nearRight.longitude);
		distance = nearLocation.distanceTo(farLocation);
		radius = Double.valueOf(distance/2).floatValue();
		
		center = googleMap.getCameraPosition().target;
		searchLat = center.latitude;
		searchLong = center.longitude;
		app.MapSearchLat = searchLat;
		app.MapSearchLong = searchLong;
		app.MapRadius = radius;

		new GetMapVenuesTask().execute();
	}
	
	private void getDefaultLatLong(){
		if(app.Venues == null)
			app.Venues = new ArrayList<Venue>();
		
		if(app.Venues.size() == 1){
        	searchLat = app.Venues.get(0).getLatitude();
        	searchLong = app.Venues.get(0).getLongitude();
        }else if(app.Venues.size() > 1){
        	double maxLat = 0;
    		double maxLong = 0;
    		double minLat = 0;
    		double minLong = 0;
    		
        	for(int j = 0; j < app.Venues.size(); j++){
        		if( j == 25){
        			break;
        		}
        		venue = app.Venues.get(j);
        		if(minLong == 0 || minLong > venue.getLongitude())
        			minLong = venue.getLongitude();
        		if(maxLong == 0 || maxLong < venue.getLongitude())
        			maxLong = venue.getLongitude();
        		if(minLat == 0 || minLat > venue.getLatitude())
        			minLat = venue.getLatitude();
        		if(maxLat == 0 || maxLat < venue.getLatitude())
        			maxLat = venue.getLatitude();
        	}
        	
        	searchLat = ((maxLat + minLat)/2);
        	searchLong = ((maxLong + minLong)/2);
        }else{
        	searchLat = 41.88414F;
        	searchLong = -87.63237900000001F;
        }
	}
	
	private void loadMapMarkers(ArrayList<Venue> venues){
		double maxLat = 0;
		double maxLong = 0;
		double minLat = 0;
		double minLong = 0;
		Log.d("Map", "loading markers");
		markerHash = new HashMap<Marker, Venue>();
		Marker marker;
		
		googleMap.clear();
		
    	for(int j = 0; j < venues.size(); j++){
    		if( j == 25){
    			break;
    		}
    		
    		venue = venues.get(j);
    		
    		if(venue.getUserBarColor() == 0){
				resId = R.drawable.map_pin_4;
			}else if(venue.getUserBarColor() == 1){
				resId = R.drawable.map_pin_3;
			}else if(venue.getUserBarColor() == 2){
				resId = R.drawable.map_pin_2;
			}else if(venue.getUserBarColor() == 3){
				resId = R.drawable.map_pin_1;
			}else{
				resId = R.drawable.map_pin_0;
			}
    		
    		BitmapDrawable bd = (BitmapDrawable) getResources().getDrawable(R.drawable.map_pin_0);
    		if(app.TrendingType.equalsIgnoreCase("group"))
    			bd = (BitmapDrawable) getResources().getDrawable(R.drawable.map_pin_0);
    		else
    			bd = (BitmapDrawable) getResources().getDrawable(resId);
    		Bitmap original = bd.getBitmap();
    		Bitmap resized = Bitmap.createScaledBitmap(original, original.getWidth() / 2, original.getHeight() / 2, false);
    		
    		if(app.TrendingType.equalsIgnoreCase("group")){
	    		IconGenerator iconGenerator = new IconGenerator(groupActivity);
	    		if(venue.getUserVenueValue() == 0)
	    			iconGenerator.setTextAppearance(R.style.IgNight_Theme_Pin_Neutral);
	    		else if(venue.getUserVenueValue() > 0)
	    			iconGenerator.setTextAppearance(R.style.IgNight_Theme_Pin_Positive);
	    		else
	    			iconGenerator.setTextAppearance(R.style.IgNight_Theme_Pin_Negative);
	    		iconGenerator.setBackground(new BitmapDrawable(getResources(), resized));
	    		resized = iconGenerator.makeIcon((venue.getUserVenueValue() > 0 ? "+" : "") + (int)venue.getUserVenueValue());
    		}
    		
    		marker = googleMap.addMarker(new MarkerOptions()
    		.position(new LatLng(venue.getLatitude(), venue.getLongitude()))
    		.title(venue.getName())
    		.icon(BitmapDescriptorFactory.fromBitmap(resized)));
    		
    		markerHash.put(marker, venue);
    		
    		if(minLong == 0 || minLong > venue.getLongitude())
    			minLong = venue.getLongitude();
    		if(maxLong == 0 || maxLong < venue.getLongitude())
    			maxLong = venue.getLongitude();
    		if(minLat == 0 || minLat > venue.getLatitude())
    			minLat = venue.getLatitude();
    		if(maxLat == 0 || maxLat < venue.getLatitude())
    			maxLat = venue.getLatitude();
    	}

    	LatLng initView;
    	if(venues.size() > 0){
    		if(venues.size() == 1){
    			initView = new LatLng(venues.get(0).getLatitude(), venues.get(0).getLongitude());
    		}else{
    			initView = new LatLng(((maxLat + minLat)/2), ((maxLong + minLong)/2));
    		}
    	}else{
    		if(nearMeShowing)
    			initView = new LatLng(user.getLatitude(), user.getLongitude());
    		else
    			initView = new LatLng(41.88414F, -87.63237900000001F);
    		
    		if(app.TrendingType.equalsIgnoreCase("group")){
    			groupActivity.showMessage("No More Results");
    		}else{
        		if(trendingActivity.upVoteOn){
        			if(zoomedIn || overrideZoom){
        				trendingActivity.showMessage("You haven't upvoted anything yet today in the current map area.");
        			}else{
        				trendingActivity.showMessage("You haven't upvoted anything yet today.");
        			}
        		}else if(trendingActivity.musicOn){
        			if(zoomedIn || overrideZoom){
        				trendingActivity.showMessage("No "+trendingActivity.txtTabMusic.getText().toString()+" venues trending yet today in the current map area.");
        			}else{
        				trendingActivity.showMessage("No "+trendingActivity.txtTabMusic.getText().toString()+" venues trending yet today.");
        			}
        		}else{
        			trendingActivity.showMessage("No More Results");
        		}
    		}
    	}
    	/*overrideZoom = false;
		if(!zoomedIn){
			zoomedIn = true;
			CameraPosition cameraPosition = new CameraPosition.Builder()
				.target(initView)
				.zoom(app.MapZoom)
				.build();
			googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
		} else if(moveCamera) {
			googleMap.moveCamera(CameraUpdateFactory.newLatLng(initView));
		} else {
			moveCamera = true;
		}*/
	}
	
	private void showMessage(String msg){
		if(app.TrendingType.equalsIgnoreCase("group"))
			groupActivity.showProgress(msg);
		else
			trendingActivity.showProgress(msg);
	}
	
	private void startGetMapVenues(){
		showMessage("Getting venues...");
	}
	
	private void getMapVenuesResult(String result){
		JSONObject jObject;
		ArrayList<Venue> venues;
		
		if(result.equals("success")){
			venues = new ArrayList<Venue>();
			try{
				for(int i = 0; i < jsonArray.length(); i++){
					jObject = jsonArray.getJSONObject(i);
					venue = new Venue(jObject.getInt("venueId"), jObject.getString("venueName"), 0, 0);
					venue.setUserInput(jObject.getInt("userInput"));
					if(jObject.has("userBarColor"))
						venue.setUserBarColor(jObject.getInt("userBarColor"));
					if(jObject.has("userVenueValue"))
						venue.setUserVenueValue(jObject.getDouble("userVenueValue"));
					venue.setLatitude(jObject.getDouble("latitude"));
					venue.setLongitude(jObject.getDouble("longitude"));
					venues.add(venue);
				}
				app.Venues = venues;
				loadMapMarkers(venues);
			}catch(JSONException e){
				showMessage("Error parsing venues: " + e.getMessage());
			}
		}else{
			showMessage("Error, could not load local venues.");
		}
	}
	
	private class GetMapVenuesTask extends AsyncTask<Void, Void, String>{
		
		@Override
		protected String doInBackground(Void... args) {
			String httpString = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				httpClient = new CustomHttpClient(fragmentActivity);
				if(app.TrendingState.equals("MusicFiltered")){
					app.TrendingMusicPref = trendingActivity.musicPreference;
				}
				if(app.TrendingType.equalsIgnoreCase("group")){
					httpString = getString(R.string.api_base) + getString(R.string.api_group_trending) + app.ActiveGroup.getGroupId() + "/?userId="+app.getActiveUser().getId();
				}else{
					if(app.TrendingState.equalsIgnoreCase("UpvoteFiltered")){
						httpString = getString(R.string.api_base) + getString(R.string.api_trending) + app.getActiveUser().getId() + "/?upvote_filter=true&count=20";
					}else if(app.TrendingState.equals("MusicFiltered")){
						app.TrendingMusicPref = trendingActivity.musicPreference;
						httpString = getString(R.string.api_base) + getString(R.string.api_trending) + app.getActiveUser().getId() + "/?music_filter="+app.TrendingMusicPref+"&count=20";
					}else{
						httpString = getString(R.string.api_base) + getString(R.string.api_trending) + app.getActiveUser().getId() + "/?count=20";
					}
					httpString += "&search_radius="+radius+"&latitude="+searchLat+"&longitude="+searchLong;
				}
				//Log.d("Map", httpString);
				httpGet = new HttpGet(httpString);
				httpGet.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpGet.setHeader("PLATFORM","Android");
				httpGet.setHeader("USER_ID",""+app.getActiveUser().getId());
				//Log.d("MapUrl",getString(R.string.api_base) + getString(R.string.api_user) + "?type=trendingList&musicPreference="+app.TrendingMusicPref+"&searchRadius="+radius+"&latitude="+searchLat+"&longitude="+searchLong+"&trendingFilter="+trendingActivity.filterValue+"&trendingVenueStartIndex=0&trendingNoOfVenues=25&userId="+user.getId());
				httpResponse = httpClient.execute(httpGet);
				httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();
				bufferReader = new BufferedReader(new InputStreamReader(inputStream));
				stringBuilder = new StringBuilder();
				
				while((singleLine = bufferReader.readLine()) != null){
					stringBuilder.append(singleLine);
				}
				
				httpResult = stringBuilder.toString();
				inputStream.close();
				jsonObject = new JSONObject(httpResult);

				if(jsonObject.getBoolean("res")){
					retVal = "success";
					jsonArray = jsonObject.getJSONArray("body");
				}else{
					retVal = "failed";
				}
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			if(app.TrendingType.equalsIgnoreCase("group"))
				groupActivity.hideProgress();
			else
				trendingActivity.dismissProgress();
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startGetMapVenues();
		}
		
		@Override
		protected void onPostExecute(String result){
			getMapVenuesResult(result);
		}
	}
}
