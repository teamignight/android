package com.ignight.android;

public class MessageFlagGroup {
	private class NewItem{
		private int groupId;
		private int userId;
		
		NewItem(int groupId, int userId){
			this.groupId = groupId;
			this.userId = userId;
		}
	}
	
	private NewItem msg;
	
	MessageFlagGroup(int groupId, int userId){
		this.msg = new NewItem(groupId, userId);
	}
}
