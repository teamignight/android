package com.ignight.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class FragmentGroupInbox extends android.support.v4.app.Fragment{
	private boolean accept;
	private ActivityGroups activityGroups;
	private String acceptedGroup;
	private AppBase app;
	private BufferedReader bufferReader;
	private LinearLayout emptyInboxView;
	private AdapterInbox inboxAdapter;
	private Activity fragmentActivity;
	private Gson gson;
	private HttpClient httpClient;
	private HttpGet httpGet;
	private HttpEntity httpEntity;
	private HttpPost httpPost;
	private HttpResponse httpResponse;
	private String httpResult;
	private ArrayList<Inbox> inboxItems;
	private InputStream inputStream;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private ListView listRequest;
	private ProgressDialog progressDialog;
	private StringBuilder stringBuilder;
	private User user;
	private View view;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_group_inbox, container, false);
        
        activityGroups = ((ActivityGroups)getActivity());
        user = activityGroups.CurrentUser;
        
        listRequest = (ListView)view.findViewById(R.id.listInbox);
		listRequest.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				activityGroups.app.ActiveGroup = new Group();
				activityGroups.app.ActiveGroup.setGroupId(inboxItems.get(position).getGroupId());
				activityGroups.app.ActiveGroup.setGroupName(inboxItems.get(position).getGroupName());
				activityGroups.app.ActiveGroup.setWasInvited(true);
				Intent intent = new Intent(fragmentActivity, ActivityGroupDetails.class);
				startActivity(intent);
			}
		});
		emptyInboxView = (LinearLayout)view.findViewById(R.id.emptyInboxView);
        
		activityGroups.highlightInbox();
		
		if (fragmentActivity != null) {
        	app = ((AppBase)fragmentActivity.getApplication());
        }
		app.RefreshInbox = false;
		new GetInboxTask().execute();
		
        return view;
	}
	
	@Override
	public void onResume(){
		super.onResume();
		if(app.RefreshInbox){
			app.RefreshInbox = false;
			new GetInboxTask().execute();
		}
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		fragmentActivity = activity;
	}
	
	public void confirmGroupInvite(View view){
		accept = true;
		acceptedGroup = view.getTag().toString();
		new GroupResponseTask().execute();
	}
	
	public void denyGroupInvite(View view){
		accept = false;
		acceptedGroup = view.getTag().toString();
		new GroupResponseTask().execute();
	}
	
	private void startLoadingInbox(){
		activityGroups.showProgress("Loading group request...");
	}
	
	private void loadInboxResult(String result){
		int counter = 0;
		JSONObject jObject;
		Inbox inboxItem;
		
		if(result.equals("success")){
			try{
				inboxItems = new ArrayList<Inbox>();
				jsonArray = jsonObject.getJSONArray("body");
				for(int i = 0; i < jsonArray.length(); i++){
					jObject = jsonArray.getJSONObject(i);
					inboxItem = new Inbox();
					inboxItem.setGroupId(jObject.getInt("groupId"));
					inboxItem.setGroupName(jObject.getString("groupName"));
					inboxItem.setUserName(jObject.getString("userName"));
					if(jObject.has("userPicture"))
						inboxItem.setUserPicture(jObject.getString("userPicture"));
					else
						inboxItem.setUserPicture(app.DefaultChatUrl);
					inboxItem.setMessageType("IncomingGroupInvites");
					inboxItem.setDateStamp(jObject.getLong("timestamp"));
					inboxItems.add(inboxItem);
					counter++;
				}
				inboxAdapter = new AdapterInbox(fragmentActivity, R.layout.list_inbox, inboxItems);
				listRequest.setAdapter(inboxAdapter);
				
				if(counter > 0){
					emptyInboxView.setVisibility(View.GONE);
				}else{
					listRequest.setVisibility(View.GONE);
				}
			}catch(JSONException e){
				activityGroups.showMessage("Error parsing messages: " + e.getMessage());
			}
		}else{
			activityGroups.showMessage("Error, could not get messages: " + result);
		}
	}
	
	private class GetInboxTask extends AsyncTask<String, Void, String>{

		@Override
		protected String doInBackground(String... args) {
			String retVal = "";
			String singleLine = "";
			
			try{
				httpClient = new CustomHttpClient(fragmentActivity);
				httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_groups_inbox) + app.getActiveUser().getId());
				httpGet.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpGet.setHeader("PLATFORM","Android");
				httpGet.setHeader("USER_ID",""+app.getActiveUser().getId());
				httpResponse = httpClient.execute(httpGet);
				httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();
				bufferReader = new BufferedReader(new InputStreamReader(inputStream));
				stringBuilder = new StringBuilder();
				
				while((singleLine = bufferReader.readLine()) != null){
					stringBuilder.append(singleLine);
				}
				
				httpResult = stringBuilder.toString();
				inputStream.close();
				jsonObject = new JSONObject(httpResult);

				if(jsonObject.getBoolean("res")){
					retVal = "success";
				}else{
					retVal = "failed";
				}
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			activityGroups.hideProgress();
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startLoadingInbox();
		}
		
		@Override
		protected void onPostExecute(String result){
			loadInboxResult(result);
		}
	}
	
	private void startGroupResponse(){
		activityGroups.showProgress("Updating...");
	}
	
	private void groupResponseResult(String result){
		Group newGroup;
		Inbox inboxItem = null;
		
		if(result.equals("success")){
			if(inboxAdapter != null){
				for(int i = 0; i < inboxAdapter.getCount(); i++){
					inboxItem = inboxAdapter.getItem(i);
					if(inboxItem.getGroupId() == Integer.parseInt(acceptedGroup)){
						inboxAdapter.remove(inboxItem);
					}
				}
				if(inboxAdapter != null)
					inboxAdapter.notifyDataSetChanged();
			}
			
			if(accept){
				activityGroups.showMessage("Accepted group invite.");
				if(inboxItem != null){
					newGroup = new Group();
					newGroup.setGroupId(inboxItem.getGroupId());
					newGroup.setGroupName(inboxItem.getGroupName());
					newGroup.setGroupDescription("");
				}
			}
			else
				activityGroups.showMessage("Denied group invite.");
		}else{
			activityGroups.showMessage("Error, could not process request");
		}
	}
	
	private class GroupResponseTask extends AsyncTask<Void, Void, String>{
		@Override
		protected String doInBackground(Void... args) {
			String json = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				gson = new GsonBuilder().create();
				MessageGroupResponse groupResponse = new MessageGroupResponse(accept, user.getCity().getId(), Integer.parseInt(acceptedGroup), user.getId());
				
				json = gson.toJson(groupResponse);
				
				StringEntity stringEntity = new StringEntity(json);
				httpClient = new CustomHttpClient(fragmentActivity);
				httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_group_invite_respond) + Integer.parseInt(acceptedGroup));
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID",""+app.getActiveUser().getId());
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					//Log.d("Invite", httpResult);
					if(httpResult != null){
						jsonObject = new JSONObject(httpResult);
						
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							retVal = "Error, could not join group.";
						}
					}else{
						retVal = "Error, no result.";
					}
				}
			}catch(ClientProtocolException e){
				retVal = "Client Protocal Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "IO Error, " + e.getMessage();
			
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			activityGroups.hideProgress();
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startGroupResponse();
		}
		
		@Override
		protected void onPostExecute(String result){
			groupResponseResult(result);
		}
	}
}
