package com.ignight.android;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

public class ActivityPhotoBrowser extends ActivityBase implements LoaderManager.LoaderCallbacks<Cursor>{
	private AdapterPhotoBrowser adapter;
	private Cursor cursor;
	private GridView grdLocalPhotos;
	private static final int URL_LOADER = 0;
	
	final Uri sourceUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_photo_browser);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,R.layout.module_title_bar);
		
		wireupHeader("Select Photo", "");
		
		hideHeaderButton();
		
		grdLocalPhotos = (GridView)findViewById(R.id.grdLocalPhotos);
		String[] from = { MediaStore.MediaColumns.DATA };
		int[] to = { R.id.imgBuzzPhoto };
		adapter = new AdapterPhotoBrowser(this, R.layout.grid_photos, cursor, from, to, 0);
		grdLocalPhotos.setAdapter(adapter);
		grdLocalPhotos.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Cursor selectedCursor = adapter.getCursor();
				selectedCursor.moveToPosition(position);
				String selectedUrl = selectedCursor.getString(selectedCursor.getColumnIndex(MediaStore.Images.Media.DATA));
				app.LocalImagePath = selectedUrl;
				app.AddBuzzPhoto = false;
				approvePhoto();
			}
			
		});
		getSupportLoaderManager().initLoader(URL_LOADER, null, this);
	}
	
	@Override
	public void onResume(){
		super.onResume();
		if(app.AddBuzzPhoto)
			finish();
	}
	
	@Override
	public Loader<Cursor> onCreateLoader(int loaderID, Bundle bundle)
	{
	    switch (loaderID) {
	        case URL_LOADER:
	        	String[] projection = new String[]{MediaStore.Images.ImageColumns._ID, MediaStore.Images.ImageColumns.DATA};
	        	return new CursorLoader(this, MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, MediaStore.Images.ImageColumns.DATE_TAKEN);
	        default:
	            // An invalid id was passed in
	            return null;
	    }
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		adapter.swapCursor(cursor);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		adapter.swapCursor(null);
	}

	private void approvePhoto(){
		Intent intent = new Intent(this, ActivityApprovePhoto.class);
		startActivity(intent);
	}
}
