package com.ignight.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;

public class FragmentTrendingList extends android.support.v4.app.Fragment {
	private AppBase app;
	private Button btnAddNewVenue;
	private BufferedReader bufferReader;
	private boolean buttonHidden;
	private boolean didSearch;
	private View footerViewSearch;
	private Activity fragmentActivity;
	private ActivityGroup groupActivity;
	private View headerView;
	private HttpClient httpClient;
	private HttpGet httpGet;
	private HttpEntity httpEntity;
	private HttpResponse httpResponse;
	private String httpResult;
	private InputStream inputStream;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private ListView listVenues;
	private boolean loadingMore;
	private boolean loadingSearchResults;
	private int resultSet;
	private String searchQuery;
	private boolean searching;
	private StringBuilder stringBuilder;
	private ActivityTrending trendingActivity;
	private AdapterTrending trendingAdapter;
	private TextView txtClearSearch;
	private EditText txtTrendingSearch;
	private TextView txtTrendingSearchText;
	private ArrayList<Venue> trendingVenues;
	private boolean updatedMap;
	private User user;
	private Venue venue;
	private SwipeRefreshLayout swipeContainer;
	
	private int counter = 15;
	private final int limit = 15;
	private int offset = 0;
	private boolean loadMore = false;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View view = inflater.inflate(R.layout.fragment_trending_list, container, false);
        
        listVenues = (ListView)view.findViewById(R.id.trendingVenues);
        resultSet = 25;
        
        footerViewSearch = inflater.inflate(R.layout.list_search_footer, null, false);
        headerView = inflater.inflate(R.layout.list_trending_header, null, false);
        
        txtTrendingSearchText = (TextView)footerViewSearch.findViewById(R.id.txtTrendingSearchText);
        btnAddNewVenue = (Button)footerViewSearch.findViewById(R.id.btnAddNewVenue);
        
        txtClearSearch = (TextView)headerView.findViewById(R.id.txtClearSearch);
        txtTrendingSearch = (EditText)headerView.findViewById(R.id.txtTrendingSearch);
        
        if (fragmentActivity != null) {
        	app = ((AppBase)fragmentActivity.getApplication());
        	if(app.TrendingType.equalsIgnoreCase("group")){
        		if(app.ActiveGroup != null && app.DirtyGroup == app.ActiveGroup.getGroupId()){
        			app.TrendingType = "trending";
        			showMessage("You have removed this group.");
        			Intent intent = new Intent(fragmentActivity, ActivityTrending.class);
        			startActivity(intent);
        		}
            	groupActivity = ((ActivityGroup)getActivity());
            	user = groupActivity.CurrentUser;
            	txtTrendingSearch.setHint("Search for Venues");
            }else{
            	trendingActivity = ((ActivityTrending)getActivity());
            	user = trendingActivity.CurrentUser;
            	
            	updatedMap = app.UpdatedTrendingMap;
            }
	        app.ChangedVenues = new ArrayList<Venue>();
	        
	        if(user.getLatitude() == 0){
	        	user.setLatitude(-87.6500);
	        	user.setLongitude(41.8500);
	        	//showMessage("0 latitude");
	        }
		}
        
        trendingVenues = null;
        trendingAdapter = null;
        searching = false;
        
        swipeContainer = (SwipeRefreshLayout)view.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new OnRefreshListener() {
			@Override
			public void onRefresh() {
				counter = 15;
				if(app.TrendingType.equalsIgnoreCase("venue")){
					updatedMap = false;
					app.MapZoomedIn = false;
					app.UpdatedTrendingMap = false;
					trendingActivity.clearFilters();
				}
				updateList();
			}
		});
        swipeContainer.setColorScheme(android.R.color.holo_blue_dark, android.R.color.holo_green_dark, android.R.color.holo_orange_dark, android.R.color.holo_red_dark);
        txtClearSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				searching = false;
				txtTrendingSearch.setText("");
				updateList();
			}
		});
        txtTrendingSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                	filterVenues(txtTrendingSearch.getText().toString());
                    return true;
                }
                return false;
            }
        });
        
        listVenues.addFooterView(footerViewSearch);
        listVenues.addHeaderView(headerView);
        listVenues.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            	if(position > trendingVenues.size())
            		return;
            	if(trendingVenues.size() > 0)
            		position = position - 1;
            	Venue venue = trendingVenues.get(position);
            	app.ActiveVenue = venue;
            	app.CalledFromTrending = true;
            	if(app.TrendingType.equalsIgnoreCase("group")){
            		app.CalledFromTrendingType = "group";
            	}else{
            		app.CalledFromTrendingType = "venue";
            	}
            	loadVenue();
            }
        });
        btnAddNewVenue.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View button) {
				if(loadMore){
					counter = counter + limit;
					new GetTrendingVenuesTask().execute(String.valueOf(0), String.valueOf(50));
				}else{
					app.CalledFromTrending = true;
					if(app.TrendingType.equalsIgnoreCase("group")){
						groupActivity.gotoAddVenue(button);
					}else{
						trendingActivity.gotoAddVenue(button);
					}
				}
			}
		});
        
        new GetTrendingVenuesTask().execute(String.valueOf(0), String.valueOf(50));
        return view;
    }
	
	@Override
	public void onResume(){
		super.onResume();
	}
	
	@Override
	public void onDetach(){
		super.onDetach();
	}
	
	public void onDestroyView(){
		super.onDestroyView();
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		fragmentActivity = activity;
	}
	
	public void updateList(){
		resultSet = 25;
		if(trendingAdapter != null)
			trendingAdapter.clear();
		trendingAdapter = null;
		trendingVenues = null;
		
		new GetTrendingVenuesTask().execute(String.valueOf(0), String.valueOf(50));
	}
	
	public void clearSearch(){
		counter = 15;
		loadMore = true;
		txtTrendingSearch.setText("");
	}
	
	private void filterVenues(String query){
		searchQuery = query;
		if(loadingSearchResults){
			return;
		}
		
		if(query.length() > 0){
			if(!buttonHidden){
				if(!app.TrendingType.equalsIgnoreCase("group")){
					trendingActivity.toggleMapButton(true);
				}else{
					groupActivity.toggleMapButton(true);
				}
			}
			buttonHidden = true;
		}else if(query.length() == 0 && !didSearch){
			if(!app.TrendingType.equalsIgnoreCase("group") && buttonHidden){
				trendingActivity.toggleMapButton(false);
			}else{
				groupActivity.toggleMapButton(false);
			}
			buttonHidden = false;
		}
		
		if(query.length() == 0 && didSearch){
			searching = false;
			didSearch = false;
		}else if(query.length() > 0){
			didSearch = true;
			searching = true;
		}
		
		if(trendingAdapter != null)
			trendingAdapter.clear();
		trendingAdapter = null;
		trendingVenues = null;
		counter = 15;
		new GetTrendingVenuesTask().execute(String.valueOf(0), String.valueOf(50));
	}
	
	private void loadVenue(){
		Intent intent;
    	intent = new Intent(fragmentActivity, ActivityVenue.class);
    	startActivity(intent);
	}
	
	private void showMessage(String msg){
		if(app.TrendingType.equalsIgnoreCase("group")){
			groupActivity.showMessage(msg);
		}else{
			trendingActivity.showMessage(msg);
		}
	}
	
	private void getTrendingVenuesResult(String result){
		int length;
		JSONObject jObject;
		String enteredSearch;
		//Log.d("Trending", "Save position: " + app.SaveListPosition + ", counter: " + counter);
		if(result.equals("success")){
			trendingVenues = new ArrayList<Venue>();
			app.Venues = new ArrayList<Venue>();
			
			try{
				length = jsonArray.length();
				
				if((resultSet == 25 || resultSet < 25) && length < 25)
					resultSet = length;
				else if(jsonArray.length() < 25)
					resultSet += length;
				else
					resultSet += 25;
				
				trendingAdapter = new AdapterTrending(fragmentActivity, R.layout.list_trending, trendingVenues, user, app);
				listVenues.setAdapter(trendingAdapter);
				
				for(int i = 0; i < length; i++){
					jObject = jsonArray.getJSONObject(i);
					venue = new Venue(jObject.getInt("venueId"), jObject.getString("venueName"), 0, 0);
					venue.setUserInput(jObject.getInt("userInput"));
					if(jObject.has("userBarColor"))
						venue.setUserBarColor(jObject.getInt("userBarColor"));
					venue.setLatitude(jObject.getDouble("latitude"));
					venue.setLongitude(jObject.getDouble("longitude"));
					if(jObject.has("userVenueValue"))
						venue.setUserVenueValue(jObject.getDouble("userVenueValue"));
					else
						venue.setUserVenueValue(-9999);
					app.Venues.add(venue);
					trendingAdapter.add(venue);
				}
				
				trendingAdapter.notifyDataSetChanged();
				if(app.TrendingType.equalsIgnoreCase("trending") && trendingActivity != null){
					if((searching || trendingActivity.musicOn || trendingActivity.upVoteOn || updatedMap) && app.Venues.size() > 0){
						txtTrendingSearchText.setVisibility(View.VISIBLE);
						txtTrendingSearchText.setText("No More Results");
					}else if (trendingActivity.musicOn && app.Venues.size() == 0){
						txtTrendingSearchText.setVisibility(View.VISIBLE);
						if(app.MapZoomedIn){
							txtTrendingSearchText.setText("No "+trendingActivity.txtTabMusic.getText().toString()+" venues trending yet today in the current map area.");
						}else{
							txtTrendingSearchText.setText("No "+trendingActivity.txtTabMusic.getText().toString()+" venues trending yet today.");
						}
					}else if(trendingActivity.upVoteOn && app.Venues.size() == 0){
						txtTrendingSearchText.setVisibility(View.VISIBLE);
						if(app.MapZoomedIn){
							txtTrendingSearchText.setText("You haven't upvoted anything yet today in the current map area.");
						}else{
							txtTrendingSearchText.setText("You haven't upvoted anything yet today.");
						}
					}else if(searching && app.Venues.size() == 0){
						txtTrendingSearchText.setVisibility(View.GONE);
					}else{
						txtTrendingSearchText.setVisibility(View.VISIBLE);
						txtTrendingSearchText.setText("No more results.");
					}
				}else{
					if(app.Venues.size() == 0){
						if(searching){
							txtTrendingSearchText.setVisibility(View.GONE);
						}else{
							txtTrendingSearchText.setVisibility(View.VISIBLE);
							txtTrendingSearchText.setText(Html.fromHtml("No voting activity in <b>\""+app.ActiveGroup.getGroupName()+"\"</b> yet today."));
						}
					}else{
						txtTrendingSearchText.setVisibility(View.VISIBLE);
						txtTrendingSearchText.setText("No More Results");
					}
				}
				
				if(searching){
					btnAddNewVenue.setText("Add Venue");
					btnAddNewVenue.setVisibility(View.VISIBLE);
					loadMore = false;
				}
				
				if(app.SaveListPosition)
					listVenues.setSelection(counter - 15);
				
				if((counter - length) == 0){
					btnAddNewVenue.setText("Load More");
					txtTrendingSearchText.setVisibility(View.GONE);
					btnAddNewVenue.setVisibility(View.VISIBLE);
					loadMore = true;
				}else{
					btnAddNewVenue.setText("Add Venue");
					if(searching){
						btnAddNewVenue.setVisibility(View.VISIBLE);
					}else{
						btnAddNewVenue.setVisibility(View.GONE);
					}
					loadMore = false;
				}

				loadingSearchResults = false;
				enteredSearch = txtTrendingSearch.getText().toString();
				if(searchQuery != null && searchQuery.length() > 0 && !searchQuery.equalsIgnoreCase(enteredSearch)){
					filterVenues(enteredSearch);
				}
				if(searching){
					txtTrendingSearch.requestFocus();
				}
			}catch(JSONException e){
				showMessage("Error parsing venues: " + e.getMessage());
			}
		}else{
			showMessage("Error, could not get trending venues: " + result);
		}
		//Log.d("Trending", "counter: " + counter);
		app.SaveListPosition = true;
		loadingMore = false;
		swipeContainer.setRefreshing(false);
	}
	
	private void startGetTrendingVenues(){
		loadingMore = true;
		loadingSearchResults = true;
		if(listVenues.getFooterViewsCount() == 0)
			listVenues.addFooterView(footerViewSearch);
		
		//footerViewSearch.setVisibility(View.VISIBLE);
		btnAddNewVenue.setVisibility(View.GONE);
		txtTrendingSearchText.setVisibility(View.VISIBLE);
		txtTrendingSearchText.setText("Loading venues...");
		if(searching){
			updatedMap = app.UpdatedTrendingMap = false;
			txtClearSearch.setVisibility(View.VISIBLE);
		}else{
			if(!app.TrendingType.equalsIgnoreCase("group")){
				trendingActivity.toggleMapButton(false);
				buttonHidden = false;
			}
			txtClearSearch.setVisibility(View.INVISIBLE);
		}
	}
	
	private class GetTrendingVenuesTask extends AsyncTask<String, Void, String>{

		@Override
		protected String doInBackground(String... args) {
			String httpString = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				httpClient = new CustomHttpClient(fragmentActivity);
				if(app.TrendingType.equalsIgnoreCase("group")){
					if(searching){
						httpString = getString(R.string.api_base) + getString(R.string.api_group_trending_search) + app.ActiveGroup.getGroupId() + "/?userId="+app.getActiveUser().getId() + "&search="+URLEncoder.encode(searchQuery, "UTF-8");
					}else{
						httpString = getString(R.string.api_base) + getString(R.string.api_group_trending) + app.ActiveGroup.getGroupId() + "/?userId="+app.getActiveUser().getId();
					}
				}else{
					app.TrendingMusicPref = -1;
					if(app.TrendingState.equalsIgnoreCase("UpvoteFiltered")){
						httpString = getString(R.string.api_base) + getString(R.string.api_trending) + app.getActiveUser().getId() + "/?upvote_filter=true";
					}else if(app.TrendingState.equals("MusicFiltered")){
						app.TrendingMusicPref = trendingActivity.musicPreference;
						httpString = getString(R.string.api_base) + getString(R.string.api_trending) + app.getActiveUser().getId() + "/?music_filter="+app.TrendingMusicPref;
					}else if(searching){
						httpString = getString(R.string.api_base) + getString(R.string.api_trending) + app.getActiveUser().getId() + "/?search="+URLEncoder.encode(searchQuery, "UTF-8");
					}else{
						httpString = getString(R.string.api_base) + getString(R.string.api_trending) + app.getActiveUser().getId() + "/?dummy=0";
					}
					
					if(updatedMap){
						//Log.d("Trending", "Map was updated");
						httpString += "&search_radius="+app.MapRadius+"&latitude="+app.MapSearchLat+"&longitude="+app.MapSearchLong;
					}
					
				}
				httpString += "&count=" + counter;
				httpGet = new HttpGet(httpString);
				httpGet.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpGet.setHeader("PLATFORM","Android");
				httpGet.setHeader("USER_ID",""+app.getActiveUser().getId());
				httpResponse = httpClient.execute(httpGet);
				
				httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();
				//Log.d("Trending", "Got input stream");
				bufferReader = new BufferedReader(new InputStreamReader(inputStream));
				stringBuilder = new StringBuilder();
				
				while((singleLine = bufferReader.readLine()) != null){
					stringBuilder.append(singleLine);
				}
				
				httpResult = stringBuilder.toString();
				inputStream.close();
				//Log.d("Trending", httpResult);
				jsonObject = new JSONObject(httpResult);
				
				if(jsonObject.getBoolean("res")){
					retVal = "success";
					jsonArray = jsonObject.getJSONArray("body");
				}else{
					retVal = "failed";
				}
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
				//Log.d("Trending", "protocal: " + e.getMessage());
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
				//Log.d("Trending", "IO: " + e.getMessage());
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
				//Log.d("Trending", "JSon: " + e.getMessage());
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
				//Log.d("Trending", "General: " + e.getMessage());
			}
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startGetTrendingVenues();
		}
		
		@Override
		protected void onPostExecute(String result){
			getTrendingVenuesResult(result);
		}
    }
}
