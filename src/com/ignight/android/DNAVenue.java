package com.ignight.android;

import java.util.ArrayList;

import android.content.Context;

public class DNAVenue {
	private int Id;
	
	public DNAVenue(int id){
		this.Id = id;
	}
	
	public int getId(){
		return this.Id;
	}
	
	public static String getVenueName(int venueId, Context context, AppBase appBase){
		DataAdapter dataAdapter;
		String retVal = "";
		ArrayList<Venue> venues;

		venues = appBase.LocalVenues;
		
		if(venues == null || venues.size() == 0){
			dataAdapter = new DataAdapter(context);
			appBase.LocalVenues = dataAdapter.GetVenues();
		}
		
		for(Venue venue:venues){
			if(venue.getId() == venueId){
				retVal = venue.getName();
				break;
			}
		}
		
		return retVal;
	}
}
