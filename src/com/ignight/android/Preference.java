package com.ignight.android;

public class Preference {
	private int DisplayName;
	private int DisplayPhotos;
	private int Invites;
	
	public Preference(){}
	
	public Preference(int displayName, int displayPhotos, int invites){
		this.DisplayName = displayName;
		this.DisplayPhotos = displayPhotos;
		this.Invites = invites;
	}
	
	public int getDisplayName(){
		return this.DisplayName;
	}
	
	public String getDisplayNameString(){
		return this.DisplayName == 1 ? "Yes" : "No";
	}
	
	public void setDisplayName(int displayName){
		this.DisplayName = displayName;
	}
	
	public int getDisplayPhotos(){
		return this.DisplayPhotos;
	}
	
	public String getDisplayPhotosString(){
		return this.DisplayPhotos == 1 ? "Yes" : "No";
	}
	
	public void setDisplayPhotos(int displayPhotos){
		this.DisplayPhotos = displayPhotos;
	}
	
	public int getInvites(){
		return this.Invites;
	}
	
	public String getInvitesString(){
		return this.Invites == 1 ? "Yes" : "No";
	}
	
	public void setInvites(int invites){
		this.Invites = invites;
	}
}
