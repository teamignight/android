package com.ignight.android;

public class MessageNewUser {
	private class SimpleUser{
		private String email;
		private String userName;
		private String password;
		private String type;
		
		SimpleUser(String email, String userName, String password){
			this.email = email;
			this.userName = userName;
			this.password = password;
			this.type = "add";
		}
	}
	private SimpleUser msg;
	
	MessageNewUser(String email, String userName, String password){
		this.msg = new SimpleUser(email, userName, password);
	}
}
