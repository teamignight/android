package com.ignight.android;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AdapterBuzz extends ArrayAdapter<Buzz> {
	private Buzz activeBuzz;
	private BufferedReader bufferReader;
	private Context context;
	private Buzz buzz;
	private ArrayList<Buzz> buzzList;
	private Gson gson;
	private String httpResult;
	private HttpClient httpClient;
	private HttpEntity httpEntity;
	private HttpPost httpPost;
	private HttpResponse httpResponse;
	private InputStream inputStream;
	private JSONObject jsonObject;
	private User user;
	private int screenWidth;
	
	public AdapterBuzz(Context context, int textViewResourceId, ArrayList<Buzz> items, User user){
		super(context, textViewResourceId, items);
		
		this.context = context;
		this.buzzList = items;
		this.user = user;
		
		WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		screenWidth = size.x;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View view = convertView;
		LayoutInflater layoutInflater;
		TextView txtBuzzUserName;
		ImageView imgBuzzUser;
		ImageView imgBuzzText;
		TextView txtBuzzText;
		TextView txtBuzzTime;
		TextView txtReportBuzz;
		//ImageView imgBuzzTrending;
		RelativeLayout layout;
		LinearLayout userInfoLeft;
		LinearLayout userInfoRight;
		LinearLayout buzzLayout;
		RelativeLayout.LayoutParams params;
		LinearLayout.LayoutParams params2;
		RelativeLayout layoutBuzzHeader;
		View buzzDivider;
		ImageView buzzArrowLeft;
		ImageView buzzArrowRight;
		
		if(view == null){
			layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layoutInflater.inflate(R.layout.list_buzz, null);
		}
		
		txtBuzzUserName = (TextView)view.findViewById(R.id.txtBuzzUserName2);
		txtBuzzText = (TextView)view.findViewById(R.id.txtBuzzText);
		imgBuzzText = (ImageView)view.findViewById(R.id.imgBuzzImg);
		txtBuzzTime = (TextView)view.findViewById(R.id.txtBuzzTime);
		txtReportBuzz = (TextView)view.findViewById(R.id.txtReportBuzz);
		layout = (RelativeLayout)view.findViewById(R.id.loadingPanel);
		userInfoLeft = (LinearLayout)view.findViewById(R.id.userInfoLeft);
		userInfoRight = (LinearLayout)view.findViewById(R.id.userInfoRight);
		buzzLayout = (LinearLayout)view.findViewById(R.id.buzzLayout);
		layoutBuzzHeader = (RelativeLayout)view.findViewById(R.id.layoutBuzzHeader);
		buzzDivider = (View)view.findViewById(R.id.buzzDivider);
		buzzArrowLeft = (ImageView)view.findViewById(R.id.buzzArrow);
		buzzArrowRight = (ImageView)view.findViewById(R.id.buzzArrowRight);
		
		buzz = buzzList.get(position);
		layout.setVisibility(View.GONE);
		
		if(buzz != null){
			if(buzz.getUserId() == user.getId()){
				userInfoLeft.setVisibility(View.GONE);
				userInfoRight.setVisibility(View.VISIBLE);
				imgBuzzUser = (ImageView)view.findViewById(R.id.imgBuzzUserRight);
				//imgBuzzTrending = (ImageView)view.findViewById(R.id.imgBuzzTrendingRight);
				
				params = new android.widget.RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
				txtBuzzTime.setLayoutParams(params);
				
				//params = new android.widget.RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				//params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
				//txtReportBuzz.setLayoutParams(params);
				
				params2 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
				params2.gravity = Gravity.RIGHT;
				buzzLayout.setLayoutParams(params2);
				buzzLayout.setBackground(context.getResources().getDrawable(R.drawable.drawable_buzz_bg_mine));

				txtBuzzText.setTextColor(Color.parseColor("#ffffff"));
				txtBuzzUserName.setVisibility(View.GONE);
				buzzDivider.setVisibility(View.GONE);
				layoutBuzzHeader.setVisibility(View.GONE);
				buzzArrowLeft.setVisibility(View.GONE);
				buzzArrowRight.setVisibility(View.VISIBLE);
				txtReportBuzz.setVisibility(View.GONE);
				//imgBuzzTrending.setVisibility(View.GONE);
			}else{
				userInfoLeft.setVisibility(View.VISIBLE);
				userInfoRight.setVisibility(View.GONE);
				imgBuzzUser = (ImageView)view.findViewById(R.id.imgBuzzUser);
				//imgBuzzTrending = (ImageView)view.findViewById(R.id.imgBuzzTrending);
				
				params = new android.widget.RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
				txtBuzzTime.setLayoutParams(params);
				
				params = new android.widget.RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
				txtReportBuzz.setLayoutParams(params);

				params2 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
				params2.gravity = Gravity.LEFT;
				buzzLayout.setLayoutParams(params2);
				buzzLayout.setBackground(context.getResources().getDrawable(R.drawable.drawable_buzz_bg));
				
				txtBuzzText.setTextColor(Color.parseColor("#000000"));
				txtBuzzUserName.setVisibility(View.VISIBLE);
				buzzDivider.setVisibility(View.VISIBLE);
				layoutBuzzHeader.setVisibility(View.VISIBLE);
				buzzArrowLeft.setVisibility(View.VISIBLE);
				buzzArrowRight.setVisibility(View.GONE);
				
				txtReportBuzz.setVisibility(View.VISIBLE);
				txtReportBuzz.setText(buzz.getBuzzReporting() ? "Reporting..." : buzz.getBuzzReported() ? "Reported" : "Report");
				txtReportBuzz.setTag(buzz);
				txtReportBuzz.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View view) {
						activeBuzz = (Buzz)view.getTag();
						if(activeBuzz.getBuzzReported() || activeBuzz.getBuzzReporting())
							return;
						final int id = activeBuzz.getId();
						AlertDialog.Builder alert = new AlertDialog.Builder(context);
				        alert.setTitle("Report Buzz Post");
				        alert.setMessage("Are you sure you want to report this as inappropriate?");
				        alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
				            @Override
				            public void onClick(DialogInterface dialog, int which) { 
				            	dialog.dismiss();
				            	activeBuzz.setBuzzReporting(true);
				            	notifyDataSetChanged();
				            	new ReportBuzzTask().execute(id);
				            }
				        });
				        alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				            @Override
				            public void onClick(DialogInterface dialog, int which) {
				                dialog.dismiss();
				            }
				        });

				        alert.show();
					}
				});
			}
			
			txtBuzzUserName.setText(buzz.getBuzzUser().getUserName());
			txtBuzzTime.setText(buzz.getTime());
			
			if(!buzz.getChatPictureURL().isEmpty() && !buzz.getChatPictureURL().equalsIgnoreCase("default")){
				imgBuzzUser.setTag(buzz.getChatPictureURL());
				Picasso.with(context).load(buzz.getChatPictureURL()).noFade().transform(new CircleTransform()).into(imgBuzzUser);
			}else{
				Picasso.with(context).load(R.drawable.avatar_default).noFade().transform(new CircleTransform()).into(imgBuzzUser);
			}
			
			if(buzz.getIsImage()){
				txtBuzzText.setText("");
				txtBuzzText.setVisibility(View.GONE);
				
				imgBuzzText.setVisibility(View.VISIBLE);
				Picasso.with(context).load(buzz.getBuzzText()).transform(new ScaleToFitWidhtHeigthTransform((int)Math.floor(screenWidth/2), true)).placeholder(R.drawable.image_placeholder).into(imgBuzzText);
			}else{
				txtBuzzText.setVisibility(View.VISIBLE);
				txtBuzzText.setText(buzz.getBuzzText());
				imgBuzzText.setVisibility(View.GONE);
			}
		}
		return view;
	}
	
	private class ReportBuzzTask extends AsyncTask<Integer, Void, String>{
		
		@Override
		protected String doInBackground(Integer... buzzId) {
			int activeBuzzId = 0;
			String json = "";
			String retVal = "";
			String singleLine = "";
			StringBuilder stringBuilder;
			StringEntity stringEntity;
			
			try{
				gson = new GsonBuilder().create();
				activeBuzzId = buzzId[0];
				MessageFlagGroupBuzz item = new MessageFlagGroupBuzz(activeBuzzId, user.getId());
				
				json = gson.toJson(item);
				stringEntity = new StringEntity(json);
				httpClient = new CustomHttpClient(context);
				if(activeBuzz.getGroupId() > -1)
					httpPost = new HttpPost(context.getResources().getString(R.string.api_base) + context.getResources().getString(R.string.api_flag_group_buzz) + activeBuzz.getGroupId());
				else
					httpPost = new HttpPost(context.getResources().getString(R.string.api_base) + context.getResources().getString(R.string.api_flag_venue_buzz) + activeBuzz.getVenueId());
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(context.getResources().getString(R.string.api_token_key),context.getResources().getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID",""+user.getId());
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					
					if(httpResult != null){
						//Log.d("Adapter", "result: " + httpResult);
						jsonObject = new JSONObject(httpResult);
						
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							retVal = "failed";
						}
					}else{
						retVal = "failed";
					}
				}
			}catch(Exception ex){
				retVal = "failed";
			}
			
			return retVal;
		}
		
		@Override
		protected void onPostExecute(String retVal){
			if(retVal.equalsIgnoreCase("success")){
				activeBuzz.setBuzzReported(true);
				activeBuzz.setBuzzReporting(false);
				notifyDataSetChanged();
				
			}
		}
	}
}
