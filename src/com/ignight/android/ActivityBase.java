package com.ignight.android;

import net.hockeyapp.android.CrashManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import com.squareup.picasso.Picasso;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public abstract class ActivityBase extends FragmentActivity {
	private String actionPage;
	protected AppBase app;
	private boolean drawerAnimating;
	private DrawerLayout drawerLayout;
	private boolean drawerOpen;
	private ScrollView drawerView;
	private AdapterGroups groupsAdapter;
	private Toast message;
	private User user;
	protected ArrayList<Group> sideGroups;
	
	protected User CurrentUser;
	
	public double UserLatitude = 0;
	public double UserLongitude = 0;
	
	protected final int PICTURE_TAKEN_FROM_CAMERA = 1;
	protected final int PICTURE_TAKEN_FROM_GALLERY = 2;
	protected boolean storeImage = false;
	protected String filePath;
	protected Bitmap takenPictureData;
	protected final CharSequence[] items = { "Take Photo", "Choose from Library", "Cancel" };
	protected TextView txtTutorialMenu;
	
	public static int activities_num = 0;
	
	enum Pages{
		User
	}
	
	private BroadcastReceiver sidePanelReceiver;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		getWindow().setFormat(PixelFormat.RGBA_8888);
		
		app = ((AppBase)getApplication());
		app.IsGroupSearch = false;
		app.OnGroupPage = false;
		CurrentUser = app.getActiveUser();
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		CrashManager.register(this, "c98da10e01d9d2681db75cacc63b87b5");
		activities_num--;
		
		if(app == null)
			app = ((AppBase)getApplication());
		
		if(CurrentUser == null)
			CurrentUser = app.getActiveUser();
	}
	
	@Override
    protected void onStart() {
        super.onStart();
        activities_num++;
    }
	
	@Override
	protected void onStop(){
		super.onStop();
		try{
			this.unregisterReceiver(this.sidePanelReceiver);
		}catch(Exception ex){
			
		}
	}
	
	protected void showAddGroupButton(){
		ImageView imgTitleAddGroupButton = (ImageView)findViewById(R.id.imgTitleAddGroupButton);
		if(imgTitleAddGroupButton != null){
			imgTitleAddGroupButton.setVisibility(View.VISIBLE);
			imgTitleAddGroupButton.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					loadAddGroup();
				}
			});
		}
	}
	
	protected void showTitleInfoButton(String page){
		actionPage = page;
		ImageView imgTitleInfo = (ImageView)findViewById(R.id.imgTitleInfoButton);
		
		if(imgTitleInfo != null){
			imgTitleInfo.setVisibility(View.VISIBLE);
		
			imgTitleInfo.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if(actionPage.equalsIgnoreCase("group")){
						loadGroupDetails();
					}
				}
			});
		}
	}
	
	public void updateHeaderText(String subTitle){
		TextView titleBarSubTitle = (TextView)findViewById(R.id.titleBarSubTitle);
		
		if(titleBarSubTitle != null){
			titleBarSubTitle.setText(subTitle);
		}
	}	

	public void toggleTutorialMenu(boolean showMenu, String label){
		txtTutorialMenu = (TextView)findViewById(R.id.txtTutorialMenu);
		
		if(txtTutorialMenu != null){
			if(showMenu){
				txtTutorialMenu.setVisibility(View.VISIBLE);
				txtTutorialMenu.setText(label);
			}else{
				txtTutorialMenu.setVisibility(View.GONE);
			}
		}
	}
	
	public void wireupHeader(String mainTitle, String subTitle){
		TextView titleBarBigTitle = (TextView)findViewById(R.id.titleBarBigTitle);
		TextView titleBarTitle = (TextView)findViewById(R.id.titleBarTitle);
		TextView titleBarSubTitle = (TextView)findViewById(R.id.titleBarSubTitle);
		
		RelativeLayout btnSideBarCity = (RelativeLayout)findViewById(R.id.btnSideBarCity);
		RelativeLayout btnSideBarGroups = (RelativeLayout)findViewById(R.id.btnSideBarGroups);
		RelativeLayout btnSideBarLogout = (RelativeLayout)findViewById(R.id.btnSideBarLogout);
		RelativeLayout btnSideBarProfile = (RelativeLayout)findViewById(R.id.btnSideBarProfile);
		TextView sideBarProfile = (TextView)findViewById(R.id.sideBarProfile);
		ImageView sideBarProfileIcon = (ImageView)findViewById(R.id.sideBarProfileIcon);
		RelativeLayout btnSideBarTutorial = (RelativeLayout)findViewById(R.id.btnSideBarTutorial);
		
		setupDrawer();
		
		
		if(titleBarBigTitle != null && subTitle.isEmpty()){
			titleBarBigTitle.setText(mainTitle);
		}else{
			if(titleBarTitle != null){
				titleBarTitle.setText(mainTitle);
			}
			if(titleBarSubTitle != null){
				titleBarSubTitle.setText(subTitle);
			}
		}
		
		if(btnSideBarGroups != null){
			btnSideBarGroups.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v){
					loadGroups();
				}
			});
		}
		if(btnSideBarCity != null){
			TextView sideBarCity = (TextView)findViewById(R.id.sideBarCity);
			sideBarCity.setText(CurrentUser.getCity().getName());
			btnSideBarCity.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v){
					loadTrending();
				}
			});
		}
		if(btnSideBarLogout != null){
			btnSideBarLogout.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v){
					logoutUser();
				}
			});
		}
		if(btnSideBarProfile != null){
			sideBarProfile.setText(CurrentUser.getUserName());
			sideBarProfile.setTypeface(null, Typeface.BOLD);
			if(!CurrentUser.getUserPhoto().isEmpty() && !CurrentUser.getUserPhoto().equalsIgnoreCase("default")){
				Picasso.with(this).load(CurrentUser.getUserPhoto()).transform(new CircleTransform()).into(sideBarProfileIcon);
			}else{
				Picasso.with(this).load(R.drawable.avatar_default).transform(new CircleTransform()).into(sideBarProfileIcon);
			}
			btnSideBarProfile.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v){
					loadUserProfile();
				}
			});
		}
		if(btnSideBarTutorial != null){
			btnSideBarTutorial.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					loadTutorial();
				}
			});
		}
	}

	protected void showMessage(String msg){
		message = Toast.makeText(this, msg, Toast.LENGTH_LONG);
		message.setGravity(Gravity.CENTER, message.getXOffset() / 2, message.getYOffset() / 2);
		message.show();
	}
	
	protected Bitmap media_getBitmapFromFile(File imgFile) throws Exception{
        Bitmap res=null;
    	
    	try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;            
            BitmapFactory.decodeStream(new FileInputStream(imgFile),null,o);
            
            //Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE=70;
            int width_tmp=o.outWidth, height_tmp=o.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
                    break;
                width_tmp/=2;
                height_tmp/=2;
                scale*=2;
            }
            
            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            //o2.inSampleSize=scale;
            o2.inSampleSize = calculateSampleSize(o.outWidth, o.outHeight, 640, 480);
            res=BitmapFactory.decodeStream(new FileInputStream(imgFile), null, o2);
            
        }catch (FileNotFoundException e){
        	showMessage("FileNotFoundException: "+e.getMessage());
        }catch (Exception e){
        	showMessage("Error getting image: " + e.getMessage());
        }
        
        return res;
    }
	
	protected Bitmap media_correctImageOrientation (String imagePath){
		Bitmap res = null;

		try {
	        File f = new File(imagePath);
	        ExifInterface exif = new ExifInterface(f.getPath());
	        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

	        int angle = 0;

	        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
	            angle = 90;
	        } 
	        else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
	            angle = 180;
	        } 
	        else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
	            angle = 270;
	        }
	        
	        Matrix mat = new Matrix();
	        mat.postRotate(angle);
	        BitmapFactory.Options options = new BitmapFactory.Options();
	        options.inSampleSize = 2;

	        Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
	        res = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
	                bmp.getHeight(), mat, true);

	    }catch(OutOfMemoryError e) {
	    	showMessage("media_correctImageOrientation() [OutOfMemory!]: " + e.getMessage());
	    }catch (Exception e) {
	    	showMessage("media_correctImageOrientation(): "+e.getMessage());
	    }catch(Throwable e){
	    	showMessage("media_correctImageOrientation(): "+e.getMessage());
	    }


		return res;
	}

	protected void getPictureFromCamera(){
	    boolean cameraAvailable = ToolBox.device_isHardwareFeatureAvailable(this, PackageManager.FEATURE_CAMERA);
	    if(cameraAvailable && 
	        ToolBox.system_isIntentAvailable(this, MediaStore.ACTION_IMAGE_CAPTURE)){

	        Intent takePicIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

	        //We prepare the intent to store the taken picture
	        try{
	            File outputDir = ToolBox.storage_getExternalPublicFolder(Environment.DIRECTORY_PICTURES, "IgNight", true);
	            app.OutFile = ToolBox.storage_createUniqueFileName("cameraPic", ".jpg", outputDir);
	            
	            takePicIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(app.OutFile));             
	            storeImage = true;
	        }catch(Exception e){
	            showMessage("Error setting output destination.");
	        }
	        
	        if(app.OutFile == null || app.OutFile.getAbsolutePath().length() == 0){
	        	showMessage("Error, could not create temp directory.");
	        } else {
	        	startActivityForResult(takePicIntent, PICTURE_TAKEN_FROM_CAMERA);
	        }
	    }else{
	        if(cameraAvailable){
	            showMessage("No application that can receive the intent camera.");
	        }else{
	            showMessage("No camera present!!");
	        }
	    }
	}
	
	protected void getPictureFromGallery(){
	    //This allows to select the application to use when selecting an image.
	    /*Intent i = new Intent(Intent.ACTION_GET_CONTENT);
	    i.setType("image/*");
	    startActivityForResult(Intent.createChooser(i, "Select a photo"), PICTURE_TAKEN_FROM_GALLERY);*/ 
	    

	    //This takes images directly from gallery
	    //Intent gallerypickerIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		/*Intent gallerypickerIntent = new Intent();
	    gallerypickerIntent.setType("image/*");
		gallerypickerIntent.setAction(Intent.ACTION_GET_CONTENT);
	    gallerypickerIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
	    //startActivityForResult(gallerypickerIntent, PICTURE_TAKEN_FROM_GALLERY);
	    startActivityForResult(Intent.createChooser(gallerypickerIntent, "Select a photo"), PICTURE_TAKEN_FROM_GALLERY);*/
	    
	    Intent customChooser = new Intent(this, ActivityPhotoBrowser.class);
	    startActivity(customChooser);
	}
	
	protected Bitmap handleResultFromCamera(Intent data){
	    Bitmap takenPictureData = null;

	    if(data != null){
	    	//Log.d("Photo", "data was not null");
	        //Android sets the picture in extra data.
	        Bundle extras = data.getExtras();
	        if(extras!=null && extras.get("data")!=null){
	            takenPictureData = (Bitmap) extras.get("data");
	        }
	    }else{
	        //If we used EXTRA_OUTPUT we do not have the data so we get the image
	        //from the output.
	    	//Log.d("Photo", "Is outfile null: " + (app.OutFile == null ? "yes": "no") + ", storeImage: " + storeImage);
	        try{
	        	//app.UploadingPhoto = true;
	            takenPictureData = media_getBitmapFromFile(app.OutFile);
	            takenPictureData = media_correctImageOrientation(app.OutFile.getAbsolutePath());
	            
	            filePath = app.OutFile.getAbsolutePath();
	        }catch(Exception e){
	            showMessage("Error getting saved taken picture.");
	        }
	    }
	    
	    //We add the taken picture file to the gallery so user can see the image directly                   
        new MediaScannerNotifier(this,app.OutFile.getAbsolutePath(),"image/*", false);

	    return takenPictureData;
	}

	protected Bitmap handleResultFromChooser(Intent data){
	    Uri photoUri = data.getData();
	    if (photoUri != null){
	        try {
	        	filePath = ImageFilePath.getPath(this, photoUri);
	        	takenPictureData = media_getBitmapFromFile(new File(filePath));
	            
	            //We get the file path from the media info returned by the content resolver
	            /*
	            String[] filePathColumn = {MediaStore.Images.Media.DATA};
	            Cursor cursor = getContentResolver().query(photoUri, filePathColumn, null, null, null); 
	            cursor.moveToFirst();
	            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	            filePath = cursor.getString(columnIndex);
	            cursor.close();
	            
	            takenPictureData = media_getBitmapFromFile(new File(filePath));*/

	        }catch(Exception e){
	            showMessage("Error getting selected image: " + e.getMessage());
	        }
	    }

	    return takenPictureData;
	}
	
	protected Bitmap getCompressBitmap(String filePath){
		Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);
        options.inJustDecodeBounds = false;
        options.inSampleSize = calculateSampleSize(options.outWidth, options.outHeight, 640, 480);
        Bitmap unscaledBitmap = BitmapFactory.decodeFile(filePath, options);

        return unscaledBitmap;
	}
	
	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
	    // Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;
	
	    if (height > reqHeight || width > reqWidth) {
	
	        final int halfHeight = height / 2;
	        final int halfWidth = width / 2;
	
	        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
	        // height and width larger than the requested height and width.
	        while ((halfHeight / inSampleSize) > reqHeight
	                && (halfWidth / inSampleSize) > reqWidth) {
	            inSampleSize *= 2;
	        }
	    }
	
	    return inSampleSize;
	}
	
	public static void expand(final View v) {
	    v.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
	    final int targtetHeight = v.getMeasuredHeight();

	    v.getLayoutParams().height = 0;
	    v.setVisibility(View.VISIBLE);
	    Animation a = new Animation()
	    {
	        @Override
	        protected void applyTransformation(float interpolatedTime, Transformation t) {
	            v.getLayoutParams().height = interpolatedTime == 1
	                    ? LayoutParams.WRAP_CONTENT
	                    : (int)(targtetHeight * interpolatedTime);
	            v.requestLayout();
	        }

	        @Override
	        public boolean willChangeBounds() {
	            return true;
	        }
	    };

	    // 1dp/ms
	    a.setDuration((int)(targtetHeight / v.getContext().getResources().getDisplayMetrics().density));
	    v.startAnimation(a);
	}

	public static void collapse(final View v) {
	    final int initialHeight = v.getMeasuredHeight();

	    Animation a = new Animation()
	    {
	        @Override
	        protected void applyTransformation(float interpolatedTime, Transformation t) {
	            if(interpolatedTime == 1){
	                v.setVisibility(View.GONE);
	            }else{
	                v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
	                v.requestLayout();
	            }
	        }

	        @Override
	        public boolean willChangeBounds() {
	            return true;
	        }
	    };

	    // 1dp/ms
	    a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
	    v.startAnimation(a);
	}
	
	protected void hideHeaderButton(){
		((ImageView)findViewById(R.id.btnLeftMenu)).setVisibility(View.INVISIBLE);
	}
	
	private int calculateSampleSize(int srcWidth, int srcHeight, int dstWidth, int dstHeight) {
		final float srcAspect = (float)srcWidth / (float)srcHeight;
        final float dstAspect = (float)dstWidth / (float)dstHeight;

        if (srcAspect > dstAspect) {
            return srcWidth / dstWidth;
        } else {
            return srcHeight / dstHeight;
        }
    }
	
	private void setupDrawer(){
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawerView = (ScrollView) findViewById(R.id.drawerView);
        
		if(drawerLayout != null && drawerView != null){
	        DrawerListener drawerListener = new DrawerListener() {
				@Override
				public void onDrawerStateChanged(int newState) {
				}
				
				@Override
				public void onDrawerSlide(View drawerView, float sideOffset) {
				}
				
				@Override
				public void onDrawerOpened(View view) {
					drawerOpen = true;
					drawerAnimating = false;
				}
				
				@Override
				public void onDrawerClosed(View drawerView) {
					drawerOpen = false;
					drawerAnimating = false;
					if(app.RefreshGroupTrending){
						app.RefreshGroupTrending = false;
						
						Intent intent = new Intent(ActivityBase.this, ActivityTrending.class);
						ActivityBase.this.startActivity(intent);
					}
				}
			};
			drawerLayout.setDrawerListener(drawerListener);
			
			ImageView btnLeftMenu = (ImageView)findViewById(R.id.btnLeftMenu);
			if(btnLeftMenu != null){
				btnLeftMenu.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if(drawerAnimating){
							return;
						}
						drawerAnimating = true;
						if(drawerOpen){
							drawerLayout.closeDrawer(drawerView);
						}else{
							drawerLayout.openDrawer(Gravity.LEFT);
						}
					}
				});
			}
		}
	}
	
	private final LocationListener locationListener = new LocationListener() {
	    public void onLocationChanged(Location location) {
	    	UserLatitude = location.getLatitude();
	    	UserLongitude = location.getLongitude();
	    	
	    	//Log.d("Current Location", String.valueOf(UserLatitude));
	    }

		@Override
		public void onProviderDisabled(String arg0) { Log.d("Current Location", "Provider disabled"); }

		@Override
		public void onProviderEnabled(String arg0) { Log.d("Current Location", "Provider enabled"); }

		@Override
		public void onStatusChanged(String arg0, int arg1, Bundle arg2) { }
	};
    
    public void getUserLocation(){
    	/*LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE); 

		Criteria crit = new Criteria();
		crit.setAccuracy(Criteria.ACCURACY_FINE);
		crit.setPowerRequirement(Criteria.POWER_LOW);
		
		String provider = lm.getBestProvider(crit, true);
		float distance = 30;
		long time = 60000;
		
		lm.requestLocationUpdates(provider, time, distance, locationListener);*/
    }
    
    private void loadGroupDetails(){
    	Intent intent = new Intent(this, ActivityGroupDetails.class);
    	startActivity(intent);
    }
    
    private void loadTrending(){
    	Intent intent;
    	intent = new Intent(this, ActivityTrending.class);
    	startActivity(intent);
    }
	
	private void loadUserProfile(){
		Intent intent;
    	intent = new Intent(this, ActivityProfile.class);
    	startActivity(intent);
	}
	
	private void loadGroups(){
		Intent intent = new Intent(this, ActivityGroups.class);
		startActivity(intent);
	}
	
	private void loadAddGroup(){
		Intent intent;
    	intent = new Intent(this, ActivityAddGroup.class);
    	startActivity(intent);
	}
	
	private void loadTutorial(){
		Intent intent;
    	intent = new Intent(this, ActivityTutorial.class);
    	startActivity(intent);
	}
	
	private void logoutUser(){
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Confirm log out");
        alert.setMessage("Are you sure you want to log out?");
        alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {                
            	logout();
            }
        });
        alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alert.show();
	}
	
	private void logout(){
		ProgressDialog progressDialog = ProgressDialog.show(this, "", "Logging out....", false, true);
		
		DataAdapter dataAdapter = new DataAdapter(this);
		dataAdapter.cleanDB();
		
		progressDialog.hide();
		
		Intent intent;
    	intent = new Intent(this, ActivityLogin.class);
    	startActivity(intent);
	}
}
