package com.ignight.android;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AdapterVenues extends ArrayAdapter<Venue> {
	private Context context;
	private Venue venue;
	private ArrayList<Venue> venues;
	
	public AdapterVenues(Context context, int textViewResourceId, ArrayList<Venue> items){
		super(context, textViewResourceId, items);
		
		this.context = context;
		this.venues = items;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View view = convertView;
		LayoutInflater layoutInflater;
		TextView txtVenue;
		
		if(view == null){
			layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layoutInflater.inflate(R.layout.list_venues, null);
		}
		venue = venues.get(position);
		if(venue != null){
			txtVenue = (TextView)view.findViewById(R.id.txtVenue);
			if(txtVenue != null){ txtVenue.setText(venue.getName()); }
		}
		
		return view;
	}
}
