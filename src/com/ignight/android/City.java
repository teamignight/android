package com.ignight.android;

import java.util.ArrayList;

public class City {
	private int Id;
	private String Name;
	
	public City(int id, String name){
		this.Id = id;
		this.Name = name;
	}
	
	public static String getCityName(int cityId){
		String[] CityNames = {"Chicago", "New York"};
		return CityNames[cityId];
	}
	
	public static ArrayList<City> getCityOptions(){
		String[] _CityNames = {"Chicago", "New York"};
		int cursor = 0;
		ArrayList<City> cityOptions = new ArrayList<City>();
		
		for(String option:_CityNames){
			cityOptions.add(new City(cursor, option));
			cursor++;
		}
		
		return cityOptions;
	}
	
	public int getId(){
		return this.Id;
	}
	
	public void setId(int id){
		this.Id = id;
	}
	
	public String getName(){
		return this.Name;
	}
	
	public void setName(String name){
		this.Name = name;
	}
}
