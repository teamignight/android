package com.ignight.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class ActivityResetPassword extends ActivityBase {
	private ActivityResetPassword activity;
	private BufferedReader bufferReader;
	private DataAdapter dataAdapter;
	private Gson gson;
	private String httpResult;
	private HttpClient httpClient;
	private HttpEntity httpEntity;
	private HttpPost httpPost;
	private HttpResponse httpResponse;
	private InputStream inputStream;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private EditText passWord;
	private ProgressDialog progressDialog;
	private StringBuilder stringBuilder;
	private EditText tempPassword;
	private User user;
	private EditText userName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_reset_password);
		
		app = ((AppBase)getApplication());
		user = new User();
		activity = this;
		
		userName = (EditText) findViewById(R.id.txtUserName);
		if(getIntent().getExtras() != null){
			userName.setText(getIntent().getExtras().getString("userName"));
		}
	}
	
	public void btnLogin_ClickEvent(View view){
		passWord = (EditText) findViewById(R.id.txtPassword);
		tempPassword = (EditText) findViewById(R.id.txtTempPassword);
		
		if(userName.getText().toString().equals("") || passWord.getText().toString().equals("") || tempPassword.getText().toString().equals("")){
			showMessage("All fields required.");
		} else if(passWord.getText().length() < 6) {
			showMessage("Password must be at least 6 characters.");
		} else {
			user.setUserName(userName.getText().toString());
			user.setUserPassword(passWord.getText().toString());
			app.setActiveUser(user);
			new LoginTask().execute();
		}
	}
	
	private void startLogin(){
		progressDialog = ProgressDialog.show(this, "", "Validating login....", true);
	}
	
	private void loginResult(String result){
		ArrayList<DNAAtmosphere> atmosphereOptions = new ArrayList<DNAAtmosphere>();
		Intent intent;
		ArrayList<DNAMusic> musicOptions = new ArrayList<DNAMusic>();
		
		if(result.equals("success")){
			//did user set dna
			try {
				if(jsonObject.getJSONObject("body").getBoolean("dnaSet")){
					user.setUserEmail(jsonObject.getJSONObject("body").getString("email"));
					user.setUserPhoto(app.DefaultChatUrl);
					user.setGender(DNAGender.getGender(jsonObject.getJSONObject("body").getInt("genderId")));
					user.setAge(jsonObject.getJSONObject("body").getInt("age"));
					user.setCity(new City(jsonObject.getJSONObject("body").getInt("cityId"), 
							City.getCityName(jsonObject.getJSONObject("body").getInt("cityId"))));
					user.setId(jsonObject.getJSONObject("body").getInt("id"));
					user.setSpendingPreference(jsonObject.getJSONObject("body").getInt("spending"));
					jsonArray = jsonObject.getJSONObject("body").getJSONArray("atmosphere");
					if(jsonArray != null){
						for(int i = 0; i < jsonArray.length(); i++){
							atmosphereOptions.add(new DNAAtmosphere(jsonArray.getInt(i), DNAOptions.AtmosphereOptions().get(jsonArray.getInt(i)).getName()));
						}
						user.setAtmosphereOptions(atmosphereOptions);
					}
					jsonArray = jsonObject.getJSONObject("body").getJSONArray("music");
					if(jsonArray != null){
						for(int i = 0; i < jsonArray.length(); i++){
							musicOptions.add(new DNAMusic(jsonArray.getInt(i), DNAOptions.MusicOptions().get(jsonArray.getInt(i)).getName()));
						}
						user.setMusicOptions(musicOptions);
					}
					//set user preference here
					//user.getUserPreference().setDisplayName(jsonObject.getJSONObject("body").getBoolean("displayNameInGroups") ? 1 : 0);
					user.getUserPreference().setInvites(jsonObject.getJSONObject("body").getBoolean("notifyOfGroupInvites") ? 1 : 0);
					
					dataAdapter = new DataAdapter(this);
					dataAdapter.deleteUser();
					dataAdapter.AddUser(user);
					app.setActiveUser(user);
					
					intent = new Intent(ActivityResetPassword.this, ActivityTrending.class);
				}else{
					intent = new Intent(ActivityResetPassword.this, ActivityDNAUserInfo.class);
				}
			} catch (JSONException e) {
				//progressDialog = ProgressDialog.show(this, "", jsonObject.toString(), true);
				showMessage("Error, could not log in. Please try again: " + e.getMessage());
				intent = new Intent(ActivityResetPassword.this, ActivityResetPassword.class);
			}
			startActivity(intent);
		}else{
			showMessage(result);
		}
	}
	
	private class LoginTask extends AsyncTask<Void, Void, String>{

		@Override
		protected String doInBackground(Void... args) {
			String json = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				gson = new GsonBuilder().create();
				MessageResetPassword login = new MessageResetPassword(passWord.getText().toString(), tempPassword.getText().toString(), userName.getText().toString());
				json = gson.toJson(login);
				
				StringEntity stringEntity = new StringEntity(json);
				httpClient = new CustomHttpClient(activity);
				httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_reset_password));
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID","");
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					//Log.d("Password", httpResult);
					if(httpResult != null){
						jsonObject = new JSONObject(httpResult);
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							retVal = jsonObject.getString("reason");
						}
						//Log.d("PasswordReset", httpResult);
					}else{
						retVal = "Error, no result.";
					}
				}
				
			}catch(ClientProtocolException e){
				retVal = "Client Protocal Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "IO Error, " + e.getMessage();
			
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			progressDialog.dismiss();

			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startLogin();
		}
		
		@Override
		protected void onPostExecute(String result){
			loginResult(result);
		}
	}
}
