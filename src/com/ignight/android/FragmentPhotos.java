package com.ignight.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ignight.android.R.drawable;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

public class FragmentPhotos extends android.support.v4.app.Fragment {
	private AdapterPhotos adapter;
	private AppBase app;
	private BufferedReader bufferReader;
	private ArrayList<Buzz> buzzes;
	private Activity fragmentActivity;
	private GridView grdView;
	private Group group;
	private ActivityGroup groupActivity;
	private HttpClient httpClient;
	private HttpGet httpGet;
	private HttpEntity httpEntity;
	private HttpResponse httpResponse;
	private String httpResult;
	private InputStream inputStream;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private StringBuilder stringBuilder;
	private User user;
	private Venue venue;
	private ActivityVenue venueActivity;
	private TextView txtNoBuzzPhotos;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View view = inflater.inflate(R.layout.fragment_photos, container, false);
        
        app = ((AppBase)fragmentActivity.getApplication());
        
        if(app.PhotoGalleryType.equalsIgnoreCase("group")){
        	if(app.ActiveGroup != null && app.DirtyGroup == app.ActiveGroup.getGroupId()){
    			app.DirtyGroup = -1;
    			app.TrendingType = "trending";
    			showMessage("You have removed this group.");
    			Intent intent = new Intent(fragmentActivity, ActivityTrending.class);
    			startActivity(intent);
    		}
        	groupActivity = ((ActivityGroup)getActivity());
        	group = app.ActiveGroup;
        } else {
        	venueActivity = ((ActivityVenue)getActivity());
        	venue = app.ActiveVenue;
        	venueActivity.imgVenueBuzz.setImageResource(drawable.venue_buzz);
        	venueActivity.imgVenueInfo.setImageResource(drawable.venue_info);
        	venueActivity.imgVenuePhotos.setImageResource(drawable.venue_photos_active);
        }
        user = app.getActiveUser();
        txtNoBuzzPhotos = (TextView)view.findViewById(R.id.txtNoBuzzImages);
        
        new GetPhotosTask().execute();
        
        grdView = (GridView)view.findViewById(R.id.grdPhotos);
        grdView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
            	app.LargeImage = buzzes.get(position).getBuzzText();
            	app.BuzzImagePosition = position;
            	app.BuzzImageId = buzzes.get(position).getId();
            	Intent i = new Intent(fragmentActivity, ActivityVenuePhoto.class);
            	startActivity(i);
            }
        });
        return view;
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		fragmentActivity = activity;
	}
	
	private void getPhotosResult(String result){
		Buzz buzz;
		buzzes = new ArrayList<Buzz>();
		
		if(result.equals("success")){
			try{
				jsonArray = jsonObject.getJSONObject("body").optJSONArray("buzz");
				if(jsonArray != null){
					for(int i = 0; i < jsonArray.length(); i++){
						buzz = new Buzz();
						buzz.setBuzzText(jsonArray.getJSONObject(i).getString("buzzText"));
						buzzes.add(buzz);
					}
					adapter = new AdapterPhotos(fragmentActivity, R.layout.grid_photos, buzzes);
					grdView.setAdapter(adapter);
					app.BuzzImages = buzzes;
				}
				if(jsonArray == null || jsonArray.length() == 0){
					txtNoBuzzPhotos.setVisibility(View.VISIBLE);
					txtNoBuzzPhotos.setText("Head to Buzz to post a picture. Don't get crazy with the selfies!");
					grdView.setVisibility(View.GONE);
				}
			}catch(Exception e){
				showMessage("Error parsing buzz: " + e.getMessage());
			}
		}else{
			showMessage("Error: " + result);
		}
		
		if(app.PhotoGalleryType.equalsIgnoreCase("group")){
			groupActivity.hideProgress();
		}else{
			venueActivity.hideProgressDialog();
		}
	}
	
	private void showMessage(String msg){
		if(app.PhotoGalleryType.equalsIgnoreCase("group")){
			groupActivity.showMessage(msg);
		}else{
			venueActivity.showMessage(msg);
		}
	}
	
	private void startGetPhotos(){
		if(app.PhotoGalleryType.equalsIgnoreCase("group")){
			groupActivity.showProgress("Loading...");
		}else{
			venueActivity.showProgressDialog("Loading...");
		}
	}
	
	private class GetPhotosTask extends AsyncTask<String, Void, String>{
		@Override
		protected String doInBackground(String... args) {
			String retVal = "";
			String singleLine = "";
			
			try{
				httpClient = new CustomHttpClient(fragmentActivity);
				if(app.PhotoGalleryType.equalsIgnoreCase("group")){
					httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_group_images) + app.ActiveGroup.getGroupId() + "/?buzzImageStartId=-1&noOfBuzzImages=50");
				}else{
					httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_venue_images) + app.ActiveVenue.getId() + "/?buzzImageStartId=-1&noOfBuzzImages=50");
				}
				httpGet.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpGet.setHeader("PLATFORM","Android");
				httpGet.setHeader("USER_ID",""+app.getActiveUser().getId());
				httpResponse = httpClient.execute(httpGet);
				
				httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();
				bufferReader = new BufferedReader(new InputStreamReader(inputStream));
				stringBuilder = new StringBuilder();
				
				while((singleLine = bufferReader.readLine()) != null){
					stringBuilder.append(singleLine);
				}
				
				httpResult = stringBuilder.toString();
				//Log.d("Buzz", httpResult);
				inputStream.close();
				jsonObject = new JSONObject(httpResult);

				if(jsonObject.getBoolean("res")){
					retVal = "success";
				}else{
					retVal = "failed";
				}
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startGetPhotos();
		}
		
		@Override
		protected void onPostExecute(String result){
			getPhotosResult(result);
		}
	}
}
