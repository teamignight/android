package com.ignight.android;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterInbox extends ArrayAdapter<Inbox> {
	private Context context;
	private Inbox inboxItem;
	private ArrayList<Inbox> inboxItems;
	
	public AdapterInbox(Context context, int textViewResourceId, ArrayList<Inbox> items){
		super(context, textViewResourceId, items);
		
		this.context = context;
		this.inboxItems = items;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View view = convertView;
		LayoutInflater layoutInflater;
		Transformation transformation;
		TextView txtGroupName;
		TextView txtRequestDate;
		TextView txtUserName;
		ImageView userPhoto;
		
		ImageView btnInboxConfirm;
		ImageView btnInboxDeny;
		
		if(view == null){
			layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layoutInflater.inflate(R.layout.list_inbox, null);
		}
		
		inboxItem = inboxItems.get(position);
		
		if(inboxItem != null){
			txtGroupName = (TextView)view.findViewById(R.id.txtInboxGroupName);
			txtRequestDate = (TextView)view.findViewById(R.id.txtInboxDate);
			txtUserName = (TextView)view.findViewById(R.id.txtInboxUserName);
			userPhoto = (ImageView)view.findViewById(R.id.imgGroupUser);
			
			btnInboxConfirm = (ImageView)view.findViewById(R.id.btnInboxConfirm);
			btnInboxDeny = (ImageView)view.findViewById(R.id.btnInboxDeny);
			
			if(txtGroupName != null){
				txtGroupName.setText(inboxItem.getGroupName());
			}
			if(txtRequestDate != null){
				txtRequestDate.setText(inboxItem.getTime());
			}
			if(txtUserName != null){
				txtUserName.setText("Request sent by " + inboxItem.getUserName());
			}
			if(userPhoto != null){
				transformation = new RoundedTransformationBuilder()
		          .borderColor(Color.WHITE)
		          .borderWidthDp(1)
		          .cornerRadiusDp(30)
		          .oval(false)
		          .build();
				Picasso.with(context).load(inboxItem.getUserPicture()).noFade().fit().transform(transformation).into(userPhoto);
			}
			if(btnInboxConfirm != null)
				btnInboxConfirm.setTag(inboxItem.getGroupId());
			if(btnInboxDeny != null)
				btnInboxDeny.setTag(inboxItem.getGroupId());
		}
		
		return view;
	}
}
