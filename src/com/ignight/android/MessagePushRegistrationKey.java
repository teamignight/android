package com.ignight.android;

public class MessagePushRegistrationKey {
	private class RegistrationKey{
		String token;
		int osId;
		
		RegistrationKey(String androidRegistrationId){
			this.token = androidRegistrationId;
			this.osId = 1;
		}
	}
	private RegistrationKey msg;
	MessagePushRegistrationKey(String token){
		msg = new RegistrationKey(token);
	}
}
