package com.ignight.android;

import java.util.ArrayList;

public class MessageEditUser {
	private class EditUser{
		private int age;
		private ArrayList<Integer> atmosphere;
		private int cityId;
		private boolean displayNameInGroups;
		String email;
		private int genderId;
		private ArrayList<Integer> music;
		private boolean notifyOfGroupInvites;
		private int spendingLimit;
		int userId;
		String type;
		
		EditUser(int age, ArrayList<Integer> atmosphere, int cityId, boolean displayName, String email, int genderId, 
				ArrayList<Integer> music, boolean notifyInvites, int spendingLimit, int userId, String type){
			
			this.age = age;
			this.atmosphere = atmosphere;
			this.cityId = cityId;
			this.displayNameInGroups = displayName;
			this.email = email;
			this.genderId = genderId;
			this.music = music;
			this.notifyOfGroupInvites = notifyInvites;
			this.spendingLimit = spendingLimit;
			this.userId = userId;
			this.type = type;
		}
	}
	private EditUser msg;
	MessageEditUser(int age, ArrayList<Integer> atmosphere, int cityId, boolean displayName, String email, int genderId, 
			ArrayList<Integer> music, boolean notifyInvites, int spendingLimit, int userId, String type){
		this.msg = new EditUser(age, atmosphere, cityId, displayName, email, genderId, music, notifyInvites, spendingLimit, userId, type);
	}
}
