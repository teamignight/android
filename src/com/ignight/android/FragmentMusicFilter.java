package com.ignight.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class FragmentMusicFilter extends android.support.v4.app.Fragment {
	private AdapterMusic adapter;
	private AppBase app;
	private Activity fragmentActivity;
	private GridView gridView;
	private ActivityTrending trendingActivity;
	private User user;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View view = inflater.inflate(R.layout.fragment_music_filter, container, false);
        
        if (fragmentActivity != null) {
	        app = ((AppBase)fragmentActivity.getApplication());
	        user = app.getActiveUser();
		}
        
        trendingActivity = ((ActivityTrending)getActivity());
        
        adapter = new AdapterMusic(fragmentActivity, DNAOptions.MusicOptions(), user);
        gridView = (GridView)view.findViewById(R.id.grdMusicFilters);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id){
				if(!app.TrendingType.equalsIgnoreCase("group")){
					trendingActivity.clearSearch();
				}
				app.TrendingMusicFilter = DNAOptions.MusicOptions().get(position);
				trendingActivity.filterMusic(position);
			}
        });
        
        return view;
	}
	
	@Override
	public void onDetach(){
		super.onDetach();
	}
	
	public void onDestroyView(){
		super.onDestroyView();
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		fragmentActivity = activity;
	}
}
