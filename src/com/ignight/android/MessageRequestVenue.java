package com.ignight.android;

public class MessageRequestVenue {
	private class NewRequestVenue{
		private String address;
		private int cityId;
		private String comment;
		private String number;
		private String type;
		private String url;
		private int userId;
		private String venueName;
		
		NewRequestVenue(String address, int cityId, String comment, String phoneNumber, String url, int userId, String venueName){
			this.address = address;
			this.cityId = cityId;
			this.comment = comment;
			this.number = phoneNumber;
			this.type = "requestVenueAddition";
			this.url = url;
			this.userId = userId;
			this.venueName = venueName;
		}
	}
	private NewRequestVenue msg;
	MessageRequestVenue(String address, int cityId, String comment, String phoneNumber, String url, int userId, String venueName){
		this.msg = new NewRequestVenue(address, cityId, comment, phoneNumber, url, userId, venueName);
	}
}
