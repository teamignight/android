package com.ignight.android;

public class MessageGroup {
	private class NewGroup{
		private int cityId;
		private String groupDescription;
		private String groupName;
		private boolean isPrivateGroup;
		private int userId;
		private String type;
		
		NewGroup(int cityId, String groupDescription, String groupName, boolean isPrivateGroup, int userId, String type){
			this.cityId = cityId;
			this.groupDescription = groupDescription;
			this.groupName = groupName;
			this.isPrivateGroup = isPrivateGroup;
			this.userId = userId;
			this.type = type;
		}
	}
	private NewGroup msg;
	MessageGroup(int cityId, String groupDescription, String groupName, boolean isPrivateGroup, int userId){
		this.msg = new NewGroup(cityId, groupDescription, groupName, isPrivateGroup, userId, "addGroup");
	}
}
