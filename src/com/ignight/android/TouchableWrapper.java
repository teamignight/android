package com.ignight.android;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public class TouchableWrapper extends FrameLayout {

	private AppBase app;
	
	public TouchableWrapper(Context context, AppBase appBase) {
		super(context);
		app = appBase;
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			app.MapIsTouched = true;
			break;
		case MotionEvent.ACTION_UP:
			app.MapIsTouched = false;
			break;
		}
		return super.dispatchTouchEvent(ev);
	}
}
