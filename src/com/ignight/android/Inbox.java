package com.ignight.android;

import java.util.Calendar;
import java.util.Locale;

import android.text.format.DateFormat;

public class Inbox {
	private long DateStamp;
	private int GroupId;
	private String GroupName;
	private String MessageType;
	private String UserName;
	private String UserPicture;
	
	public Inbox(){
		
	}
	
	public long getDateStamp(){
		return this.DateStamp;
	}
	
	public void setDateStamp(long value){
		this.DateStamp = value;
	}
	
	public String getTime(){
		double difference;
		long today;
		java.util.Date todayDate = new java.util.Date();
		
		today = todayDate.getTime();
		difference = (today - DateStamp) / 1000 / 60 / 60;
		
		if(difference <= 0)
			return "Now";
		else if(difference < 1)
			return "A few minutes ago";
		else if(difference < 24)
			return Math.round(difference) + " hours ago";
		else{
			Calendar cal = Calendar.getInstance(Locale.ENGLISH);
		    cal.setTimeInMillis(DateStamp);
		    String date = DateFormat.format("MMM dd, yyyy", cal).toString();
		    return date;
		}
	}

	public int getGroupId(){
		return this.GroupId;
	}
	
	public void setGroupId(int value){
		this.GroupId = value;
	}
	
	public String getGroupName(){
		return this.GroupName;
	}
	
	public void setGroupName(String value){
		this.GroupName = value;
	}
	
	public String getMessageType(){
		return this.MessageType;
	}
	
	public void setMessageType(String value){
		this.MessageType = value;
	}
	
	public String getUserName(){
		return this.UserName;
	}
	
	public void setUserName(String value){
		this.UserName = value;
	}
	
	public String getUserPicture(){
		return this.UserPicture;
	}
	
	public void setUserPicture(String value){
		this.UserPicture = value;
	}
}
