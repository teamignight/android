package com.ignight.android;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class ActivityDNAUserInfo extends ActivityBase {
	private User user;
	private boolean ageSet, citySet, genderSet;
	private boolean ready;
	private Button btnSignup;
	private Button btnCityChicago;
	private String[] cities = { "Chicago" };
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_dna_user_info);
		
        app = ((AppBase)getApplication());
        user = app.getActiveUser();
        btnSignup = (Button)findViewById(R.id.btnSignup);
        btnCityChicago = (Button)findViewById(R.id.btnCityChicago);
        
        btnCityChicago.setText(Html.fromHtml(getString(R.string.btn_city_chicago)));
        btnCityChicago.setSelected(true);
        City city = new City(0, cities[0]);
		user.setCity(city);
		citySet = true;
	}
	
	@Override
	public void onStop(){
		super.onStop();
	}
	
	public void btnButton_ClickEvent(View button){
		boolean activeTag;
		int pos = 0;
		String tag = button.getTag().toString();
		String text = ((Button)button).getText().toString();
		int val = -1;
		boolean itemSelected = false;
		
		button.setSelected(!button.isSelected());
		
		if(button.isSelected()){
			ViewGroup parent = (ViewGroup)findViewById(R.id.userInfoParent);
			final int childCount = parent.getChildCount();
			for(int i = 0; i < childCount; i++){
				final View child = parent.getChildAt(i);
				if(child instanceof Button){
					activeTag = child.getTag().toString().equals(tag);
					if(activeTag){
						if(child.getId() != button.getId()){
							child.setSelected(false);
							pos++;
						}else{
							itemSelected = true;
							val = pos;
						}
					}
				}else if(child instanceof ViewGroup){
					final ViewGroup childViewGroup = (ViewGroup)child;
					final int viewGroupChildCount = childViewGroup.getChildCount();
					for(int j = 0; j < viewGroupChildCount; j++){
						final View viewGroupChild = childViewGroup.getChildAt(j);
						if(viewGroupChild instanceof Button){
							activeTag = viewGroupChild.getTag().toString().equals(tag);
							if(activeTag){
								if(viewGroupChild.getId() != button.getId()){
									viewGroupChild.setSelected(false);
									pos++;
								}else{
									itemSelected = true;
									val = pos;
								}
							}
						}
					}
				}
			}
		}
		
		updateItemSet(tag, itemSelected);
		if(tag.equals("city")){
			City city = new City(val, cities[val == -1 ? 0 : val]);
			user.setCity(city);
		}else if(tag.equals("age")){
			user.setAge(val);
		}else if(tag.equals("gender")){
			user.setGender(text);
		}
		
		checkIfReady();
	}
	
	public void btnSignup_ClickEvent(View v){
		if(!ageSet){
			showMessage("Age is required.");
			return;
		}
		if(!citySet){
			showMessage("City is required.");
			return;
		}
		if(!genderSet){
			showMessage("Gender is required.");
			return;
		}
		if(!ageSet || !citySet || !genderSet){
			return;
		}
		user.setMusicOptions(new ArrayList<DNAMusic>());
		user.setAtmosphereOptions(new ArrayList<DNAAtmosphere>());
		user.setSpendingPreference(-1);
		app.setActiveUser(user);
		//app.getVenues(user.getCity().getId());
		Intent intent = new Intent(ActivityDNAUserInfo.this, ActivityDNAMusic.class);
		startActivity(intent);
	}
	
	private void updateItemSet(String item, boolean val){
		if(item.equalsIgnoreCase("age")){
			ageSet = val;
		}else if(item.equalsIgnoreCase("city")){
			citySet = val;
		}else if(item.equalsIgnoreCase("gender")){
			genderSet = val;
		}
	}

	private void checkIfReady(){
		ready = ageSet && citySet && genderSet;
		if(ready){
			btnSignup.setTextColor(Color.parseColor("#d849b10a"));
		}else{
			btnSignup.setTextColor(Color.parseColor("#626262"));
		}
	}
}
