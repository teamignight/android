package com.ignight.android;

public class MessageVoteVenue {
	private class VenueVote{
		private int userActivity;
		private int userId;
		private int venueId;
		private String type;
		
		VenueVote(int userActivity, int userId, int venueId, String type){
			this.userActivity = userActivity;
			this.userId = userId;
			this.venueId = venueId;
			this.type = type;
		}
	}
	private VenueVote msg;
	
	MessageVoteVenue(int userActivity, int userId, int venueId){
		this.msg = new VenueVote(userActivity, userId, venueId, "setUserActivity");
	}
}
