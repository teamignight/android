package com.ignight.android;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class VenueImage {
	private AdapterVenuePhotoGallery Adapter;
	private int Id;
	private Bitmap Image;
	private LinearLayout ImageHolder;
	private String Url;
	
	public VenueImage(){
		
	}
	
	public VenueImage(String url, int id){
		this.Id = id;
		this.Url = url;
	}
	
	public Bitmap getImage(){
		return this.Image;
	}
	
	public void setImage(AdapterVenuePhotoGallery adapter){
		this.Adapter = adapter;
		new ImageLoadTask().execute(Url);
	}
	
	public void setImage(LinearLayout layout, boolean flag){
		this.ImageHolder = layout;
	}
	
	public String getUrl(){
		return this.Url;
	}
	
	public void setUrl(String url){
		this.Url = url;
	}
	
	private class ImageLoadTask extends AsyncTask<String, String, Bitmap>{
		@Override
		protected Bitmap doInBackground(String... param){
			Bitmap bitmap;
			InputStream inputStream;
			URL url = null;
			HttpURLConnection urlConnection;
			
			try{
				url = new URL(param[0]);
			}catch(MalformedURLException e){
				
			}
			
			if(url != null){
				try{
					urlConnection = (HttpURLConnection)url.openConnection();
					urlConnection.setDoInput(true);
					urlConnection.connect();
					
					inputStream = urlConnection.getInputStream();
					bitmap = BitmapFactory.decodeStream(inputStream);
					
					return bitmap;
				}catch(IOException e){
					return null;
				}
			}
			return null;
		}
		
		protected void onPostExecute(Bitmap bitmap){
			if(bitmap != null){
				Log.d("Image", "bitmap is not null");
				Image = bitmap;
				((ImageView)ImageHolder.getChildAt(Id)).setImageBitmap(Image);
				/*if(Adapter != null){
					Log.d("Image", "adapter is not null");
					Adapter.notifyDataSetChanged();
				}else{
					Log.d("Image", "adapter is null");
				}*/
			}else{
				Log.d("Image", "bitmap is null");
			}
		}
	}
}
