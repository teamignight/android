package com.ignight.android;
import java.util.ArrayList;

import org.json.JSONArray;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;


public class DataAdapter{
	public static final String APP_NAME = "IgNight";
	private Context context;
	private static final String CREATE_TABLE_USER = "CREATE TABLE User (" +
								"Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
								"UserId INTEGER NOT NULL DEFAULT 0, " +
								"EmailAddress TEXT  NOT NULL DEFAULT '', " +
								"GenderId INTEGER  NOT NULL DEFAULT 0, " +
								"UserAge INTEGER  NOT NULL DEFAULT 0, " +
								"UserName TEXT NOT NULL DEFAULT '', " + 
								"UserPassword TEXT NOT NULL DEFAULT '', " + 
								"UserCityName TEXT NOT NULL DEFAULT '', " +
								"UserCityId INTEGER NOT NULL DEFAULT 0, " +
								"UserPhoto TEXT NOT NULL DEFAULT '', " + 
								"SpendingPreference INTEGER NOT NULL DEFAULT 0, " + 
								"DNAMusicOneId INTEGER NOT NULL DEFAULT 0, " + 
								"DNAMusicOneName TEXT NOT NULL DEFAULT '', " +  
								"DNAMusicOneResource INTEGER NOT NULL DEFAULT 0, " +  
								"DNAMusicTwoId INTEGER NOT NULL DEFAULT 0, " + 
								"DNAMusicTwoName TEXT NOT NULL DEFAULT '', " +    
								"DNAMusicTwoResource INTEGER NOT NULL DEFAULT 0, " +
								"DNAMusicThreeId INTEGER NOT NULL DEFAULT 0, " + 
								"DNAMusicThreeName TEXT NOT NULL DEFAULT '', " +  
								"DNAMusicThreeResource INTEGER NOT NULL DEFAULT 0, " +  
								"DNAVenueOneId INTEGER NOT NULL DEFAULT 0, " + 
								"DNAVenueOneName TEXT NOT NULL DEFAULT '', " +  
								"DNAVenueOneResource INTEGER NOT NULL DEFAULT 0, " +  
								"DNAVenueTwoId INTEGER NOT NULL DEFAULT 0, " + 
								"DNAVenueTwoName TEXT NOT NULL DEFAULT '', " +    
								"DNAVenueTwoResource INTEGER NOT NULL DEFAULT 0, " +
								"DNAVenueThreeId INTEGER NOT NULL DEFAULT 0, " + 
								"DNAVenueThreeName TEXT NOT NULL DEFAULT '', " +  
								"DNAVenueThreeResource INTEGER NOT NULL DEFAULT 0, " +   
								"DNAAtmosphereOneId INTEGER NOT NULL DEFAULT 0, " + 
								"DNAAtmosphereOneName TEXT NOT NULL DEFAULT '', " +  
								"DNAAtmosphereOneResource INTEGER NOT NULL DEFAULT 0, " +  
								"DNAAtmosphereTwoId INTEGER NOT NULL DEFAULT 0, " + 
								"DNAAtmosphereTwoName TEXT NOT NULL DEFAULT '', " +    
								"DNAAtmosphereTwoResource INTEGER NOT NULL DEFAULT 0, " +
								"DNAAtmosphereThreeId INTEGER NOT NULL DEFAULT 0, " + 
								"DNAAtmosphereThreeName TEXT NOT NULL DEFAULT '', " + 
								"DNAAtmosphereThreeResource INTEGER NOT NULL DEFAULT 0, " + 
								"FirstName TEXT NOT NULL DEFAULT '', " +   
								"PreferenceDisplayName INTEGER NOT NULL DEFAULT 0, " +  
								"PreferenceDisplayPhotos INTEGER NOT NULL DEFAULT 0, " +  
								"PreferenceInvites INTEGER NOT NULL DEFAULT 0, " + 
								"HasGroups INTEGER NOT NULL DEFAULT 0, " +  
								"HasLastBuzzed INTEGER NOT NULL DEFAULT 0, " + 
								"LastName TEXT NOT NULL DEFAULT '')";
	private static final String CREATE_TABLE_GROUP = "CREATE TABLE MemberGroup (" +
								"Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
								"GroupId INTEGER NOT NULL DEFAULT 0, " +
								"GroupName TEXT NOT NULL DEFAULT '', " +
								"GroupDescription TEXT NOT NULL DEFAULT '', " +
								"IsPrivate INTEGER NOT NULL DEFAULT 0, " +
								"IsOwner INTEGER NOT NULL DEFAULT 0" +
								")";
	private static final String CREATE_TABLE_VENUE = "CREATE TABLE Venue (" + 
								"Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
								"VenueId INTEGER NOT NULL DEFAULT 0, " +
								"Name TEXT NOT NULL DEFAULT '', " +
								"Latitude DOUBLE NOT NULL DEFAULT 0, " +
								"Longitude DOUBLE NOT NULL DEFAULT 0" +
								")";
	private static final String DATABASE_NAME = "IgNightDB";
	private static final int DATABASE_VERSION = 1;
	private SQLiteDatabase db;
	private DBHelper dbHelper;
	private static final String DB_PATH = "/data/data/com.ignight.ignight/databases/";
	public static long ROW_INSERT_FAILED = -3;
	private static final String QUERY_VALID_USER = "SELECT * FROM User;";
	
	public DataAdapter(Context context){
		this.context = context;
		this.dbHelper = new DBHelper(this.context);
	}
	
	public boolean isOpen(){
		boolean retVal;
		if(this.dbHelper == null || this.dbHelper.mySqlDatabase == null)
			retVal = false;
		else
			retVal = this.dbHelper.mySqlDatabase.isOpen();
		return retVal;
	}
	
	public boolean open() throws SQLException{
		boolean retVal = false;
		try{
			if(checkDataBase()){
				retVal = this.dbHelper.mySqlDatabase.isOpen();
			}
		}catch(SQLException e){
			Log.d("Database Error","Error opening database");
		}
		return retVal;
	}
	
	public void close(){
		this.dbHelper.mySqlDatabase.close();
	}
	
	public void initDB(){
		Cursor cursor;
		
		db = dbHelper.getWritableDatabase();
		cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='User'", null);
		if(cursor.getCount() == 0){
			cursor.close();
			try{
				db.execSQL(CREATE_TABLE_USER);
			}catch(Exception e) { 
				Log.d("DataLayer","Failed creating user table: " + e.getMessage());
			}
		}
		else{
			cursor.close();
		}
		cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='Venue'", null);
		if(cursor.getCount() == 0){
			cursor.close();
			try{
				db.execSQL(CREATE_TABLE_VENUE);
			}catch(Exception e) { 
				Log.d("DataLayer","Failed creating venue table: " + e.getMessage());
			}
		}
		else{
			cursor.close();
		}
	}
	
	public void cleanDB(){
		Cursor cursor;
		try{
			db = dbHelper.getWritableDatabase();
			cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type = 'table' AND name='User'", null);
			if(cursor.getCount() > 0){
				db.execSQL("DELETE FROM User;");
			}
			cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type = 'table' AND name='SideGroup'", null);
			if(cursor.getCount() > 0){
				db.execSQL("DELETE FROM SideGroup;");
			}
			cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type = 'table' AND name='VenueInteraction'", null);
			if(cursor.getCount() > 0){
				db.execSQL("DELETE FROM VenueInteraction;");
			}
			cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type = 'table' AND name='InboxMessage'", null);
			if(cursor.getCount() > 0){
				db.execSQL("DELETE FROM InboxMessage;");
			}
			cursor.close();
		}catch(Exception ex){
			Log.d("DataLayer", ex.getMessage());
		}finally{
			this.dbHelper.close();
		}
	}
	
	public int updateUserProfilePic(String picUrl){
		int retVal = 0;
		StringBuilder stringBuilder = new StringBuilder();
		
		db = dbHelper.getWritableDatabase();
		
		stringBuilder.append("UPDATE User SET UserPhoto =  '"+picUrl+"'");
		
		try{
			db = dbHelper.getWritableDatabase();
			SQLiteStatement sqlStatement = db.compileStatement(stringBuilder.toString());
			sqlStatement.execute();
			retVal = 1;
		}catch(Exception ex){
			retVal = 0;
			Log.d("DataLayer", ex.getMessage());
		}finally{
			this.dbHelper.close();
		}
		
		return retVal;
	}
	
	public User LoggedInUser(){
		Cursor cursor = null;
		ArrayList<DNAAtmosphere> dnaAtmosphere = new ArrayList<DNAAtmosphere>();
		ArrayList<DNAMusic> dnaMusic = new ArrayList<DNAMusic>();
		ArrayList<DNAVenue> dnaVenue = new ArrayList<DNAVenue>();
		User user = null;

		try{
			if(checkDataBase()){
				//Log.d("DataLayer","Got database. Checking for user");
				db = dbHelper.getReadableDatabase();
				cursor = db.rawQuery(QUERY_VALID_USER, null);
				
				if(cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()){
					//Log.d("DataLayer","Found a user");
					
					user = new User(cursor.getString(cursor.getColumnIndex("EmailAddress")), 
							cursor.getString(cursor.getColumnIndex("UserPassword"))
					);
					
					user.setId(cursor.getInt(cursor.getColumnIndex("UserId")));
					
					//add music
					if(cursor.getInt(cursor.getColumnIndex("DNAMusicOneId")) != -1){
						dnaMusic.add(new DNAMusic(cursor.getInt(cursor.getColumnIndex("DNAMusicOneId")),
								cursor.getString(cursor.getColumnIndex("DNAMusicOneName"))));
					}
					if(cursor.getInt(cursor.getColumnIndex("DNAMusicTwoId")) != -1){
						dnaMusic.add(new DNAMusic(cursor.getInt(cursor.getColumnIndex("DNAMusicTwoId")),
								cursor.getString(cursor.getColumnIndex("DNAMusicTwoName"))));
					}
					if(cursor.getInt(cursor.getColumnIndex("DNAMusicThreeId")) != -1){
						dnaMusic.add(new DNAMusic(cursor.getInt(cursor.getColumnIndex("DNAMusicThreeId")),
								cursor.getString(cursor.getColumnIndex("DNAMusicThreeName"))));
					}
					user.setMusicOptions(dnaMusic);
					
					//add atmosphere
					if(cursor.getInt(cursor.getColumnIndex("DNAAtmosphereOneId")) != -1){
						dnaAtmosphere.add(new DNAAtmosphere(cursor.getInt(cursor.getColumnIndex("DNAAtmosphereOneId")),
								cursor.getString(cursor.getColumnIndex("DNAAtmosphereOneName"))));
					}
					if(cursor.getInt(cursor.getColumnIndex("DNAAtmosphereTwoId")) != -1){
						dnaAtmosphere.add(new DNAAtmosphere(cursor.getInt(cursor.getColumnIndex("DNAAtmosphereTwoId")),
								cursor.getString(cursor.getColumnIndex("DNAAtmosphereTwoName"))));
					}
					if(cursor.getInt(cursor.getColumnIndex("DNAAtmosphereThreeId")) != -1){
						dnaAtmosphere.add(new DNAAtmosphere(cursor.getInt(cursor.getColumnIndex("DNAAtmosphereThreeId")),
								cursor.getString(cursor.getColumnIndex("DNAAtmosphereThreeName"))));
					}
					user.setAtmosphereOptions(dnaAtmosphere);
					
					//add venues
					if(cursor.getInt(cursor.getColumnIndex("DNAVenueOneId")) != -1){
						dnaVenue.add(new DNAVenue(cursor.getInt(cursor.getColumnIndex("DNAVenueOneId"))));
					}
					if(cursor.getInt(cursor.getColumnIndex("DNAVenueTwoId")) != -1){
						dnaVenue.add(new DNAVenue(cursor.getInt(cursor.getColumnIndex("DNAVenueTwoId"))));
					}
					if(cursor.getInt(cursor.getColumnIndex("DNAVenueThreeId")) != -1){
						dnaVenue.add(new DNAVenue(cursor.getInt(cursor.getColumnIndex("DNAVenueThreeId"))));
					}
					user.setVenueOptions(dnaVenue);
					
					//add spending preference
					user.setSpendingPreference(cursor.getInt(cursor.getColumnIndex("SpendingPreference")));
					
					//add age
					user.setAge(cursor.getInt(cursor.getColumnIndex("UserAge")));
					
					//get city
					City city = new City(
							cursor.getInt(cursor.getColumnIndex("UserCityId")),
							cursor.getString(cursor.getColumnIndex("UserCityName"))
					);
					user.setCity(city);
					
					//get name
					user.setFirstName(cursor.getString(cursor.getColumnIndex("FirstName")));
					user.setLastName(cursor.getString(cursor.getColumnIndex("LastName")));
					user.setUserName(cursor.getString(cursor.getColumnIndex("UserName")));
					
					//set preference
					Preference preference = new Preference();
					preference.setDisplayName(cursor.getInt(cursor.getColumnIndex("PreferenceDisplayName")));
					preference.setDisplayPhotos(cursor.getInt(cursor.getColumnIndex("PreferenceDisplayPhotos")));
					preference.setInvites(cursor.getInt(cursor.getColumnIndex("PreferenceInvites")));
					user.setUserPreference(preference);
					
					user.setHasGroups(cursor.getInt(cursor.getColumnIndex("HasGroups")));
					user.setHasLastBuzzed(cursor.getInt(cursor.getColumnIndex("HasLastBuzzed")));
					user.setGender(cursor.getInt(cursor.getColumnIndex("GenderId")) == 0 ? "Male" : "Female");
					user.setUserPhoto(cursor.getString(cursor.getColumnIndex("UserPhoto")));

					cursor.close();
					cursor = null;
				}else{
					Log.d("DataLayer","User not found");
				}
			}
		}catch(Exception e){
			Log.e(APP_NAME, "An error accured while getting logged in user.", e);
			Log.d("DataLayer", e.getMessage());
		}finally{
			this.dbHelper.close();
		}
		
		return user;
	}
	
	public int UpdateUser(User user){
		Cursor cursor;
		int retVal = 0;
		boolean userDBCreated = false;
		
		db = dbHelper.getWritableDatabase();
		cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='User'", null);
		if(cursor.getCount() == 0){
			cursor.close();
		} else {
			cursor.close();
			userDBCreated = true;
		}
		
		if(userDBCreated && user != null){
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("UPDATE User ");
			stringBuilder.append("SET EmailAddress = " + DatabaseUtils.sqlEscapeString(user.getUserEmail()) + ", ");
			stringBuilder.append("UserName = " + DatabaseUtils.sqlEscapeString(user.getUserName()) + ", ");
			stringBuilder.append("UserPhoto = " + DatabaseUtils.sqlEscapeString(user.getUserPhoto()) + ", ");
			stringBuilder.append("UserCityName = " + DatabaseUtils.sqlEscapeString(user.getCity().getName()) + ", ");
			stringBuilder.append("UserCityId = " + user.getCity().getId() + ", ");
			stringBuilder.append("SpendingPreference = " + user.getSpendingPreference() + ", ");
			stringBuilder.append("DNAMusicOneId = " + (user.getMusicOptions().size() > 0 ? user.getMusicOptions().get(0).getId() : -1) + ", ");
			stringBuilder.append("DNAMusicOneName = " + (user.getMusicOptions().size() > 0 ? DatabaseUtils.sqlEscapeString(user.getMusicOptions().get(0).getName()) : "''") + ", ");
			stringBuilder.append("DNAMusicTwoId = " + (user.getMusicOptions().size() > 1 ? user.getMusicOptions().get(1).getId() : -1) + ", ");
			stringBuilder.append("DNAMusicTwoName = " + (user.getMusicOptions().size() > 1 ? DatabaseUtils.sqlEscapeString(user.getMusicOptions().get(1).getName()) : "''") + ", ");
			stringBuilder.append("DNAMusicThreeId = " + (user.getMusicOptions().size() > 2 ? user.getMusicOptions().get(2).getId() : -1) + ", ");
			stringBuilder.append("DNAMusicThreeName = " + (user.getMusicOptions().size() > 2 ? DatabaseUtils.sqlEscapeString(user.getMusicOptions().get(2).getName()) : "''") + ", ");
			stringBuilder.append("DNAAtmosphereOneId = " + (user.getAtmosphereOptions().size() > 0 ? user.getAtmosphereOptions().get(0).getId() : -1) + ", ");
			stringBuilder.append("DNAAtmosphereOneName = " + (user.getAtmosphereOptions().size() > 0 ? DatabaseUtils.sqlEscapeString(user.getAtmosphereOptions().get(0).getName()) : "''") + ", ");
			stringBuilder.append("DNAAtmosphereTwoId = " + (user.getAtmosphereOptions().size() > 1 ? user.getAtmosphereOptions().get(1).getId() : -1) + ", ");
			stringBuilder.append("DNAAtmosphereTwoName = " + (user.getAtmosphereOptions().size() > 1 ? DatabaseUtils.sqlEscapeString(user.getAtmosphereOptions().get(1).getName()) : "''") + ", ");
			stringBuilder.append("DNAAtmosphereThreeId = " + (user.getAtmosphereOptions().size() > 2 ? user.getAtmosphereOptions().get(2).getId() : -1) + ", ");
			stringBuilder.append("DNAAtmosphereThreeName = " + (user.getAtmosphereOptions().size() > 2 ? DatabaseUtils.sqlEscapeString(user.getAtmosphereOptions().get(2).getName()) : "''") + ", ");
			stringBuilder.append("PreferenceDisplayName = " + user.getUserPreference().getDisplayName() + ", ");
			stringBuilder.append("PreferenceDisplayPhotos = " + user.getUserPreference().getDisplayPhotos() + ", ");
			stringBuilder.append("GenderId = " + (user.getGender().equalsIgnoreCase("male") ? 0 : 1) + ", ");
			stringBuilder.append("UserAge = " + user.getAge() + ", ");
			stringBuilder.append("PreferenceInvites = " + user.getUserPreference().getInvites());
			stringBuilder.append(" WHERE UserId = " + user.getId() + ";");
			
			try{
				db = dbHelper.getWritableDatabase();
				SQLiteStatement sqlStatement = db.compileStatement(stringBuilder.toString());
				sqlStatement.execute();
				retVal = 1;
			}catch(Exception ex){
				retVal = 0;
				Log.d("DataLayer", ex.getMessage());
			}finally{
				this.dbHelper.close();
				//db.close();
			}
		}
		
		return retVal;
	}
	
	public int AddUser(User user){
		Cursor cursor;
		int retVal = 0;
		boolean userDBCreated = false;
		
		db = dbHelper.getWritableDatabase();
		
		cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='User'", null);
		if(cursor.getCount() == 0){
			cursor.close();
			try{
				db.execSQL(CREATE_TABLE_USER);
				userDBCreated = true;
			}catch(Exception e) { 
				Log.d("DataLayer","Failed creating user table: " + e.getMessage());
			}
		}
		else{
			cursor.close();
			userDBCreated = true;
		}
		
		if(userDBCreated && user != null){
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("INSERT INTO User (UserId, EmailAddress, GenderId, UserName, UserPassword, UserPhoto, UserCityName, UserCityId, UserPhoto, SpendingPreference, ");
			stringBuilder.append("DNAMusicOneId, DNAMusicOneName, DNAMusicOneResource, DNAMusicTwoId, DNAMusicTwoName, DNAMusicTwoResource, DNAMusicThreeId, ");
			stringBuilder.append("DNAMusicThreeName, DNAMusicThreeResource, DNAVenueOneId, DNAVenueOneName, DNAVenueOneResource, DNAVenueTwoId, DNAVenueTwoName, ");
			stringBuilder.append("DNAVenueTwoResource, DNAVenueThreeId, DNAVenueThreeName, DNAVenueThreeResource, DNAAtmosphereOneId, DNAAtmosphereOneName, DNAAtmosphereOneResource, ");
			stringBuilder.append("DNAAtmosphereTwoId, DNAAtmosphereTwoName, DNAAtmosphereTwoResource, DNAAtmosphereThreeId, DNAAtmosphereThreeName, DNAAtmosphereThreeResource, FirstName, PreferenceDisplayName, PreferenceDisplayPhotos, PreferenceInvites, HasGroups, HasLastBuzzed, LastName, UserAge) ");
			stringBuilder.append("VALUES (");
			stringBuilder.append(user.getId() + ", " +  DatabaseUtils.sqlEscapeString(user.getUserEmail()) + ", " + (user.getGender().equalsIgnoreCase("male") ? 0 : 1) + ", " + DatabaseUtils.sqlEscapeString(user.getUserName()) + ", '" + user.getUserPassword() + "', '" + user.getUserPhoto() + "', '" + user.getCity().getName() + "', " + user.getCity().getId() + ", '', " + user.getSpendingPreference() + ", ");
			stringBuilder.append(user.getMusicOptions().get(0).getId() + ", " + DatabaseUtils.sqlEscapeString(user.getMusicOptions().get(0).getName()) + ", 0, ");
			stringBuilder.append((user.getMusicOptions().size() > 1 ? user.getMusicOptions().get(1).getId() : -1) + ", " + (user.getMusicOptions().size() > 1 ? DatabaseUtils.sqlEscapeString(user.getMusicOptions().get(1).getName()) : "''") + ", 0, ");
			stringBuilder.append((user.getMusicOptions().size() > 2 ? user.getMusicOptions().get(2).getId() : -1) + ", " + (user.getMusicOptions().size() > 2 ? DatabaseUtils.sqlEscapeString(user.getMusicOptions().get(2).getName()) : "''") + ", 0, ");
			stringBuilder.append((user.getVenueOptions().size() > 0 ? user.getVenueOptions().get(0).getId() : -1) + ", '', 0, ");
			stringBuilder.append((user.getVenueOptions().size() > 1 ? user.getVenueOptions().get(1).getId() : -1) + ", '', 0, ");
			stringBuilder.append((user.getVenueOptions().size() > 2 ? user.getVenueOptions().get(2).getId() : -1) + ", '', 0, ");
			stringBuilder.append(user.getAtmosphereOptions().get(0).getId() + ", " + DatabaseUtils.sqlEscapeString(user.getAtmosphereOptions().get(0).getName()) + ", 0, ");
			stringBuilder.append((user.getAtmosphereOptions().size() > 1 ? user.getAtmosphereOptions().get(1).getId() : -1) + ", " + (user.getAtmosphereOptions().size() > 1 ? DatabaseUtils.sqlEscapeString(user.getAtmosphereOptions().get(1).getName()) : "''") + ", 0, ");
			stringBuilder.append((user.getAtmosphereOptions().size() > 2 ? user.getAtmosphereOptions().get(2).getId() : -1) + ", " + (user.getAtmosphereOptions().size() > 2 ? DatabaseUtils.sqlEscapeString(user.getAtmosphereOptions().get(2).getName()) : "''") + ", 0, ");
			stringBuilder.append(DatabaseUtils.sqlEscapeString(user.getFirstName()) + ","+user.getUserPreference().getDisplayName()+","+user.getUserPreference().getDisplayPhotos()+","+user.getUserPreference().getInvites()+", " + user.getHasGroups() + ", " + user.getHasLastBuzzed() + ", " + DatabaseUtils.sqlEscapeString(user.getLastName()));
			stringBuilder.append(","+user.getAge()+");");
			
			try{
				db = dbHelper.getWritableDatabase();
				SQLiteStatement sqlStatement = db.compileStatement(stringBuilder.toString());
				sqlStatement.execute();
				retVal = 1;
			}catch(Exception ex){
				retVal = 0;
				Log.d("DataLayer", ex.getMessage());
			}finally{
				this.dbHelper.close();
			}
			Log.d("DataLayer","User status: " + retVal);
		}else{
			Log.d("DataLayer","User is null or database was not created.");
		}
		return retVal;
	}
	
	public int AddVenues(ArrayList<Venue> venues){
		Cursor cursor;
		int retVal = 0;
		boolean venueDBCreated = false;
		
		if(db == null || !db.isOpen()){
			db = dbHelper.getWritableDatabase();
		}
		
		cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='Venue'", null);
		if(cursor.getCount() == 0){
			cursor.close();
			try{
				db.execSQL(CREATE_TABLE_VENUE);
				venueDBCreated = true;
			}catch(Exception e) { 
				Log.d("DataLayer","Failed creating venue table: " + e.getMessage());
			}
		}
		else{
			cursor.close();
			venueDBCreated = true;
		}
		
		if(venueDBCreated){
			db.execSQL("DELETE FROM Venue;");
			
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("INSERT INTO Venue (");
			stringBuilder.append("VenueId, Name, Latitude, Longitude");
			stringBuilder.append(") VALUES (");
			stringBuilder.append("?,?,?,?)");
			
			SQLiteStatement sqlStatement = db.compileStatement(stringBuilder.toString());
			db.beginTransaction();
			
			try{
				for(Venue venue: venues){
					sqlStatement.clearBindings();
					sqlStatement.bindLong(1, venue.getId());
					sqlStatement.bindString(2, venue.getName());
					sqlStatement.bindDouble(3, venue.getLatitude());
					sqlStatement.bindDouble(4, venue.getLongitude());
					sqlStatement.execute();
				}
				db.setTransactionSuccessful();
				retVal = 1;
			}catch (Exception e) {
	            Log.e(APP_NAME, "An error occurred while inserting the row: "+e.toString(), e);
	            retVal = 0;
	        } finally{
	        	db.endTransaction();
	        	this.dbHelper.close();
	        }
		}
		return retVal;
	}
	
	public ArrayList<Venue> GetVenues(){
		Cursor cursor = null;
		ArrayList<Venue> venues = new ArrayList<Venue>();
		
		try{
			db = dbHelper.getReadableDatabase();
			
			cursor = db.rawQuery("SELECT DISTINCT tbl_name from sqlite_master where tbl_name = '"+DATABASE_NAME+"'", null);
			if(cursor != null){
				cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='Venue'", null);
				if(cursor.getCount() == 0){
					cursor.close();
					try{
						db.execSQL(CREATE_TABLE_VENUE);
					}catch(Exception e) { }
				}
				cursor = db.rawQuery("SELECT * FROM Venue;", null);
				
				if(cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()){
					Log.d("DataLayer", "Getting venues");
					do{
	                    venues.add(
	                        new Venue(
	                        		cursor.getInt(cursor.getColumnIndex("VenueId")),
	                        		cursor.getString(cursor.getColumnIndex("Name")),
	                        		cursor.getDouble(cursor.getColumnIndex("Latitude")),
	                        		cursor.getDouble(cursor.getColumnIndex("Longitude"))                		
	                        )
	                    );
	                }while(cursor.moveToNext());
				}else{
					Log.d("DataLayer", "No venues in database");
				}
				cursor.close();
				Log.d("DataLayer", "got venues: " + venues.size());
			}
		}catch(Exception e){
			Log.e(APP_NAME, "An error accured while getting venues.", e);
			Log.d("DataLayer", "Error getting venues");
		}finally{
			this.dbHelper.close();
		}
		
		return venues;
	}
	
	public void deleteUser(){
		try{
			db = dbHelper.getWritableDatabase();
			db.execSQL("DROP TABLE IF EXISTS User;");
		}catch(Exception ex){
			Log.d("DataLayer", ex.getMessage());
		}finally{
			this.dbHelper.close();
		}
	}
	
	private boolean checkDataBase(){
		Cursor cursor;
		boolean retVal = false;
		
		try{
			db = dbHelper.getReadableDatabase();
			cursor = db.rawQuery("SELECT DISTINCT tbl_name from sqlite_master where tbl_name = '"+DATABASE_NAME+"'", null);
			if(cursor != null){
				cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='User'", null);
				if(cursor.getCount() == 0){
					cursor.close();
					db.execSQL(CREATE_TABLE_USER);
				}
				else{
					cursor.close();
				}
				retVal = true;
			}
		}catch(Exception ex){
			
		}
		
		return retVal;
	}
	
	private static class DBHelper extends SQLiteOpenHelper{
		private Context myContext;
		private SQLiteDatabase mySqlDatabase;
		
		public DBHelper(Context context){
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			myContext = context;
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			//db.execSQL(CREATE_TABLE_USER);
		}

		@Override
		public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
			// TODO Auto-generated method stub
		}
	}
}
