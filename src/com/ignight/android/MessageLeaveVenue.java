package com.ignight.android;

public class MessageLeaveVenue {
	private class NewLeaveVenue{
		private int userId;
		private int venueId;
		private String type;
		
		NewLeaveVenue(int userId, int venueId){
			this.userId = userId;
			this.venueId = venueId;
			this.type = "leaveVenue";
		}
	}
	private NewLeaveVenue msg;
	MessageLeaveVenue(int userId, int venueId){
		msg = new NewLeaveVenue(userId, venueId);
	}
}
