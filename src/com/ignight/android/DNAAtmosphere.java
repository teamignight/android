package com.ignight.android;

public class DNAAtmosphere {
	private int Id;
	private String Name;
	private int Position;
	private int[] ResourceIds = {R.drawable.dna_atmosphere_0, R.drawable.dna_atmosphere_5, R.drawable.dna_atmosphere_6, 
			R.drawable.dna_atmosphere_1, R.drawable.dna_atmosphere_7, R.drawable.dna_atmosphere_2, 
			R.drawable.dna_atmosphere_3, R.drawable.dna_atmosphere_4};
	private boolean Selected;
	
	public DNAAtmosphere(int id, String name){
		this.Id = id;
		this.Name = name;
	}
	
	public int getId(){
		return this.Id;
	}
	
	public String getName(){
		return this.Name;
	}
	
	public int getPosition(){
		return this.Position;
	}
	
	public void setPosition(int position){
		this.Position = position;
	}
	
	public int getResourceId(int id){
		return ResourceIds[id];
	}
	
	public boolean isSelected(){
		return this.Selected;
	}
	
	public void setSelected(boolean selected){
		this.Selected = selected;
	}
}
