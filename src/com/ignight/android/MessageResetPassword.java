package com.ignight.android;

public class MessageResetPassword {
	private class NewResetPassword{
		private String password;
		private String tempPassword;
		private String userName;
		private String type;
		
		NewResetPassword(String password, String tempPassword, String userName){
			this.password = password;
			this.tempPassword = tempPassword;
			this.userName = userName;
			this.type = "resetPassword";
		}
	}
	private NewResetPassword msg;
	MessageResetPassword(String password, String tempPassword, String userName){
		msg = new NewResetPassword(password, tempPassword, userName);
	}
}
