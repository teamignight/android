package com.ignight.android;

import java.util.ArrayList;

import com.ignight.android.AdapterMusic.GridHolder;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterSpendingPreference extends BaseAdapter {
	private LayoutInflater inflater;
	private Context mContext;
	private ArrayList<DNASpendingPreference> preferences;
	private User user;
	
	public AdapterSpendingPreference(Context context, ArrayList<DNASpendingPreference> dnaSpending, User user){
		this.mContext = context;
		this.preferences = dnaSpending;
		this.user = user;
	}
	
	@Override
	public int getCount() {
		return preferences.size();
	}

	@Override
	public Object getItem(int position) {
		return preferences.get(position);
	}

	@Override
	public long getItemId(int position) {
		return preferences.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		DNASpendingPreference dnaPreference;
		boolean found = false;
		GridHolder holder = null;
		View view = convertView;
						
		if(view == null){
			inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.list_dna_spending_preference, null);
			
			holder = new GridHolder();
			holder.imageView = (ImageView)view.findViewById(R.id.gridSpendingDNAImage);
			holder.imageNumber = (ImageView)view.findViewById(R.id.gridSpendingNumber);
			holder.txtTitle = (TextView)view.findViewById(R.id.gridSpendingTitle);
			view.setTag(holder);
		}else{
			holder = (GridHolder) view.getTag();
		}
		
		dnaPreference = preferences.get(position);

		holder.txtTitle.setText(dnaPreference.getName());
		//holder.imageView.setImageResource(dnaPreference.getResourceId(dnaPreference.getId()));
		Picasso.with(mContext).load(dnaPreference.getResourceId(dnaPreference.getId())).noFade().into(holder.imageView);
		
		if(dnaPreference.getId() == (user.getSpendingPreference())){
			found = true;
		}
		
		if(found){
			holder.imageNumber.setImageResource(R.drawable.dna_selected);
			holder.imageNumber.setTag(user.getSpendingPreference());
		}else{
			holder.imageNumber.setImageDrawable(null);
			holder.imageNumber.setTag(-1);
		}/**/

		return view;
	}
	
	static class GridHolder {
		ImageView imageNumber;
		ImageView imageView;
		TextView txtTitle;
    }
}
