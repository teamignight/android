package com.ignight.android;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

public class Music {
	private AdapterMusic Adapter;
	private int Id;
	private Bitmap Image;
	private int ImageResourceId;
	private String ImageUrl;
	private String Name;
	private int Position;
	private boolean Selected;
	
	public Music(int id, int resourceId, String name){
		this.Id = id;
		this.ImageResourceId = resourceId;
		this.Name = name;
		this.Selected = false;
	}
	
	public int getId(){
		return this.Id;
	}
	public void setId(int id){
		this.Id = id;
	}
	public int getImageResourceId(){
		return this.ImageResourceId;
	}
	public void setImageResourceId(int resourceId){
		this.ImageResourceId = resourceId;
	}
	public String getImageUrl(){
		return this.ImageUrl;
	}
	public void setImageUrl(String url){
		this.ImageUrl = url;
	}
	public Bitmap getImage(){
		return this.Image;
	}
	public AdapterMusic getAdapter(){
		return this.Adapter;
	}
	public void setAdapter(AdapterMusic adapter){
		this.Adapter = adapter;
	}
	public void loadImage(AdapterMusic adapter){
		this.Adapter = adapter;
		if(ImageUrl != null && !ImageUrl.equals("")){
			new ImageLoadTask().execute(ImageUrl);
		}
	}
	public String getName(){
		return this.Name;
	}
	public void setName(String name){
		this.Name = name;
	}
	public int getPosition(){
		return this.Position;
	}
	public void setPosition(int position){
		this.Position = position;
	}
	public boolean isSelected(){
		return this.Selected;
	}
	public void setSelected(boolean selected){
		this.Selected = selected;
	}
	private class ImageLoadTask extends AsyncTask<String, String, Bitmap>{

		@Override
		protected Bitmap doInBackground(String... param) {
			Bitmap bitmap;
			URL url = null;
			InputStream inputStream;
			HttpURLConnection urlConnection;
			
			//Log.i("ImageLoadTask","Attempting to load image");
			try{
				url = new URL(param[0]);
			}catch(MalformedURLException e){
				url = null;
			}
			
			if(url != null){
				try{
					urlConnection = (HttpURLConnection)url.openConnection();
					urlConnection.setDoInput(true);
					urlConnection.connect();
					
					inputStream = urlConnection.getInputStream();
					
					bitmap = BitmapFactory.decodeStream(inputStream);
					
					return bitmap;
				}catch(IOException e){
					return null;
				}
			}
			return null;
		}
		
		protected void onPostExecute(Bitmap bitmap){
			if(bitmap != null){
				//Log.i("ImageLoadTask","Successfully loaded image");
				Image = bitmap;
				if(Adapter != null){
					Adapter.notifyDataSetChanged();
					//Log.i("ImageLoadTask","Adapter is not null");
				}else{
					//Log.i("ImageLoadTask","Adapter is null");
				}
			}
		}
	}
}
