package com.ignight.android;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AppBase extends Application {
	private int CityId;
	private static Context currentContext;
	private DataAdapter dataAdapter;
	private User ActiveUser;
	private BufferedReader bufferReader;
	private Gson gson;
	private HttpClient httpClient;
	private HttpGet httpGet;
	private HttpEntity httpEntity;
	private HttpPost httpPost;
	private HttpResponse httpResponse;
	private String httpResult;
	private InputStream inputStream;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private StringBuilder stringBuilder;

	public int InboxCount;
	public boolean InitSideMenu;
	public boolean ShowReportVenue;
	
	public Group ActiveGroup;
	public Venue ActiveVenue;
	public Group UpdatedGroup;
	public Venue UpdatedVenue;
	public ArrayList<Venue> ChangedVenues;
	public boolean IsGroupSearch;
	public String LargeImage;
	public ArrayList<Venue> LocalVenues;
	public boolean MapIsTouched;
	public float MapRadius;
	public double MapSearchLat;
	public double MapSearchLong;
	public float MapZoom;
	public boolean MapZoomedIn;
	public boolean ShowBuzz;
	public boolean ShowMembers;
	public boolean UpdatedTrendingMap;
	public ArrayList<Venue> Venues;
	
	//Buzz States
	public String BuzzType;
	//public final String DefaultChatUrl = "http://s3.amazonaws.com/ignight.chicago.profile-pics2/DefaultChatImage.jpeg";
	public final String DefaultChatUrl = "default";
	public ArrayList<Integer> BuzzedInGroups;
	public ArrayList<Integer> BuzzedInVenues;
	public boolean RefreshGroupsBuzz;
	public ArrayList<Buzz> BuzzImages;
	public int BuzzImagePosition;
	public int BuzzImageId;
	
	//Trending States
	public String TrendingButton;
	public boolean TrendingFilterShowing;
	public DNAMusic TrendingMusicFilter;
	public String TrendingState;
	public String TrendingType;
	public boolean TrendingUpvoteShowing;
	public String TrendingView;
	public boolean CalledFromTrending;
	public String CalledFromTrendingType;
	public boolean CalledFromVenueInfo;
	public int TrendingMusicPref;
	public boolean RefreshTrending;
	public boolean UpdatedTrendingVenue;
	public boolean RefreshGroupTrending;
	public boolean NearMeShowing;
	public boolean SaveListPosition;
	
	//Group Members Type
	public boolean AddingGroupMembers;
	public int DirtyGroup;
	public boolean OnGroupPage;
	public boolean LeftGroup;
	
	//photo gallery type
	public String PhotoGalleryType;
	
	public boolean ShowInbox;
	public boolean RefreshInbox;
	public SparseArray<DNAAtmosphere> hash;
	public boolean NewTutorial = false;
	
	//Google Cloud Messaging
	private GoogleCloudMessaging gcm;
	private String regid;
	public String appRegistered = "no";
	public boolean NewGroupBuzz = false;
	public boolean NewVenueBuzz = false;
	public int NewVenueBuzzId;
	
	//photo upload
	public File OutFile = null;
	public boolean UploadingPhoto;
	public boolean UploadPhotoInit;
	public String LocalImagePath;
	public boolean AddBuzzPhoto;
	
	@Override
    public void onCreate() {
        super.onCreate();
        currentContext = this;
        DirtyGroup = -1;
    }

    public static Context getContext(){
        return currentContext;
    }
    
    public void clearApp(){
    	InboxCount = 0;
    	ActiveGroup = null;
    	ActiveVenue = null;
    	CalledFromTrending = false;
    	CalledFromTrendingType = "";
    	BuzzedInGroups = null;
    	BuzzedInVenues = null;
    }
    
    public void showMessage(String msg){
    	Toast message = Toast.makeText(this, msg, Toast.LENGTH_LONG);
		message.setGravity(Gravity.CENTER, message.getXOffset() / 2, message.getYOffset() / 2);
		message.show();
    }
    
    public double getDistance(double longitude, double latitude){
    	double retVal = 0;
    	float results[] = new float[1];
    	
    	try {
    		 Location.distanceBetween(this.ActiveUser.getLatitude(),this.ActiveUser.getLongitude(), 
    				 latitude, longitude, results);
    		 
    		 int dist = (int) results[0];
    		 if(dist<=0)
    		 return 0D;
    		 
    		 DecimalFormat decimalFormat = new DecimalFormat("#.##");
    		 results[0]/=1000D;
    		 String distance = decimalFormat.format(results[0]);
    		 retVal = Double.parseDouble(distance);
		 } catch (Exception e) {
    		 if (e != null)
    			 e.printStackTrace();
		 }
    	
    	return retVal;
    }
    
    public void reportError(String error){
    	//LOG this error
    }
    
    public void setActiveUser(User user){
    	this.ActiveUser = user;
    }
    
    public User getActiveUser(){
    	if(ActiveUser == null){
    		dataAdapter = new DataAdapter(currentContext);
    		ActiveUser = dataAdapter.LoggedInUser();
    	}
    	if(ActiveUser != null){
	    	/*if(ActiveUser.getGroups().size() == 0 && ActiveUser.getHasGroups() == 1){
	    		new GetGroupsTask().execute();
	    	}
	    	if(ActiveUser.getVenuesIBuzzedIn().size() == 0 && ActiveUser.getHasLastBuzzed() == 1){
	    		new GetLastBuzzedTask().execute();
	    	}*/
    	}
		return ActiveUser;
    }
    
    public void setGridViewHeightBasedOnChildren(GridView gridView) {
        ListAdapter listAdapter = gridView.getAdapter(); 
        View gridItem;
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int colCount = (int)Math.ceil(listAdapter.getCount()/3);
        if((listAdapter.getCount() % 3) != 0){
        	colCount = colCount + 1;
        }
        for (int i = 0; (i < colCount); i++) {
        	gridItem = listAdapter.getView(i, null, gridView);
            if (gridItem instanceof ViewGroup){
            	gridItem.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT));
            }
            WindowManager wm = (WindowManager) currentContext.getSystemService(Context.WINDOW_SERVICE);
			Display display = wm.getDefaultDisplay();
			Point p = getDisplaySize(display);
			int screenWidth = p.x;

			int listViewWidth = screenWidth - 10 - 10;
			int widthSpec = MeasureSpec.makeMeasureSpec(listViewWidth,MeasureSpec.AT_MOST);
			gridItem.measure(widthSpec, 0);

			totalHeight += gridItem.getMeasuredHeight();
        }
        
        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight + (gridView.getVerticalSpacing() * (colCount));
        gridView.setLayoutParams(params);
        gridView.requestLayout();
    }
    
    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter(); 
        View listItem;
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup){
				listItem.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT));
            }
            WindowManager wm = (WindowManager) currentContext.getSystemService(Context.WINDOW_SERVICE);
			Display display = wm.getDefaultDisplay();
			Point p = getDisplaySize(display);
			int screenWidth = p.x;

			int listViewWidth = screenWidth - 10 - 10;
			int widthSpec = MeasureSpec.makeMeasureSpec(listViewWidth,MeasureSpec.AT_MOST);
			listItem.measure(widthSpec, 0);

			totalHeight += listItem.getMeasuredHeight();
            /*if(listItem != null){
            	listItem.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED), MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
            	totalHeight += listItem.getMeasuredHeight();
            }*/
        }
        
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
    
    public void setPushNotificationKey(){
    	if(checkPlayServices()){
    		new GetRegIdTask().execute();
    	}
    }
    
    public void checkPushNotification(){
    	new GetPushStatusTask().execute();
    }
    
    public void getVenues(int cityId){
    	this.CityId = cityId;
    	new GetVenuesTask().execute();
    }
    
    private class GetPushStatusTask extends AsyncTask<Void, Void, String>{
    	@Override
		protected String doInBackground(Void... args) {
			String retVal = "";
			String singleLine = "";
			
			try{
				httpClient = new CustomHttpClient(getContext());
				httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_user) + "?type=getInfo&userId=" + ActiveUser.getId());
				httpGet.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpResponse = httpClient.execute(httpGet);
				
				httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();
				bufferReader = new BufferedReader(new InputStreamReader(inputStream));
				stringBuilder = new StringBuilder();
				
				while((singleLine = bufferReader.readLine()) != null){
					stringBuilder.append(singleLine);
				}
				
				httpResult = stringBuilder.toString();
				inputStream.close();
				
				jsonObject = new JSONObject(httpResult);
				
				if(jsonObject.getBoolean("res")){
					retVal = "success";
				}else{
					retVal = "failed";
				}
				
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			return retVal;
		}
    	
    	@Override
		protected void onPostExecute(String result){
    		boolean haveKey = true;
			if(result.equalsIgnoreCase("success")){
				try {
					haveKey = (jsonObject.getJSONObject("body").getJSONArray("androidRegistrationIdsList").length() > 0);
				} catch (JSONException e) {
					
				}
			}
			
			if(!haveKey){
				setPushNotificationKey();
			}else{
				if(ActiveUser != null)
					ActiveUser.setPushSet(true);
			}
				
		}
    }
    
    private void getVenuesResult(String result){
    	JSONObject jObject;
    	//Log.d("Venues","Returned with result: " + result);
    	if(result.equals("success")){
    		Venues = new ArrayList<Venue>();
    		try{
				for(int i = 0; i < jsonArray.length(); i++){
					jObject = jsonArray.getJSONObject(i);
					Venues.add(new Venue(jObject.getInt("id"),jObject.getString("name"), jObject.getDouble("latitude"), jObject.getDouble("longitude") ));
				}
				DataAdapter dataAdapter = new DataAdapter(this);
				dataAdapter.AddVenues(Venues);
				LocalVenues = Venues;
    		}catch(JSONException e){
    			//Log.d("Venues", e.getMessage());
			}
    	}
    }
    
    private class GetVenuesTask extends AsyncTask<Void, Void, String>{

		@Override
		protected String doInBackground(Void... args) {
			String retVal = "";
			String singleLine = "";
			
			try{
				httpClient = new DefaultHttpClient();
				httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_data) + "?type=venueList&cityId=" + String.valueOf(CityId));
				httpResponse = httpClient.execute(httpGet);
				
				httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();
				bufferReader = new BufferedReader(new InputStreamReader(inputStream));
				stringBuilder = new StringBuilder();
				
				while((singleLine = bufferReader.readLine()) != null){
					stringBuilder.append(singleLine);
				}
				
				httpResult = stringBuilder.toString();
				inputStream.close();
				
				jsonObject = new JSONObject(httpResult);
				
				if(jsonObject.getBoolean("res")){
					retVal = "success";
					jsonArray = jsonObject.getJSONArray("body");
				}else{
					retVal = "failed";
				}
				
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			return retVal;
		}
		@Override
		protected void onPostExecute(String result){
			getVenuesResult(result);
		}
    }

    private static Point getDisplaySize(final Display display) {
        final Point point = new Point();
        try {
            display.getSize(point);
        } catch (java.lang.NoSuchMethodError ignore) { // Older device
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        return point;
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            return false;
        }
        return true;
    }
    
    private void sendPushResult(String result){
    	//Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
    }
    
    public void sendTestPush(){
    	new SendPushTask().execute();
    }
    
    private class SendPushTask extends AsyncTask<Void, Void, String>{
    	@Override
		protected String doInBackground(Void... args) {
    		String retVal = "success";
    		
    		try {
                Bundle data = new Bundle();
                    data.putString("my_message", "Hello World");
                    data.putString("my_action",
                            "com.ignight.android.ECHO_NOW");
                    Random generator = new Random(); 
                    String id = String.valueOf(generator.nextInt(100) + 1);
                    if (gcm == null) {
    	                gcm = GoogleCloudMessaging.getInstance(currentContext);
    	            }
                    gcm.send("849661973283@developer.gserviceaccount.com", id, data);
                    retVal = "Sent message";
            } catch (IOException ex) {
            	retVal = "Error :" + ex.getMessage();
            }
    		
    		return retVal;
    	}
    	
    	@Override
        protected void onPostExecute(String result) {
    		sendPushResult(result);
    	}
    }
    
    private void getRegIdResult(String result){
    	if(result.equals("success")){
    		//Log.d("app","successfully got reg id");
    		/*Toast.makeText(getApplicationContext(),
    	            "Registered with GCM Server: " + regid, Toast.LENGTH_LONG)
    	            .show();*/
    		new GetRegistrationKeyTask().execute();
    	}else{
    		//Log.d("app","Error getting regid");
    		/*Toast.makeText(getApplicationContext(),
    	            "Error, count not Register with GCM Server. " + result, Toast.LENGTH_LONG)
    	            .show();*/
    	}
    }
    
    private class GetRegIdTask extends AsyncTask<Void, Void, String>{
    	@Override
		protected String doInBackground(Void... args) {
    		String retVal = "success";
    		
    		try{
	    		if (gcm == null) {
	                gcm = GoogleCloudMessaging.getInstance(currentContext);
	            }
	    		regid = gcm.register(getString(R.string.project_push_notifications_number));
	    		if(ActiveUser != null)
	    			ActiveUser.setPushSet(true);
    		}catch(IOException e){
    			retVal = "failed : " + e.getMessage().toString();
    		}
    		return retVal;
    	}
    	
    	@Override
        protected void onPostExecute(String result) {
    		getRegIdResult(result);
    	}
    }
    
    private void getPushResult(String result){
		if(result.equals("success")){
			//Log.d("app","successfully registered");
			appRegistered = "registered";
			/*Toast.makeText(getApplicationContext(),
    	            "Got key: ", Toast.LENGTH_LONG)
    	            .show();*/
		}else{
			//Log.d("app", "Error, could not register: " + result);
			appRegistered = "Error, could not register: " + result;
			/*Toast.makeText(getApplicationContext(),
    	            "Error, no key: " + result, Toast.LENGTH_LONG)
    	            .show();*/
		}
	}
	private class GetRegistrationKeyTask extends AsyncTask<Void, Void, String>{
		
		@Override
		protected String doInBackground(Void... args) {
			String json = "";
			String retVal = "";
			String singleLine = "";
			try{
				gson = new GsonBuilder().create();
				MessagePushRegistrationKey regKey = new MessagePushRegistrationKey(regid);
				json = gson.toJson(regKey);
				StringEntity stringEntity = new StringEntity(json);
				httpClient = new CustomHttpClient(getContext());
				httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_push_token) + getActiveUser().getId());
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					if(httpResult != null){
						jsonObject = new JSONObject(httpResult);
						
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							retVal = "Error, could not register for push notifications.";
						}
					}else{
						retVal = "Error, no result.";
					}
				}
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			return retVal;
		}
		
		@Override
		protected void onPostExecute(String result){
			getPushResult(result);
		}
	}
}
