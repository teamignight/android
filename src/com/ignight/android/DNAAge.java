package com.ignight.android;

public class DNAAge {
	private int Id;
	private String Name;
	private int[] ResourceIds = { R.drawable.dna_age_1, R.drawable.dna_age_2, R.drawable.dna_age_3, R.drawable.dna_age_4,
			R.drawable.dna_age_5, R.drawable.dna_age_6};
	
	public DNAAge(int id, String name){
		this.Id = id;
		this.Name = name;
	}

	public int getId(){
		return this.Id;
	}
	
	public String getName(){
		return this.Name;
	}
	
	public int getResourceId(int id){
		return ResourceIds[id];
	}
}
