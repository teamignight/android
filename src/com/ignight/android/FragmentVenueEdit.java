package com.ignight.android;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ignight.android.R.drawable;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

public class FragmentVenueEdit extends android.support.v4.app.Fragment {
	private AppBase app;
	private BufferedReader bufferReader;
	private Activity fragmentActivity;
	private Gson gson;
	private HttpClient httpClient;
	private HttpGet httpGet;
	private HttpEntity httpEntity;
	private HttpPost httpPost;
	private HttpResponse httpResponse;
	private String httpResult;
	private InputStream inputStream;
	private JSONObject jsonObject;
	private boolean newVenue;
	private StringBuilder stringBuilder;
	private Venue venue;
	private ActivityVenue venueActivity;
	private EditText venueAddress;
	private EditText venueName;
	private EditText venuePhone;
	private EditText venueReport;
	private EditText venueWebsite;
	private View view;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        view = inflater.inflate(R.layout.fragment_venue_edit, container, false);
        venueActivity = ((ActivityVenue)getActivity());
        if (fragmentActivity != null) {
	        app = venueActivity.app;
	        venue = app.ActiveVenue;
        }
        
        venueAddress = (EditText)view.findViewById(R.id.txtVenueAddress);
        venueName = (EditText)view.findViewById(R.id.txtEditVenueTitle);
        venuePhone = (EditText)view.findViewById(R.id.txtVenuePhone);
        venueWebsite = (EditText)view.findViewById(R.id.txtVenueWebsite);
        venueReport = (EditText)view.findViewById(R.id.txtEditReason);
        
        venuePhone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        venueActivity.imgVenueBuzz.setImageResource(drawable.venue_buzz);
    	venueActivity.imgVenueInfo.setImageResource(drawable.venue_info);
    	venueActivity.imgVenuePhotos.setImageResource(drawable.venue_photos);
        
        setData();
        
        return view;
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		fragmentActivity = activity;
	}
	
	@Override
	public void onResume(){
		setData();
		super.onResume();
	}
	
	public void reportVenue(){
		if(venueName.getText().toString().isEmpty())
			venueActivity.showMessage("Venue name is required.");
		else
			new ReportVenueTask().execute();
	}
	
	private void setData(){
		if(app.ShowReportVenue){
        	app.ShowReportVenue = false;
        	newVenue = true;
        	TextView txtAddVenueTitle = (TextView)view.findViewById(R.id.txtAddVenueTitle);
        	txtAddVenueTitle.setText("Add a Venue Below");
        	venueActivity.wireupHeader("Add a Venue","");
        	venueActivity.toggleBar(true);
        	venueAddress.setHint("Venue Address");
        	venueName.setHint("Venue Name");
        	venuePhone.setHint("Venue Phone Number");
        	venueWebsite.setHint("Venue Website");
        	venueName.setInputType(EditorInfo.TYPE_CLASS_TEXT);
        }else{
	        venueAddress.setText(venue.getAddress());
	        venueName.setText(venue.getName());
	        venuePhone.setText(venue.getPhoneNumber());
	        venueWebsite.setText(venue.getUrl());
        }
		venueReport.setText("");
	}
	
	private void reportVenueResult(String result){
		venueActivity.hideProgressDialog();
		if(result.equals("success")){
			venueActivity.showMessage("Information submitted successfully.");
			Intent intent;
			if(!newVenue){
				intent = new Intent(venueActivity, ActivityVenue.class);
				startActivity(intent);
			}else{
				venueActivity.finish();
			}
		}else{
			venueActivity.showMessage(result);
		}
	}
	
	private void startReportVenue(){
		venueActivity.showProgressDialog("Submitting venue details...");
	}
	
	private class ReportVenueTask extends AsyncTask<Void, Void, String>{

		@Override
		protected String doInBackground(Void... params) {
			String apiString = "";
			String json = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				gson = new GsonBuilder().create();
				if(!newVenue){
					MessageReportVenue reportVenue = new MessageReportVenue(venueAddress.getText().toString(), app.getActiveUser().getCity().getId(), venueReport.getText().toString(), venuePhone.getText().toString(), venueWebsite.getText().toString(), app.getActiveUser().getId(), app.ActiveVenue.getId(), venueName.getText().toString());
					json = gson.toJson(reportVenue);
					apiString = getString(R.string.api_base) + getString(R.string.api_venue_report) + app.ActiveVenue.getId();
				}else{
					MessageRequestVenue requestVenue = new MessageRequestVenue(venueAddress.getText().toString(), app.getActiveUser().getCity().getId(),  venueReport.getText().toString(), venuePhone.getText().toString(), venueWebsite.getText().toString(), app.getActiveUser().getId(), venueName.getText().toString());
					json = gson.toJson(requestVenue);
					apiString = getString(R.string.api_base) + getString(R.string.api_venue_add) + app.getActiveUser().getId();
				}
				StringEntity stringEntity = new StringEntity(json);
				httpClient = new CustomHttpClient(fragmentActivity);
				httpPost = new HttpPost(apiString);
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID",""+app.getActiveUser().getId());
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					
					if(httpResult != null){
						jsonObject = new JSONObject(httpResult);
						
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							if(jsonObject.has("reason"))
								retVal = jsonObject.getString("reason");
							else
								retVal = "Error, could not report venue.";
						}
					}else{
						retVal = "Error, could not report venue.";
					}
				}
			}catch(Exception ex){
				retVal = "Error, could not report venue.";
			}
			
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			
			startReportVenue();
		}
		
		@Override
		protected void onPostExecute(String result){
			reportVenueResult(result);
		}
	}
}
