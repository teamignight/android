package com.ignight.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;

public class ActivityVenuePhoto extends ActivityBase {
	//private ImageWithProgress imgAndLayout;
	//private ImageView imgVenueFullPhoto;
	private Buzz activeBuzz;
	private ActivityVenuePhoto activity;
	private BufferedReader bufferReader;
	private Gson gson;
	private HttpClient httpClient;
	private HttpGet httpGet;
	private HttpEntity httpEntity;
	private HttpPost httpPost;
	private HttpResponse httpResponse;
	private String httpResult;
	private InputStream inputStream;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private StringBuilder stringBuilder;
	private TextView txtReportPhoto;
	private ViewPager viewPager;
	private AdapterPhotoPager photoPagerAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_venue_photo);
		activity = this;
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,R.layout.module_title_bar);
		if(app.BuzzType.equalsIgnoreCase("group"))
			wireupHeader(app.ActiveGroup.getGroupName(),"Buzz Photos");
		else if(app.BuzzType.equalsIgnoreCase("venue"))
			wireupHeader(app.ActiveVenue.getName(),"Buzz Photos");
		txtReportPhoto = (TextView)findViewById(R.id.txtReportPhoto);
		//hideHeaderButton();
		
		/*imgVenueFullPhoto = (ImageView)findViewById(R.id.imgVenueFullPhoto);
		imgVenueFullPhoto.setTag(app.LargeImage);
		
		imgAndLayout = new ImageWithProgress();
		imgAndLayout.setImg(imgVenueFullPhoto);
		imgAndLayout.setLayout(null);
		new AsyncImageDownload().execute(imgAndLayout);*/
		
		//Picasso.with(this).load(app.LargeImage).into(imgVenueFullPhoto);
		
		//get buzz type
		//new GetBuzzImages().execute();
		
		viewPager = (ViewPager)findViewById(R.id.viewPager);
		photoPagerAdapter = new AdapterPhotoPager(this, app.BuzzImages);
		viewPager.setAdapter(photoPagerAdapter);
		viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
		viewPager.setCurrentItem(app.BuzzImagePosition);
		
		txtReportPhoto.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder alert = new AlertDialog.Builder(activity);
		        alert.setTitle("Report Photo");
		        alert.setMessage("Are you sure you want to report this as inappropriate?");
		        alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
		            @Override
		            public void onClick(DialogInterface dialog, int which) { 
		            	txtReportPhoto.setText("Reporting...");
		            	activeBuzz = app.BuzzImages.get(viewPager.getCurrentItem());
		            	new ReportPhotoTask().execute();
		            }
		        });
		        alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
		            @Override
		            public void onClick(DialogInterface dialog, int which) {
		                dialog.dismiss();
		            }
		        });

		        alert.show();
			}
		});
	}
	
	public void startGetBuzz(){
		
	}
	
	private class ReportPhotoTask extends AsyncTask<Void, Void, String>{
		
		@Override
		protected String doInBackground(Void... args) {
			String json = "";
			String retVal = "success";
			String singleLine = "";
			StringBuilder stringBuilder;
			StringEntity stringEntity;
			
			try{
				gson = new GsonBuilder().create();
				MessageFlagGroupBuzz item = new MessageFlagGroupBuzz(activeBuzz.getId(), CurrentUser.getId());
				
				json = gson.toJson(item);
				stringEntity = new StringEntity(json);
				httpClient = new CustomHttpClient(activity);
				if(activeBuzz.getGroupId() > -1)
					httpPost = new HttpPost(getResources().getString(R.string.api_base) + getResources().getString(R.string.api_flag_group_buzz) + activeBuzz.getGroupId());
				else
					httpPost = new HttpPost(getResources().getString(R.string.api_base) + getResources().getString(R.string.api_flag_venue_buzz) + activeBuzz.getVenueId());
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(getResources().getString(R.string.api_token_key),getResources().getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID",""+CurrentUser.getId());
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					
					if(httpResult != null){
						//Log.d("Adapter", "result: " + httpResult);
						jsonObject = new JSONObject(httpResult);
						
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							retVal = "failed";
						}
					}else{
						retVal = "failed";
					}
				}
			}catch(Exception ex){
				retVal = "failed";
			}
			
			return retVal;
		}
		
		@Override
		protected void onPostExecute(String result){
			if(result.equalsIgnoreCase("success")){
				txtReportPhoto.setText("Reported");
			}else{
				txtReportPhoto.setText("Reported");
			}
		}
	}
	
	private void getBuzzResult(String result){
		if(result.equalsIgnoreCase("success")){
			
		}else{
			
		}
	}
	
	private class GetBuzzImages extends AsyncTask<Void, Void, String>{

		@Override
		protected String doInBackground(Void... params) {
			String retVal = "";
			String singleLine = "";
			
			try{
				httpClient = new CustomHttpClient(activity);
				
				if(app.BuzzType.equalsIgnoreCase("group")){
					httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_group) + "?type=getGroupBuzzImages&cityId="+app.getActiveUser().getCity().getId()+"&groupId="+app.ActiveGroup.getGroupId()+"&buzzImageStartId=0&noOfBuzzImages=25");
				}else{
					httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_venue) + "?type=getVenueBuzzImages&venueId="+app.ActiveVenue.getId()+"&buzzImageStartId=0&noOfBuzzImages=25");
				}
				httpGet.setHeader("PLATFORM","Android");
				httpGet.setHeader("USER_ID",""+app.getActiveUser().getId());
				httpResponse = httpClient.execute(httpGet);
				
				httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();
				bufferReader = new BufferedReader(new InputStreamReader(inputStream));
				stringBuilder = new StringBuilder();
				
				while((singleLine = bufferReader.readLine()) != null){
					stringBuilder.append(singleLine);
				}
				
				httpResult = stringBuilder.toString();
				inputStream.close();
				jsonObject = new JSONObject(httpResult);

				if(jsonObject.getBoolean("res")){
					retVal = "success";
					jsonArray = jsonObject.getJSONArray("body");
				}else{
					retVal = "failed";
				}
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startGetBuzz();
		}
		
		@Override
		protected void onPostExecute(String result){
			getBuzzResult(result);
		}
	}
}
