package com.ignight.android;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import android.text.format.DateFormat;
import android.util.Log;

public class Group {
	private ArrayList<Buzz> Buzz;
	private long DateStamp;
	private boolean DeleteFlag;
	private String GroupDescription;
	private int GroupId;
	private String GroupName;
	private int Id;
	private boolean IsMember;
	private boolean IsOwner;
	private boolean IsPrivate;
	private boolean IsSubscribed;
	private boolean Reported;
	private boolean Reporting;
	private int MemberCount;
	private boolean NewGroupBuzzAvailable;
	private boolean WasInvited;
	
	public Group(){
		Buzz = new ArrayList<Buzz>();
	}
	
	public Group(String description, int groupId, boolean isOwner, boolean isPrivate, String name){
		this.GroupDescription = description;
		this.Id = groupId;
		this.GroupId = groupId;
		this.GroupName = name;
		this.IsOwner = isOwner;
		this.IsPrivate = isPrivate;
		Buzz = new ArrayList<Buzz>();
	}
	
	public Group(int groupId, String groupName, boolean isPrivate){
		this.GroupId = groupId;
		this.GroupName = groupName;
		this.IsPrivate = isPrivate;
		Buzz = new ArrayList<Buzz>();
	}

	public ArrayList<Buzz> getBuzz(){
		return this.Buzz;
	}
	
	public void setBuzz(ArrayList<Buzz> buzz){
		this.Buzz = buzz;
	}
	
	public long getDateStamp(){
		return this.DateStamp;
	}
	
	public void setDateStamp(long value){
		this.DateStamp = value;
	}
	
	public String getTime(){
		double difference;
		int holder;
		double minutes;
		double seconds;
		long today;
		java.util.Date todayDate = new java.util.Date();
		
		today = todayDate.getTime();
		difference = (today - DateStamp) / 1000 / 60 / 60;
		
		if(difference < 1){
			minutes = (today - DateStamp) / 1000 / 60;
			if(minutes <= 0){
				seconds = (today - DateStamp) / 1000;
				if(seconds <= 0)
					return "Now";
				else{
					holder = (int)Math.round(seconds);
					return holder + (holder == 1 ? " second ago" : " seconds ago");
				}
			} else {
				holder = (int)Math.round(minutes);
				return holder + (holder == 1 ? " minute ago" : " minutes ago");
			}
		} else if(difference < 24) {
			holder = (int)Math.round(difference);
			return holder + (holder == 1 ? " hour ago" : " hours ago");
		} else {
			Calendar cal = Calendar.getInstance(Locale.ENGLISH);
		    cal.setTimeInMillis(DateStamp);
		    String date = DateFormat.format("MMM dd, yyyy", cal).toString();
		    return date;
		}
	}
	
	public boolean getDeleteFlag(){
		return this.DeleteFlag;
	}
	
	public void setDeleteFlag(boolean value){
		this.DeleteFlag = value;
	}
	
	public String getGroupDescription(){
		return this.GroupDescription == null || this.GroupDescription.isEmpty() ? "No Description" : this.GroupDescription;
	}
	
	public void setGroupDescription(String description){
		this.GroupDescription = description;
	}
	
	public int getGroupId(){
		return this.GroupId;
	}
	
	public void setGroupId(int groupId){
		this.GroupId = groupId;
	}
	
	public String getGroupName(){
		return this.GroupName;
	}
	
	public void setGroupName(String name){
		this.GroupName = name;
	}
	
	public int getId(){
		return this.Id;
	}
	
	public void setId(int id){
		this.Id = id;
	}
	
	public boolean getIsMember(){
		return this.IsMember;
	}
	
	public void setIsMember(boolean isMember){
		this.IsMember = isMember;
	}
	
	public boolean getIsOwner(){
		return this.IsOwner;
	}
	
	public void setIsOwner(boolean isOwner){
		this.IsOwner = isOwner;
	}
	
	public boolean getIsPrivate(){
		return this.IsPrivate;
	}
	
	public void setIsPrivate(boolean isPrivate){
		this.IsPrivate = isPrivate;
	}
	
	public boolean getIsSubscribed(){
		return this.IsSubscribed;
	}
	
	public void setIsSubscribed(boolean isSubscribed){
		this.IsSubscribed = isSubscribed;
	}
	
	public int getMemberCount(){
		return this.MemberCount;
	}
	
	public void setMemberCount(int memberCount){
		this.MemberCount = memberCount;
	}
	
	public boolean getNewGroupBuzzAvailable(){
		return this.NewGroupBuzzAvailable;
	}
	
	public void setNewGroupBuzzAvailable(boolean val){
		this.NewGroupBuzzAvailable = val;
	}
	
	public boolean getReported(){
		return this.Reported;
	}
	
	public void setReported(boolean reported){
		this.Reported = reported;
	}
	
	public boolean getReporting(){
		return this.Reporting;
	}
	
	public void setReporting(boolean reporting){
		this.Reporting = reporting;
	}
	
	public boolean getWasInvited(){
		return this.WasInvited;
	}
	
	public void setWasInvited(boolean wasInvited){
		this.WasInvited = wasInvited;
	}
}
