package com.ignight.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

public class ActivityDNASpendingPreference extends ActivityBase {
	private ActivityDNASpendingPreference activity;
	private AdapterSpendingPreference adapter;
	private BufferedReader bufferReader;
	private DataAdapter dataAdapter;
	private GridView gridView;
	private Gson gson;
	private String httpResult;
	private HttpClient httpClient;
	private HttpEntity httpEntity;
	private HttpPost httpPost;
	private HttpResponse httpResponse;
	private InputStream inputStream;
	private JSONObject jsonObject;
	private ProgressDialog progressDialog;
	private StringBuilder stringBuilder;
	private User user;
	private SparseArray<DNASpendingPreference> hash;
	private Button btnSignup;
	private boolean ready;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_dna_spending_preference);
		
		app = ((AppBase)getApplication());
        user = app.getActiveUser();
        activity = this;
        btnSignup = (Button)findViewById(R.id.btnSignup);
        adapter = new AdapterSpendingPreference(this, DNAOptions.SpendingPreferenceOptions(), user);
        hash = new SparseArray<DNASpendingPreference>();
        
        gridView = (GridView)findViewById(R.id.grdDNASpendingPreference);
		gridView.setAdapter(adapter);
		gridView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id){
				ImageView numberView;
				
				numberView = (ImageView)v.findViewById(R.id.gridSpendingNumber);
				if(hash.get(position) != null){
					user.setSpendingPreference(-1);
					hash.remove(position);
					numberView.setImageDrawable(null);
					numberView.setTag(-1);
					checkIfReady();
					return;
				}
				
				if(user.getSpendingPreference() > -1 && hash.get(position) == null){
					View childView;
					ImageView imgView;
					for(int i = 0; i < gridView.getChildCount(); i++){
						childView = gridView.getChildAt(i);
						imgView = (ImageView)childView.findViewById(R.id.gridSpendingNumber);
						
						if(imgView.getTag() != null && Integer.parseInt(imgView.getTag().toString()) > -1){
							imgView.setTag(-1);
							imgView.setImageDrawable(null);
							break;
						}
					}
				}

				user.setSpendingPreference(position);
				numberView.setImageResource(R.drawable.dna_selected);
				numberView.setTag(user.getSpendingPreference());
				checkIfReady();
				//Picasso.with(ActivityDNASpendingPreference.this).load(R.drawable.dna_selected).into(numberView);
			}
		});
	}
	
	@Override
	public void onStop(){
		super.onStop();
	}

	public void btnNext_ClickEvent(View button){
		if(user.getSpendingPreference() != -1){
			new AddDNATask().execute();
		}else{
			showMessage("Spending preference is required.");
		}
	}
	
	private void checkIfReady(){
		ready = user.getSpendingPreference() != -1;
		if(ready){
			btnSignup.setTextColor(Color.parseColor("#d849b10a"));
		}else{
			btnSignup.setTextColor(Color.parseColor("#626262"));
		}
	}
	
	private void startAddDNA(){
		progressDialog = ProgressDialog.show(this, "", "Please wait....", true);
	}
	
	private void addDNAResult(String result){
		Intent intent;

		if(result.equals("success")){
			try {
				//insert into db
				//Log.d("Signup","success");
				user.setUserName(jsonObject.getJSONObject("body").getString("userName"));
				user.setUserEmail(jsonObject.getJSONObject("body").getString("email"));
				user.setUserPassword("Temp");
				user.setFirstName("Temp");
				user.setLastName("Temp");
				user.setUserPreference(new Preference(1, 1, 1));
				dataAdapter = new DataAdapter(this);
				if(dataAdapter.AddUser(user) == 1){
					//Log.d("Signup", "User added to db");
					app.setActiveUser(user);
					app.NewTutorial = true;
					intent = new Intent(ActivityDNASpendingPreference.this, ActivityTutorial.class);
					startActivity(intent);
				}else{
					showMessage("Error, could not add user to local db.");
				}
			} catch (Exception e) {
				showMessage("Error getting user id: " + e.getMessage() + ": " + user.getId());
			}
		}else if(!result.equals("failed")){
			showMessage(result);
		}
	}
	
	private class AddDNATask extends AsyncTask<Void, Void, String>{

		@Override
		protected String doInBackground(Void... args) {
			ArrayList<Integer> atmosphere;
			ArrayList<Integer> music;
			ArrayList<Integer> venues;
			String json = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				atmosphere = new ArrayList<Integer>();
				for(int i = 0; i < user.getAtmosphereOptions().size(); i++){
					atmosphere.add(user.getAtmosphereOptions().get(i).getId());
				}
				music = new ArrayList<Integer>();
				for(int i = 0; i < user.getMusicOptions().size(); i++){
					music.add(user.getMusicOptions().get(i).getId());
				}
				venues = new ArrayList<Integer>();
				for(int i = 0; i < user.getVenueOptions().size(); i++){
					venues.add(user.getVenueOptions().get(i).getId());
				}
				
				gson = new GsonBuilder().create();
				MessageUserDNA userDNA = new MessageUserDNA(user.getAge(), atmosphere, user.getCity().getId(), user.getGender().equals("Male") ? 0 : 1,
						music, user.getSpendingPreference(), user.getId());
				
				json = gson.toJson(userDNA);

				StringEntity stringEntity = new StringEntity(json);
				httpClient = new CustomHttpClient(activity);
				HttpPut httpPut = new HttpPut(getString(R.string.api_base) + getString(R.string.api_user) + "/" + user.getId());
				//httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_user) + "/" + user.getId());
				httpPut.setEntity(stringEntity);
				httpPut.setHeader("Accept","application/json");
				httpPut.setHeader("Content-type","application/json; charset=utf-8");
				httpPut.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpPut.setHeader("PLATFORM","Android");
				httpPut.setHeader("USER_ID",""+app.getActiveUser().getId());
				
				httpResponse = httpClient.execute(httpPut);
				httpEntity = httpResponse.getEntity();
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					if(httpResult != null){
						//Log.d("Signup", httpResult);
						jsonObject = new JSONObject(httpResult);
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							retVal = "Ths email address already exist.";
						}
					}else{
						retVal = "Error, no result.";
					}
				}else{
					retVal = "Error, null entity.";
				}
				
			}catch(ClientProtocolException e){
				retVal = "Client Protocal Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "IO Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			progressDialog.dismiss();
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startAddDNA();
		}
		
		@Override
		protected void onPostExecute(String result){
			addDNAResult(result);
		}
	}
}
