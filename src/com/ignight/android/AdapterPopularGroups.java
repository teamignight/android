package com.ignight.android;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AdapterPopularGroups extends ArrayAdapter<Group>{
	private AppBase app;
	private BufferedReader bufferReader;
	private Context context;
	private Group group;
	private ArrayList<Group> groups;
	private Gson gson;
	private String httpResult;
	private HttpClient httpClient;
	private HttpEntity httpEntity;
	private HttpPost httpPost;
	private HttpResponse httpResponse;
	private InputStream inputStream;
	private JSONObject jsonObject;
	private LayoutInflater layoutInflater;
	
	private DialogInterface activeDialog;
	private Group activeGroup;
	private View activeView;
	
	public AdapterPopularGroups(Context context, int textViewResourceId, ArrayList<Group> items, AppBase appBase){
		super(context, textViewResourceId, items);
		
		this.app = appBase;
		this.context = context;
		
		groups = items;
		
		layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {	
		ViewHolder holder;
		View view = convertView;
		
		if(view == null){
			view = layoutInflater.inflate(R.layout.list_explore_groups, null);
			holder = new ViewHolder();
			holder.groupName = (TextView)view.findViewById(R.id.txtMyGroupName);
			holder.groupMembers = (TextView)view.findViewById(R.id.txtMyGroupCount);
			holder.groupReport = (TextView)view.findViewById(R.id.txtReportGroup);
			
			view.setTag(holder);
		}else{
			holder = (ViewHolder)view.getTag();
		}
		
		group = groups.get(position);
		holder.groupName.setText(group.getGroupName());
		holder.groupMembers.setText(group.getMemberCount() + " members");
		holder.groupReport.setText(group.getReporting() ? "Reporting..." : group.getReported() ? "Reported" : "Report");
		holder.groupReport.setTag(group);
		holder.groupReport.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				activeGroup = (Group)view.getTag();
				if(activeGroup.getReported() || activeGroup.getReporting()){
					return;
				}
				final int id = activeGroup.getId();
				AlertDialog.Builder alert = new AlertDialog.Builder(context);
		        alert.setTitle("Report Group");
		        alert.setMessage("Are you sure you want to report this as inappropriate?");
		        alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
		            @Override
		            public void onClick(DialogInterface dialog, int which) { 
		            	dialog.dismiss();
		            	activeGroup.setReporting(true);
		            	notifyDataSetChanged();
		            	new ReportGroupTask().execute(id);
		            }
		        });
		        alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
		            @Override
		            public void onClick(DialogInterface dialog, int which) {
		                dialog.dismiss();
		            }
		        });

		        alert.show();
			}
		});
		
		return view;
	}
	
	public static class ViewHolder{
		TextView groupName;
		TextView groupMembers;
		TextView groupReport;
	}
	
	private class ReportGroupTask extends AsyncTask<Integer, Void, String>{
		
		@Override
		protected String doInBackground(Integer... groupId) {
			int activeGroupId = 0;
			String json = "";
			String retVal = "";
			String singleLine = "";
			StringBuilder stringBuilder;
			StringEntity stringEntity;
			User user = app.getActiveUser();
			
			try{
				gson = new GsonBuilder().create();
				activeGroupId = groupId[0];
				MessageFlagGroup item = new MessageFlagGroup(activeGroupId, user.getId());
				
				json = gson.toJson(item);
				stringEntity = new StringEntity(json);
				httpClient = new CustomHttpClient(context);
				httpPost = new HttpPost(context.getResources().getString(R.string.api_base) + context.getResources().getString(R.string.api_flag_group) + activeGroupId);
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(context.getResources().getString(R.string.api_token_key),context.getResources().getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID",""+user.getId());
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					
					if(httpResult != null){
						//Log.d("Adapter", "result: " + httpResult);
						jsonObject = new JSONObject(httpResult);
						
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							retVal = "failed";
						}
					}else{
						retVal = "failed";
					}
				}
				retVal = "success";
			}catch(Exception ex){
				retVal = "failed";
			}
			
			return retVal;
		}
		
		@Override
		protected void onPostExecute(String retVal){
			if(retVal.equalsIgnoreCase("success")){
				activeGroup.setReported(true);
				activeGroup.setReporting(false);
				notifyDataSetChanged();
			}else{
				
			}
		}
	}
}
