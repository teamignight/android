package com.ignight.android;

import java.util.ArrayList;

public class Venue {
	private String Address;
	private int Age;
	private int Atmosphere;
	private int AverageSpendingLimit;
	private ArrayList<Buzz> Buzz;
	private boolean DeleteFlag;
	private int DownVote;
	private String Genre;
	private int Id;
	private ArrayList<VenueImage> Images;
	private double Latitude;
	private double Longitude;
	private int MusicType;
	private String Name;
	private boolean NewVenueBuzzAvailable;
	private String PhoneNumber;
	private String State;
	private ArrayList<Group> TrendingGroups;
	private int UpVote;
	private String Url;
	private int UserBarColor;
	private int UserInput;
	private double UserVenueValue;
	private City VenueCity;
	private String Zip;

	public Venue(){
		TrendingGroups = new ArrayList<Group>();
	}
	public Venue(int id, String name){
		this.Id = id;
		this.Name = name;
		
		Images = new ArrayList<VenueImage>();
		Buzz = new ArrayList<Buzz>();
		TrendingGroups = new ArrayList<Group>();
	}
	public Venue(int id, String name, double latitude, double longitude){
		this.Id = id;
		this.Name = name;
		this.Latitude = latitude;
		this.Longitude = longitude;
		
		Images = new ArrayList<VenueImage>();
		Buzz = new ArrayList<Buzz>();
		TrendingGroups = new ArrayList<Group>();
	}
	public Venue(int id, String name, int userInput, double userVenueValue){
		this.Id = id;
		this.Name = name;
		this.UserInput = userInput;
		this.UserVenueValue = userVenueValue;
		
		Images = new ArrayList<VenueImage>();
		Buzz = new ArrayList<Buzz>();
		TrendingGroups = new ArrayList<Group>();
	}
	public String getAddress(){
		return this.Address;
	}
	public void setAddress(String address){
		this.Address = address;
	}
	public int getAge(){
		return this.Age;
	}
	public void setAge(int age){
		this.Age = age;
	}
	public int getAtmosphere(){
		return this.Atmosphere;
	}
	public void setAtmosphere(int atmosphere){
		this.Atmosphere = atmosphere;
	}
	public int getAverageSpendingLimit(){
		return this.AverageSpendingLimit;
	}
	public void setAverageSpendingLimit(int spendingLimit){
		this.AverageSpendingLimit = spendingLimit;
	}
	public ArrayList<Buzz> getBuzz(){
		return this.Buzz;
	}
	public void setBuzz(ArrayList<Buzz> buzz){
		this.Buzz = buzz;
	}
	public boolean getDeleteFlag(){
		return this.DeleteFlag;
	}
	public void setDeleteFlag(boolean value){
		this.DeleteFlag = value;
	}
	public String getGenre(){
		return this.Genre;
	}
	public void setGenre(String genre){
		this.Genre = genre;
	}
	public int getId(){
		return this.Id;
	}
	public void setId(int id){
		this.Id = id;
	}
	public ArrayList<VenueImage> getImages(){
		return this.Images;
	}
	public double getLatitude(){
		return this.Latitude;
	}
	public void setLatitude(double latitude){
		this.Latitude = latitude;
	}
	public double getLongitude(){
		return this.Longitude;
	}
	public void setLongitude(double longitude){
		this.Longitude = longitude;
	}
	public int getMusicType(){
		return this.MusicType;
	}
	public void setMusicType(int musicType){
		this.MusicType = musicType;
	}
	public String getName(){
		return this.Name;
	}
	public void setName(String name){
		this.Name = name;
	}
	public boolean getNewVenueBuzzAvailable(){
		return this.NewVenueBuzzAvailable;
	}
	public void setNewVenueBuzzAvailable(boolean val){
		this.NewVenueBuzzAvailable = val;
	}
	public String getPhoneNumber(){
		return this.PhoneNumber;
	}
	public void setPhoneNumber(String phoneNumber){
		this.PhoneNumber = phoneNumber;
	}
	public String getState(){
		return this.State;
	}
	public void setState(String state){
		this.State = state;
	}
	public ArrayList<Group> getTrendingGroups(){
		return this.TrendingGroups;
	}
	public void setTrendingGroups(ArrayList<Group> trendingGroups){
		this.TrendingGroups = trendingGroups;
	}
	public int getUserBarColor(){
		return this.UserBarColor;
	}
	public void setUserBarColor(int userBarColor){
		this.UserBarColor = userBarColor;
	}
	public int getUserInput(){
		return this.UserInput;
	}
	public void setUserInput(int userInput){
		this.UserInput = userInput;
	}
	public double getUserVenueValue(){
		return this.UserVenueValue;
	}
	public void setUserVenueValue(double userVenueValue){
		this.UserVenueValue = userVenueValue;
	}
	
	public int getDownVote(){
		return this.DownVote;
	}
	public void setDownVote(int vote){
		this.DownVote = vote;
	}
	public int getUpVote(){
		return this.UpVote;
	}
	public void setUpVote(int vote){
		this.UpVote = vote;
	}
	public String getUrl(){
		return this.Url;
	}
	public void setUrl(String url){
		this.Url = url;
	}
	public City getVenueCity(){
		return this.VenueCity;
	}
	public void setVenueCity(City venueCity){
		this.VenueCity = venueCity;
	}
	public String getZip(){
		return this.Zip;
	}
	public void setZip(String zip){
		this.Zip = zip;
	}
}
