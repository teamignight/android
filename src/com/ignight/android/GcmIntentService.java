package com.ignight.android;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GcmIntentService extends IntentService {
	Context context;
	public static final int BUZZ_NOTIFICATION_ID = 1;
	public static final int INVITE_NOTIFICATION_ID = 2;
	public int activeNotification = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    public String ProcessActivity;
    private int numMessages = 0;
    
	public GcmIntentService() {
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		String messageType = gcm.getMessageType(intent);

		AppBase app;
		String groupId;
		String groupName;
		Intent gotoIntent = null;
		String msg = "";
		String notificationType;
		String title = "";
		String userName;
		String userPhoto = null;
		
		 if (!extras.isEmpty()) {
			 //Log.d("Push", extras.toString());
			 if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
				 notificationType = extras.getString("notificationType");
				 if(notificationType != null && !notificationType.isEmpty()){
					 app = ((AppBase)getApplication());
					 if(notificationType.equalsIgnoreCase("INVITE_USER_TO_GROUP_REQUEST")){
						 title = "Group Invite";
						 groupName = extras.getString("groupName", "Group");
						 userName = extras.getString("userName", "IgNight Member");
						 userPhoto = extras.getString("userPicture");
						 groupId = extras.getString("groupId", "");
						 msg = userName + " invited you to join the group: " + groupName;
						 activeNotification = INVITE_NOTIFICATION_ID;
						 app.ShowInbox = true;
						 gotoIntent = new Intent(this, ActivityGroups.class);
						 gotoIntent.putExtra("com.ignight.android.action", "GroupInvite");
						 gotoIntent.putExtra("com.ignight.android.id", groupId);
					 }else if(notificationType.equalsIgnoreCase("JOIN_PRIVATE_GROUP_REQUEST")){
						 title = "Join Group Request";
						 groupName = extras.getString("groupName", "Group");
						 userName = extras.getString("UserName", "IgNight Member");
						 groupId = extras.getString("groupId");
						 userPhoto = extras.getString("userPicture");
						 msg = userName + " will like to join your group: " + groupName;
						 activeNotification = INVITE_NOTIFICATION_ID;
						 app.ShowInbox = true;
						 gotoIntent = new Intent(this, ActivityGroups.class);
						 gotoIntent.putExtra("com.ignight.android.action", "GroupInvite");
						 gotoIntent.putExtra("com.ignight.android.id", groupId);
					 }else if(notificationType.equalsIgnoreCase("NEW_GROUP_BUZZ_AVAILABLE_NOTIFICATION")){
						 title = "New Group Buzz Post";
						 groupId = extras.getString("groupId", "0");
						 groupName = extras.getString("groupName", "Group");
						 msg = "New group buzz post in " + groupName;
						 //Log.d("GroupBuzz","Got new buzz: " + groupName);
						 activeNotification = BUZZ_NOTIFICATION_ID;
						 Intent i = new Intent("com.ignight.android.updateBuzz");
						 i.putExtra("itemToUpdate", groupId);
						 sendBroadcast(i);
						 
						 gotoIntent = new Intent(this, ActivityRouteBuzz.class);
						 gotoIntent.putExtra("itemId", Integer.parseInt(groupId));
						 gotoIntent.putExtra("itemType", "group");
						 sendNotification(msg, title, gotoIntent, userPhoto);
					 }
				 }else{
					 //Log.d("Notification", "no noti: " + extras.toString());
				 }
			 }
		 }
		 GcmBroadcastReceiver.completeWakefulIntent(intent);
	}
	
	private void sendNotification(String msg, String title, Intent myintent, String photo) {
		Bitmap largeIcon = null;
        mNotificationManager = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);
        
        if(myintent != null){
	        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, myintent, PendingIntent.FLAG_UPDATE_CURRENT);
	        
	        if(photo != null && photo.length() > 0){
	        	largeIcon = getBitmapFromURL(photo);
	        }
	        
	        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
		        .setSmallIcon(R.drawable.launcher_icon_simple)
		        .setContentTitle(title)
		        .setStyle(new NotificationCompat.BigTextStyle()
		        .bigText(msg))
		        .setContentText(msg)
		        .setAutoCancel(true);
	        
	        if(largeIcon != null)
	        	mBuilder.setLargeIcon(largeIcon);
	
	        mBuilder.setContentIntent(contentIntent).setNumber(++numMessages);
	        mNotificationManager.notify(activeNotification, mBuilder.build());
        }
    }
	
	private Bitmap getBitmapFromURL(String strURL) {
	    try {
	        URL url = new URL(strURL);
	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	        connection.setDoInput(true);
	        connection.connect();
	        InputStream input = connection.getInputStream();
	        Bitmap myBitmap = BitmapFactory.decodeStream(input);
	        return myBitmap;
	    } catch (IOException e) {
	        e.printStackTrace();
	        return null;
	    }
	}
}
