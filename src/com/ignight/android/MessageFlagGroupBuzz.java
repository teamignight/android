package com.ignight.android;

public class MessageFlagGroupBuzz {
	private class NewItem{
		private int buzzId;
		private int userId;
		
		NewItem(int buzzId, int userId){
			this.buzzId = buzzId;
			this.userId = userId;
		}
	}
	
	private NewItem msg;
	
	MessageFlagGroupBuzz(int buzzId, int userId){
		this.msg = new NewItem(buzzId, userId);
	}
}
