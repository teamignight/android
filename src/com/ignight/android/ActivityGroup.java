package com.ignight.android;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ignight.android.R.drawable;

public class ActivityGroup extends ActivityBase {
	private ActivityGroup activity;
	private BufferedReader bufferReader;
	private FragmentBuzz fragmentBuzz;
	private FragmentGroupMembers fragmentGroupMembers;
	private FragmentTransaction fragmentTransaction;
	private FragmentTrendingList fragmentTrendingList;
	private FragmentTrendingMap fragmentTrendingMap;
	private FragmentPhotos fragmentPhotos;
	private Gson gson;
	private HttpClient httpClient;
	private HttpGet httpGet;
	private HttpEntity httpEntity;
	private HttpPost httpPost;
	private HttpResponse httpResponse;
	private String httpResult;
	private ImageView imgGroupBuzz;
	private ImageView imgGroupMembers;
	private ImageView imgGroupPhotos;
	private ImageView imgGroupTrending;
	private InputStream inputStream;
	private JSONObject jsonObject;
	private ProgressDialog progressDialog;
	private User user;
	private StringBuilder stringBuilder;
	private EditText txtBuzzText;
	private TextView txtPostBuzz;
	private boolean uploadedPhoto;
	
	public int filterValue;
	public int musicPreference;
	public double searchRadious;
	private BroadcastReceiver mReceiver;
	private LinearLayout footerButtonWrapper;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        user = CurrentUser;
        app.OnGroupPage = true;
        activity = this;
        if (savedInstanceState != null && app.UploadingPhoto)
        {
			app.UploadPhotoInit = true;
			app.ShowBuzz = true;
        }
        
        if(app.ActiveGroup == null){
        	Intent notiIntent = new Intent(this, ActivityTrending.class);
			startActivity(notiIntent);
        } else {
        
	        if(app.ActiveGroup.getGroupId() != app.DirtyGroup){
	        	app.DirtyGroup = -1;
	        }
	        
	        IntentFilter intentFilter = new IntentFilter("com.ignight.android.updatebuzzphoto");
			mReceiver = new BroadcastReceiver() {
	            @Override
	            public void onReceive(Context context, Intent intent) {
	            	handleReceiver();
	            }
			};
			this.registerReceiver(mReceiver, intentFilter);
	        
	        app.UpdatedTrendingMap = false;
	        
	        GPSTracker gpsTracker = new GPSTracker(this);
	        if (gpsTracker.canGetLocation())
	        {
	        	user.setLatitude(gpsTracker.latitude);
	        	user.setLongitude(gpsTracker.longitude);
	        }else{
	        	user.setLatitude(UserLatitude);
	        	user.setLongitude(UserLongitude);
	        }
	        
			requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
			setContentView(R.layout.activity_group);
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,R.layout.module_title_bar);
			
	        app.BuzzType = "Group";
	        app.PhotoGalleryType = "Group";
	        if(app.ActiveGroup.getBuzz() != null)
	        	app.ActiveGroup.getBuzz().clear();
	        wireupHeader(app.ActiveGroup.getGroupName(),"Trending");
	        
	        fragmentBuzz = new FragmentBuzz();
	        fragmentTrendingList = new FragmentTrendingList();
	        fragmentGroupMembers = new FragmentGroupMembers();
	        fragmentPhotos = new FragmentPhotos();
	        fragmentTrendingMap = new FragmentTrendingMap();
	
	        imgGroupBuzz = (ImageView)findViewById(R.id.imgGroupBuzz);
	        imgGroupMembers = (ImageView)findViewById(R.id.imgGroupMembers);
	        imgGroupTrending = (ImageView)findViewById(R.id.imgGroupTrending);
	        imgGroupPhotos = (ImageView)findViewById(R.id.imgGroupPhotos);
	        footerButtonWrapper = (LinearLayout)findViewById(R.id.footerButtonWrapper);
	        
	        filterValue = -1;
	        app.TrendingFilterShowing = false;
	        app.TrendingState = "Normal";
	        app.TrendingType = "Group";
	        app.TrendingView = "ListView";
	        
	        app.Venues = null;
	        
			if(app.ShowBuzz){
				app.ShowBuzz = false;
				imgGroupBuzz.setImageResource(drawable.venue_buzz_active);
				imgGroupTrending.setImageResource(drawable.group_trending);
				updateHeaderText("Buzz");
				fragmentTransaction = getSupportFragmentManager().beginTransaction();
				fragmentTransaction.add(R.id.fragmentHolder, fragmentBuzz);
				fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				fragmentTransaction.commit();
			} else if (app.ShowMembers){
				app.ShowMembers = false;
				imgGroupMembers.setImageResource(drawable.group_members_active);
				imgGroupTrending.setImageResource(drawable.group_trending);
				updateHeaderText("Members");
				fragmentTransaction = getSupportFragmentManager().beginTransaction();
				fragmentTransaction.add(R.id.fragmentHolder, fragmentGroupMembers);
				fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				fragmentTransaction.commit();
			} else{
				fragmentTransaction = getSupportFragmentManager().beginTransaction();
				fragmentTransaction.add(R.id.fragmentHolder, fragmentTrendingList);
				fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				fragmentTransaction.commit();
				footerButtonWrapper.setVisibility(View.VISIBLE);
			}
			showTitleInfoButton("group");
        }
	}
	
	@Override
	protected void onResume(){
		app.TrendingType = "group";
		if(app.LeftGroup){
			finish();
		}
		super.onResume();
	}
	
	@Override
	protected void onStop(){
		super.onStop();
		if(progressDialog != null && progressDialog.isShowing())
			progressDialog.dismiss();
		try{
			this.unregisterReceiver(this.mReceiver);
		}catch(Exception ex){
			
		}
	}
	
	public void clearSearch(View view){
		fragmentTrendingList.clearSearch();
	}
	
	public void toggleMapButton(boolean value){
		if(value){
			footerButtonWrapper.setVisibility(View.GONE);
		} else{
			footerButtonWrapper.setVisibility(View.VISIBLE);
		}
	}
	
	public void getPhoto(View view){
		AlertDialog.Builder builder = new AlertDialog.Builder(ActivityGroup.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                	app.ShowBuzz = true;
                	app.UploadingPhoto = true;
                	getPictureFromCamera();
                } else if (items[item].equals("Choose from Library")) {
                	getPictureFromGallery();
                } else{
                	dialog.dismiss();
                }
            }
        });
        builder.show();
	}
	
	public void postPhotoFromGallery(){
		if(takenPictureData!=null){
	    	new PostPhotoTask().execute();
	    }
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);

	    takenPictureData = null;

	    switch(requestCode){
	    case PICTURE_TAKEN_FROM_GALLERY:                
            if(resultCode ==  Activity.RESULT_OK) {
            	takenPictureData = handleResultFromChooser(data);
            }
            break;
	    case PICTURE_TAKEN_FROM_CAMERA:
	    	if(resultCode==Activity.RESULT_OK) {
                takenPictureData = handleResultFromCamera(data);
            }       
	    	break;
	    }
	    
	    if(takenPictureData!=null){
	    	new PostPhotoTask().execute();
	    }
	}
	
	public void gotoAddVenue(View view){
		app.ShowReportVenue = true;
		app.ActiveVenue = new Venue();
		Intent intent;
    	intent = new Intent(this, ActivityVenue.class);
    	startActivity(intent);
	}
	
	public void postBuzz(View view){
		if(txtBuzzText == null){
			showMessage("Error, no valid textbox");
		}else if(txtBuzzText.getText().toString().isEmpty()){
			showMessage("Buzz text required.");
		}else{
			txtPostBuzz.setText("posting...");
			txtPostBuzz.setClickable(false);
			new PostBuzzTask().execute();
		}
	}
	
	public void setBuzzTextBox(EditText textBox, TextView postBuzz){
		txtBuzzText = textBox;
		txtPostBuzz = postBuzz;
	}
	
	public void loadGroupBuzz(View view){
		fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.fragmentHolder, fragmentBuzz);
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();

		imgGroupBuzz.setImageResource(drawable.venue_buzz_active);
		imgGroupMembers.setImageResource(drawable.group_members);
		imgGroupTrending.setImageResource(drawable.group_trending);
		imgGroupPhotos.setImageResource(drawable.venue_photos);
		
		updateHeaderText("Buzz");
		footerButtonWrapper.setVisibility(View.GONE);
	}
	
	public void loadGroupMembers(View view){
		app.AddingGroupMembers = false;
		
		fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.fragmentHolder, fragmentGroupMembers);
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();

		imgGroupBuzz.setImageResource(drawable.venue_buzz);
		imgGroupMembers.setImageResource(drawable.group_members_active);
		imgGroupTrending.setImageResource(drawable.group_trending);
		imgGroupPhotos.setImageResource(drawable.venue_photos);
		
		updateHeaderText("Members");
		footerButtonWrapper.setVisibility(View.GONE);
	}
	
	public void loadAGroupPhotos(View view){
		fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.fragmentHolder, fragmentPhotos);
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
		
		imgGroupBuzz.setImageResource(drawable.venue_buzz);
		imgGroupMembers.setImageResource(drawable.group_members);
		imgGroupTrending.setImageResource(drawable.group_trending);
		imgGroupPhotos.setImageResource(drawable.venue_photos_active);
		
		updateHeaderText("Photos");
		footerButtonWrapper.setVisibility(View.GONE);
	}
	
	public void loadGroupTrending(View view){
		fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.fragmentHolder, fragmentTrendingList);
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();

		imgGroupBuzz.setImageResource(drawable.venue_buzz);
		imgGroupMembers.setImageResource(drawable.group_members);
		imgGroupTrending.setImageResource(drawable.group_trending_active);
		imgGroupPhotos.setImageResource(drawable.venue_photos);
		
		updateHeaderText("Trending");
		footerButtonWrapper.setVisibility(View.VISIBLE);
	}
	
	public void showProgress(String message){
		if(progressDialog == null || !progressDialog.isShowing())
			progressDialog = ProgressDialog.show(this, "", message, true);
	}
	
	public void hideProgress(){
		if(progressDialog != null && progressDialog.isShowing())
			progressDialog.dismiss();
	}
	
	public void trendingToggle(View view){
		if(!app.TrendingView.equals("MapView")){
			app.TrendingView = "MapView";
			((Button)view).setText("Switch to List View");
			loadTrendingMap();
		}else{
			app.TrendingView = "ListView";
			((Button)view).setText("Switch to Map View");
			loadGroupTrending(null);
		}
	}
	
	private void loadTrendingMap(){
		fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.fragmentHolder, fragmentTrendingMap);
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
	}
	
	private void handleReceiver(){
		app.UploadingPhoto = app.UploadPhotoInit = false;
		if(fragmentBuzz.isVisible())
			fragmentBuzz.refreshBuzz();
	}
	
	private void startPostBuzz(){
		
	}
	
	private void postBuzzResult(String result){
		Buzz buzz;
		String text = "";
		
		if(result.equals("success")){
			try{
				buzz = new Buzz();
				if(uploadedPhoto){
					uploadedPhoto = false;
					buzz.setIsImage(true);
					buzz.setNewUpload(true);
					//buzz.setNewUploadImage(takenPictureData);
					takenPictureData = null;
					text = jsonObject.getJSONObject("body").getString("buzzUrl");
				}else{
					buzz.setIsImage(false);
					text = txtBuzzText.getText().toString();
					txtBuzzText.setText("");
				}
				buzz.setBuzzText(text);
				buzz.setId(jsonObject.getJSONObject("body").getInt("buzzId"));
				buzz.setUserId(user.getId());
				buzz.setBuzzUser(user);
				buzz.setVenueId(0);
				buzz.setType(0);
				java.util.Date date= new java.util.Date();
				buzz.setDateStamp(date.getTime());
				buzz.setChatPictureURL(user.getUserPhoto().isEmpty() ? "" : user.getUserPhoto());
				app.ActiveGroup.getBuzz().add(buzz);

				if(buzz.getIsImage()){
					if(app.BuzzImages == null)
						app.BuzzImages = new ArrayList<Buzz>();
					app.BuzzImages.add(buzz);
				}
				
				if(!app.UploadingPhoto || (app.UploadingPhoto && !app.UploadPhotoInit))
					fragmentBuzz.updateAdapter(buzz);
				else{
					Intent i = new Intent("com.ignight.android.updatebuzzphoto");
			        sendBroadcast(i);
				}
			} catch(Exception ex) {
				
			}
			
		}else{
			showMessage("Error, failed to add buzz: " + result);
		}
		txtPostBuzz.setText("Send");
		txtPostBuzz.setClickable(true);
	}
	
	private class PostBuzzTask extends AsyncTask<Void, Void, String>{
		@Override
		protected String doInBackground(Void... args) {
			String json = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				gson = new GsonBuilder().create();
				MessageGroupBuzz buzz = new MessageGroupBuzz(txtBuzzText.getText().toString(), user.getCity().getId(), user.getId(), app.ActiveGroup.getGroupId());
				json = gson.toJson(buzz);
				
				StringEntity stringEntity = new StringEntity(json);
				httpClient = new CustomHttpClient(activity);
				httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_group_buzz_add_text) + app.ActiveGroup.getGroupId());
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID",""+app.getActiveUser().getId());
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					
					if(httpResult != null){
						//retVal = httpResult;
						jsonObject = new JSONObject(httpResult);
						
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							retVal = "Error posting buzz.";
						}
						//Log.d("GroupBuzz",httpResult);
					}else{
						retVal = "Error, no result.";
					}
				}
				
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}

			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startPostBuzz();
		}
		
		@Override
		protected void onPostExecute(String result){
			postBuzzResult(result);
		}
	}

	private void startPostPhoto(){
		uploadedPhoto = true;
		progressDialog = ProgressDialog.show(this, "", "Uploading photo....", true);
	}
	
	private class PostPhotoTask extends AsyncTask<Void, Void, String>{

		@Override
		protected String doInBackground(Void... params) {
			String json = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				gson = new GsonBuilder().create();
				MessageGroupBuzz buzz = new MessageGroupBuzz(txtBuzzText.getText().toString(), user.getCity().getId(), user.getId(), app.ActiveGroup.getGroupId());
				json = gson.toJson(buzz);
				
				httpClient = new CustomHttpClient(activity);
				httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_group_buzz_add_image) + app.ActiveGroup.getGroupId()+"/?groupId=" + app.ActiveGroup.getGroupId());
				httpPost.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID",""+app.getActiveUser().getId());
				
				MultipartEntityBuilder meBuilder = MultipartEntityBuilder.create();
				meBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
				
				String compressImagePath = filePath;
				
				//get compress bitmap
				Bitmap b = getCompressBitmap(filePath);
				
				//fix rotation
	            ExifInterface exif = new ExifInterface(filePath);
		        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

		        int angle = 0;

		        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
		            angle = 90;
		        } 
		        else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
		            angle = 180;
		        } 
		        else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
		            angle = 270;
		        }

		        Matrix mat = new Matrix();
		        mat.postRotate(angle);
		        
		        BitmapFactory.Options options = new BitmapFactory.Options();
		        options.inSampleSize = 2;
		        
		        Bitmap bmp = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), mat, true);
				
				//save file to temp
	            File f = new File(ActivityGroup.this.getCacheDir().getAbsolutePath(), new java.util.Date().getTime() + ".jpg");
	            
	            compressImagePath = f.getAbsolutePath();
	            
	            //Log.d("File", "location: " + compressImagePath);
	            FileOutputStream fos = null;
	            try {
	                fos = new FileOutputStream(f);
	                bmp.compress(Bitmap.CompressFormat.JPEG, 80, fos);
	                fos.flush();
	                fos.close();
	            } catch (FileNotFoundException e) {
	                e.printStackTrace();
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	            b.recycle();
				
				File file = new File(compressImagePath);
				FileBody fb = new FileBody(file);
				
				meBuilder.addPart("userImage", fb);
				meBuilder.addTextBody("json_body",json);
				httpPost.setEntity(meBuilder.build());
				httpPost.setHeader("Accept","application/json");
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					
					if(httpResult != null){
						jsonObject = new JSONObject(httpResult);
						
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							retVal = "Error, posting photo.";
						}
					}else{
						retVal = "Error, no result.";
					}
				}
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			hideProgress();
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startPostPhoto();
		}
		
		@Override
		protected void onPostExecute(String result){
			postBuzzResult(result);
		}
	}
}
