package com.ignight.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ActivityLogin extends ActivityBase {
	private ActivityLogin activity;
	private BufferedReader bufferReader;
	private DataAdapter dataAdapter;
	private EditText emailAddress;
	private Gson gson;
	private String httpResult;
	private HttpClient httpClient;
	private HttpEntity httpEntity;
	private HttpPost httpPost;
	private HttpResponse httpResponse;
	private InputStream inputStream;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private EditText passWord;
	private ProgressDialog progressDialog;
	private StringBuilder stringBuilder;
	private User user;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_login);
		
		app = ((AppBase)getApplication());
		app.clearApp();
		user = new User();
		activity = this;
	}
	
	public void btnLogin_ClickEvent(View button){
		emailAddress = (EditText) findViewById(R.id.txtEmailAddress);
		passWord = (EditText) findViewById(R.id.txtPassword);
		
		if(emailAddress.getText().toString().equals("") || passWord.getText().toString().equals("")){
			showMessage("Username and password required.");
		}/*else if(!Pattern.matches("^(.+)@([^@]+[^.]).([^.])$", emailAddress.getText().toString())){
			showMessage("Invalid email address");
		}*/else{
			user.setUserName(emailAddress.getText().toString());
			user.setUserPassword(passWord.getText().toString());
			app.setActiveUser(user);
			new LoginTask().execute();
		}
	}

	public void btnSignup_ClickEvent(View button){
		Intent intent;
		intent = new Intent(ActivityLogin.this, ActivityRegister.class);
		startActivity(intent);
	}
	
	public void btnForgotPassword_ClickEvent(View button){
		Intent intent;
		intent = new Intent(ActivityLogin.this, ActivityForgotPassword.class);
		startActivity(intent);
	}
	
	private void startLogin(){
		progressDialog = ProgressDialog.show(this, "", "Validating login....", true);
	}
	
	private void loginResult(String result){
		ArrayList<DNAAtmosphere> atmosphereOptions = new ArrayList<DNAAtmosphere>();
		Intent intent;
		ArrayList<DNAMusic> musicOptions = new ArrayList<DNAMusic>();
		
		if(result.equals("success")){
			//did user set dna
			try {
				if(jsonObject.getJSONObject("body").getBoolean("userInfoSet")){
					user.setUserEmail(jsonObject.getJSONObject("body").getString("email"));
					if(jsonObject.getJSONObject("body").has("profilePictureURL"))
						user.setUserPhoto(jsonObject.getJSONObject("body").getString("profilePictureURL"));
					else
						user.setUserPhoto(app.DefaultChatUrl);
					user.setGender(DNAGender.getGender(jsonObject.getJSONObject("body").getInt("genderId")));
					user.setAge(jsonObject.getJSONObject("body").getInt("age"));
					user.setCity(new City(jsonObject.getJSONObject("body").getInt("cityId"), 
							City.getCityName(jsonObject.getJSONObject("body").getInt("cityId"))));
					user.setId(jsonObject.getJSONObject("body").getInt("id"));
					user.setSpendingPreference(jsonObject.getJSONObject("body").getInt("spending"));
					jsonArray = jsonObject.getJSONObject("body").getJSONArray("atmosphere");
					if(jsonArray != null){
						for(int i = 0; i < jsonArray.length(); i++){
							if(jsonArray.getInt(i) != -1){
								atmosphereOptions.add(new DNAAtmosphere(jsonArray.getInt(i), DNAOptions.AtmosphereOptions().get(jsonArray.getInt(i)).getName()));
							}
						}
						user.setAtmosphereOptions(atmosphereOptions);
					}
					jsonArray = jsonObject.getJSONObject("body").getJSONArray("music");
					if(jsonArray != null){
						for(int i = 0; i < jsonArray.length(); i++){
							if(jsonArray.getInt(i) != -1){
								musicOptions.add(new DNAMusic(jsonArray.getInt(i), DNAOptions.MusicOptions().get(jsonArray.getInt(i)).getName()));
							}
						}
						user.setMusicOptions(musicOptions);
					}
					//set user preference here
					//user.getUserPreference().setDisplayName(jsonObject.getJSONObject("body").getBoolean("displayNameInGroups") ? 1 : 0);
					user.getUserPreference().setInvites(jsonObject.getJSONObject("body").getBoolean("notifyOfGroupInvites") ? 1 : 0);
					
					dataAdapter = new DataAdapter(this);
					dataAdapter.deleteUser();
					dataAdapter.AddUser(user);
					app.setActiveUser(user);
					app.checkPushNotification();
					intent = new Intent(ActivityLogin.this, ActivityTrending.class);
				}else{
					user.setId(jsonObject.getJSONObject("body").getInt("id"));
					user.setUserPhoto(app.DefaultChatUrl);
					app.setActiveUser(user);
					intent = new Intent(ActivityLogin.this, ActivityDNAUserInfo.class);
				}
			} catch (JSONException e) {
				//progressDialog = ProgressDialog.show(this, "", jsonObject.toString(), true);
				showMessage("Error, could not log in. Please try again: " + e.getMessage());
				intent = new Intent(ActivityLogin.this, ActivityLogin.class);
			}
			startActivity(intent);
		}else{
			showMessage(result);
		}
	}
	
	private class LoginTask extends AsyncTask<Void, Void, String>{

		@Override
		protected String doInBackground(Void... args) {
			String json = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				gson = new GsonBuilder().create();
				MessageLogin login = new MessageLogin(emailAddress.getText().toString(), passWord.getText().toString());
				json = gson.toJson(login);
				
				StringEntity stringEntity = new StringEntity(json);
				httpClient = new CustomHttpClient(activity);
				httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_login));
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID","");
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					
					if(httpResult != null){
						jsonObject = new JSONObject(httpResult);
						//Log.d("Login", httpResult);
						if(jsonObject.getBoolean("res")){
							retVal = "success";
							DataAdapter dataAdapter = new DataAdapter(ActivityLogin.this);
							dataAdapter.cleanDB();
						}else{
							retVal = jsonObject.getString("reason");
						}
					}else{
						retVal = "Error, could not process login.";
					}
				}
				
			}catch(ClientProtocolException e){
				retVal = "Client Protocal Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "IO Error, " + e.getMessage();
			
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			progressDialog.dismiss();

			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startLogin();
		}
		
		@Override
		protected void onPostExecute(String result){
			loginResult(result);
		}
	}

}
