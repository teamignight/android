package com.ignight.android;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class ActivityDNAMusic extends ActivityBase {
	private AdapterMusic adapter;
	private GridView gridView;
	private final int[] overlays = { R.drawable.selected_one, R.drawable.selected_two, R.drawable.selected_three, R.drawable.selected_four, R.drawable.selected_five };
	protected TextView title;
	private User user;
	private SparseArray<DNAMusic> musicHash;
	private Button btnSignup;
	private boolean ready;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_dna_music);
		
        app = ((AppBase)getApplication());
        user = app.getActiveUser();
        
        btnSignup = (Button)findViewById(R.id.btnSignup);
        adapter = new AdapterMusic(this, DNAOptions.MusicOptions(), user);
        musicHash = new SparseArray<DNAMusic>();
        
        gridView = (GridView)findViewById(R.id.grdDNAMusicItems);
		gridView.setAdapter(adapter);
		gridView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id){
				ImageView numberView;
				int pos = 0;
				DNAMusic currentItem;
				int currItemPos = 0;
				DNAMusic item = DNAOptions.MusicOptions().get(position);
				
				numberView = (ImageView)v.findViewById(R.id.gridNumber);
				if(musicHash.get(position) != null){
					for(int i = 0; i < user.getMusicOptions().size(); i++){
						currentItem = user.getMusicOptions().get(i);
						if(currentItem.getId() == musicHash.get(position).getId()){
							currItemPos = i;
						}
					}
					if((currItemPos + 1) < user.getMusicOptions().size()){
						checkIfReady();
						return;
					}
					user.getMusicOptions().remove(musicHash.get(position));
					musicHash.remove(position);
					numberView.setImageDrawable(null);
					checkIfReady();
					return;
				}
				if(musicHash.size() == 3){
					checkIfReady();
					return;
				}
				user.getMusicOptions().add(item);
				musicHash.put(position, item);
				for(int i = 0; i < user.getMusicOptions().size(); i++){
					if(musicHash.get(position).getId() == user.getMusicOptions().get(i).getId()){
						pos = i;
						break;
					}
				}
				numberView.setImageResource(overlays[pos]);
				checkIfReady();
				//Picasso.with(ActivityDNAMusic.this).load(overlays[pos]).into(numberView);
			}
		});
	}
	
	@Override
	public void onResume(){
		super.onResume();
		//musicHash = new SparseArray<DNAMusic>();
		/*for(int i = 0; i < user.getMusicOptions().size(); i++){
			
		}*/
	}
	
	@Override
	public void onPause(){
		super.onPause();
	}
	
	@Override
	public void onStop(){
		super.onStop();
	}

	public void btnNext_ClickEvent(View button){
		Intent intent;
		
		if(user.getMusicOptions().size() < 1){
			showMessage("Please select at least 1 music preference.");
		}else{
			app.setActiveUser(user);
			intent = new Intent(ActivityDNAMusic.this, ActivityDNAAtmosphere.class);
			startActivity(intent);
		}
	}
	private void checkIfReady(){
		ready = user.getMusicOptions().size() > 0;
		if(ready){
			btnSignup.setTextColor(Color.parseColor("#d849b10a"));
		}else{
			btnSignup.setTextColor(Color.parseColor("#626262"));
		}
	}
}
