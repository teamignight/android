package com.ignight.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ToggleButton;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ActivityAddGroup extends ActivityBase{
	private ActivityAddGroup activity;
	private BufferedReader bufferReader;
	private CheckBox checkBox;
	private DataAdapter dataAdapter;
	private EditText groupDescription;
	private EditText groupName;
	private boolean groupPublic;
	private Gson gson;
	private String httpResult;
	private HttpClient httpClient;
	private HttpEntity httpEntity;
	private HttpPost httpPost;
	private HttpResponse httpResponse;
	private InputStream inputStream;
	private JSONObject jsonObject;
	private ProgressDialog progressDialog;
	private StringBuilder stringBuilder;
	private User user;
	private ToggleButton togglePublic;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_add_group);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,R.layout.module_title_bar);
		
		activity = this;
		user = CurrentUser;
		
		checkBox = (CheckBox)findViewById(R.id.chPublicAccess);
		groupDescription = (EditText)findViewById(R.id.txtGroupDetails);
		groupName = (EditText)findViewById(R.id.txtGroupName);
		togglePublic = (ToggleButton)findViewById(R.id.togglePublic);
		
		togglePublic.setChecked(true);
		groupPublic = true;
		
		wireupHeader("Create a Group", "");
	}
	
	@Override
	public void onResume(){
		super.onResume();
		app.LeftGroup = false;
	}
	
	public void btnAddGroup_ClickEvent(View v){
		if(groupDescription.getText().toString().isEmpty() || groupName.getText().toString().isEmpty()){
			showMessage("Group name and description are required.");
		}else if(groupDescription.getText().toString().length() < 10){
			showMessage("Group description must be at least 10 characters.");
		}else{
			new AddGroupTask().execute();
		}
	}
	
	public void btnCancelGroup_ClickEvent(View v){
		finish();
	}
	
	public void togglePublic(View view){
		boolean on = ((ToggleButton) view).isChecked();
		groupPublic = on;
	}
	
	private void startAddGroup(){
		progressDialog = ProgressDialog.show(this, "", "Adding group....", true);
	}
	
	private void addGroupResult(String result){
		
		if(result.equals("success")){
			try{
				Group newGroup = new Group();
				newGroup.setGroupName(groupName.getText().toString());
				newGroup.setGroupDescription(groupDescription.getText().toString());
				newGroup.setIsOwner(true);
				newGroup.setIsPrivate(!togglePublic.isChecked());
				newGroup.setGroupId(jsonObject.getJSONObject("body").getInt("id"));
				
				groupDescription.setText("");
				groupName.setText("");
				
				app.ActiveGroup = newGroup;
				Intent intent = new Intent(this, ActivityGroup.class);
				startActivity(intent);
			}catch(Exception ex){
				showMessage("Error, failed to add group: " + ex.getMessage());
			}
		}else{
			showMessage(result);
		}
	}
	
	private class AddGroupTask extends AsyncTask<Void, Void, String>{
		@Override
		protected String doInBackground(Void... args) {
			String json = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				gson = new GsonBuilder().create();
				
				MessageGroup group = new MessageGroup(user.getCity().getId(), groupDescription.getText().toString(), groupName.getText().toString(), !groupPublic, user.getId());
				json = gson.toJson(group);
				
				StringEntity stringEntity = new StringEntity(json);
				//retVal = json;
				//if(retVal != "")
				//return retVal;
				
				httpClient = new CustomHttpClient(activity);
				httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_group_add) + app.getActiveUser().getId());
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID",""+app.getActiveUser().getId());
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					
					if(httpResult != null){
						//Log.d("Group", httpResult);
						jsonObject = new JSONObject(httpResult);
						
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							if(jsonObject.has("reason"))
								retVal = jsonObject.getString("reason");
							else
								retVal = "Error, could not create group.";
						}
					}else{
						retVal = "Error, no result.";
					}
				}
				
			}catch(ClientProtocolException e){
				retVal = "Client Protocal Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "IO Error, " + e.getMessage();
			
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			progressDialog.dismiss();

			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startAddGroup();
		}
		
		@Override
		protected void onPostExecute(String result){
			addGroupResult(result);
		}
	}
}
