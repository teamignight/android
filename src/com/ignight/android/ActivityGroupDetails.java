package com.ignight.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ActivityGroupDetails extends ActivityBase {
	private ActivityGroupDetails activity;
	private BufferedReader bufferReader;
	private TextView txtGroupDetails;
	private TextView txtGroupPublic;
	private Gson gson;
	private String httpResult;
	private HttpClient httpClient;
	private HttpEntity httpEntity;
	private HttpGet httpGet;
	private HttpPost httpPost;
	private HttpResponse httpResponse;
	private InputStream inputStream;
	private JSONObject jsonObject;
	private ProgressDialog progressDialog;
	private StringBuilder stringBuilder;
	private TextView txtGroupMembers;
	private User user;
	
	private ImageView imgMosacMusic;
	private TextView txtMosacMusic;
	private ImageView imgMosacAtmosphere;
	private TextView txtMosacAtmosphere;
	private ImageView imgMosacSpending;
	private TextView txtMosacSpending;
	private ImageView imgMosacAge;
	private TextView txtMosacAge;
	private Button btnJoinGroup;
	private Button btnLeaveGroup;
	private RelativeLayout notifiWrapper;
	private boolean subscribedToBuzz;
	private ToggleButton btnToggleNotifications;
	private boolean wasInvited;
	
	private int[] arrSpending = { R.drawable.detail_spending_1, R.drawable.detail_spending_2, R.drawable.detail_spending_3, R.drawable.detail_spending_4 };
	private String[] arrSpendingText = { "Broke", "Getting By", "Doing Well", "Living The Dream" };
	private int[] arrMusic = { R.drawable.detail_music_classic_rock, R.drawable.detail_music_classical, 
					R.drawable.detail_music_country, R.drawable.detail_music_electronic, R.drawable.detail_music_folk,
					R.drawable.detail_music_indie_rock, R.drawable.detail_music_jazz, R.drawable.detail_music_latin,
					R.drawable.detail_music_oldies, R.drawable.detail_music_pop, R.drawable.detail_music_rnb, 
					R.drawable.detail_music_rap, R.drawable.detail_music_reggae, R.drawable.detail_music_rock, 
					R.drawable.detail_music_top_40};
	private String[] arrMusicText = { "Classic Rock", "Classical", "Country", "Electronic", "Folk", "Indie Rock", "Jazz", "Latin",  "Oldies", 
							"Pop", "R&B", "Rap", "Reggae", "Rock", "Top 40" };
	
	private int[] arrAtmosphere = { R.drawable.detail_atmosphere_cocktail_lounge, R.drawable.detail_atmosphere_dive_bar, 
					R.drawable.detail_atmosphere_gay_bar, R.drawable.detail_atmosphere_hipster_lounge, 
					R.drawable.detail_atmosphere_live_music, R.drawable.detail_atmosphere_sports_bar,
					R.drawable.detail_atmosphere_techno_club, R.drawable.detail_atmosphere_vip_club};
	private String[] arrAtmosphereText = { "Cocktail Lounge", "Dive Bar", "Gay Bar", "Hipster Lounge","Live Music", 
							"Sports Bar", "Techno Club", "VIP Club" };
	private int[] arrAge = { R.drawable.detail_age_1, R.drawable.detail_age_2, R.drawable.detail_age_3, R.drawable.detail_age_4,
					R.drawable.detail_age_5, R.drawable.detail_age_6 };
	private String[] arrAgeText = { "18-20","21-25","26-30","31-35","36-40","41+" };
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_group_details);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,R.layout.module_title_bar);
		
        user = CurrentUser;
        activity = this;
        wasInvited = app.ActiveGroup.getWasInvited();
        app.ActiveGroup.setWasInvited(false);
        
        wireupHeader(app.ActiveGroup.getGroupName(), "");
        
        txtGroupMembers = (TextView)findViewById(R.id.txtGroupDetailsMembers);
        txtGroupPublic = (TextView)findViewById(R.id.txtGroupDetailsPublic);
        txtGroupDetails = (TextView)findViewById(R.id.txtGroupDetailsDescription);
        imgMosacMusic = (ImageView)findViewById(R.id.imgMosacMusic);
        txtMosacMusic = (TextView)findViewById(R.id.txtMosacMusic);
        imgMosacAtmosphere = (ImageView)findViewById(R.id.imgMosacAtmosphere);
        txtMosacAtmosphere = (TextView)findViewById(R.id.txtMosacAtmosphere);
        imgMosacSpending = (ImageView)findViewById(R.id.imgMosacSpending);
        txtMosacSpending = (TextView)findViewById(R.id.txtMosacSpending);
        imgMosacAge = (ImageView)findViewById(R.id.imgMosacAge);
        txtMosacAge = (TextView)findViewById(R.id.txtMosacAge);
        btnJoinGroup = (Button)findViewById(R.id.btnJoinGroup);
        btnLeaveGroup = (Button)findViewById(R.id.btnLeaveGroup);
        notifiWrapper = (RelativeLayout)findViewById(R.id.notifiWrapper);
        btnToggleNotifications = (ToggleButton)findViewById(R.id.toggleNotifications);
        
        new GetGroupTask().execute();
	}
	
	public void btnLeaveGroup_ClickEvent(View view){
		new LeaveGroupTask().execute();
	}
	
	public void btnJoinGroup_ClickEvent(View view){
		if(app.ActiveGroup.getIsPrivate()){
			new GroupResponseTask().execute();
		}else{
			new JoinGroupTask().execute();
		}
	}
	
	public void btnCancelJoinGroup_ClickEvent(View view){
		finish();
	}
	
	public void toggleNotifications(View view){
		subscribedToBuzz = !((ToggleButton) view).isChecked();
		new ToggleBuzzNotification().execute();
	}
	
	private void startLeaveGroup(){
		progressDialog = ProgressDialog.show(this, "", "Leaving group....", true, true);
	}
	
	private void leaveGroupResult(String result){
		if(result.equals("success")){
			app.LeftGroup = true;
			finish();
		}else{
			showMessage("Error, could not leave group. " + result);
		}
	}
	
	private class LeaveGroupTask extends AsyncTask<Void, Void, String>{
		@Override
		protected String doInBackground(Void... args) {
			String json = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				gson = new GsonBuilder().create();
				MessageLeaveGroup leaveGroup = new MessageLeaveGroup(user.getId(), app.ActiveGroup.getGroupId(), user.getCity().getId());
				
				json = gson.toJson(leaveGroup);
				
				StringEntity stringEntity = new StringEntity(json);
				
				httpClient = new CustomHttpClient(activity);
				httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_group_leave) + app.ActiveGroup.getGroupId());
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID",""+app.getActiveUser().getId());
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					
					if(httpResult != null){
						retVal = httpResult;
						jsonObject = new JSONObject(httpResult);
						
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							retVal = "Error, could not leave group.";
						}
					}else{
						retVal = "Error, no result.";
					}
				}
			}catch(ClientProtocolException e){
				retVal = "Client Protocal Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "IO Error, " + e.getMessage();
			
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			if(progressDialog != null && progressDialog.isShowing())
				progressDialog.dismiss();

			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startLeaveGroup();
		}
		
		@Override
		protected void onPostExecute(String result){
			leaveGroupResult(result);
		}
	}
	
	private void startGetGroup(){
		progressDialog = ProgressDialog.show(this, "", "Getting group details....", true, true);
	}
	
	private void getGroupResult(String result){
		JSONObject jsonGroup;
		int mode = 0;
		
		if(result.equals("success")){
			try{
				jsonGroup = jsonObject.getJSONObject("body");
				
				mode = jsonGroup.getInt("musicTypeMode");
				imgMosacMusic.setImageResource(arrMusic[mode == -1 ? 0 : mode]);
				txtMosacMusic.setText(arrMusicText[mode == -1 ? 0 : mode]);
				mode = jsonGroup.getInt("atmosphereTypeMode");
				imgMosacAtmosphere.setImageResource(arrAtmosphere[mode == -1 ? 0 : mode]);
				txtMosacAtmosphere.setText(arrAtmosphereText[mode == -1 ? 0 : mode]);
				mode = jsonGroup.getInt("spendingLimitMode");
				imgMosacSpending.setImageResource(arrSpending[mode == -1 ? 0 : mode]);
				txtMosacSpending.setText(arrSpendingText[mode == -1 ? 0 : mode]);
				mode = jsonGroup.getInt("ageBucketMode");
				imgMosacAge.setImageResource(arrAge[mode == -1 ? 0 : mode]);
				txtMosacAge.setText(arrAgeText[mode == -1 ? 0 : mode]);
				
				int members = jsonGroup.getInt("membersCount");
				txtGroupMembers.setText(members == 1 ? members + " Member" : members + " Members");
				txtGroupPublic.setText((jsonGroup.getBoolean("public") ? "Public" : "Private") + " Group");
				txtGroupDetails.setText(jsonGroup.getString("description"));
				
				app.ActiveGroup.setIsPrivate(!jsonGroup.getBoolean("public"));
				app.ActiveGroup.setIsMember(jsonGroup.getBoolean("isMember"));
				app.ActiveGroup.setGroupName(jsonGroup.getString("name"));
				app.ActiveGroup.setIsSubscribed(jsonGroup.getBoolean("subscribed"));
				btnJoinGroup.setText(app.ActiveGroup.getIsPrivate() ? "Request to Join Group" : "Join Group");
				
				if(!app.ActiveGroup.getIsPrivate() && !app.ActiveGroup.getIsMember()){
		        	btnJoinGroup.setVisibility(View.VISIBLE);
		        }
				
				if(app.ActiveGroup.getIsPrivate() && !app.ActiveGroup.getIsMember() && wasInvited){
					btnJoinGroup.setText("Accept Group Invite");
					btnJoinGroup.setVisibility(View.VISIBLE);
				}
				
				if(app.ActiveGroup.getIsPrivate() && app.ActiveGroup.getIsMember())
					notifiWrapper.setVisibility(View.VISIBLE);
				
				if(app.ActiveGroup.getIsMember()){
					btnLeaveGroup.setVisibility(View.VISIBLE);
					if(app.ActiveGroup.getIsSubscribed())
						btnToggleNotifications.setChecked(true);
					else
						btnToggleNotifications.setChecked(false);
				}
			} catch(JSONException e) {
				showMessage("Error parsing group details: " + e.getMessage());
			}
		} else if (result.equalsIgnoreCase("Group does not exist.")){
			showMessage("Error, invalid group.");
			finish();
		} else {
			showMessage("Error, could not load group details.");
		}
	}
	
	private class GetGroupTask extends AsyncTask<Void, Void, String>{
		@Override
		protected String doInBackground(Void... args) {
			String retVal = "";
			String singleLine = "";
			
			try{httpClient = new CustomHttpClient(activity);
				httpGet = new HttpGet(getString(R.string.api_base) + getString(R.string.api_group_info) + app.ActiveGroup.getGroupId() + "/?userId=" + user.getId());
				httpGet.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpGet.setHeader("PLATFORM","Android");
				httpGet.setHeader("USER_ID",""+app.getActiveUser().getId());
				httpResponse = httpClient.execute(httpGet);
				
				httpEntity = httpResponse.getEntity();
				inputStream = httpEntity.getContent();
				bufferReader = new BufferedReader(new InputStreamReader(inputStream));
				stringBuilder = new StringBuilder();
				
				while((singleLine = bufferReader.readLine()) != null){
					stringBuilder.append(singleLine);
				}
				
				httpResult = stringBuilder.toString();
				inputStream.close();
				jsonObject = new JSONObject(httpResult);
				if(jsonObject.getBoolean("res")){
					retVal = "success";
				}else{
					retVal = jsonObject.getString("reason");
				}
				
			}catch(ClientProtocolException e){
				retVal = "Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "Error, " + e.getMessage();
			}catch(JSONException e){
				retVal = "Error, " + e.getMessage();
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			if(progressDialog != null && progressDialog.isShowing())
				progressDialog.dismiss();
			
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startGetGroup();
		}
		
		@Override
		protected void onPostExecute(String result){
			getGroupResult(result);
		}
	}
	
	private void startJoinGroup(){
		progressDialog = ProgressDialog.show(this, "", "Joining group....", true, true);
	}
	
	private void joinGroupResult(String result){
		if(result.equals("success")){
			showMessage("Group added successfully.");
			Intent intent = new Intent(this, ActivityGroup.class);
			startActivity(intent);
		}else{
			showMessage("Error, could not join group.");
		}
	}
	
	private class JoinGroupTask extends AsyncTask<Void, Void, String>{
		@Override
		protected String doInBackground(Void... args) {
			String json = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				gson = new GsonBuilder().create();
				
				MessageJoinGroup join = new MessageJoinGroup(user.getCity().getId(), app.ActiveGroup.getGroupId(), user.getId(), "");
				json = gson.toJson(join);
				
				StringEntity stringEntity = new StringEntity(json);
				
				httpClient = new CustomHttpClient(activity);
				httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_group_join) + app.ActiveGroup.getGroupId());
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID",""+app.getActiveUser().getId());
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					
					if(httpResult != null){
						retVal = httpResult;
						jsonObject = new JSONObject(httpResult);
						//Log.d("Group", httpResult);
						if(jsonObject.getBoolean("res")){
							retVal = "success";
							
							//now remove invite
							try{
								MessageGroupResponse groupResponse = new MessageGroupResponse(true, user.getCity().getId(), app.ActiveGroup.getGroupId(), user.getId());
								
								json = gson.toJson(groupResponse);
								
								stringEntity = new StringEntity(json);
								httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_group_invite_respond) + app.ActiveGroup.getGroupId());
								httpPost.setEntity(stringEntity);
								httpPost.setHeader("Accept","application/json");
								httpPost.setHeader("Content-type","application/json; charset=utf-8");
								httpPost.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
								httpClient.execute(httpPost);
								app.RefreshInbox = true;
							}catch(Exception ex){
								
							}
						}else{
							retVal = "Error, could not join group.";
						}
					}else{
						retVal = "Error, no result.";
					}
				}
				
			}catch(ClientProtocolException e){
				retVal = "Client Protocal Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "IO Error, " + e.getMessage();
			
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			if(progressDialog != null && progressDialog.isShowing())
				progressDialog.dismiss();

			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startJoinGroup();
		}
		
		@Override
		protected void onPostExecute(String result){
			joinGroupResult(result);
		}
	}

	private void startToggleBuzz(){
		if(!subscribedToBuzz){
			progressDialog = ProgressDialog.show(this, "", "Unsubscribing to group buzz notifications....", true, true);
		}else{
			progressDialog = ProgressDialog.show(this, "", "Subscribing to group buzz notifications....", true, true);
		}
	}
	
	private void toggleBuzzResult(String result){
		if(result.equalsIgnoreCase("success")){
			subscribedToBuzz = !subscribedToBuzz;
		}else{
			showMessage("Error, could not update notification settings.");
		}
	}
	
	private class ToggleBuzzNotification extends AsyncTask<Void, Void, String>{
		@Override
		protected String doInBackground(Void... args) {
			String json = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				gson = new GsonBuilder().create();
				MessageToggleGroupBuzzNotification msg = new MessageToggleGroupBuzzNotification(app.getActiveUser().getId());
				
				json = gson.toJson(msg);
				
				StringEntity stringEntity = new StringEntity(json);
				
				httpClient = new CustomHttpClient(activity);
				if(!subscribedToBuzz){
					httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_group_buzz_subscribe) + app.ActiveGroup.getGroupId());
				}else{
					httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_group_buzz_unsubscribe) + app.ActiveGroup.getGroupId());
				}
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID",""+app.getActiveUser().getId());
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					
					if(httpResult != null){
						retVal = httpResult;
						jsonObject = new JSONObject(httpResult);
						//Log.d("GroupNoti", httpResult);
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							retVal = "Error, could not update notification settings.";
						}
					}else{
						retVal = "Error, no result.";
					}
				}
			}catch(ClientProtocolException e){
				retVal = "Client Protocal Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "IO Error, " + e.getMessage();
			
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			if(progressDialog != null && progressDialog.isShowing())
				progressDialog.dismiss();
			
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startToggleBuzz();
		}
		
		@Override
		protected void onPostExecute(String result){
			toggleBuzzResult(result);
		}
	}

	private class GroupResponseTask extends AsyncTask<Void, Void, String>{
		@Override
		protected String doInBackground(Void... args) {
			String json = "";
			String retVal = "";
			String singleLine = "";
			
			try{
				gson = new GsonBuilder().create();
				MessageGroupResponse groupResponse = new MessageGroupResponse(true, user.getCity().getId(), app.ActiveGroup.getGroupId(), user.getId());
				
				json = gson.toJson(groupResponse);
				
				StringEntity stringEntity = new StringEntity(json);
				httpClient = new CustomHttpClient(activity);
				httpPost = new HttpPost(getString(R.string.api_base) + getString(R.string.api_group_invite_respond) + app.ActiveGroup.getGroupId());
				httpPost.setEntity(stringEntity);
				httpPost.setHeader("Accept","application/json");
				httpPost.setHeader("Content-type","application/json; charset=utf-8");
				httpPost.setHeader(getString(R.string.api_token_key),getString(R.string.api_token_value));
				httpPost.setHeader("PLATFORM","Android");
				httpPost.setHeader("USER_ID",""+app.getActiveUser().getId());
				
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
				if(httpEntity != null){
					inputStream = httpEntity.getContent();
					bufferReader = new BufferedReader(new InputStreamReader(inputStream));
					stringBuilder = new StringBuilder();
					
					while((singleLine = bufferReader.readLine()) != null){
						stringBuilder.append(singleLine);
					}
					
					httpResult = stringBuilder.toString();
					inputStream.close();
					
					if(httpResult != null){
						jsonObject = new JSONObject(httpResult);
						
						if(jsonObject.getBoolean("res")){
							retVal = "success";
						}else{
							retVal = "Error, could not join group.";
						}
					}else{
						retVal = "Error, no result.";
					}
				}
			}catch(ClientProtocolException e){
				retVal = "Client Protocal Error, " + e.getMessage();
			}catch(IOException e){
				retVal = "IO Error, " + e.getMessage();
			
			}catch(Exception e){
				retVal = "General Error, " + e.getMessage();
			}
			
			if(progressDialog != null && progressDialog.isShowing())
				progressDialog.dismiss();
			
			return retVal;
		}
		
		@Override
		protected void onPreExecute(){
			startJoinGroup();
		}
		
		@Override
		protected void onPostExecute(String result){
			joinGroupResult(result);
		}
	}
}
