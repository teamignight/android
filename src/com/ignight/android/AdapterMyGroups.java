package com.ignight.android;

import java.util.ArrayList;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AdapterMyGroups extends ArrayAdapter<Group> {
	private AppBase app;
	private Context context;
	private Group group;
	private ArrayList<Group> groups;
	private LayoutInflater layoutInflater;
	
	public AdapterMyGroups(Context context, int textViewResourceId, ArrayList<Group> items, AppBase appBase){
		super(context, textViewResourceId, items);
		
		this.app = appBase;
		this.context = context;
		groups = items;
		
		layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {	
		ViewHolder holder;
		View view = convertView;
		
		if(view == null){
			view = layoutInflater.inflate(R.layout.list_my_group, null);
			holder = new ViewHolder();
			holder.groupName = (TextView)view.findViewById(R.id.txtMyGroupName);
			holder.groupMembers = (TextView)view.findViewById(R.id.txtMyGroupCount);
			holder.imgBuzz = (ImageView)view.findViewById(R.id.imgGroupBuzz);
			holder.imgGroupPrivate = (ImageView)view.findViewById(R.id.imgGroupPrivate);
			holder.imgGroupPublic = (ImageView)view.findViewById(R.id.imgGroupPublic);
			holder.buzzWrapper = (RelativeLayout)view.findViewById(R.id.buzzWrapper);
			holder.txtBuzzTime = (TextView)view.findViewById(R.id.txtMyGroupBuzzTime);
					
			view.setTag(holder);
		}else{
			holder = (ViewHolder)view.getTag();
		}
		
		group = groups.get(position);
		holder.groupName.setText(group.getGroupName());
		holder.groupMembers.setText(group.getMemberCount() + (group.getMemberCount() == 1 ? " member" : " members"));
		if(group.getNewGroupBuzzAvailable())
			Picasso.with(context).load(R.drawable.list_icon_buzz_on).noFade().into(holder.imgBuzz);
		else
			Picasso.with(context).load(R.drawable.list_icon_buzz_off).noFade().into(holder.imgBuzz);
		if(group.getIsPrivate()){
			holder.imgGroupPublic.setVisibility(View.GONE);
			holder.imgGroupPrivate.setVisibility(View.VISIBLE);
			//Picasso.with(context).load(R.drawable.venue_private_group_icon).noFade().into(holder.imgPublicPrivate);
		} else{
			holder.imgGroupPublic.setVisibility(View.VISIBLE);
			holder.imgGroupPrivate.setVisibility(View.GONE);
			//Picasso.with(context).load(R.drawable.venue_public_group_icon).noFade().into(holder.imgPublicPrivate);
		}
		
		holder.buzzWrapper.setTag(group);
		holder.buzzWrapper.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				app.ActiveGroup = (Group)v.getTag();
				app.ShowBuzz = true;
				Intent intent = new Intent(context, ActivityGroup.class);
				context.startActivity(intent);
			}
		});
		
		if(group.getDateStamp() == -1)
			holder.txtBuzzTime.setVisibility(View.INVISIBLE);
		else{
			holder.txtBuzzTime.setVisibility(View.VISIBLE);
			holder.txtBuzzTime.setText(group.getTime());
		}
		return view;
	}
	
	public static class ViewHolder{
		TextView groupName;
		TextView groupMembers;
		ImageView imgBuzz;
		ImageView imgGroupPrivate;
		ImageView imgGroupPublic;
		TextView txtBuzzTime;
		RelativeLayout buzzWrapper;
	}
}
