package com.ignight.android;

public class DNAMusic {
	private int Id;
	private String Name;
	private int Position;
	private int[] ResourceIds = {R.drawable.dna_music_13, R.drawable.dna_music_14, R.drawable.dna_music_6, R.drawable.dna_music_5, 
			R.drawable.dna_music_10, R.drawable.dna_music_7, R.drawable.dna_music_9, R.drawable.dna_music_11, R.drawable.dna_music_12,
			R.drawable.dna_music_1, R.drawable.dna_music_4, R.drawable.dna_music_3, R.drawable.dna_music_8, R.drawable.dna_music_2,  
			R.drawable.dna_music_0};
	private boolean Selected;
	
	public DNAMusic(int id, String name){
		this.Id = id;
		this.Name = name;
	}
	
	public int getId(){
		return this.Id;
	}
	
	public String getName(){
		return this.Name;
	}
	
	public int getPosition(){
		return this.Position;
	}
	
	public void setPosition(int position){
		this.Position = position;
	}
	
	public int getResourceId(int id){
		return ResourceIds[id];
	}
	
	public boolean isSelected(){
		return this.Selected;
	}
	
	public void setSelected(boolean selected){
		this.Selected = selected;
	}
}
